<?php
class CityState {
	const CA = 1;
	const US = 2;
	const MAP_HOST = 'http://maps.googleapis.com/maps/api/geocode/xml';
	
	protected static $_country = array(self::CA => 'Canada', self::US => 'United States');
	
	public static $country = array('CA' => 'Canada','US' => 'United States',);
	public static $state_ca = array(
		'AB' => 'Alberta',
		'BC' => 'British Columbia',
		'MB' => 'Manitoba',
		'NB' => 'New Brunswick',
		'NF' => 'Newfoundland',
		'NT' => 'Northwest Territories',
		'NS' => 'Nova Scotia',
		'NU' => 'Nunavut',
		'ON' => 'Ontario',
		'PE' => 'Prince Edward Island',
		'QC' => 'Quebec',
		'SK' => 'Saskatchewan',
		'YT' => 'Yukon');
	public static $state_us = array(
		'AL' =>    'Alabama',
		'AK' =>    'Alaska',
		'AZ' =>    'Arizona',
		'AR' =>    'Arkansas',
		'CA' =>    'California',
		'CO' =>    'Colorado',
		'CT' =>    'Connecticut',
		'DE' =>    'Delaware',
		'DC' =>    'District of Columbia',
		'FL' =>    'Florida',
		'GA' =>    'Georgia',
		'GU' =>    'Guam',
		'HI' =>    'Hawaii',
		'ID' =>    'Idaho',
		'IL' =>    'Illinois',
		'IN' =>    'Indiana',
		'IA' =>    'Iowa',
		'KS' =>    'Kansas',
		'KY' =>    'Kentucky',
		'LA' =>    'Louisiana',
		'ME' =>    'Maine',
		'MD' =>    'Maryland',
		'MA' =>    'Massachusetts',
		'MI' =>    'Michigan',
		'MN' =>    'Minnesota',
		'MS' =>    'Mississippi',
		'MO' =>    'Missouri',
		'MT' =>    'Montana',
		'NE' =>    'Nebraska',
		'NV' =>    'Nevada',
		'NH' =>    'New Hampshire',
		'NJ' =>    'New Jersey',
		'NM' =>    'New Mexico',
		'NY' =>    'New York',
		'NC' =>    'North Carolina',
		'ND' =>    'North Dakota',
		'OH' =>    'Ohio',
		'OK' =>    'Oklahoma',
		'OR' =>    'Oregon',
		'PW' =>    'Palau',
		'PA' =>    'Pennsylvania',
		'RI' =>    'Rhode Island',
		'SC' =>    'South Carolina',
		'SD' =>    'South Dakota',
		'TN' =>    'Tennessee',
		'TX' =>    'Texas',
		'UT' =>    'Utah',
		'VT' =>    'Vermont',
		'VA' =>    'Virginia',
		'WA' =>    'Washington',
		'WV' =>    'West Virginia',
		'WI' =>    'Wisconsin',
		'WY' =>    'Wyoming');

	/**
	* Get the id of city/state/country based on the string
	*
	* @param string $cpc - comma separated city, state, country
	* @param bool $as_array - false = returns a string, true = returns an array
	*
	* @return string or array
	*/
	public static function get_area_id($cpc, $as_array = false) {
		$params = explode(',', $cpc);
		if (isset($params[2])) {
			if (trim($params[2]) === "Canada"){
				$country = "1";
				$prov_state_select = "ca_provinces";
				$sort_by = "province";
				$sort_by_2 = "prov_abb";
				$city_db_select = "ca_cities";
				$city_sort = "province_id";
				$city_select = "name";
				$and = "province_id";
			} elseif (trim($params[2]) === "United States"){
				$country = "2";
				$prov_state_select = "us_states";
				$sort_by = "state";
				$sort_by_2 = "state_code";
				$city_db_select = "us_cities";
				$city_sort = "state_id";
				$city_select = "name";
				$and = "state_id";
			}
		}
		if (isset($city_select)) {
			$sprov_state = \DB::select('id')->from($prov_state_select)
			->where($sort_by, trim($params[1]))
			->or_where($sort_by_2, trim($params[1]))
			->execute()->get('id');
			$scity = \DB::select('id')->from($city_db_select)
			->where($city_select, trim($params[0]))
			->where($and, $sprov_state)
			->execute()->get('id');

			if ($as_array) {
				$location = array($scity,$sprov_state,$country);
			} else {
				$location = $scity.','.$sprov_state.','.$country;
			}
		} else {
			if ($as_array) {
				$location = array(0,0,0);
			} else {
				$location = '';
			}
		}
		return $location;
	}

	/**
	* Get the names of city/state/country based on the id
	*
	* @param int $city
	* @param int $province
	* @param int $country
	* @param bool $as_array - false = returns a string, true = returns an array
	* @param book $no_country - false = don't include country name when string
	*
	* @return string or array
	*/
	public static function get_area_name($city, $province, $country, $as_array = false, $no_country = false) {
		if ($country === "1") {
			$country = "Canada";
			$prov_state_select = "ca_provinces";
			$sort_by = "id";
			$city_db_select = "ca_cities";
			$city_sort = "province";
			$city_select = "name";
			$and = "province_id";
		} elseif ($country === "2") {
			$country = "United States";
			$prov_state_select = "us_states";
			$sort_by = "id";
			$city_db_select = "us_cities";
			$city_sort = "state";
			$city_select = "name";
			$and = "state_id";
		}

		if (isset($city_select)) {
			$sprov_state = \DB::select($city_sort)->from($prov_state_select)
			->where($sort_by, $province)
			->execute()->get($city_sort);
			$scity = \DB::select($city_select)->from($city_db_select)
			->where($sort_by, $city)
			->where($and, $province)
			->execute()->get($city_select);
			if ($as_array) {
				$location = array($scity,$sprov_state,$country);
			} else {
				if ($no_country) {
					$location = $scity . ', ' . $sprov_state;
				} else {
					$location = $scity . ', ' . $sprov_state . ', ' . $country;
				}
			}
		} else {
			if ($as_array) {
				$location = array('','','');
			} else {
				$location = '';
			}
		}
		return $location;
	}

	public static function get_lat_lng($address,$city,$prov_state,$country){
		$return['lat'] = 0.00;
		$return['lng'] = 0.00;

		if ($country == 1) {
			$Country_G = 'Canada';
			$select_CAinfo = \Model\Location::load_ca_info($city, $prov_state);
			$prov_state_G = $select_CAinfo['province'];
			$city_G = $select_CAinfo['name'];
		} elseif ($country == 2) {
			$Country_G = 'United States';
			$select_USinfo = \Model\Location::load_us_info($city, $prov_state);
			$prov_state_G = $select_USinfo['state'];
			$city_G = $select_USinfo['name'];
		}

		$address = $address.' '.$city_G.' '.$prov_state_G.' '.$Country_G;
		$base_url = self::MAP_HOST . '?address=' . urlencode($address) . '&sensor=false';
		$request_url = $base_url . '&q=' . urlencode($address);
		$xml = simplexml_load_file($base_url) or die('url not loading');
		$status = $xml->status;

		if ($status == 'OK') {
			// Successful geocode
			$geocode_pending = false;
			// Format: Longitude, Latitude, Altitude
			$array = json_decode(json_encode($xml), true);

			if(isset($array['result']['geometry'])){
				$return['lat'] = $array['result']['geometry']['location']['lat'];
				$return['lng'] = $array['result']['geometry']['location']['lng'];
			} else {
				$return['lat'] = $array['result'][0]['geometry']['location']['lat'];
				$return['lng'] = $array['result'][0]['geometry']['location']['lng'];
			}
		} else {
			$address = $city_G. ' '.$prov_state_G.' '.$Country_G;
			$base_url = self::MAP_HOST . '?address=' . urlencode($address).'&sensor=false';
			$request_url = $base_url . '&q=' . urlencode($address);
			$xml = simplexml_load_file($base_url) or die('url not loading');

			if ($xml->status == 'OK') {
				// Successful geocode
				$geocode_pending = false;
				// Format: Longitude, Latitude, Altitude

				$array = json_decode(json_encode($xml), true);
				$return['lat'] = $array['result']['geometry']['location']['lat'];
				$return['lng'] = $array['result']['geometry']['location']['lng'];

			}
		}
		return $return;
	}
	public static function reverse_lat_lng($lat, $long){
		$base_url = self::MAP_HOST . '?latlng='.$lat.','.$long.'&key='.KEY.'&sensor=true';
		$xml = simplexml_load_file($base_url);
		$comb = array();
		$status = $xml->status;
		if ($status == 'OK') {
			foreach($xml->result->address_component as $addy){
				$types[] .= $addy->type;
				$names[] .= $addy->long_name;
			}
			$comb = array_combine($types, $names);
			$city_name = $comb['locality'];
			return $city_name;
		} else {
			print_r($xml);
			return false;
		}
		return false;
	}

	public static function get_visitor_location(){
		$client  = @$_SERVER['HTTP_CLIENT_IP'];
		$forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
		$remote  = @$_SERVER['REMOTE_ADDR'];
		$result  = NULL;

		if(filter_var($client, FILTER_VALIDATE_IP)){
			$ip = $client;
		}elseif(filter_var($forward, FILTER_VALIDATE_IP)){
			$ip = $forward;
		}else{
			$ip = $remote;
		}

		$ip_data = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip));

		if($ip_data && $ip_data->geoplugin_countryName != null){
			$country = $ip_data->geoplugin_countryName;
			$prov = $ip_data->geoplugin_region;
			$city = $ip_data->geoplugin_city;

			$result = $city.', '.$prov.', '.$country;
		}
		return $result;
	}

	public static function get_surrounding_area($city){

	}

	public static function format_zip($value) {
		// If letter then CA zip
		if (is_numeric(substr($value, 0, 1))) {
			return $value;
		}
		$value = strtoupper(str_replace(' ', '', $value));
		return substr($value, 0, 3) . ' ' . substr($value, -3);
	}

	public static function get_state_code($state_name, $country){
		$tbl = 'us_states';
		$col = 'state';
		$get = 'state_code';
		if(trim($country) == 'Canada'){
			$tbl = 'ca_provinces';
			$col = 'province';
			$get = 'prov_abb';
		}
		$sprov_state = \DB::select()->from($tbl)
			->where($col, trim($state_name))
			->execute()->get($get);
		return $sprov_state;
	}

	public static function get_state_id($state_name, $country){
		$tbl = 'us_states';
		$col = 'state';
		$col2 = 'state_code';
		if ((trim($country) == 'Canada') || (trim($country) == 'CA') || (trim($country) == '1')) {
			$tbl = 'ca_provinces';
			$col = 'province';
			$col2 = 'prov_abb';
		}
		$sprov_state = \DB::select()->from($tbl)
			->where($col, trim($state_name))
			->or_where($col2, trim($state_name))
			->execute();
		return $sprov_state->get('id');;
	}

	public static function get_state_name($state_code, $country){
		$tbl = 'us_states';
		$col = 'state_code';
		$get = 'state';
		if ((trim($country) == 'Canada') || (trim($country) == 'CA')) {
			$tbl = 'ca_provinces';
			$col = 'prov_abb';
			$get = 'province';
		}
		$sprov_state = \DB::select()->from($tbl)
			->where($col, trim($state_code))
			->execute()->get($get);
		return $sprov_state;
	}

	public static function get_state_array($country, $short = true) {
		$data = array();
		if ((trim($country) == self::CA) || (trim($country) == 'CA')) {
			$result = \DB::select()->from('ca_provinces')->execute();
			$val = ($short) ? 'prov_abb' : 'province';
		} else {
			$result = \DB::select()->from('us_states')->execute();
			$val = ($short) ? 'state_code' : 'state';
		}
		foreach($result as $row) {
			$data[$row['id']] = $row[$val];
		}
		return $data;
	}
	
	public static function get_country($id=null) {
		if($id === null) {
			return self::$_country;
		}
		return self::$_country[$id];
	}
	
	public static function get_state($country = self::CA, $provstate_id = null){
		$data = array(0 => 'All');
		if($country == self::CA) {
			$data = \Model\Provstate\Canada::forge()->get_province($provstate_id);
		}
		if($country == self::US) {
			$data = \Model\Provstate\Usa::forge()->get_state($provstate_id);
		}
		return $data;
	}
}