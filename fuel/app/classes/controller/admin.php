<?php
namespace Controller;

class Admin extends \Controller {

	public function action_index(){
		\Session::set_flash('admin_dialog', true);
		$view = \View::forge('home_page');
		$view->tracking_code = \Config::get('track_code');
		return \Response::forge($view);
	}

}