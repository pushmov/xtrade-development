<?php
namespace Controller\Admins;
class Listing extends Auth {
	public $name = 'listing';

	public function action_index(){
		\Lang::load('common');
		$listing = \Model\Client\Has::forge($this->param('lid'));
		if(!$listing->loaded()){
			\Response::redirect('/admins/dashboard');
		}
		$data = \Listing::forge()->render_listing($listing->listing_id);
		$data['matches_notification'] = \Model\Listing::forge()->status_completion($listing->listing_id, false);
		$data['client_info'] = \View::forge('admin/listing/client_info', $data);
		$data['has_info'] = \View::forge('admin/listing/client_has_info', $data);
		$data['image_info'] = \View::forge('admin/listing/image_info', $data);
		$data['wants_info'] = \View::forge('admin/listing/wants_info', $data);
		$data['change_status'] = \View::forge('admin/listing/change_status', $data);
		$this->template->title = 'Listing &raquo; Edit';
		$this->template->scripts = array('auto-complete.min.js', 'jquery.tn3.min.js', 'form_data.js', 'er_check.js', 'jquery.guillotine.js', 'jquery.uploadfile.min.js', 'jquery.dad.js', 'jquery.sumoselect.min.js', 'edit.js');
		$this->template->styles = array_merge($this->template->styles,array('uploadfile.css', 'auto-complete.css', 'sumoselect.css', 'style.css'));
		$this->template->js_vars = array(
			'listing_id' => $listing->listing_id,
			'scrollto' => \Input::get('pos'),
			'type' => $data['has']['type_of_listing']
		);
		$this->template->content = \View::forge('admin/listing/edit', $data);
	}

	public function get_completed(){
		\Lang::load('listing');
		$view = \View::forge('dialog/admin/listing_completed');
		$view->txt = \Model\Client::forge()->get_completion_code_txt(\Input::get('status'));
		return $this->response($view);
	}

	public function action_match($listing_id=null){
		if(!isset($listing_id) || !\Input::get('realtor_id')){
            \Response::redirect('/admins/dashboard/');
        }
		\Lang::load('common');
		\Lang::load('matches');
		$data = \Match::forge()->render_matches($listing_id, \Input::get('realtor_id'));
        $js_var = array(
            'id' => $listing_id
        );
        $this->template->scripts = array('matches.js', 'jquery.tooltipster.min.js');
		$this->template->styles = array('matches.css', 'tooltipster.css');
        $this->template->js_vars = $js_var;
        $this->template->content = \View::forge('matches/index', $data);
	}

	public function action_detail(){
		\Lang::load('listing');
		\Lang::load('common');
		$listing_id = $this->param('lid');
		$data = \Listing::forge()->listing_detail($listing_id, false);
		if($data === false){
			\Response::redirect('/admins/dashboard');
		}
		$this->template->meta_title = 'Listing Details';
		$this->template->scripts = array('listing_detail.js', 'lightslider.min.js', 'contact_agent.js', 'jquery.tooltipster.min.js', 'jquery.mask.js');
		$this->template->styles = array('lightslider.min.css', 'tooltipster.css');
		$this->template->js_vars = array(
			'lat' => $data['lat'],
			'lng' => $data['lng']
		);
		$data['param'] = $listing_id;
		$data['is_logged_in'] = !($this->member === null);
		$data['nav'] = \Model\Listing::forge()->listing_navigation($listing_id);
		$view = \View::forge('listing/detail');
		$view->data = $data;
		$this->template->content = $view;
	}
	
	public function get_delete(){
		\Lang::load('listing');
		$view = \View::forge('dialog/admin/listing_delete');
		$view->id = \Input::get('listing_id');
		return $this->response($view);
	}

	public function post_delete(){
		return $this->response(\Model\Listing::forge()->delete_listing(\Input::post('data')));
	}

}