<?php
namespace Controller\Admins;
use Model;
class Login extends \Controller_Hybrid {
	public $template = 'admin/base_page';

	public function action_index() {
		$view = \View::forge('admin/login');
		$email = $message = $remember = '';
		if (\Input::method() == 'POST') {
			$authlite = \Authlite::instance('auth_admin');

			if ($authlite->login(\Input::post('login_email'), \Input::post('login_pass'), $remember)) {
				\Response::redirect('/admins/dashboard');
			} else {
				$message = 'Login Failed';
				$email = \Input::post('login_email');
			}
		}
		$view->email = $email;
		$view->message = $message;
		$this->template->meta_title = 'xTradeHomes Admin Login';
		$this->template->meta_desc = '';
		$this->template->header = '';
		$this->template->styles = array();
		$this->template->scripts = array();
		$this->template->content = $view;
	}

	public function action_logout() {
		\Authlite::instance('auth_admin')->logout();
		\Model\User::forge()->unset_user_type();
		\Response::redirect('/admins/login');
	}
}
