<?php
namespace Controller\Admins;

class Plans extends Auth {
	const MONTHLY = 99;

	public $name = 'plans';
	public function action_index(){
		$view = \View::forge('admin/plan/index');
		$this->template->styles = array('datatables.min.css');
		$this->template->scripts = array('datatables.min.js', 'plans.js');
		$this->template->content = $view;
	}

	public function action_edit(){
		\Lang::load('admin');
		$plan = \Model\Plan::forge($this->param('id'));
		$view = \View::forge('admin/plan/form');
		if ($plan->id == null) {
			$view->page_title = 'Create Plan';
		} elseif ($plan->status_id == \Model\Plan::S_ARCHIVED) {
			$view->page_title = 'Archived Plan';
		} else {
			$view->page_title = 'Edit Plan';
		}
		$view->readonly = '';
		if ($plan->status_id >= \Model\Plan::S_ACTIVE) {
			$view->readonly = 'disabled';
		}
		$view->data = $plan->as_array();
		$status = \Model\Plan::forge()->get_status_array();
		unset($status[0]);
		$view->status = $status;
		$this->template->styles = array_merge($this->template->styles, array('jquery-te.css', 'admin.css'));
		$this->template->scripts = array_merge($this->template->scripts, array('jquery-te.min.js', 'plans.js'));
		$this->template->content = $view;
	}

	public function post_submit(){
		return $this->response(\Model\Plan::forge()->doupdate(\Input::post('data')));
	}

	public function get_datatable(){
		\Lang::load('common');
		return $this->response(\Model\Plan::forge()->admin_list(\Input::get()));
	}

	public function get_state(){
		return $this->response(\Form::select('data[state_id]', null, \CityState::get_state(\Input::get('id')), array('id' => 'state_id')));
	}

	public function get_active(){
		return $this->response(\View::forge('dialog/admin/plan/confirm_activate'));
	}

	public function post_active(){
		return $this->response(\Model\Plan::forge()->create(\Input::post('data')));
	}

	public function get_pause(){
		return $this->response(\View::forge('dialog/admin/plan/confirm_pause'));
	}

	public function post_pause(){
		return $this->response(\Model\Plan::forge()->pause(\Input::post('data')));
	}

	public function get_delete(){
		return $this->response(\View::forge('dialog/admin/plan/confirm_delete'));
	}

	public function post_delete(){
		return $this->response(\Model\Plan::forge()->deleteplan(\Input::post('data')));
	}

	public function post_generate(){
		$plans = \Model\Plan::forge();
		$taxes = \Model\Tax::forge()->load_by_country(\CityState::CA);
		foreach($taxes->as_array() as $row) {
			if (!$plans->is_exists($row['country'], $row['prov_state'], \Model\Plan::PT_REALTOR)) {

				$paypal = new \Model\Paypalrest();
				//create billing plan and activate it
				$prov = array_values(array_flip(\CityState::$state_ca));

				$plan = \Model\Plan::forge();
				$plan->id = null;
				$plan->country_id = $row['country'];
				$plan->state_id = $row['prov_state'];
				$plan->type_id = \Model\Plan::PT_REALTOR;
				$plan->name = 'xTrade Realtor';
				$plan->info = 'Realtor Monthly CA - ' . $prov[$row['prov_state'] - 1];
				$plan->rate = self::MONTHLY;
				$plan->rate_tax = self::MONTHLY * $row['rate'];
				$plan->created_at = date_create()->format('Y-m-d H:i:s');
				$plan->status_id = \Model\Plan::S_NEW;
				$plan->save(false);

				//send plan create request to paypal
				$data['name'] = $plan->name;
				$data['desc'] = $plan->info;
				$data['amount'] = $plan->rate;
				$data['tax'] = $plan->rate_tax;
				$data['trial_len'] = $data['trial_duration'] = $data['trial_amt'] = 0;
				$data['currency'] = 'CAD';
				$pp_plan = $paypal->create_plan($data);

				$plan->status_id = \Model\Plan::S_ACTIVE;
				$plan->plan_id = $pp_plan->id;
				$plan->save(false);

				//create plan for promo version
				$plan = \Model\Plan::forge();
				$plan->id = null;
				$plan->country_id = $row['country'];
				$plan->state_id = $row['prov_state'];
				$plan->type_id = \Model\Plan::PT_REALTOR;
				$plan->name = 'xTrade Promo1';
				$plan->info = 'Realtor Promo1 CA - ' . $prov[$row['prov_state'] - 1];
				$plan->promo_len = 6;
				$plan->rate = self::MONTHLY;
				$plan->rate_tax = self::MONTHLY * $row['rate'];
				$plan->created_at = date_create()->format('Y-m-d H:i:s');
				$plan->status_id = \Model\Plan::S_NEW;
				$plan->save(false);
				//send plan create promo request to paypal

				$data['name'] = $plan->name;
				$data['desc'] = $plan->info;
				$data['amount'] = $plan->rate;
				$data['tax'] = $plan->rate_tax;
				$data['trial_len'] = 1;
				$data['trial_duration'] = $plan->promo_len;
				$data['trial_amt'] = 0;
				$data['currency'] = 'CAD';
				$pp_plan = $paypal->create_plan($data);

				$plan->status_id = \Model\Plan::S_ACTIVE;
				$plan->plan_id = $pp_plan->id;
				$plan->save(false);
			}
		}
		$response= array('success' => true);
		return $this->response($response);
	}

	public function action_getplan(){
		$pp = new \Model\Paypalrest();
		$data = \PayPal\Api\Plan::get(\Input::get('id'), $pp->get_api());
		echo '<pre>';
		print_r($data->toArray());
		exit();
	}

	public function action_agreement_detail(){
		$pp = new \Model\Paypalrest\Agreement();
		$agreement = $pp->detail(\Input::get('id'));
		$agreement['readable_date'] = date_create($agreement['next_date'])->format('F d, Y');
		echo '<pre>';
		print_r($agreement);
		exit();
	}

	public function action_getagreement(){
		$pp = new \Model\Paypalrest();
		$data = \PayPal\Api\Agreement::get(\Input::get('id'), $pp->get_api());
		echo '<pre>';
		print_r($data);
		exit();
	}

	public function action_create(){
		$pp = new \Model\Paypalrest();
		$plan = new \PayPal\Api\Plan();
		$plan->setName('xTrade Vendor Monthly')
			->setDescription('Vendor monthly CA 5% GST')
			->setType('fixed');

		$paymentDefinition = new \PayPal\Api\PaymentDefinition();
		$paymentDefinition->setName('Regular Payments')
			->setType('REGULAR')
			->setFrequency('Month')
			->setFrequencyInterval(1)
			->setCycles(12)
			->setAmount(new \PayPal\Api\Currency(array('value' => 29.95, 'currency' => 'CAD')));

		$merchantPreferences = new \PayPal\Api\MerchantPreferences();
		$baseUrl = 'http://xtrade.dev';
		$merchantPreferences->setReturnUrl("$baseUrl/ExecuteAgreement.php?success=true")
			->setCancelUrl("$baseUrl/ExecuteAgreement.php?success=false")
			->setAutoBillAmount("yes")
			->setInitialFailAmountAction("CONTINUE")
			->setMaxFailAttempts("0");

		$chargeModel = new \PayPal\Api\ChargeModel();
		$chargeModel->setType('TAX')
		->setAmount(new \PayPal\Api\Currency(array('value' => 1.50, 'currency' => 'CAD')));
		$paymentDefinition->setChargeModels(array($chargeModel));

		$plan->setPaymentDefinitions(array($paymentDefinition));
		$plan->setMerchantPreferences($merchantPreferences);
		try {
			$output = $plan->create($pp->get_api());

			$patch = new \PayPal\Api\Patch();
			$value = new \PayPal\Common\PayPalModel('{
				   "state":"ACTIVE"
				 }');
			$patch->setOp('replace')
				->setPath('/')
				->setValue($value);
			$patchRequest = new \PayPal\Api\PatchRequest();
			$patchRequest->addPatch($patch);
			$plan->update($patchRequest, $pp->get_api());


			echo '<pre>';
			print_r(\PayPal\Api\Plan::get($output->getId(), $pp->get_api()));
			exit();
		} catch (\PayPal\Exception\PayPalConnectionException $ex) {
			// NOTE: PLEASE DO NOT USE RESULTPRINTER CLASS IN YOUR ORIGINAL CODE. FOR SAMPLE ONLY
			echo '<pre>';
			print_r($ex);
			exit(1);
		}
	}

	public function action_payment() {
		$pp = new \Model\Paypalrest();
		$agreement = new \PayPal\Api\Agreement();
		$agreement->setName('DPRP Payment')
			->setDescription('Payment with credit Card')
			->setStartDate(date_create()->format('Y-m-d\TH:i:s\Z'));
		// Add Plan ID
		// Please note that the plan Id should be only set in this case.
		$plan = new \PayPal\Api\Plan();
		$plan->setId('P-4K533757LJ787515T4HTLXIY');
		$agreement->setPlan($plan);
		// Add Payer
		$payer = new \PayPal\Api\Payer();
		$payer->setPaymentMethod('credit_card')
			->setPayerInfo(new \PayPal\Api\PayerInfo(array('email' => 'rizki.a.aprilian@gmail.com', 'first_name' => 'rizki1', 'last_name' => 'tester1')));
		// Add Credit Card to Funding Instruments
		$card = new \PayPal\Api\CreditCard();
		$card->setType('visa')
			->setNumber('4032034243475889')
			->setExpireMonth('04')
			->setExpireYear('2021')
			->setCvv2('889');
		$fundingInstrument = new \PayPal\Api\FundingInstrument();
		$fundingInstrument->setCreditCard($card);
		$payer->setFundingInstruments(array($fundingInstrument));
		//Add Payer to Agreement
		$agreement->setPayer($payer);
		// Add Shipping Address

		try {
			// Please note that as the agreement has not yet activated, we wont be receiving the ID just yet.
			$agreementresponse = $agreement->create($pp->get_api());

			echo '<pre>';
			print_r($agreementresponse);
			exit();


		} catch (\PayPal\Exception\PayPalConnectionException $ex) {
			echo '<pre>';
			print_r($ex);
			exit(1);
		}
	}

	public function action_myplans(){
		$pp = new \Model\Paypalrest();
		try {
		    // Get the list of all plans
		    // You can modify different params to change the return list.
		    // The explanation about each pagination information could be found here
		    // at https://developer.paypal.com/webapps/developer/docs/api/#list-plans
		    $params = array('page_size' => '20', 'status' => 'ACTIVE');
		    $planList = \Paypal\Api\Plan::all($params, $pp->get_api());
		    echo '<pre>';
		    print_r($planList);exit();
		} catch (Exception $ex) {
		    // NOTE: PLEASE DO NOT USE RESULTPRINTER CLASS IN YOUR ORIGINAL CODE. FOR SAMPLE ONLY
		    ResultPrinter::printError("List of Plans", "Plan", null, $params, $ex);
		    exit(1);
		}
	}

}