<?php
namespace Controller\Admins;

class Profile extends Auth {
	public $name = 'profile';

	public function action_index(){
		\Lang::load('common');
		\Lang::load('signup');
		if(!in_array($this->param('type'), \Model\User::forge()->get_types())){
			\Response::redirect('/admins/dashboard');
		}
		$model = '\\Model\\'.  ucwords($this->param('type'));
		$data = $model::forge($this->param('tid'))->profile();
		$js_vars = array('member' => 'admins/', 'type' => $this->param('type'), 'memberid' => $data[$this->param('type').'_id']);
		if ($this->param('type') == 'vendor') {
			$view = \View::forge('vendor/profile')->bind('errors', $errors)->bind('msg', $msg);
			$view->renewal = \Model\Admin::forge()->load_vendor_renewal_date($data['paypal_sub_id']);
			$view->advertising = \Model\Vendor::forge()->areaid_to_advertise($data['locale']);
			$view->services = \Model\Vendor::forge()->typeid_to_services($data['type']);
			$data['type'] = explode(';', $data['type']);
			$data['target'] = '/admins/vendor/doupload.json';
			$js_vars += array('vendor_id' => $data['vendor_id']);
			$this->template->styles = array('tooltipster.css', 'uploadfile.css', 'sumoselect.css', 'token-input.css', 'jquery-te.css','style.css');
			$this->template->scripts = array('jquery.mask.js', 'er_check.js', 'jquery.tooltipster.min.js', 'profile.js', 'vendor_home.js', 'jquery.uploadfile.min.js', 'jquery.sumoselect.min.js', 'jquery.tokeninput.js', 'jquery-te.min.js', 'auto-complete.min.js');
		} else {
			$view = \View::forge('member/profile')->bind('errors', $errors)->bind('msg', $msg);
			$view->readonly = '';
			if ($this->param('type') == 'realtor') {
				$view->title = 'Agent Profile';
				$view->promo_code = \Model\Plan::forge($data['plan_id'])->promo_code;
			} else {
				$view->title = 'Broker Profile';
				$view->promo_code = \Model\Plan::forge($data['plan_id'])->promo_code;
			}

			if ($data['country'] == \Model\User::COUNTRY_CA) {
				$view->nrb_txt = 'CREA ID';
				$view->nrb_tip = __('crea_disabled');
			} else {
				$view->nrb_txt = 'NRDS ID';
				$view->nrb_tip = __('nrds_disabled');
			}
			$this->template->styles = array('tooltipster.css', 'auto-complete.css');
			$this->template->scripts = array('jquery.mask.js', 'er_check.js', 'jquery.tooltipster.min.js', 'profile.js', 'auto-complete.min.js');
		}
		$view->suspend_or_not = \Model\Admin::forge()->suspend_or_not($data['account_status'], $data[$this->param('type').'_id'], $this->param('type'));
		$data['utype'] = $this->param('type');
		$view->data = $data;
		$this->template->js_vars = $js_vars;
		$this->template->content = $view;
	}

	public function post_update() {
		\Lang::load('common');
		try {
			$type = \Input::post('data.utype');
			if(!in_array($type, \Model\User::forge()->get_types())){
				\Response::redirect('/admins/dashboard');
			}
			$model = '\\Model\\'.  ucwords($type);
			$model::forge(\Input::post('data.id'))->doupdate(\Input::post());
			$json['status'] = 'OK';
			$json['message'] = '<div class="callout success" data-closable>'.__('profile_updated').'
			<button class="close-button" aria-label="'.__('dismiss_alert').'" type="button" data-close><span aria-hidden="true">&times;</span></div>';
			return $this->response($json);
		} catch(\AutoModelerException $e) {
			return $this->response(array('errors' => $e->errors));
		}
	}

	public function get_billing_history() {
		\Lang::load('orders');
		$users = array_flip(\Model\User::forge()->get_types());
		$view = \View::forge('dialog/order_history');
		$view->data = \Model\Order::forge()->get_history($this->member->id, $users[\Input::get('type')]);
		return $this->response($view);
	}

	public function get_resend_welcome_email() {
		$param = \Input::get();
		$view = \View::forge('dialog/admin/resend_welcome_email_confirm');
		$model = '\\Model\\'.  ucwords($param['utype']);
		$data = $model::forge($param['id'])->as_array();
		$data['utype'] = $param['utype'];
		$view->data = $data;
		return $this->response($view);
	}

	public function post_resend_welcome_email() {
		$param = \Input::post('data');
		$model = '\\Model\\'.  ucwords($param['utype']);
		$data = $model::forge($param['id'])->as_array();
		if ($param['utype'] == 'vendor') {
			$data['website'] = $data['web_site'];
			$data['type'] = \Model\Vendor::forge()->typeid_to_services($data['type']);
			$data['locale'] = \Model\Vendor::forge()->areaid_to_advertise($data['locale']);
		}
		$response['status'] = 'error';
		$response['message'] = 'Unknown error while sending email. Please try again later';
		if(\Emails::forge()->welcome($data, $param['utype'])) {
			$response['status'] = 'ok';
			$response['message'] = 'Welcome email resent';
		}
		return $this->response($response);
	}

	public function get_resend_success() {
		return $this->response(\View::forge('dialog/admin/resend_welcome_success'));
	}
}