<?php

namespace Controller\Admins;

class User extends Auth{
    const PER_PAGE = 5;
    public $name = 'realtor';

    public function action_index(){
		\Lang::load('listing');
        $view = \View::forge('admin/user/realtor_list');
		$this->template->styles = array('datatables.min.css', 'responsive.dataTables.min.css', 'responsive.foundation.min.css');
		$this->template->scripts = array('datatables.min.js', 'dataTables.responsive.min.js', 'responsive.foundation.min.js');
        $this->template->content = $view;
    }

	public function action_activate($realtor_id){
		/** bypass activation through email mode */
		$realtor = \Model\Realtor::forge()->load(\DB::select()->where('realtor_id', '=', $realtor_id));
		if($realtor->loaded()){
			$realtor->account_status = \Model\User::AS_VALID;
			$realtor->save();
		}
	}

	public function action_impersonate(){
		$realtor_id = \Input::get('id');
		$user = \Model\Realtor::forge()->load(\DB::select()->where('realtor_id', '=', $realtor_id));
		$authrealtor = \Authlite::instance('auth_realtor');
		\Model\User::forge()->unset_user_type();
		$authrealtor->logout();
		if($authrealtor->force_login($user->email)){
			\Response::redirect('realtors');
		}
		\Response::redirect('admins/dashboard');
	}

	public function get_list(){
		$all = \Model\Realtor::forge()->load_users(\Input::get());
		return $this->response($all);
	}

    public function post_detail(){
        $view = \View::forge('admin/user/realtor_detail');
        $view->data = \Model\Admin::forge()->load_realtor(\Model\Realtor::forge(\Input::post('realtor_id'))->as_array());
        return $this->response($view);
    }

    public function post_exclusive(){
		\Lang::load('common');
		$user = \Model\Realtor::forge(\Input::post('realtor_id'))->as_array();
        $view = \View::forge('admin/user/exclusive_listing');
        $view->data = \Model\Admin::forge()->load_exclusive_listing($user['realtor_id']);
        $view->user = $user;
        return $this->response($view);
    }

    public function post_downloaded(){
		\Lang::load('common');
		$user = \Model\Realtor::forge(\Input::post('realtor_id'))->as_array();
        $view = \View::forge('admin/user/downloaded_listing');
        $view->user = $user;
        $view->data = \Model\Admin::forge()->load_downloaded_listing($user['realtor_id']);
        return $this->response($view);
    }

    public function post_ready(){
		\Lang::load('common');
		$user = \Model\Realtor::forge(\Input::post('realtor_id'))->as_array();
        $view = \View::forge('admin/user/ready_listing');
        $view->data = \Model\Admin::forge()->load_ready_listing($user['realtor_id']);
        $view->user = $user;
        return $this->response($view);
    }

    public function post_matches(){
		\Lang::load('common');
        $view = \View::forge('admin/user/matches_listing');
        $user = \Model\Realtor::forge(\Input::post('realtor_id'));
		$view->user = $user->as_array();
        $view->data = \Model\Client\Has::forge()->load_matches_listing($view->user['realtor_id']);
        return $this->response($view);
    }

    public function get_change(){
        $view = \View::forge('dialog/admin/user/change_broker');
        $user = \Model\Realtor::forge()->load(\DB::select_array()->where('realtor_id', '=', \Input::get('realtor_id')))->as_array();
        $view->realtor_id = \Input::get('realtor_id');
        $view->office_id = $user['office_id'];
        return $this->response($view);
    }

    public function post_change(){
        $post = \Input::post('data');
        $user = \Model\Realtor::forge()->load(\DB::select_array()->where('realtor_id', '=', $post['realtor_id']));
        $user->set_fields(array('office_id' => $post['broker']));
		if($user->save()){
            $response['status'] = 'ok';
            $response['message'] = __('realtor_added');
            $response['id'] = $post['realtor_id'];
		}else{
			$response['status'] = 'error';
            $response['message'] = __('realtor_added_error');
		}
        return $this->response($response);
    }

    public function get_reset(){
        $view = \View::forge('dialog/admin/user/reset_password');
        $view->realtor_id = \Input::get('realtor_id');
        return $this->response($view);
    }

    public function post_reset(){
		$model = '\Model\\' . ucwords($this->name);
        return $this->response($model::forge()->admin_reset_password(\Input::post('realtor_id'), $this->name));
    }

    public function get_suspend(){
        $view = \View::forge('dialog/admin/user/suspend_account');
        $view->realtor_id = \Input::get('realtor_id');
        return $this->response($view);
    }

    public function post_suspend(){
		$model = '\Model\\' . ucwords($this->name);
        return $this->response($model::forge()->suspend_user(\Input::post('data.realtor_id'), $this->name));
    }

    public function get_unsuspend(){
        $view = \View::forge('dialog/admin/user/unsuspend_account');
        $view->realtor_id = \Input::get('realtor_id');
        return $this->response($view);
    }

    public function post_unsuspend(){
		$model = '\Model\\' . ucwords($this->name);
        return $this->response($model::forge()->unsuspend_user(\Input::post('data.realtor_id'), $this->name));
    }

    public function get_delete(){
        $view = \View::forge('dialog/admin/user/delete');
        $view->realtor_id = \Input::post('realtor_id');
        return $this->response($view);
    }

    public function post_delete(){
        return $this->response(\Model\Realtor::forge()->delete_realtor(\Input::post('data.realtor_id'), $this->name));
    }

    public function post_approve(){
		$model = '\Model\\'.  ucwords($this->name);
        return $this->response($model::forge()->approve_account(\Input::post('id'), $this->name));
    }

	public function post_import() {
		return $this->response(\Model\Admin::forge()->import_listing(\Input::post()));
	}

}