<?php

namespace Controller\Admins;

class Vendor extends Auth{
    
	public $name = 'vendor';
	
    public function before(){
        parent::before();
    }
    
    public function action_index(){
		\Lang::load('common');
		$this->template->styles = array('datatables.min.css', 'responsive.dataTables.min.css', 'responsive.foundation.min.css');
		$this->template->scripts = array('datatables.min.js', 'dataTables.responsive.min.js', 'responsive.foundation.min.js');
        $view = \View::forge('admin/vendor/vendor_list');
		$this->template->content = $view;
    }
	
	public function get_list(){
		return $this->response(\Model\Vendor::forge()->load_vendors(\Input::get()));
	}
    
    public function get_detail(){
		\Lang::load('common');
        $view = \View::forge('admin/vendor/vendor_detail');
		$vendor = \Model\Vendor::forge(\Input::get('vendor_id'));
		$view->data = $vendor->as_array();
        $view->renewal = \Model\Admin::forge()->load_vendor_renewal_date($vendor->paypal_sub_id);
		$view->advertising = \Model\Vendor::forge()->areaid_to_advertise($vendor->locale);
		$view->services = \Model\Vendor::forge()->typeid_to_services($vendor->type);
		$view->suspend_or_not = \Model\Admin::forge()->suspend_or_not($vendor->account_status, $vendor->vendor_id, $this->name);
        return $this->response($view);
    }
    
    public function get_preview(){
        $view = \View::forge('dialog/admin/vendor/ad_preview');
        $view->data = \Model\Vendor::forge()->get_vendor_ads(\Input::get('vendor_id'));
        return $this->response($view);
    }
    
    public function get_reset(){
        $view = \View::forge('dialog/admin/vendor/reset_password');
        $view->vendor_id = \Input::get('vendor_id');
        return $this->response($view);
    }
    
    public function post_reset(){
		$model = '\Model\\' . ucwords($this->name);
        return $this->response($model::forge()->admin_reset_password(\Input::post('vendor_id'), $this->name));
    }
    
    public function get_delete(){
        $view = \View::forge('dialog/admin/vendor/delete_vendor');
        $view->vendor_id = \Input::get('vendor_id');
        return $this->response($view);
    }
    
    public function post_delete(){
        return $this->response(\Model\Vendor::forge()->delete_vendor(\Input::post('data.vendor_id'), $this->name));
    }
	
	public function post_approve(){
		$model = '\Model\\'.  ucwords($this->name);
		return $this->response($model::forge()->approve_account(\Input::post('id'), $this->name));
	}
	
	public function get_suspend(){
		$view = \View::forge('dialog/admin/vendor/suspend_account');
		$view->vendor_id = \Input::get('vendor_id');
		return $this->response($view);
	}

	public function post_suspend(){
		$model = '\Model\\' . ucwords($this->name);
		return $this->response($model::forge()->suspend_user(\Input::post('data.vendor_id'), $this->name));
	}
	
	public function get_unsuspend(){
		$view = \View::forge('dialog/admin/vendor/unsuspend_account');
		$view->vendor_id = \Input::get('vendor_id');
		return $this->response($view);
	}

	public function post_unsuspend(){
		$model = '\Model\\' . ucwords($this->name);
		return $this->response($model::forge()->unsuspend_user(\Input::post('data.vendor_id'), $this->name));
	}
	
	public function post_doupload() {
		$output_dir = DOCROOT . \Model\Vendor::IMG_PATH;
		$json = array();

		if (!is_dir($output_dir)){
			mkdir($output_dir);
		}

		if ($_FILES["myfile"]["error"]) {
			$json['msg'] = $_FILES["myfile"]["error"];
		} else {
			$ext = pathinfo($_FILES["myfile"]["name"], PATHINFO_EXTENSION);
			//fix me
			$filename = str_pad(\Input::post('id'), 8, '0', STR_PAD_RIGHT).'.'.$ext;
			if(is_readable(DOCROOT. \Model\Vendor::IMG_PATH.$filename)){
				unlink(DOCROOT. \Model\Vendor::IMG_PATH.$filename);
			}
			move_uploaded_file($_FILES["myfile"]["tmp_name"], $output_dir . $filename);
			$json[] = $filename;
		}
		return $this->response($json);
	}
	
	public function get_resend_activation_confirm(){
		$view = \View::forge('dialog/admin/vendor/resend_activation_confirm');
		$view->vendor_id = \Input::get('vendor_id');
		return $this->response($view);
	}
	
	public function post_resend_activation(){
		return $this->response(\Model\Vendor::forge()->resend_activation(\Input::post('data.vendor_id')));
	}
	
	public function get_resend_success(){
		return $this->response(\View::forge('dialog/admin/vendor/resend_success'));
	}
    
}