<?php
namespace Controller\Brokers;
use \Model\Broker\Realtor;

class Dashboard extends Auth {
	public function action_index() {
		// Needs to update password
		if (($this->member->account_status == \Model\User::AS_NEEDS_PASS) || ($this->member->account_status == \Model\User::AS_NEW)) {
			\Response::redirect('/brokers/profile');
		}
		\Lang::load('broker');
		\Session::set(\Model\Listing::SESS_REFERER_NAME, \Uri::current());
		$model = Realtor::forge($this->member->id);
		$view = \View::forge('broker/dashboard');
		$view->requests = $model->get_dash_requests();
		$view->realtors = $model->get_dash_list();
		$this->template->scripts = 'brokers.js';
		$this->template->js_vars = array(
			'action' => \Input::get('action'),
			'rid' => \Input::get('rid')
		);
		$this->template->content = $view;
	}
	// We use a post here because that's how jQuery load works when vars are passed
	public function post_details() {
		\Lang::load('signup');
		\Lang::load('common');
		$view = \View::forge('broker/realtor_details');
		$view->data = Realtor::forge($this->member->id)->get_details(\Input::post('id'));
		return $this->response($view);
	}

	public function get_approve_realtor() {
		\Lang::load('broker');
		return $this->response(Realtor::forge($this->member->id)->approve(\Input::get('id')));
	}

	public function get_decline_realtor() {
		\Lang::load('common');
		return $this->response(Realtor::forge($this->member->id)->decline(\Input::get('id')));
	}

	public function get_remove_realtor_dialog() {
		\Lang::load('signup');
		return $this->response(\View::forge('dialog/remove_realtor', \Input::get()));
	}
	public function get_remove_realtor() {
		\Lang::load('broker');
		return $this->response(Realtor::forge($this->member->id)->remove_realtor(\Input::get('id')));
	}
	// We use a post here because that's how jQuery load works when vars are passed
	public function post_xlistings() {
		\Lang::load('signup');
		$view = \View::forge('broker/realtor_xlistings');
        $view->data = Realtor::forge()->get_local_listing(\Input::post('id'));
		return $this->response($view);
	}
	// We use a post here because that's how jQuery load works when vars are passed
	public function post_olistings() {
		\Lang::load('signup');
		\Lang::load('common');
		$view = \View::forge('broker/realtor_olistings');
		$view->data = Realtor::forge()->get_external_listing(\Input::post('id'));
		return $this->response($view);
	}
	// We use a post here because that's how jQuery load works when vars are passed
	public function post_matches() {
		\Lang::load('signup');
		$view = \View::forge('broker/realtor_matches');
		$view->data = Realtor::forge()->get_matches_listing(\Input::post('id'));
		return $this->response($view);
	}
	public function get_add_realtor_dialog() {
		\Lang::load('signup');
		$view = \View::forge('dialog/add_realtor');
		$view->data = \Model\Realtor::forge(\Input::get('rid'));
		return $this->response($view);
	}

    public function post_addrealtor(){
		\Lang::load('signup');
        $response = Realtor::forge()->invite_realtor(\Input::post());
        return $this->response($response);
    }
}