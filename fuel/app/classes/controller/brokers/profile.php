<?php
namespace Controller\Brokers;

class Profile extends Auth {
	public function before() {
		parent::before();
	}

	public function action_index() {
		\Lang::load('common');
		\Lang::load('signup');
		$view = \View::forge('member/profile');
		$view->title = 'Broker Profile';
		$data = \Model\Broker::forge($this->member->id)->profile();
        // crea & nrds tool tips
        if ($data['country'] == \Model\User::COUNTRY_CA) {
        	$view->nrb_txt = 'CREA ID';
			$view->nrb_tip = '<span>Look up your CREA ID by clicking <a href="http://mms.realtorlink.ca/indDetails.aspx" target="_blank">here</a></span>';
		} else {
        	$view->nrb_txt = 'NRDS ID';
			$view->nrb_tip = 'Look up your NRDS ID by clicking <a href="https://reg.realtor.org/roreg.nsf/retrieveID?OpenForm" target="_blank">here</a></span>';
		}
		$data['listing_btn'] = '';
		$view->data = $data;
		$view->promo_code = \Model\Plan::forge($data['plan_id'])->promo_code;
		$this->template->meta_title = 'xTradeHomes Broker Profile';
		$this->template->styles = array('tooltipster.css');
		$this->template->scripts = array('jquery.mask.js', 'er_check.js', 'jquery.tooltipster.min.js', 'profile.js');
		$this->template->js_vars = array('member' => 'brokers/', 'type' => 'broker', 'memberid' => $this->member->broker_id);
		$this->template->content = $view;
	}

	public function get_welcome() {
		\Lang::load('signup');
		return $this->response(\View::forge('dialog/welcome_broker'));
	}

	public function get_password() {
		\Lang::load('signup');
		$view = \View::forge('dialog/change_password')->bind('err_msg', $msg);
		if (\Input::get('status') == \Model\User::AS_NEEDS_PASS) {
			$msg = __('please_create_a_new_password');
		}
		$view->type = \Model\User::UT_BROKER;
		$view->id = \Input::get('id');
		return $this->response($view);
	}

	public function post_pass_update() {
		return $this->response(\Model\Broker::forge(\Input::post('id'))->update_password(\Input::post()));
	}

	public function get_close_account() {
		\Lang::load('signup');
		return $this->response(\View::forge('dialog/close_account'));
	}


	public function post_update() {
		\Lang::load('common');
		try {
			\Model\Broker::forge(\Input::post('data.id'))->doupdate(\Input::post());
			$json['status'] = 'OK';
			$json['message'] = '<div class="callout success" data-closable>'.__('profile_updated').'
			<button class="close-button" aria-label="'.__('dismiss_alert').'" type="button" data-close><span aria-hidden="true">&times;</span></div>';
			return $this->response($json);
		} catch(\AutoModelerException $e) {
			return $this->response(array('errors' => $e->errors));
		}
	}

	public function get_broker_dialog() {
		return $this->response(\View::forge('dialog/broker_change'));
	}
	public function get_broker_request() {
		return $this->response(\View::forge('dialog/broker_ismember'));
	}

	public function get_broker_invite() {
		return $this->response(\View::forge('dialog/broker_invite'));
	}

	public function post_broker_invite() {
		return \Model\Realtor\Broker::forge($this->member->id)->request_new(\Input::post());
	}

	public function get_broker_change() {
		\Lang::load('broker');
		$view = \View::forge('dialog/broker_select');
		if (\Input::get('id') == 'btn_broker_member_yes') {
			$view->title = __('broker_request');
			$view->broker = __('req_assoc');
		} else {
			$view->title = __('change_broker');
			$view->broker = __('current_broker');
		}
		$view->cboBrokers = \Form::select('broker', '', \Model\Realtor\Broker::forge($this->member->id)->list_array(true,false), array('id' => 'broker'));
		return $this->response($view);
	}

	public function post_broker_request() {
		return \Model\Realtor\Broker::forge($this->member->id)->request_assoc(\Input::post('id'));
	}

	public function get_broker_cancel() {
		if ($this->member->pending_broker != 0) {
			return $this->response(\View::forge('dialog/cancel_request'));
		} else {
			return $this->response(\View::forge('dialog/broker_ismember'));
		}
	}

	public function get_broker_remove() {
		\Lang::load('signup');
		return $this->response(\View::forge('dialog/remove_broker'));
	}

	public function get_billing_history() {
		\Lang::load('orders');
		$view = \View::forge('dialog/order_history');
		$view->data = \Model\Order::forge()->get_history($this->member->id, \Model\User::UT_BROKER);
		return $this->response($view);
	}

	public function get_change_billing() {
		\Lang::load('signup');
		$view = \View::forge('dialog/billing_details');
		$pp = new \Model\Paypal();
		$pp->profile_id = $this->member->paypal_sub_id;
		//$view->cboCardTypes = array('' => 'Select') + \Model\Payment::forge()->get_cc_type();
		$view->cboCardTypes = array('' => 'Select') + $pp->get_cc_type();
		$months = array_map(create_function('&$v', 'return sprintf("%02d", $v);'), range(1, 12));
		$view->cboMonths = array_combine($months, $months);
		$years = range(date('Y'), date('Y') + 8);
		$view->cboYears = array_combine($years, $years);
		$view->data = $pp->profile();
		$view->btnTxt = 'Update';
		return $this->response($view);
	}

	public function post_alert_update() {
		$member = \Model\Broker::forge($this->member->id);
		$member->alerts = \Input::post('alerts');
		$member->save();
	}

	public function get_activity(){
		$view = \View::forge('dialog/account_activity');
		$data = \Model\Broker::forge($this->member->id)->profile();
		$data['user_id'] = $this->member->broker_id;
		$view->data = $data;
		$view->data['login_log'] = \Model\Loginlog::forge()->user_last_success_login($this->member->broker_id);
		return $this->response($view);
	}
}