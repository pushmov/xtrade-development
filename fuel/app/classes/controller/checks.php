<?php
namespace Controller;
use Model;
class Checks extends \Controller_Rest {
	protected $_types = array(1 => 'realtor', 2 => 'broker', 3 => 'vendor');
	protected $_session = '';

	public function post_email() {
		$json = array('status' => 0, 'session' => $this->_session, 'C_Type' => \Input::post('check_type'), 'msg' => '');
		$type = 'realtor';
		if (isset($this->_types[\Input::post('check_type')])) {
			$type = $this->_types[\Input::post('check_type')];
		}
		$model = '\Model\\' . ucwords($type);
		$user = $model::forge()->load(\DB::select_array()->where('email', '=', \Input::post('Email')));
		if ($user->loaded() && (\Input::post('ID') > 0)) {
			$field = $type . '_id';
			if ($user->$field != \Input::post('ID')) {
				$json['status'] = 1;
			}
		} elseif ($user->loaded()) {
			$json['status'] = 1;
		}
		if ($json['status'] == 1) {
			\Lang::load('signup');
			$json['msg'] = __('email_in_use');
		}
		return $this->response(json_encode($json));
	}
}