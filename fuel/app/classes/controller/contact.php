<?php
namespace Controller;

class Contact extends PublicTemplate {
	public function action_index() {
		\Lang::load('contact');
		$this->template->scripts = array('jquery.mask.js');
		$this->template->meta_title = 'xTradeHomes Contact Us';
		$this->template->content = \View::forge('contact');
	}

	public function post_send() {
		$json = array('status' => 'FAILED');
		$post = array_map('trim', \Input::post());
		$validation = \Validation::forge();
		$validation->add_field('name', 'Name', 'required');
		$validation->add_field('email', 'Valid email', 'required|valid_email');
		$validation->add_field('comment', 'Comment', 'required');
		if ($validation->run($post)) {
            $response = \Emails::forge()->contact_us($post);
			$json = array('status' => 'OK', 'message' => '<div class="callout success small">Your comment has been sent. We will contact you shortly. </div>');
		} else {
			$json['errors'] = $validation->error_message();
		}
		return $this->response($json);
	}

	public function get_agent(){
		$view = \View::forge('dialog/contact_agent');
		$agent_info = \Model\Realtor::forge()->agent_info(\Input::get('id'));
		$agent_info['ut'] = \Session::get('type') ? \Model\User::forge()->get_types(\Session::get('type')) : '';
		$agent_info['user_xid'] = \Model\User::forge()->get_session_xid(\Session::get('type'));
		$agent_info['listing_id'] = \Input::get('id');
		$agent_info['logged_in'] = \Authlite::instance('auth_realtor')->logged_in();
		$view->data = $agent_info;
        return $this->response($view);
	}

	public function post_agent(){
		$post = \Input::post('contact');
		try {
			$response = \Model\Realtor::forge()->contact_agent($post);
		} catch(\AutomodelerException $e){
			return array('status' => 'ERROR', 'errors' => $e->errors);
		}
		return $this->response($response);
	}
}