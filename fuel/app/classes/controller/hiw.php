<?php
namespace Controller;

class Hiw extends PublicTemplate {
	public function before() {
		parent::before();
		$this->template->meta_title = 'xTradeHomes How Matching Works';
	}

	public function action_index() {
		\Lang::load('hiw');
		$this->template->styles = 'matches.css';
		$this->template->scripts = array('jquery.tooltipster.min.js', 'matches.js');
		$this->template->content = \View::forge('hiw');
	}

}
