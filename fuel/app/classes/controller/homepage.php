<?php
namespace Controller;

class Homepage extends \Controller {
	public function action_index() {
		\Lang::load('common');
		$view = \View::forge('home_page');
		// Login check
		foreach(array('realtor','broker','vendor','admin') as $k) {
			if (($user = \Authlite::instance('auth_' . $k)->logged_in()) !== false) {
				// Used on header link
				$user_info = \Html::anchor($k . 's', __($k.'_home'), array('title' => "{$user->first_name} {$user->last_name}"));
				$view->set(array('user_info' => $user_info, 'logout' => $k . 's'));
				break;
			}
		}
		$view->saved_email = '';
		\Config::load('google');
		$view->tracking_code = \Config::get('track_code');
		if(\Session::get_flash('unsubscribe')){
			$view->scripts = array('unsubscribe.js');
		}
		if (\Input::get('showlogin') !== null) {
			if (\Input::get('showlogin') == 'realtor') {
				$view->showlogin = "<script>loginDialog('realtor');</script";
			} elseif (\Input::get('showlogin') == 'broker') {
				$view->showlogin = "<script>loginDialog('broker');</script>";
			} elseif (\Input::get('showlogin') == 'vendor') {
				$view->showlogin = "<script>loginDialog('vendor');</script>";
			}
		}
		// Check if the video has been seen
		if (($video = \Cookie::get('how_video')) === null) {
			// Set for 1 year
			\Cookie::set('how_video', 'yes', 31556926);
			$view->show_video = true;
		}
		return \Response::forge($view);
	}

	public function action_reset() {
		$type = strtolower($this->param('type'));
		// Valid types
		if (in_array($type, array('realtor','broker','vendor','admin'))) {
			$model = '\Model\\' . ucwords($type);
			if ($type == \Model\User::forge()->get_types(\Model\User::UT_VENDOR)){
				//vendor dont have pass_reset column, need to display popup

			}
			if ($model::forge()->reset_password($this->param('code'))) {
				\Response::redirect('/' . $type . 's');
			}
		}
		\Response::redirect('/');
	}

	/**
	* use to handle user reset password, auto trigger login popup
	*/
	public function action_dialog(){
		$view = \View::forge('home_page');
		$view->saved_email = '';
		$view->js_vars = array(
			'type' => '\''.$this->param('type').'\''
		);
		$view->scripts = 'auto_login_dialog.js';
		$view->tracking_code = \Config::get('track_code');
		return \Response::forge($view);
	}

	/**
	* The 404 action for the application.
	*
	* @access  public
	* @return  Response
	*/
	public function action_404() {
		return \Response::forge(\View::forge('404_error'));
	}
}
