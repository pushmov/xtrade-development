<?php

namespace Controller\Realtors;

class Client extends Auth{
	public $listing_id;
	public $type;

	public function before(){
		parent::before();
		$this->listing_id = \Sysconfig::create_string('listing_id');
		$this->type = \Input::post('type');
	}

	public function action_xtrade(){
		$this->set_client_information();
		$this->set_client_has(array(
			'type_of_listing' => 0,
			'completed_section' => 0
		));
		$this->set_client_wants(array('completed_section' => 0));
		return $this->response(array('url' => \Uri::create('realtors/listing/edit/' . $this->listing_id)));
	}

	public function action_sell(){
		$this->set_client_information();
		$this->set_client_has(array(
			'type_of_listing' => 1,
			'completed_section' => 0
		));
		$this->set_client_wants(array('completed_section' => 1));
		return $this->response(array('url' => \Uri::create('realtors/listing/edit/' . $this->listing_id)));
	}

	public function action_buy(){
		$this->set_client_information();
		$this->set_client_has(array(
			'type_of_listing' => 2,
			'completed_section' => 1,
			'current_images' => 1
		));
		$this->set_client_wants(array('completed_section' => 0));
		return $this->response(array('url' => \Uri::create('realtors/listing/edit/' . $this->listing_id)));
	}

	private function set_client_information(){
		$fields = array(
			'id' => NULL,
			'listing_id' => $this->listing_id,
			'realtor_id' => \Model\User::realtor_id(),
			'completed_section' => 0
		);
		$model_info = \Model\Client\Information::forge();
		$model_info->set_fields(array_map('trim', $fields));
		$model_info->save();
	}

	private function set_client_has($extra){
		$init = array(
			'listing_id' => $this->listing_id,
			'realtor_id' => \Model\User::realtor_id(),
			'date_created' => date_create()->format(DATETIME_FORMAT_DB),
			'paypal_status' => 0,
			'listing_status' => 9,
			'allowed_images' => 50
		);
		$fields = array_merge($init, $extra);
		$obj = \Model\Client\Has::forge();
		$obj->set_fields(array_map('trim', $fields));
		$obj->save();
	}

	private function set_client_wants($extra){
		$init = array(
			'listing_id' => $this->listing_id,
			'realtor_id' => \Model\User::realtor_id()
		);
		$fields = array_merge($init, $extra);
		$obj = \Model\Client\Want::forge();
		$obj->set_fields(array_map('trim', $fields));
		$obj->save();
	}
}