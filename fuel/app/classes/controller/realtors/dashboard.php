<?php
namespace Controller\Realtors;

class Dashboard extends Auth {
	public function before() {
		parent::before();
		// Needs to update password
		if (($this->member->account_status == \Model\User::AS_NEEDS_PASS) || ($this->member->account_status == \Model\User::AS_NEW)) {
			\Response::redirect('realtors/profile');
		}
	}

	public function action_index() {
		\Lang::load('listing');
		\Session::set(\Model\Listing::SESS_REFERER_NAME, \Uri::current());
		\Session::set('SESS_REFFERER_DASHBOARD', \Model\Listing::SESS_REFFERER_DASHBOARD);
		$imported = \Model\Client\Has::forge()->load(\DB::select()->where('listing_id', '=', \Input::get('listing_id')));
		$view = \View::forge('realtor/dashboard');
		$view->set('filters', \Model\Listing::filter_array());
		$view->view = \Session::get_flash('view', '');
		$this->template->js_vars = array(
			'action' => \Input::get('action'),
			'listing_id' => \Input::get('listing_id'),
			'imported' => $imported->imported
		);
		$this->template->meta_title = 'xTradeHomes My Listings';
		$this->template->scripts = array('dashboard.js', 'members.js');
		$this->template->content = $view;
	}

	public function action_list() {
		\Session::set_flash('view', 'check_for_matches');
		\Response::redirect('realtors');
	}

	public function get_share(){
		$view = \View::forge('dashboard/share');
		return $this->response($view);
	}

	public function get_renew(){
		$data['listing_id'] = \Input::get('listing_id');
		$view = \View::forge('dialog/dashboard/renew', $data);
		return $this->response($view);
	}

	public function get_change(){
		$data['import'] = \Input::get('import');
		$data['listing_id'] = \Input::get('listing_id');
		$view = \View::forge('dialog/dashboard/change', $data);
		return $this->response($view);
	}

	public function get_delete(){
		$data['listing_id'] = \Input::get('listing_id');
		$view = \View::forge('dialog/dashboard/delete', $data);
		return $this->response($view);
	}

	public function get_upgrade(){
		$view = \View::forge('dialog/payment');
		$years = range(date('Y'), date('Y') + 8);
		$view->cboYears = array_combine($years, $years);
		$data['featured_ad'] = 0;
		$data['featured_ad_summary'] = '';
		$data['ut'] = 'realtor';
		$data['address'] = '';
		$data['cpc_address'] = '';
		$data['z_p_code'] = '';
		$data['promo_txt'] = '';
		$data['tax_code'] = '';

		$data['fee'] = \Sysconfig::get('listing_featured_fee');
		/*
		$data['tax'] = $data['tax_code'] = '';
		$tax = \Model\Tax::forge()->load_by_country_state($this->member->country, $this->member->prov_state);
		if ($tax->loaded() && $tax->rate != 0) {
			$data['tax'] = round($data['fee'] * $tax->rate, 2);
			$data['tax_code'] = $tax->code;
		}
		*/
		$view->total = $data['fee'];// + $data['tax'];
		$data['user_type'] = 'realtor';
		$data['broker_id'] = false;
		$data['summary'] = 'Featured listing';
		$data['realtor_id'] = $this->member->id;
		$data['listing_id'] = \Input::get('listing_id');
		$data['summary_total'] = 'Order total';

		//initial value of card
		$data['cc_number'] = $data['cc_cvv'] = $data['exp_month'] =
		$data['exp_year'] = $data['cc_type'] = '';

		$payment = new \Model\Paypalrest\Payment();
		$data['cbo_cc_type'] = $payment->get_cc_type();
		$months = array_map(create_function('&$v', 'return sprintf("%02d", $v);'), range(1, 12));
		$data['cbo_month'] = array_combine($months, $months);
		$view->set('data', array_merge((array)$this->member, $data));

		return $this->response($view);
	}

	public function post_upgrade(){
		$post = \Input::post('data');
		$post['user_xid'] = $post['vendor_id'] = $this->member->realtor_id;
		$post['email'] = $this->member->email;
		return $this->response(\Model\Payment::forge()->upgrade_listing($post));
	}

	public function get_success(){
		\Lang::load('listing');
		$view = \View::forge('dialog/listing/upgrade_success');
		$view->txt = __('upgrade_success');
		return $this->response($view);
	}

	public function get_downgrade(){
		$data['listing_id'] = \Input::get('listing_id');
		$view = \View::forge('dialog/dashboard/downgrade', $data);
		return $this->response($view);
	}

	public function get_activate(){
		$data['listing_id'] = \Input::get('listing_id');
		$view = \View::forge('dialog/dashboard/activate', $data);
		return $this->response($view);
	}

	public function get_deactivate(){
		$data['listing_id'] = \Input::get('listing_id');
		$view = \View::forge('dialog/dashboard/deactivate', $data);
		return $this->response($view);
	}

	public function get_enable(){
		$data['listing_id'] = \Input::get('listing_id');
		$view = \View::forge('dialog/dashboard/enable', $data);
		return $this->response($view);
	}

	public function get_disable(){
		$data['listing_id'] = \Input::get('listing_id');
		$view = \View::forge('dialog/dashboard/disable', $data);
		return $this->response($view);
	}

	public function get_xtraded(){
		$data['listing_id'] = \Input::get('listing_id');
		$view = \View::forge('dialog/dashboard/xtraded', $data);
		return $this->response($view);
	}

	public function get_notxtraded(){
		$data['listing_id'] = \Input::get('listing_id');
		$view = \View::forge('dialog/dashboard/notxtraded', $data);
		return $this->response($view);
	}
}