<?php
namespace Controller\Realtors;

class Match extends Auth {
	public function action_id($listing_id = NULL){
		if(!isset($listing_id)){
			\Response::redirect('realtors');
		}
		\Lang::load('common');
		\Lang::load('matches');
		$inc = \Input::get('inc') ? true : false;
		$data = \Match::forge()->render_matches($listing_id, \Authlite::instance('auth_realtor')->get_user()->realtor_id, $inc);
		\Model\Matches::forge()->set_matches_as_seen($listing_id);
		$this->template->meta_title = 'xTradeHomes Matches to '.$data['listing']['address'];
		$this->template->js_vars = array('id' => $listing_id);
		$this->template->scripts = array('matches.js','members.js');
		$this->template->styles = array('matches.css');
		$this->template->content = \View::forge('matches/index', $data);
	}
	public function action_export(){
		\Lang::load('matches');
		$listing = \Model\Client\Has::forge()->load(\DB::select()->where('listing_id', '=', \Input::get('id')));
		if ($listing->loaded()) {
			$pdf = \Pdf::forge('xTradeHomes Matches '.$listing->listing_id);
			$pdf->WriteMatchesPage($listing, \Input::get('inc') ? true : false);
			$pdf->Output('xTradeHomes Matches '.$listing->listing_id.'.pdf', 'I');
		}
	}

	public function get_optimize(){
		$view = \View::forge('dialog/matches/optimize');
		return $this->response($view);
	}

	public function get_reset(){
		$data['listing_id'] = \Input::get('listing_id');
		$view = \View::forge('dialog/matches/reset', $data);
		return $this->response($view);
	}

	public function get_send(){
		$listing_id = \Input::get('listing_id');
		$data['data'] = \Model\Client\Information::forge()->load(\DB::select_array()->where('listing_id', '=', $listing_id));
		$view = \View::forge('dialog/matches/send', $data);
		return $this->response($view);
	}

	public function get_ignore(){
		$data['listing_id'] = \Input::get('listing_id');
		$view = \View::forge('dialog/matches/ignore', $data);
		return $this->response($view);
	}

	public function get_later(){
		$data['listing_id'] = \Input::get('listing_id');
		$view = \View::forge('dialog/matches/later', $data);
		return $this->response($view);
	}

	public function post_ignore(){
		return $this->response(\Model\Matches::forge()->ignore_match(\Input::post()));
	}

	public function post_save(){
		return $this->response(\Model\Matches::forge()->save_later(\Input::post()));
	}

	public function post_reset(){
		\DB::delete('matches')->where('listing_id', '=', \Input::post('this_listing'))->execute();
		$has = \Model\Client\Has::forge()->load_by_listing_id(\Input::post('this_listing'));
		if ($has->loaded()) {
			$has->matches = 0;
			$has->save();
		}
		return $this->response(array('status' => 'ok'));
	}

	public function post_send(){
		\Lang::load('listing');
		return $this->response(\Model\Matches::forge()->send_matches(\Input::post()));
	}
}