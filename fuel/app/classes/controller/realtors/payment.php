<?php

namespace Controller\Realtors;

class Payment extends Auth {
    public function post_upgrade(){
        $response = \Model\Payment::forge()->upgrade_listing(\Input::post('payment'));
        return $this->response($response);
    }

    public function post_recurring(){
        $response = \Model\Payment::forge()->upgrade_listing(\Input::post('payment'));
        return $this->response($response);
    }
}