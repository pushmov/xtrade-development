<?php
namespace Controller\Vendors;

class Dashboard extends Auth {

	public function before() {
		parent::before();
		// Needs to update password
		if (($this->member->account_status == \Model\User::AS_NEEDS_PASS) || ($this->member->account_status == \Model\User::AS_NEW)) {
			\Response::redirect('/vendors/profile');
		}
	}

	public function action_index() {
		\Lang::load('common');
		\Lang::load('vendor');
		$view = \View::forge('vendor/dashboard');
		$vendor = \Model\Vendor::forge($this->member->id);
		$data = $vendor->as_array();
		if($data['featured'] == \Model\Vendor::VENDOR_STATUS_FEATURED){
			$view->gradeBtn = \Form::button('downgrade', __('downgrade_adv'), array('type' => 'button', 'class' => 'button downgrade'));
			$view->featured_txt = __('featured');
		} else {
			$view->gradeBtn = \Form::button('upgrade', __('upgrade_adv'), array('type' => 'button', 'class' => 'button upgrade'));
			$view->featured_txt = __('nfeatured');
		}
		$data['locale'] = \Model\Vendor::forge()->areaid_to_advertise($data['locale']);
		$data['type'] = \Model\Vendor::forge()->typeid_to_services($data['type']);
		$view->data = $data;
		$view->payment = $vendor->get_payment_info();
		$this->template->js_vars = array(
			'vendor_id' => $this->member->vendor_id
		);
		$this->template->scripts = array('vendor_home.js', 'jquery.uploadfile.min.js');
		$this->template->content = $view;
	}

	public function get_ad_preview(){
		$view = \View::forge('dialog/vendor/ad_preview');
		$view->data = \Model\Vendor::forge($this->member->id)->as_array();
		$view->data['type'] = \Model\Vendor::forge()->typeid_to_services($view->data['type']);
		$view->logo = \Model\Vendor::forge()->get_vendor_logo($this->member->vendor_id);
		return $this->response($view);
	}

	public function get_upgrade(){
		\Lang::load('orders');

		$taxes = \Model\Tax::forge()->load_info($this->member->location_address);
		list($city,$state,$country) = \CityState::get_area_id($this->member->location_address, true);
		$fplan = \Model\Plan::forge()->load_by_csp($country, $state, '', \Model\Plan::PT_VFEATURE);
		if($fplan->loaded() && ($fplan->type_id == \Model\Plan::PT_VFEATURE)) {
			$view = \View::forge('dialog/payment');
			$years = range(date('Y'), date('Y') + 8);
			$view->cboYears = array_combine($years, $years);
			$data = (array) $this->member;
			$data['cpc_address'] = $this->member->location_address;
			$data['user_type'] = 'vendor';
			$data['broker_id'] = false;
			$data['tax'] = $data['tax_code'] = $data['cc_cvv'] = '';
			$data['fee'] = 0;
			$data['promo_txt'] = '';
			$data['summary_total'] = __('monthly_featured_fee');
			$total = $fplan->rate;
			$view->total = $total;
			$data['featured_ad'] = 0;
			$data['featured_ad_summary'] = '';
			$data['fee'] = $total;
			$data['summary'] = 'Monthly Featured Ad';
			$data['listing_id'] = '';
			$data['ut'] = \Model\User::UT_VENDOR;
			/*
			$agreement = new \Model\Paypalrest\Agreement();
			$billing = $agreement->get_billing($this->member->paypal_sub_id);
			$payment = new \Model\Paypalrest\Payment();
			$data['cbo_cc_type'] = $payment->get_cc_type();
			*/
			$data['cc_number'] = $data['cc_cvv'] = $data['exp_month'] =	$data['exp_year'] = $data['cc_type'] = '';
			$payment = new \Model\Paypalrest\Payment();
			$data['cbo_cc_type'] = $payment->get_cc_type();

			$months = array_map(create_function('&$v', 'return sprintf("%02d", $v);'), range(1, 12));
			$data['cbo_month'] = array_combine($months, $months);
			//$view->set('data', array_merge($data, $billing));
			$view->set('data', $data);
			$view->broker_id = false;
			$view->vendor_id = $this->member->vendor_id;
		} else {
			$view = \View::forge('dialog/payment_error');
		}
		return $this->response($view);

	}

	public function post_upgrade(){
		$post = \Input::post('data');
		$post['user_xid'] = $post['vendor_id'] = $this->member->vendor_id;
		$post['id'] = $this->member->id;
		$post['paypal_sub_id'] = $this->member->paypal_sub_id;
		$post['email'] = $this->member->email;
		return $this->response(\Model\Vendor\Payment::forge()->upgrade($post));
	}

	public function get_downgrade(){
		$view = \View::forge('dialog/vendor_downgrade');
		return $this->response($view);
	}

	public function post_downgrade(){
		$vendor = \Model\Vendor::forge($this->member->id);
		return $this->response(\Model\Vendor\Payment::forge()->downgrade($vendor));
	}

	public function get_success(){
		\Lang::load('vendor');
		$type = \Input::get('type');
		if ($type === 'downgrade') {
			$txt = __('downgrade_success');
		} elseif($type === 'upgrade'){
			$txt = __('upgrade_success');
		}
		$view = \View::forge('dialog/vendor_downgrade_success');
		$view->txt = $txt;
		return $this->response($view);
	}
}