<?php
namespace Controller\Vendors;

class Profile extends Auth {
	public function before() {
		parent::before();
	}

	public function action_index() {
		\Lang::load('common');
		\Lang::load('signup');
		$view = \View::forge('vendor/profile')->bind('errors', $errors)->bind('msg', $msg);
		$view->data = \Model\Vendor::forge($this->member->id)->profile();
		$view->data['member_id'] = $view->data['vendor_id'];
		$view->data['type'] = explode(';', $view->data['type']);
		$view->data['target'] = '/vendors/profile/doupload.json';
		$this->template->meta_title = 'xTradeHomes Vendor Profile';
		$this->template->styles = array('tooltipster.css', 'uploadfile.css', 'sumoselect.css', 'token-input.css', 'jquery-te.css','style.css');
		$this->template->scripts = array('jquery.mask.js', 'er_check.js', 'jquery.tooltipster.min.js', 'profile.js', 'vendor_home.js', 'jquery.uploadfile.min.js', 'jquery.sumoselect.min.js', 'jquery.tokeninput.js', 'jquery-te.min.js');
		$this->template->js_vars = array('member' => 'vendors/', 'vendor_id' => $this->member->vendor_id, 'type' => 'vendor', 'memberid' => $this->member->vendor_id);
		$this->template->content = $view;
	}

	public function get_billing_history() {
		\Lang::load('orders');
		$view = \View::forge('dialog/order_history');
		$view->data = \Model\Order::forge()->get_history($this->member->id, \Model\User::UT_VENDOR);
		return $this->response($view);
	}

	public function get_change_billing() {
		\Lang::load('signup');
		$view = \View::forge('dialog/billing_details');
		$agreement = new \Model\Paypalrest\Agreement();
		$data = $agreement->get_billing($this->member->paypal_sub_id);
		$view->cboCardTypes = array('' => 'Select') + \Model\Payment::forge()->get_cc_type();
		//$view->cboCardTypes = $data['select_cc_type'];
		$months = array_map(create_function('&$v', 'return sprintf("%02d", $v);'), range(1, 12));
		$view->cboMonths = array_combine($months, $months);
		$years = range(date('Y'), date('Y') + 8);
		$view->cboYears = array_combine($years, $years);
		$this->member->cpc_address = $this->member->location_address;
		$view->data = array_merge($data, (array) $this->member);
		$view->data['user_xid'] = \Model\User::UT_VENDOR;
		$view->data['user_id'] = $this->member->vendor_id;
		$view->btnTxt = 'Update';
		return $this->response($view);
	}

	public function get_password() {
		\Lang::load('signup');
		$view = \View::forge('dialog/change_password')->bind('err_msg', $msg);
		if (\Input::get('status') == \Model\User::AS_NEEDS_PASS) {
			$msg = __('please_create_a_new_password');
		}
		$view->type = \Model\User::UT_BROKER;
		$view->id = \Input::get('id');
		return $this->response($view);
	}

	public function post_pass_update() {
		return $this->response(\Model\Vendor::forge(\Input::post('id'))->update_password(\Input::post()));
	}

	public function get_close_account() {
		\Lang::load('signup');
		return $this->response(\View::forge('dialog/close_account'));
	}

	public function post_update() {
		\Lang::load('common');
		\Lang::load('signup');
		try {
			\Model\Vendor::forge(\Input::post('data.id'))->doupdate(\Input::post());
			$json['status'] = 'OK';
			$json['message'] = '<div data-alert class="callout success">'.__('profile_updated').'<a href="#" class="close">&times;</a></div>';
			return $this->response($json);
		} catch(\AutoModelerException $e) {
			return $this->response(array('errors' => $e->errors));
		}
	}

	public function post_doupload() {
		$output_dir = DOCROOT . \Model\Vendor::IMG_PATH;
		$json = array();

		if (!is_dir($output_dir)){
			mkdir($output_dir);
		}

		if ($_FILES["myfile"]["error"]) {
			$json['msg'] = $_FILES["myfile"]["error"];
		} else {
			$ext = pathinfo($_FILES["myfile"]["name"], PATHINFO_EXTENSION);
			$filename = str_pad($this->member->id, 8, '0', STR_PAD_RIGHT).'.'.$ext;
			if(is_readable(DOCROOT. \Model\Vendor::IMG_PATH.$filename)){
				unlink(DOCROOT. \Model\Vendor::IMG_PATH.$filename);
			}
			move_uploaded_file($_FILES["myfile"]["tmp_name"], $output_dir . $filename);
			$json[] = $filename;
		}
		return $this->response($json);
	}

	public function post_billing_update(){
		$data = \Input::post('data');
		$data['email'] = $this->member->email;
		$data['id'] = $this->member->id;
		return $this->response(\Model\Vendor\Payment::forge()->billing_update($data));
	}

	public function get_billing_update_success(){
		return $this->response(\View::forge('dialog/billing_details_success'));
	}


}