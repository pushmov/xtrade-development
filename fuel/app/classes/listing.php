<?php

class Listing{
    protected $_listing;
    protected $_neighbourhoods;

    public function __construct() {
        $this->_listing = \Model\Listing::forge();
        $this->_neighbourhoods = \Model\Neighbourhoods::forge();
    }

    public static function forge(){
        return new self;
    }

    public function get_prop_options($label, $type, $rev = false, $import = false){
		if($type == 'style'){
			if(!$import){
				$options = $this->_listing->prop_options_style_non_import();
			}else{
				$prop_opt = $this->_listing->prop_options_style_import();
				$options = ($rev) ? array_flip($prop_opt) : $prop_opt;
			}
		}else{
			if(!$import){
				$options = ($rev) ? array_flip($this->_listing->prop_options_nonstyle_nonimport_nonrev()) : $this->_listing->prop_options_nonstyle_nonimport_rev();
			}else{
				$prop_opt = $this->_listing->prop_options_nonstyle_import();
				$options = ($rev) ? array_flip($prop_opt) : $prop_opt;
			}
		}
        return isset($options[$label]) ? $options[$label] : FALSE;
    }

	public function set_search_params_desires($term1, $s_type, $listing_id){

		//****************** High Priority ******************//
        $query_builder = \DB::select_array();
        $query_builder->where('country', '=', $term1['country']);

		if($term1['prov_state'] != '9999999' && $term1['prov_state'] != ''){
            $query_builder->where('prov_state', '=', $term1['prov_state']);
        }

		if($term1['county'] != '9999999' && $term1['county'] != '' && $term1['county'] != '0'){
            $query_builder->where('county', '=', $term1['county']);
        }

		if($term1['surrounding_area'] == 1){
			if($term1['city'] != '9999999' && $term1['city'] != ''){
                //todo : unknown function get_surrounding_area
                $expr = \DB::expr('city REGEXP \''.$term1['city'].'\'|'.  \CityState::get_surrounding_area($term1['city']));
                $query_builder->where($expr);
			}
		} else {
			if($term1['city'] != '9999999' && $term1['city'] != ''){
                $query_builder->where('city', '=', $term1['city']);
            }
		}

		$prop_style = ($term1['prop_style'] != '') ? str_split($term1['prop_style']) : NULL;

		if($prop_style[0] != '9' && !empty($prop_style) && $prop_style[0] != ''){
			$found_one = false;
            $regexp = $this->_return_array($prop_style, 1);
            $query_builder->where(\DB::expr('prop_style REGEXP '.$regexp));
		}

		$stories = ($term1['prop_type'] == '') ? NULL : str_split($term1['prop_type']);
		if($stories[0] != '9' && !empty($stories) && $stories[0] != ''){
			$found_one = false;
            $regexp = $this->_return_array($stories, 1);
            if($regexp != ''){
                $query_builder->where(\DB::expr('prop_type REGEXP '.$regexp));
            }

		}

		$neigh_array = $this->_neighbourhoods->load(\DB::select_array()
			->where('listing_id', '=', $listing_id)
			->where('neigh_name', '<>', 'none')
			->order_by('priority', 'asc'), NULL)->as_array();

		$nint = 1;
		$passed = 0;
        $regexp = '';
		if(!empty($neigh_array)){
			foreach($neigh_array as $neigh){
				if($neigh['neigh_name'] != 'none' || $neigh['neigh_name'] != 'none'){
					if($nint == 1){
						$regexp .= $neigh['neigh_name'];
						$nint++;
						$passed = 1;
					}else{
                        $regexp .= '|'.$neigh['neigh_name'];
					}
				}
			}
            $query_builder->where(\DB::expr("neighbourhood REGEXP '" .$regexp."'"));
		}


        $query_builder->where('p_title', '=', $term1['p_title']);
        $query_builder->where('sq_footage', 'between', array($term1['sq_footage_low'], $term1['sq_footage_high']));
        $query_builder->where('listing_price', 'between', array($term1['listing_price_low'], $term1['listing_price_high']));

		//****************** Medium Priority ******************//

		if($s_type >= 1){
			if($term1['bedrooms'] != '9999999' && $term1['bedrooms'] != ''){
                $query_builder->where('bedrooms', 'between', array($term1['bedrooms'], 100));
			}
		}
		if($s_type >= 1){
			if($term1['bathrooms'] != '9999999' && $term1['bathrooms'] != ''){
                $query_builder->where('bathrooms', 'between', array($term1['bathrooms'], 100));
			}
		}

		if($s_type >= 1){
			$finished_basement = ($term1['finished_basement'] == '') ? NULL : str_split($term1['finished_basement']);
			if($finished_basement[0] != '9' && !empty($finished_basement) && $finished_basement[0] != ''){
				$found_one = false;
                $regexp = $this->_return_array($finished_basement, 1);
                $query_builder->where(\DB::expr('finished_basement REGEXP '.$regexp));
			}
		}

		if($s_type >= 1){
			$garage_type = ($term1['garage_type'] == '') ? NULL : str_split($term1['garage_type']);
			if($garage_type[0] != '9' && !empty($garage_type) && $garage_type[0] != ''){
				$found_one = false;
                $regexp = $this->_return_array($garage_type, 1);
                $query_builder->where(\DB::expr('garage_type REGEXP '.$regexp));
			}
		}
		//****************** Low Priority ******************//

		if($s_type >= 2){
			if($term1['total_acreage'] != '9999999'){
                $query_builder->where(\DB::expr('`total_acreage` BETWEEN 0 AND '.$term1['total_acreage']));
			}
		}
		if($s_type >= 2){
			if($term1['laundry'] != '000' && $term1['laundry'] != ''){
                $query_builder->where('laundry', '=', $term1['laundry']);
			}
		}
		if($s_type >= 2){
			if($term1['kitchen'] != '000' && $term1['kitchen'] != ''){
                $query_builder->where('kitchen', '=', $term1['kitchen']);
			}
		}
		if($s_type >= 2){
			$Heating = ($term1['heating'] == '') ? NULL : str_split($term1['heating']);
			if($Heating[0] != '9' && !empty($Heating) && $Heating[0] != ''){
				$found_one = false;
                $regexp = $this->_return_array($Heating, 1);
                $query_builder->where(\DB::expr('heating REGEXP '.$regexp));
			}
		}

		if($s_type >= 2){
			$cooling = ($term1['cooling'] == '') ? NULL : str_split($term1['cooling']);
			if($cooling[0] != '9' && !empty($cooling) && $cooling[0] != ''){
				$found_one = false;
                $regexp = $this->_return_array($cooling, 1);
                $query_builder->where(\DB::expr('cooling REGEXP '.$regexp));
			}
		}

		if($s_type >= 2){
			$water = ($term1['water'] == '') ? NULL : str_split($term1['water']);
			if($water[0] != '9' && !empty($water) && $water[0] != ''){
				$found_one = false;
                $regexp = $this->_return_array($water, 1);
                $query_builder->where(\DB::expr('water REGEXP '.$regexp));
			}
		}

		if($s_type >= 2){
			$sewer = ($term1['sewer'] == '') ? NULL : str_split($term1['sewer']);
			if($sewer[0] != '9' && !empty($sewer) && $sewer[0] != ''){
				$found_one = false;
                $regexp = $this->_return_array($sewer, 1);
                $query_builder->where(\DB::expr('sewer REGEXP '.$regexp));
			}
		}

		if($s_type >= 2){
			$heat_source = ($term1['heat_source'] == '') ? NULL : str_split($term1['heat_source']);
			if($heat_source[0] != '9' && !empty($heat_source) && $heat_source[0] != ''){
				$found_one = false;
                $regexp = $this->_return_array($heat_source, 1);
                $query_builder->where(\DB::expr('heat_source REGEXP '.$regexp));
			}
		}

		if($s_type >= 2){
			$roof_type = ($term1['roof_type'] == '') ? NULL : str_split($term1['roof_type']);
			if($roof_type[0] != '9' && !empty($roof_type) && $roof_type[0] != ''){
				$found_one = false;
                $regexp = $this->_return_array($roof_type, 1);
                $query_builder->where(\DB::expr('roof_type REGEXP '.$regexp));
			}
		}

		if($s_type >= 2){
			$floor_type = ($term1['floor_type'] == '') ? NULL : str_split($term1['floor_type']);
			if($floor_type[0] != '9' && !empty($floor_type) && $floor_type[0] != ''){
				$found_one = false;
                $regexp = $this->_return_array($floor_type, 1);
                $query_builder->where(\DB::expr('floor_type REGEXP '.$regexp));
			}
		}

		if($s_type >= 2){
			$siding_type = ($term1['siding_type'] == '') ? NULL : str_split($term1['siding_type']);
			if($siding_type[0] != '9' && !empty($siding_type) && $siding_type[0] != ''){
				$found_one = false;
                $regexp = $this->_return_array($siding_type, 1);
                $query_builder->where(\DB::expr('siding_type REGEXP '.$regexp));
			}
		}


		if($s_type >= 2){
			if($term1['total_lot_size'] != '9999999'){
                $query_builder->where('lot_size', '=', $term1['total_lot_size']);
			}
		}

		return $query_builder;
	}

	public function set_search_params_info($term1, $s_type, $lid){
		//****************** High Priority ******************//

        $query_builder = \DB::select_array();
        $query_builder->where('country', '=', $term1['country']);

		if($term1['prov_state'] != '9999999'){
            $query_builder->where('prov_state', '=', $term1['prov_state']);
        }
		if($term1['county'] != '9999999'){
            if($term1['county'] != '0'){
                $query_builder->where('county', '=', $term1['county']);
            }
        }
		if($term1['city'] != '9999999'){
            $query_builder->where('city', '=', $term1['city']);
        }

		$prop_style = str_split($term1['prop_style']);
		if($prop_style[0] != '9' && !empty($prop_style) && $prop_style[0] != ''){
			$found_one = false;
            $regexp = $this->_return_array($prop_style, 1);
			if($regexp != ''){
				$query_builder->where(\DB::expr('prop_style REGEXP '.$regexp));
			}
		}

		$stories = str_split($term1['prop_type']);
		if($stories[0] != '9' && !empty($stories) && $stories[0] != ''){
			$found_one = false;
            $regexp = $this->_return_array($stories, 1);
            if($regexp != ''){
                $query_builder->where(\DB::expr('`prop_type` REGEXP '.$regexp));
            }

		}

		$neigh_array = $this->_neighbourhoods->load(\DB::select_array()
			->where('listing_id', '=', $lid)
			->where('neigh_name', '<>', 'none')
			->order_by('priority', 'asc'), NULL)->as_array();

		$nint = 1;
		$passed = 0;
        $regexp = '';
		if(!empty($neigh_array)){
			foreach($neigh_array as $neigh){
				if($neigh['neigh_name'] != 'none' || $neigh['neigh_name'] != 'none'){
					if($nint == 1){

						$regexp .= $neigh['neigh_name'];
						$nint++;
						$passed = 1;
					}else{
						$regexp .= '|'.$neigh['neigh_name'];
					}
				}
			}
            $query_builder->where(\DB::expr("neighbourhood REGEXP '".$regexp."'"));
		}

        $query_builder->where('p_title', '=', $term1['p_title']);
        $query_builder->where('sq_footage', 'between', array($term1['sq_footage_low'], $term1['sq_footage_high']));
        $query_builder->where('listing_price', 'between', array($term1['listing_price_low'], $term1['listing_price_high']));

		//****************** Medium Priority ******************//

		if($s_type >= 1){
			if($term1['bedrooms'] != '9999999'){
                $query_builder->where('bedrooms', 'between', array($term1['bedrooms'], 100));
			}
		}
		if($s_type >= 1){
			if($term1['bathrooms'] != '9999999'){
                $query_builder->where('bathrooms', 'between', array($term1['bathrooms'], 100));
			}
		}

		if($s_type >= 1){
			$finished_basement = str_split($term1['finished_basement']);
			if($finished_basement[0] != '9' && !empty($finished_basement) && $finished_basement[0] != ''){
				$found_one = false;
                $regexp = $this->_return_array($finished_basement, 1);
                $query_builder->where(\DB::expr('finished_basement REGEXP '.$regexp));
			}
		}
		if($s_type >= 1){
			$garage_type = str_split($term1['garage_type']);
			if($garage_type[0] != '9' && !empty($garage_type) && $garage_type[0] != ''){
				$found_one = false;
                $regexp = $this->_return_array($garage_type, 1);
                $query_builder->where(\DB::expr('garage_type REGEXP '.$regexp));
			}
		}

		//****************** Low Priority ******************//
		if($s_type >= 2){
			if($term1['total_acreage'] != '9'){
                $query_builder->where(\DB::expr('`total_acreage` BETWEEN 0 AND '.$term1['total_acreage']));
			}
		}
		if($s_type >= 2){
			if (($term1['laundry'] != '') && ($term1['laundry'] != '000')) {
                $query_builder->where('laundry', '=', $term1['laundry']);
			}
		}
		if($s_type >= 2){
			if (($term1['kitchen'] != '') && ($term1['kitchen'] != '000')){
                $query_builder->where('kitchen', '=', $term1['kitchen']);
			}
		}
		if($s_type >= 2){
			$heating = str_split($term1['heating']);
			if($heating[0] != '9' && !empty($heating) && $heating[0] != ''){
				$found_one = false;
                $regexp = $this->_return_array($heating, 1);
                $query_builder->where(\DB::expr('heating REGEXP '.$regexp));
			}
		}
		if($s_type >= 2){
			$cooling = str_split($term1['cooling']);
			if($cooling[0] != '9' && !empty($cooling) && $cooling[0] != ''){
				$found_one = false;
                $regexp = $this->_return_array($cooling, 1);
                $query_builder->where(\DB::expr('cooling REGEXP '.$regexp));
			}
		}
		if($s_type >= 2){
			$water = str_split($term1['water']);
			if($water[0] != '9' && !empty($water) && $water[0] != ''){
				$found_one = false;
                $regexp = $this->_return_array($water, 1);
                $query_builder->where(\DB::expr('water REGEXP '.$regexp));
			}
		}
		if($s_type >= 2){
			$sewer = str_split($term1['sewer']);
			if($sewer[0] != '9' && !empty($sewer) && $sewer[0] != ''){
				$found_one = false;
                $regexp = $this->_return_array($sewer, 1);
                $query_builder->where(\DB::expr('sewer REGEXP '.$regexp));
			}
		}
		if($s_type >= 2){
			$heat_source = str_split($term1['heat_source']);
			if($heat_source[0] != '9' && !empty($heat_source) && $heat_source[0] != ''){
				$found_one = false;
                $regexp = $this->_return_array($heat_source, 1);
                $query_builder->where(\DB::expr('heat_source REGEXP '.$regexp));
			}
		}
		if($s_type >= 2){
			$roof_type = str_split($term1['roof_type']);
			if($roof_type[0] != '9' && !empty($roof_type) && $roof_type[0] != ''){
				$found_one = false;
                $regexp = $this->_return_array($roof_type, 1);
                $query_builder->where(\DB::expr('roof_type REGEXP '.$regexp));
			}
		}
		if($s_type >= 2){
			$floor_type = str_split($term1['floor_type']);
			if($floor_type[0] != '9' && !empty($floor_type) && $floor_type[0] != ''){
				$found_one = false;
                $regexp = $this->_return_array($floor_type, 1);
                $query_builder->where(\DB::expr('floor_type REGEXP '.$regexp));
			}
		}
		if($s_type >= 2){
			$siding_type = str_split($term1['siding_type']);
			if($siding_type[0] != '9' && !empty($siding_type) && $siding_type[0] != ''){
				$found_one = false;
                $regexp = $this->_return_array($siding_type, 1);
                $query_builder->where(\DB::expr('siding_type REGEXP '. $regexp));
			}
		}

		if($s_type >= 2){
			if($term1['total_lot_size'] != '9999999'){
                $query_builder->where(\DB::expr('lot_size BETWEEN '.$term1['total_lot_size']));
			}
		}

        $query_builder->where('listing_status', '=', 0);
		return $query_builder;
	}

	/**
	 * Generate data required for listing detail page {feed}_{id}
	 * @param string $listing_id Listing Id
	 * @param boolean $active_only active listing only flag
	 * @return array data
	 */
	public function listing_detail($listing_id, $active_only=true){
		list($src, $id) = explode('_', $listing_id);
		if ($src == \Model\Listing::FEED_NON_IMPORTED) {
			$sql = \DB::select_array()->where('listing_id', '=', $id)
				->where('type_of_listing', '!=', \Model\Listing::TYPE_BUY_ONLY);
			if($active_only === true){
				$sql->where('listing_status', '=', \Model\Listing::LS_ACTIVE);
			}
			$model = \Model\Client\Has::forge()->load($sql);
			if(!$model->loaded()){
				return false;
			}
			$data = $model->as_array();
			$data['date_created'] = $data['date_created'] == '0000-00-00 00:00:00' ? '' : $data['date_created'];
			$data['photos'] = \Model\Photos\Xtrade::forge()->load_photo_detail($data['listing_id']);
			$wants = \Model\Client\Want::forge()->load(\DB::select_array()->where('listing_id', '=', $id))->as_array();
			$data['has_wants'] = (((bool) $wants['completed_section']) && ($model->type_of_listing == \Model\Listing::TYPE_XTRADED));
		} elseif ($src == \Model\Listing::FEED_IMPORTED) {
			$model = \Model\Crea\Property::forge();
			$data = $model->listing_detail($id);
			if(empty($data)){
				return false;
			}
			$data['photos'] = $model->load_photo_detail($data['id'], null, false);
			$data['prop_type'] = $data['prop_type'].' '.$data['constructionstyleattachment'];
			$data['has_wants'] = false;
			$client_has = \Model\Client\Has::forge()->load(\DB::select_array()->where('external_id', '=', $id));
			$data['has'] = $client_has->as_array();
			$data['listing_status'] = $data['has']['listing_status'];
			$data['listing_id'] = $data['has']['id'];
			$data['type_of_listing'] = \Model\Listing::FEED_IMPORTED;
			$data['date_created'] = $data['has']['date_created'] == '0000-00-00 00:00:00' ? '' : $data['has']['date_created'];
			if($client_has->loaded() && ($client_has->type_of_listing == \Model\Listing::TYPE_XTRADED)){
				$wants = \Model\Client\Want::forge()->load(\DB::select_array()->where('listing_id', '=', $client_has->listing_id));
				if($wants->loaded() && $wants->completed_section == \Model\Client::STATUS_COMPLETE){
					$data['has_wants'] = true;
				}
			}
		}

		if((float) $data['lat'] == 0 || (float) $data['lng'] == 0 || $data['lat'] == '' || $data['lng'] == ''){
			$latlng = $model->get_listing_geolocation($id);
			$data['lat'] = $latlng['lat'];
			$data['lng'] = $latlng['lng'];
		}
		if (($src == 0) && empty($data['external_id'])) {
			$data['listing_num'] = 'xTradeHomes ID#: ' . $data['listing_id'];
		} elseif (($src == 0) && !empty($data['external_id'])) {
			// If the listing is external load that id
			$data['listing_num'] = 'ML #: ' . \Model\Crea\Property::forge($data['external_id'])->listingid;
		} else {
			$data['listing_num'] = 'ML #: ' . \Model\Crea\Property::forge($id)->listingid;
		}
		$area = \CityState::get_area_name($data['city'], $data['prov_state'], $data['country'], false);
		$data['features'] = $model->get_features_list($data);
		$data['location'] = $area;
		$data['address'] = $data['address']."<br>".$area.',' . \CityState::format_zip($data['z_p_code']);
		$data['agent'] = \Model\Listing::forge()->load_listing_agent($listing_id);
		$data['feed'] = $src;
		$data['id'] = $id;
		$data['left_options'] = \Model\Listing::forge()->build_left_option_list($data);
		$data['right_options'] = \Model\Listing::forge()->build_right_option_list($data);
		$data['extra_options'] = \Model\Listing::forge()->build_extra_option_list($data);
		$data['map_key'] = \Sysconfig::get_map_key();
		return $data;
	}

	public function render_listing($listing_id){
		$data = array();
		$clienthas = \Model\Client\Has::forge()->load_by_listing_id($listing_id);
		if(!$clienthas->loaded()) {
			\Response::redirect('realtors');
		}
		$has = $clienthas->as_array();
		$data['info'] = \Model\Client\Information::forge()->load_by_listing_id($listing_id);
		$wants = \Model\Client\Want::forge()->load_by_listing_id($listing_id)->as_array();
		$data['edit']['sub_type'] = \Model\Listing::forge()->get_types($has['type_of_listing']);
		$data['edit']['listing_id'] = $listing_id;
		if($has['imported']){
			$data['storeys_h']['attr']['disabled'] = 'disabled';
		}
		$data['unchecked'] = \Model\Client\Has::FEATURES_UNCHECKED_VALUE;
		$data['checked'] = \Model\Client\Has::FEATURES_CHECKED_VALUE;
		$data['has'] = $has;
		$data['has']['c_address'] = \CityState::get_area_name($has['city'], $has['prov_state'], $has['country']);
		if($data['has']['c_address'] == ''){
			$data['neighbourhood_h']['attr']['disabled'] = 'disabled';
		}

		$bathrooms = floor($has['bathrooms'] * 2) / 2;
		$data['has']['bathrooms'] = ($bathrooms == 0 || $bathrooms == 0.0) ? '' : $bathrooms;
		$data['has']['interior_features'] = str_split($has['interior_features']);
		$data['has']['kitchen'] = str_split($has['kitchen']);
		$data['has']['laundry'] = str_split($has['laundry']);
		$data['has']['property_features'] = str_split($has['property_features']);
		$data['has']['neighbourhood_features'] = str_split($has['neighbourhood_features']);
		$data['has']['listing_price'] = ($data['has']['listing_price'] == 0) ? '' : $data['has']['listing_price'];
		$data['has']['sq_footage'] = ($data['has']['sq_footage'] == 0) ? '' : $data['has']['sq_footage'];
		$data['has']['bedrooms'] = ($data['has']['bedrooms'] == 0) ? '' : $data['has']['bedrooms'];

		//wants_info.php selects
		$data['wants'] = $wants;
		//multiple
		$data['wants']['prop_style'] = $wants['prop_style'] == '' ? array(9) : str_split($wants['prop_style']);
		$data['wants']['siding_type'] = $wants['siding_type'] == '' ? array(9) : str_split($wants['siding_type']);
		$data['wants']['garage_type'] = $wants['garage_type'] == '' ? array(9) : str_split($wants['garage_type']);
		$data['wants']['floor_type'] = $wants['floor_type'] == '' ? array(9) : str_split($wants['floor_type']);
		$data['wants']['roof_type'] = $wants['roof_type'] == '' ? array(9) : str_split($wants['roof_type']);

		//single
		$data['wants']['finished_basement'] = $wants['finished_basement'] == '' ? 9 : str_split($wants['finished_basement']);
		$data['wants']['prop_type'] = $wants['prop_type'] == '' ? 9 : str_split($wants['prop_type']);
		$data['wants']['view'] = $wants['view'] == '' ? 9 : $wants['view'];
		$data['wants']['heat_source'] = $wants['heat_source'] == '' ? 9 : $wants['heat_source'];
		$data['wants']['heating'] = $wants['heating'] == '' ? 9 : $wants['heating'];
		$data['wants']['cooling'] = $wants['cooling'] == '' ? 9 : $wants['cooling'];
		$data['wants']['water'] = $wants['water'] == '' ? 9 : $wants['water'];
		$data['wants']['sewer'] = $wants['sewer'] == '' ? 9 : $wants['sewer'];
		$data['wants']['address'] = \CityState::get_area_name($wants['city'], $wants['prov_state'], $wants['country']);
		if($data['wants']['address'] == ''){
			$data['neighbourhood_w']['attr']['disabled'] = 'disabled';
		}
		if($has['type_of_listing'] == \Model\Listing::TYPE_XTRADED && $has['listing_status'] == \Model\Listing::LS_ACTIVE){
			$matches = \Match::forge();
			$matches_txt = $matches->matches($listing_id);
			$data['client_info_data']['matches']= $matches_txt;
		}
		$data['show_request_button'] = true;
		if (!empty($data['has']['external_id'])) {
			// If the listing is external load that id
			$data['listing_num'] = 'ML #: ' . \Model\Crea\Property::forge($data['has']['external_id'])->listingid;
		} else {
			$data['listing_num'] = 'xTrade #: ' . $data['has']['listing_id'];
		}
		return $data;
	}

	private function _return_array($column_array){
		$results_for_query = "'";
		$found_one = false;

		foreach($column_array as $value){
			switch ($value) {
				case "a":
					$results_for_query .= "a";
					$found_one = true;
					break;
				case "b":
					if($found_one){
						$results_for_query .= "|";
					}
					$results_for_query .= "b";
					$found_one = true;
					break;
				case "c":
					if($found_one){
						$results_for_query .= "|";
					}
					$results_for_query .= "c";
					$found_one = true;
					break;
				case "d":
					if($found_one){
						$results_for_query .= "|";
					}
					$results_for_query .= "d";
					$found_one = true;
					break;
				case "e":
					if($found_one){
						$results_for_query .= "|";
					}
					$results_for_query .= "e";
					$found_one = true;
					break;
				case "f":
					if($found_one){
						$results_for_query .= "|";
					}
					$results_for_query .= "f";
					$found_one = true;
					break;
				case "g":
					if($found_one){
						$results_for_query .= "|";
					}
					$results_for_query .= "g";
					$found_one = true;
					break;
				case "h":
					if($found_one){
						$results_for_query .= "|";
					}
					$results_for_query .= "h";
					$found_one = true;
					break;
				case "i":
					if($found_one){
						$results_for_query .= "|";
					}
					$results_for_query .= "i";
					$found_one = true;
					break;
				case "j":
					if($found_one){
						$results_for_query .= "|";
					}
					$results_for_query .= "j";
					$found_one = true;
					break;
				case "k":
					if($found_one){
						$results_for_query .= "|";
					}
					$results_for_query .= "k";
					$found_one = true;
					break;
				case "l":
					if($found_one){
						$results_for_query .= "|";
					}
					$results_for_query .= "l";
					$found_one = true;
					break;
				case "m":
					if($found_one){
						$results_for_query .= "|";
					}
					$results_for_query .= "m";
					$found_one = true;
					break;
				case "n":
					if($found_one){
						$results_for_query .= "|";
					}
					$results_for_query .= "n";
					$found_one = true;
					break;
				case "o":
					if($found_one){
						$results_for_query .= "|";
					}
					$results_for_query .= "o";
					$found_one = true;
					break;
				case "p":
					if($found_one){
						$results_for_query .= "|";
					}
					$results_for_query .= "p";
					$found_one = true;
					break;
				case "q":
					if($found_one){
						$results_for_query .= "|";
					}
					$results_for_query .= "q";
					$found_one = true;
					break;
				case "r":
					if($found_one){
						$results_for_query .= "|";
					}
					$results_for_query .= "r";
					$found_one = true;
					break;
				case "s":
					if($found_one){
						$results_for_query .= "|";
					}
					$results_for_query .= "s";
					$found_one = true;
					break;
				case "t":
					if($found_one){
						$results_for_query .= "|";
					}
					$results_for_query .= "t";
					$found_one = true;
					break;
				case "u":
					if($found_one){
						$results_for_query .= "|";
					}
					$results_for_query .= "u";
					$found_one = true;
					break;
				case "v":
					if($found_one){
						$results_for_query .= "|";
					}
					$results_for_query .= "v";
					$found_one = true;
					break;
				case "w":
					if($found_one){
						$results_for_query .= "|";
					}
					$results_for_query .= "w";
					$found_one = true;
					break;
				case "x":
					if($found_one){
						$results_for_query .= "|";
					}
					$results_for_query .= "x";
					$found_one = true;
					break;
				case "y":
					if($found_one){
						$results_for_query .= "|";
					}
					$results_for_query .= "y";
					$found_one = true;
					break;
				case "z":
					if($found_one){
						$results_for_query .= "|";
					}
					$results_for_query .= "z";
					$found_one = true;
					break;
			}
		}
		$results_for_query .= "'";

		return $results_for_query;
	}
}