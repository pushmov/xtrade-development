<?php
namespace Model;

class Admin extends User {
	const LOG_DISPLAY_LIMIT = 500;

	const ADMIN_ENABLED = 1;
	const ADMIN_DISABLED = 0;

	protected $_class = 'admin';
	protected $_table_name = 'admins';
	protected $_data = array( 'id' => '', 'first_name' => '', 'last_name' => '', 'email' => '', 'password' => '',
		'auth_level' => '', 'logged_in' => '', 'last_login' => '', 'pass_reset' => '', 'enabled' => self::ADMIN_DISABLED,
		'session_id' => '', 'ip_address' => '');

	protected $_rules = array();

	/**
	* Admin login
	* override user/login
	*
	* @param string $remember
	* @return bool
	*/
	public function login($remember = '') {
		if ($this->enabled == self::ADMIN_ENABLED) {
			$this->last_login = date_create()->format(DATETIME_FORMAT_DB);
			$this->ip_address = \Input::ip();
			$this->logged_in = 1;
			parent::save();
			return true;
		} else {
			return false;
		}
	}

	/**
	* Generate company name dropdown from broker table
	*
	* @return array paired value of broker id and company name
	*/
	public function select_broker(){
		\Lang::load('common');
		$data['0'] = __('none');
		$brokers = \Model\Broker::forge()->load(\DB::select_array(), NULL)->as_array();
		foreach($brokers as $row){
			$data[$row['id']] = $row['company_name'];
		}
		return $data;
	}

	/**
	* Generate user action button from given user's account status
	* @param int $account_status User account status
	* @param String $realtor_id User realtor id
	* @param int $user_type UT_USER
	* @return String HTML Button tag
	*/
	public function button_account_status($account_status, $realtor_id , $user_type){
		\Lang::load('common');
		$model = '\Model\User';
		$data['approve'] = '';
		$user_type_name = $model::forge()->get_types($user_type);
		switch($account_status){
			case $model::AS_VALID:
				$data['status'] = __('usr_valid');
				break;
			case $model::AS_PENDING_VERIFY:
				$data['status'] = __('usr_pending');
				$data['approve'] = \Form::button('approve', __('usr_approve'), array('type' => 'button', 'class' => 'button approve_btn', 'id' => $realtor_id, 'data-type' => $user_type_name));
				break;
			case $model::AS_USER_CLOSED:
				$data['status'] = __('usr_closed');
				break;
			case $model::AS_ADMIN_CLOSED:
				$data['status'] = __('usr_new_pass');
				break;
			case $model::AS_SUSPENDED:
				$data['status'] = __('usr_suspended');
				break;
			case $model::AS_PAST_CLOSED:
				$data['status'] = __('usr_not_verified');
				break;
			case $model::AS_PENDING_CANCEL:
				$data['status'] = __('usr_pending');
				break;
			case $model::AS_NEEDS_PASS:
				$data['status'] = __('usr_need_pass');
				break;
			case $model::AS_BROKER_USER || $model::AS_NEW:
				$data['status'] = __('usr_new');
				break;
			case $model::AS_APPROVED:
				$data['status'] = __('usr_valid_not_verified');
				break;
			case $model::AS_VERIFIED:
				$data['status'] = __('usr_verified_not_approved');
				$data['approve'] = \Form::button('approve', __('usr_approve'), array('type' => 'button', 'class' => 'button'));
				break;
		}
		return $data;
	}

	/**
	* Generate action button for suspend/resend activation from given account status
	*
	* @param int $account_status User Account Status
	* @param id $id user id,
	* @param string $type User type[realtor,broker,vendor]
	*
	* @return String Button HTML tag
	*/
	public function suspend_or_not($account_status,$id, $type){
		\Lang::load('common');
		if($account_status == \Model\User::AS_SUSPENDED){
			$suspend_or_not = \Form::button('unsuspend', __('usr_unsuspend'),
				array('type' => 'button', 'class' => 'button unsuspend_'.$type, 'name' => 'unsuspend', 'id' => $id, 'data-id' => $id, 'data-type' => $type));
		}elseif($account_status == \Model\User::AS_VALID){
			$suspend_or_not = \Form::button('suspend', __('usr_suspend'),
				array('type' => 'button', 'class' => 'button suspend_account_'.$type, 'id' => $id, 'data-id' => $id, 'data-type' => $type));
		}elseif($account_status == \Model\User::AS_PENDING_VERIFY){
			$suspend_or_not = \Form::button('resend', __('usr_resend'),
				array('type' => 'button', 'id' => $id, 'class' => 'button resend_activation resend_activation_'.$type, 'id' => $id, 'data-id' => $id, 'data-type' => $type));
		} else {
			$suspend_or_not = '';
		}
		return $suspend_or_not;
	}

	/**
	* Generate admin header menu options
	*
	* @return array Admin menu options
	*/
	public function admin_options(){
		\Lang::load('common');
		$select = array(
			'' => __('admin_opt'),
			'user' => __('adm_users'),
			'broker' => __('adm_broker'),
			'vendor' => __('adm_advertiser'),
			'setting' => __('adm_setting'),
			'promo' => __('adm_promo'),
			'agent' => __('adm_p2'),
			'log/email' => __('adm_emaillog'),
			'log/login' => __('adm_login_log')
		);
		return $select;
	}

	/**
	* Load realtor record from given realtor ID
	*
	* @param Array $data Realtor data
	* @return array A realtor record
	*/
	public function load_realtor($data){
		\Lang::load('common');
		$row = array_merge($data, array());
		$area_name = \CityState::get_area_name($row['city'], $row['prov_state'], $row['country'], true);
		$row['city'] = $area_name[0];
		$row['prov_state'] = $area_name[1];
		$row['country'] = $area_name[2];
		$GRPPDFields = array(
			'profileid' => $row['paypal_sub_id']
		);
		$PayPalRequestData = array('GRPPDFields'=>$GRPPDFields);
		\Package::load('paypal');
		$pp = new \PayPal();
		$PayPalResult = $pp->GetRecurringPaymentsProfileDetails($PayPalRequestData);
		if(isset($PayPalResult['NEXTBILLINGDATE']) && $PayPalResult['NEXTBILLINGDATE'] != ''){
			$row['renewal'] = date(DATETIME_FORMAT, strtotime($PayPalResult['NEXTBILLINGDATE']));
		}else{
			$row['renewal'] = __('addr_notlisted');
		}
		$row['office'] = \Model\Broker::forge()->load(\DB::select_array()->where('id', '=', $row['office_id']))->as_array();
		$row['suspend_or_not'] = $this->suspend_or_not($row['account_status'], $row['realtor_id'], 'realtor');
		if($row['alerts'] == \Model\User::ALERT_NONE){
			$row['alerts'] = __('no_alert');
		}elseif($row['alerts'] == \Model\User::ALERT_WEEKLY){
			$row['alerts'] = __('weekly_alert');
		}elseif($row['alerts'] == \Model\User::ALERT_DAILY){
			$row['alerts'] = __('daily_alert');
		}
		return $row;
	}

	/**
	* Load non imported listing from given realtor id
	*
	* @param String $realtor_id Realtor ID
	* @return array Sets of listing record
	*/
	public function load_exclusive_listing($realtor_id){
		$data = array();
		$result = \DB::select_array()->from('client_has')
		->where('realtor_id', '=', $realtor_id)->where('imported', '=', \Model\Listing::NON_IMPORTED)
		->where('listing_status', 'IN', array(0,1,2,3))->execute();
		foreach($result as $row){
			if($row['type_of_listing'] == \Model\Listing::TYPE_BUY_ONLY){
				$row['address'] = __('addr_buyer_only');
				$row['xready'] = 'N/A';
			} else {
				$row['address'] = $row['address'] .' - '.\CityState::get_area_name($row['city'], $row['prov_state'], $row['country']);
				$row['xready'] = ($row['completed_section'] == 1) ? 'Yes' : 'No';
			}
			if ($row['listing_status'] == \Model\Listing::LS_ACTIVE) {
				$row['listing'] = \Html::anchor('listing/detail/0_'.$row['listing_id'], $row['listing_id']);
			} else {
				$row['listing'] = $row['listing_id'];
			}
			$row['status'] = \Model\Listing::forge()->get_status($row['listing_status']);
			$data[] = $row;
		}
		return $data;
	}

	/**
	* Load all agent listing from property agent table
	*
	* @param String $realtor_id Realtor ID
	* @return Array Sets of listing
	*/
	public function load_downloaded_listing($realtor_id){
		$data = array();
		$result = \DB::select_array(array('*', array('users.id', 'userid'), 
			array('client_has.address', 'l_address'), 
			array('client_has.prov_state', 'l_prov_state'),
			array('client_has.city', 'l_city'),
			array('client_has.country', 'l_country')))->from('client_has')->where('client_has.realtor_id', '=', $realtor_id)
		->where('imported', '=', \Model\Listing::IMPORTED)
		->join('users', 'INNER')->on('users.realtor_id', '=', 'client_has.realtor_id')
		->execute()->as_array();
		foreach($result as $row){
			$row['address'] = $row['l_address'] .' - '.\CityState::get_area_name($row['l_city'], $row['l_prov_state'], $row['l_country']);
			$row['xready'] = ($row['completed_section'] == 1) ? 'Yes' : 'No';
			if ($row['listing_status'] == \Model\Listing::LS_ACTIVE) {
				$row['listing'] = \Html::anchor('listing/detail/0_'.$row['listing_id'], $row['listing_id']);
			} else {
				$row['listing'] = $row['listing_id'];
			}
			$row['status'] = \Model\Listing::forge()->get_status($row['listing_status']);
			$data[] = $row;
		}
		return $data;
	}

	/**
	* Load all agent listing from property agent table
	*
	* @param String $realtor_id Realtor ID
	* @return Array Sets of listing
	*/
	public function load_ready_listing($realtor_id){
		$data = array();
		$result = \DB::select_array()->from('client_has')->where('realtor_id', '=', $realtor_id)
		->where('type_of_listing', '<>', \Model\Listing::TYPE_BUY_ONLY)
		->where('listing_status', '=', \Model\Listing::LS_ACTIVE)
		->where('completed_section', '=', 1)->execute()->as_array();
		foreach($result as $row){
			$row['address'] = $row['address'] .' - '.\CityState::get_area_name($row['city'], $row['prov_state'], $row['country']);
			if ($row['listing_status'] == \Model\Listing::LS_ACTIVE) {
				$row['listing'] = \Html::anchor('listing/detail/0_'.$row['listing_id'], $row['listing_id']);
			} else {
				$row['listing'] = $row['listing_id'];
			}
			$data[] = $row;
		}
		return $data;
	}

	/**
	* Load broker renewal date
	* @param String $paypal_sub_id Broker paypal sub id
	* @return Array A broker record
	*/
	public function load_broker_renewal_date($paypal_sub_id){
		if ($paypal_sub_id == '') {
			return __('addr_notlisted');
		}
		$agreement = new \Model\Paypalrest\Agreement();
		$detail = $agreement->detail($paypal_sub_id);
		if ($detail['state'] == $agreement->get_status(\Model\Paypalrest\Agreement::STATE_ACTIVE) && $detail['next_date'] != '') {
			$renewal = date_create($detail['next_date'])->format(DATE_FORMAT);
		} else {
			$renewal = __('addr_notlisted');
		}
		return $renewal;
	}

	/**
	* Load single vendor data
	*
	* @param String $paypal_sub_id Vendor paypal subscription id
	* @return string renewal date
	*/
	public function load_vendor_renewal_date($paypal_sub_id){
		if ($paypal_sub_id == '') {
			return __('addr_notlisted');
		}
		$agreement = new \Model\Paypalrest\Agreement();
		$detail = $agreement->detail($paypal_sub_id);
		if ($detail['state'] == $agreement->get_status(\Model\Paypalrest\Agreement::STATE_ACTIVE) && $detail['next_date'] != '') {
			$renewal = date_create($detail['next_date'])->format(DATE_FORMAT);
		} else {
			$renewal = __('addr_notlisted');
		}
		return $renewal;
	}

	public function get_listing_by_crea_realtor($realtor_id){
		$data = $tmp = array();
		if (!empty($realtor_id)) {
			$sql = "SELECT crea_agents.id, crea_id, officename, name, COUNT(prop_id) AS listings
			FROM crea_agents
			JOIN crea_property_agents ON crea_property_agents.agent_id = crea_agents.id
			WHERE officeid = :id
			GROUP BY crea_agents.id
			ORDER BY name";

			$results = \DB::query($sql, \DB::SELECT)->param(':id', $realtor_id)->execute();
			foreach($results as $row){
				$tmp['office_id'] = $row['officename'];
				$tmp['realtor_id'] = $row['crea_id'];
				$tmp['name'] = $row['name'];
				$tmp['listings'] = $row['listings'];
				$data[] = $tmp;
			}
		}
		return $data;
	}


	/**
	* Load all email log
	*
	* @return array email log records
	*/
	public function email_log($params){
		$dtCols = array('date', 'email_to', 'email_from', 'subject', 'cc', 'bcc');

		$subsql = '(SELECT `id`, date, DATE_FORMAT(date, \'%M %d %Y %h:%i %p\') as date_name, `email_to`, `email_from`, `subject`, `cc`, `bcc` FROM `email_log` ) as tmp';
		$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM ".$subsql;

		if (isset($params['search']['value']) && $params['search']['value'] != ''){
			$sql .= ' WHERE ';
			$sql .= 'email_from LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ' OR email_to LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ' OR subject LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ' OR cc LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ' OR bcc LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ' OR date_name LIKE \'%'.$params['search']['value'].'%\'';
		}

		if (isset($params['order']) && ((int)$params['order'] > 0)) {
			$sql .= ' ORDER BY '.$dtCols[$params['order'][0]['column']].' '.$params['order'][0]['dir'];
		} else {
			$sql .= ' ORDER BY date desc';
		}

		if (isset($params['start']) && $params['length'] != '-1') {
			$sql .= " LIMIT " . (int)$params['start'] . ", " . (int)$params['length'];
		}

		$all_data = \DB::query($sql,  \DB::SELECT)->execute()->count();
		$data = \DB::query($sql,  \DB::SELECT)->execute()->as_array();

		$dtData = array('data' => array());
		$dtData['draw'] = (isset($params['draw'])) ? (int)$params['draw'] : '1';
		foreach($data as $row){
			$row['action'] = \Html::anchor('/admins/log/delete', __('delete'), array('class' => 'actions no-link delete inline-action log-email-delete', 'data-target' => $row['id']));
			$datas = array(
				$row['date_name'],
				$row['email_to'],
				$row['email_from'],
				$row['subject'],
				$row['cc'],
				$row['bcc'],
				$row['action']
			);
			$dtData['data'][] = array_values($datas);
		}
		$dtData['recordsTotal'] = $all_data;
		$dtData['recordsFiltered'] = $all_data;
		return $dtData;
	}


	/**
	* Load all login log
	*
	* @return array login log records
	*/
	public function login_log($params){
		$cols = array('date', 'user_id', 'user_name', 'ip', 'message');
		$subsql = '(SELECT `id`, `date`, DATE_FORMAT(`date`, \'%M %d %Y %h:%i %p\') as date_name, `user_id`,`user_name`,`ip`,`message` FROM login_log) as tmp';
		$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM ".$subsql;

		if (isset($params['search']['value']) && $params['search']['value'] != ''){
			$sql .= ' WHERE ';
			$sql .= 'user_id LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ' OR user_name LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ' OR ip LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ' OR message LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ' OR date_name LIKE \'%'.$params['search']['value'].'%\'';
		}

		if (isset($params['order']) && ((int)$params['order'] > 0)) {
			$sql .= ' ORDER BY '.$cols[$params['order'][0]['column']].' '.$params['order'][0]['dir'];
		} else {
			$sql .= ' ORDER BY date desc';
		}

		if (isset($params['start']) && $params['length'] != '-1') {
			$sql .= " LIMIT " . (int)$params['start'] . ", " . (int)$params['length'];
		}
		$all_data = \DB::query($sql,  \DB::SELECT)->execute()->count();
		$data = \DB::query($sql,  \DB::SELECT)->execute()->as_array();
		$dtData = array();
		$dtData['draw'] = (isset($params['draw'])) ? (int)$params['draw'] : '1';
		foreach($data as $row){
			$row['action'] = \Html::anchor('/admins/log/delete', __('delete'), array('class' => 'actions no-link delete inline-action log-login-delete', 'data-target' => $row['id']));
			$draw = array(
				$row['date_name'],
				$row['user_id'],
				$row['user_name'],
				$row['ip'],
				$row['message'],
				$row['action']
			);
			$row['date'] = date_create($row['date'])->format(DATETIME_FORMAT);
			$dtData['data'][] = array_values($draw);
		}
		$dtData['recordsTotal'] = $all_data;
		$dtData['recordsFiltered'] = $all_data;
		return $dtData;
	}

	/**
	* override : Set the temporary password and set the needs password flag
	*
	* @param string $code - the reset code
	*/
	public function reset_password($code) {
		$this->load(\DB::select_array()->where('pass_reset', '=', $code));
		if ($this->loaded()) {
			$this->pass_reset = '';
			$this->logged_in = 1;
			$this->ip_address = \Input::ip();
			parent::save();
			// Forced login to allow setting new password
			\Authlite::instance('auth_' . $this->_class)->force_login($this->email);
			\Session::set('reset_password', true);
			return true;
		}
		return false;
	}

	/**
	* override
	* Verify and update the password
	*
	* @param array $post
	*/
	public function update_password($post) {
		\Lang::load('signup');
		$data = array('status' => 'OK', 'message' => __('password_changed'));
		$post = array_map('trim', $post);
		$validation = \Validation::forge();
		$validation->add_field('password', __('this_pass'), 'required|min_length[5]');
		$validation->add_field('cpassword', __('this_pass'), 'match_field[password]');
		if ($validation->run($post)) {
			$fieldset = $validation->fieldset();
			$this->password = \Authlite::instance('auth_' . $this->_class)->hash($post['password']);
			if(parent::save()){
				\Session::set('reset_password', null);
			}
		} else {
			$data['status'] = 'FAILED';
			$data['message'] = $validation->show_errors();
		}
		return $data;
	}

	/**
	* allow listing with following listing status bypass the validation rule for each client/* model
	* @return array array of listing status code
	*/
	public function allow_listing_incomplete_status(){
		$data = array(
			\Model\Listing::LS_HIDDEN_PUBLIC,
			\Model\Listing::LS_DELETED,
			\Model\Listing::LS_REMOVED_EXACTIVE
		);
		return $data;
	}
	public function office_info() {
		$data = array();
		$sql = "SELECT officeid,officename,officeaddress,officecity,officephone, COUNT(officeid) AS agents
		FROM crea_agents GROUP BY officeid ORDER BY officecity";
		$result = \DB::query($sql, \DB::SELECT)->execute();

		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: private",false);
		header("Content-Type: application/octet-stream");
		header("Content-Disposition: attachment; filename=\"office.csv\";" );
		header("Content-Transfer-Encoding: binary");

		foreach($result as $row) {
			$sql = "SELECT COUNT(prop_id) AS cnt FROM crea_agents LEFT JOIN crea_property_agents ON agent_id = id
			WHERE officeid = '" . $row['officeid'] . "'";
			$row['listings'] = \DB::query($sql, \DB::SELECT)->execute()->get('cnt');
			$data[] = $row;
			echo '"' . $row['officeid'] . '",';
			echo '"' . $row['officename'] . '",';
			echo '"' . $row['officeaddress'] . '",';
			echo '"' . $row['officecity'] . '",';
			echo '"' . $row['officephone'] . '",';
			echo '"' . $row['agents'] . '",';
			echo '"' . $row['listings'] . '"' . "\n";
		}
		exit;
		return $data;
	}

	public function create_agent_csv($inputs){
		$data = \Model\Crea\Agent::forge()->get_brokers($inputs, true);
		$csv = array();
		foreach($data['data'] as $item) {
			unset($item[4]);
			unset($item[5]);
			$tmp = array();
			$agents = $this->get_listing_by_crea_realtor($item[0]);
			foreach($agents as $agent){
				$tmp = $item;
				$tmp[] = $agent['realtor_id'];
				$tmp[] = $agent['name'];
				$csv[] = $tmp;
			}
		}
		return $csv;
	}
	
	public function import_listing($data) {
		//remove related data in client_*
		$has = \Model\Client\Has::forge()->load_by_external_id($data['external_id']);
		\DB::delete('client_has')->where('external_id', '=', $data['external_id'])->execute();
		\DB::delete('client_information')->where('listing_id', '=', $has->listing_id)->execute();
		\DB::delete('client_wants')->where('listing_id', '=', $has->listing_id)->execute();
		\Model\Crea\Import::do_import($data['user_id'], true, $data['external_id']);
		return $data;
	}
}