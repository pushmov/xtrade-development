<?php
namespace Model;

class Broker extends User {
	const ID_PREFIX = 'bxTH';
	protected $_class = 'broker';
	protected $_table_name = 'brokers';
	protected $_data = array('id' => NULL, 'broker_id' => '', 'nrb_id' => '',
		'first_name' => '', 'last_name' => '', 'password' => '', 'temp_pass' => '', 'email' => '',
		'phone' => '', 'cell' => '', 'toll_free' => '', 'address' => '', 'county' => '', 'city' => '',
		'prov_state' => '', 'z_p_code' => '', 'country' => '', 'company_name' => '', 'web_site' => '',
		'number_agents' => 0, 'plan_id' => 0, 'last_login' => '', 'logged_in' => '', 'session_id' => '',
		'account_creation' => '', 'activate' => '', 'pass_reset' => '', 'needs_new_pass' => '',
		'ip_address' => '', 'account_type' => '', 'paypal_sub_id' => '', 'is_notified' => '', 'alerts' => '',
		'location' => '', 'remember' => '', 'browser' => '', 'last_active' => '', 'email_type' => '',
		'promotional_end' => '', 'paypal_plan' => '', 'account_status' => '',);

	// We dont want to check every time there is a save
	protected $_valrules =	array(
		'first_name' => array('first', 'required'),
		'last_name' => array('last', 'required'),
		'company_name' => array('company', 'required'),
		//		'address' => array('address', 'required'),
		//		'city' => array('city', 'required'),
		//		'prov_state' => array('prov_state', 'required'),
		//		'z_p_code' => array('postal', 'required'),
		'phone' => array('phone', 'required|valid_phone'),
		'cell' => array('cell', 'valid_phone'),
		'web_site' => array('site', 'valid_domain'),
		'email' => array('email', 'required|valid_email'));

	protected $_agent_qty = array(0 => '5-10', '10-20', '20-30','30-40','40-50','50-60','60-70','70-80','80-90','90-100','100+');

	public function load_by_id($id) {
		$this->load(\DB::select_array()->where('broker_id', '=', $id));
		return $this;
	}

	/**
	* Validate the fields from new signup
	*
	* @param array $post
	*/
	public function register($post) {
		$data = array_map('trim', $post['data']);
		unset($post['data']);
		$data2 = array_map('trim', $post);

		$validation = \Validation::forge();
		$validation->add_callable(new \Model\Broker);
		$validation->add_callable(new \Rules);

		// Common rules
		foreach($this->_valrules as $field => $rules) {
			$validation->add_field($field, __($rules[0]), $rules[1]);
		}
		$validation->field('email')->add_rule('unique_email');
		$validation->add_field('crea_id', __('crea_in_use'), 'unique_nrb');
		$validation->add_field('cemail', __('this_email'), 'match_field[email]');
		$validation->add_field('password', __('this_pass'), 'required|min_length[5]');
		$validation->add_field('cpassword', __('this_pass'), 'match_field[password]');
		if ($validation->run(array_merge($data, $data2))) {
			// Set based on filled field
			$data['nrb_id'] = ($data['crea_id'] == '') ? $data['nrds_id'] : $data['crea_id'];
			$data['promotional_end'] = date_create()->format('Y-m-d');
			//  TODO: Set for pilot testing only
			\Cookie::set('pilot_country', (($data['crea_id'] == '') ? \CityState::US : \CityState::CA));
			return array_merge($data, $data2);
		} else {
			throw new \AutomodelerException('error', array(), $validation->error_message());
		}
	}

	public function save($validation = NULL) {
		// Do some fomatting before saving
		$this->phone = preg_replace('/\D+/', '', $this->phone);
		$this->cell = preg_replace('/\D+/', '', $this->cell);
		$this->toll_free = preg_replace('/\D+/', '', $this->toll_free);
		$this->email = strtolower($this->email);
		$this->web_site = $this->format_url($this->web_site);
		$this->nrb_id = ($this->nrb_id == '') ? NULL : $this->nrb_id;
		return parent::save($validation);
	}

	public function get_agent_qty($id = null) {
		if ($id === null) {
			return $this->_agent_qty;
		}
		if (isset($this->_agent_qty[$id])) {
			return $this->_agent_qty[$id];
		}
	}
	/**
	* Returns data used for the profile page
	*
	*/
	public function profile() {
		$data = parent::profile();
		$data['toll_free'] = \Num::format_phone($data['toll_free']);
		$data['history'] = '';
		$extra['member_id'] = $this->broker_id;
		$extra['info'] = __('profile_reminder');
		$extra['listing_btn'] = '';
		unset($data['last_payment_amt']);
		unset($data['last_payment_date']);
		unset($data['last_payment']);
		return array_merge($data, $extra);
	}

	public function doupdate($post) {
		$extra['cemail'] = $post['cemail'];
		$validation = \Validation::forge();
		$validation->add_callable(new \Model\Realtor);
		$validation->add_callable('rules');
		// Common rules
		foreach($this->_valrules as $field => $rules) {
			$validation->add_field($field, $rules[0], $rules[1]);
		}
		$validation->add_field('cemail', __('this_email'), 'match_field[email]');

		$post = array_map('trim', $post['data']);
		// Unique validation
		if ($post['email'] != $this->email) {
			$validation->field('email')->add_rule('unique_email');
		}
		if (($post['nrb_id'] != '') && ($post['nrb_id'] != $this->nrb_id)) {
			$validation->add_field('nrb_id', __('crea_in_use'), 'unique_crea');
		}
		$this->set_fields($post);
		if ($validation->run(array_merge($this->as_array(), $extra))) {
			// Update session info with updated data
			\Authlite::instance('auth_broker')->force_login($this->email);
			return $this->save();
		} else {
			throw new \AutomodelerException('error', array(), $validation->error_message());
		}
	}

	/**
	* Load all brokers record
	* @return Array Brokers
	*/
	public function load_brokers($params){
		$data['data'] = $drow = array();
		$data['draw'] = (isset($params['draw'])) ? (int)$params['draw'] : '1';
		$aColumns = array('broker_id', 'company_name', 'name', 'city', 'state', 'status');
		$statuses = $this->get_status();
		$sqlStatus = '';
		foreach ($statuses as $key => $status) {
			$sqlStatus .= ' WHEN account_status = \''.$key.'\' THEN \''.$status.'\'';
		}
		$subsql = "(SELECT brokers.id, broker_id, company_name, CONCAT(first_name, ' ', last_name) as name,
		(CASE brokers.country WHEN '1' THEN ca_cities.name ELSE us_cities.name END) AS city,
		(CASE brokers.country WHEN '1' THEN ca_provinces.prov_abb ELSE us_states.state_code END) AS state,
		(CASE ".$sqlStatus." END) AS status
		FROM brokers
		LEFT JOIN ca_cities ON ca_cities.id = brokers.city
		LEFT JOIN ca_provinces ON ca_provinces.id = brokers.prov_state
		LEFT JOIN us_cities ON us_cities.id = brokers.city
		LEFT JOIN us_states ON us_states.id = brokers.prov_state) AS tmp ";
		$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM " . $subsql;

		if (isset($params['search']['value']) && $params['search']['value'] != ''){
			$sql .= ' WHERE (';
			$sql .= 'broker_id LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ' OR company_name LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ' OR city LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ' OR state LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ' OR name LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ' OR status LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ')';
		}

		if (isset($params['order']) && ((int)$params['order'] > 0)) {
			$sql .= ' ORDER BY '.$aColumns[$params['order'][0]['column']].' '.$params['order'][0]['dir'];
		} else {
			$sql .= ' ORDER BY id asc';
		}

		if (isset($params['start']) && $params['length'] != '-1') {
			$sql .= " LIMIT " . (int)$params['start'] . ", " . (int)$params['length'];
		}

		$result = \DB::query($sql, \DB::SELECT)->execute();

		foreach ($result as $row) {
			$number_realtors = count(\Model\Realtor::forge()->load(\DB::select_array()->where('office_id', '=', $row['id']), null)->as_array());
			$drow['broker_id'] = \Html::anchor('admins/profile/broker/'.$row['id'], $row['broker_id']);
			$drow['company_name'] = $row['company_name'];
			$drow['name'] = $row['name'];
			$drow['city'] = $row['city'];
			$drow['state'] = $row['state'];
			$drow['status'] = $row['status'];
			$drow['realtors'] = \Html::anchor('#', 'Realtors ('.$number_realtors.')', array('class' => 'actions edit inline-action broker-realtors', 'id' => $row['id']));
			$data['data'][] = array_values($drow);
		}

		$total = \DB::select()->from($this->_table_name)->execute()->count();
		$data['recordsTotal'] = $total;
		$data['recordsFiltered'] = $total;
		return $data;
	}

	/**
	* Delete broker
	*
	* @param array $id broker_id
	* @param String $type User type [vendor, realtor, broker]
	*/
	public function delete_broker($id){
		$response['status'] = 'error';
		$broker = \Model\Broker::forge()->load(\DB::select()->where('broker_id', '=', $id));
		$delete = \DB::delete($this->_table_name)->where('broker_id', '=', $id)->execute();
		if($delete){
			//remove realtor from this broker
			$array_listing = array();
			$realtors = \Model\Realtor::forge()->load(\DB::select()->where('office_id', '=', $broker->id), null)->as_array();
			foreach($realtors as $row){
				$listings = \Model\Client\Has::forge()->load(\DB::select()->where('realtor_id', '=', $row['realtor_id']), null)->as_array();
				foreach($listings as $listing){
					$array_listing[] = $listing['listing_id'];
				}
				//remove client_info,has,wants of this realtor
				\DB::delete('client_information')->where('realtor_id', '=', $row['realtor_id'])->execute();
				\DB::delete('client_has')->where('realtor_id', '=', $row['realtor_id'])->execute();
				\DB::delete('client_wants')->where('realtor_id', '=', $row['realtor_id'])->execute();
				//remove matches from this listing
				\DB::delete('matches')->where('realtor_id', '=', $row['realtor_id'])->execute();
				//remove orders from realtor
				\DB::delete('orders')->where('homebase_id', '=', $row['realtor_id'])->execute();
			}
			\DB::delete('users')->where('office_id', '=', $broker->id)->execute();
			//remove xtrade_photos from listing
			foreach($array_listing as $listing_id){
				\DB::delete('xtrade_photos')->where('listing_id', '=', $listing_id)->execute();
			}
			$response['status'] = 'ok';
			$response['id'] = $id;
		}
		return $response;
	}

	public function signup($signup) {
		$this->set_fields($signup);
		$this->broker_id = \Sysconfig::create_string('broker');
		$this->account_creation = date_create()->format('Y-m-d H:i:s');
		$this->activate = \Sysconfig::create_string('activate');
		$this->password = \Authlite::instance('auth_broker')->hash($signup['password']);
		$this->country = \Cookie::get('pilot_country', \CityState::CA);
		\Cookie::delete('pilot_country');
		$id = $this->save();
		//realtor invitation process
		if(isset($signup['realtor_id'])){
			$realtor = \Model\Realtor::forge($signup['realtor_id']);
			if($signup['rref'] == 'true' && $realtor->loaded()){
				$realtor->office_id = $id;
				$realtor->save();
			}
		}
		\Emails::forge()->activate($this->as_array(), 'broker');
	}

	public function suspend_user($id, $type){
		parent::suspend_user($id, $type);
		$response['status'] = 'error';
		$response['id'] = $id;
		$this->change_agreement('default');
		$response['status'] = 'ok';
		$response['id'] = $this->id;
		return $response;
	}

	public function unsuspend_user($id, $type){
		parent::unsuspend_user($id, $type);
		$response['status'] = 'error';
		$response['id'] = $id;
		$this->change_agreement('broker');
		return $response;
	}
}