<?php
namespace Model\Broker;

class Payment extends \Model\Payment {
	/**
	* Process the signup payment
	*
	* @param array $post - data from the payment details form
	* @param array $signup - data from the signup form
	*/
	public function signup($post, $signup) {
		\Lang::load('common');
		$post = array_map('trim', $post);
		list($post['city'], $post['prov_state'], $post['country']) = \CityState::get_area_id($post['cpc_address'], true);
		// If the city is not set then clear the address for validation
		if (empty($post['city'])) {
			$post['cpc_address'] = '';
		}
		$this->_set_validation();
		if($this->_validation->run($post) === false){
			return array('status' => 'ERROR', 'message' => $this->_validation->show_errors());
		}

		$signup['broker_id'] = \Sysconfig::create_string($post['user_type']);
		$signup['address'] = $post['address'];
		$signup['z_p_code'] = $post['z_p_code'];
		$signup['account_creation'] = time();
//		$signup['user_name'] = $signup['email'];
		$signup['activate'] = \Sysconfig::create_string('activate');

		// Set for gateway
		$post = array_merge($post, $signup);
		$post['currency'] = \Config::get('currency.' . $post['country']);

		// Get the names for gateway
		list($post['city'], $post['prov_state'], $post['country']) = explode(', ', $post['cpc_address']);
		$post['start_date'] = date_create(null, new \DateTimeZone('UTC'))->format('Y-m-d\TH:i:s\Z');
		$post['user_xid'] = $signup['broker_id'];
		$post['trans_id'] = 'broker_monthly';
		$post['promo_rate'] = 0;
		$post['promo_duration'] = 0;
		$post['promo_fine_print'] = '';
		// Get valid promo details
		$paypal = new \Model\Gateway;
		$promo = \Model\Promocode::forge()->load_valid($post['promo_code']);
		if ($promo->loaded()) {
			$post['promo_duration'] = $promo->duration;
			$post['promo_fine_print'] = $promo->fine_print;
			if ($promo->rate_type == \Model\Promocode::RT_FLAT) {
				$post['promo_rate'] = $promo->rate;
			} else {
				$post['promo_rate'] = $post['fee'] * $promo->rate;
			}
			$paypal->trial = TRUE;
		}

		$paypal->set_data($post);
		$paypal_result = $paypal->create_recurring();

		if($paypal_result['ACK'] == 'Success') {
			$broker = new \Model\Broker();
			$signup['ip_address'] = \Input::ip();
			$signup['paypal_sub_id'] = $paypal_result['PROFILEID'];
			$signup['paypal_status'] = ($paypal_result['PROFILESTATUS'] == 'ActiveProfile') ? parent::PP_ACTIVE : parent::PP_SUSPEND;
			if ($post['promo_duration'] != \Model\Promocode::PROMO_AS_EXPIRED) {
				$signup['promotional_end'] = date_create('+' . $post['promo_duration'] . ' months')->format('Y-m-d');
			}
			$signup['account_status'] = \Model\User::AS_PENDING_VERIFY;
			$signup['account_creation'] = date_create()->format('Y-m-d H:i:s');
			$signup['number_agents'] = \Model\Broker::forge()->get_agent_qty($signup['number_agents']);
			$broker->set_fields($signup);
			$broker->password = \Authlite::instance('auth_broker')->hash($broker->password);
			$broker->save();

			//todo : fix TRANSACTIONID is not returned by sandbox paypal api
			$transaction_id = isset($paypal_result['TRANSACTIONID']) ? $paypal_result['TRANSACTIONID'] : '9999';
			$order = \Model\Order::forge();
			$order_fields = array(
				'user_id' => $broker->id,
				'user_type_id' => \Model\User::UT_BROKER,
				'type_id' => \Model\Order::OT_SIGNUP,
				'homebase_id' => $broker->broker_id,
				'subscriber_id' => $broker->paypal_sub_id,
				'order_date' => date_create()->format('Y-m-d H:i:s'),
				'total' => $post['fee'],
				'sub_total' => $post['fee'] + $post['tax'],
				'taxes' => $post['tax'],
				'tax_code' => $post['tax_code'],
				'trans_id' => $transaction_id,
				'status_id' => \Model\Order::OS_PENDING,
				'receipt_id' => \Sysconfig::create_string('salt')
			);
			$order->set_fields($order_fields);
			$order->save();
			$user = \Model\Broker::forge()->load(\DB::select_array()->where('id', '=', $broker->id))->as_array();
			$user['receipt_id'] = \Sysconfig::create_string('salt');
			$email_data = array_merge($user, $post);
			\Emails::forge()->welcome($email_data, $signup['cpassword'], 'broker');

			/* realtor refferal id process */
			if($signup['rref'] == 'true' && $realtor = \Model\Realtor::forge($signup['realtor_id'])){
				$realtor->office_id = $broker->id;
				$realtor->pending_broker = \Model\Broker::AS_VALID;
				$realtor->company_name = $broker->company_name;
				$realtor->save();
			}

			$response = array('status' => 'OK', 'Conf_Num' => $paypal_result['PROFILEID']);

		} elseif ($paypal_result['ACK'] == 'Failure' || $paypal_result['ACK'] == '') {
			$message = '<ul>';
			foreach($paypal_result['ERRORS'] as $v) {
				// Don't need this prefix
				$message .= '<li>' . str_replace(__('transaction_failed'),'',$v['L_LONGMESSAGE']) . '</li>';
			}
			$message .= '</ul>';
			$response = array('status' => 'ERROR', 'message' => $message);
		}

		return $response;
	}

}