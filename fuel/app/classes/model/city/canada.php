<?php

namespace Model\City;

class Canada extends \Automodeler{
  
  
	protected $_class = 'canada';
	protected $_table_name = 'ca_cities';
	protected $_data =	array( 'id' => NULL, 'province_id' => '', 'city_name' => '', 'alias' => '', 'lat' => '', 'lng' => '');
  
  
}