<?php
namespace Model\Client;

class Information extends \Model\Client {
	const ID_PREFIX = 'xTH';
	protected $_class = 'information';
	protected $_table_name = 'client_information';

	protected $_data = array('id' => NULL, 'listing_id' => '', 'realtor_id' => '', 'first_name' => '', 'last_name' => '',
		'email' => '', 'phone' => '', 'cell' => '', 'country' => '', 'prov_state' => '', 'address' => '', 'city' => '',
		'county' => '', 'z_p_code' => '', 'street_name' => '', 'street_number' => '', 'suite' => '', 'completed_section' => 0) ;

	//default validation rules
	protected $_valrules = array(
		'email' => array('email', 'valid_email'),
		'phone' => array('phone', 'valid_phone')
	);
	
	protected $_incomplete_values = array();

	public function save($validation=NULL){
		$this->phone = preg_replace('/[^0-9.]*/', '', $this->phone);
		$this->cell = preg_replace("/[^0-9.]*/", "", $this->cell);
		return parent::save($validation);
	}

	public function update_client_info($data) {
		$validation = \Validation::forge($this->_class);
		$validation->add_callable(new \Model\User());
		$this->set_fields(array_map('trim', $data));
		$this->completed_section = self::STATUS_COMPLETE;
		$validation->add_callable(new \Model\User);
		foreach($this->_valrules as $field => $rules) {
			$validation->add_field($field, __($rules[0]), $rules[1]);
		}
		if ($validation->run($this->as_array())) {
			return array('status' => $this->save(), 'message' => '', 'completed_section' => $this->completed_section);
		} else {
            //return array('status' => FALSE, 'message' => $validation->show_errors(), 'area' => 'client_info');
			throw new \AutomodelerException('error', array(), $validation->error_message());
		}
	}

}