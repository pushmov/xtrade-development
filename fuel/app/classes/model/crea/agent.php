<?php
namespace Model\Crea;

class Agent extends \Automodeler {
	const S_NEW = 0;
	const S_ACTIVE = 1;
	const S_NEW_OFFICE = 5;

	const AGENT_PRIMARY = 0;
	const AGENT_CO = 1;

	protected $_class = 'agent';
	protected $_table_name = 'crea_agents';

	protected $_data = array('id' => NULL,'crea_id' => 0,'name' => '','phone' => '','mobile' => '','fax' => '','officeid' => '',
		'officename' => '','officeaddress' => '', 'officecity' => '', 'officeprov' => '','officepostal' => '',
		'officephone' => '', 'officemobile' => '', 'officefax' => '', 'created_at' => '', 'status_id' => 0);

	protected $_agent = array(self::AGENT_PRIMARY => 'Listing Agent', self::AGENT_CO => 'Co-Listing Agent');


	public function get_table_name(){
		return $this->_table_name;
	}

	public function get_agent_type($id = null){
		if ($id === null) {
			return $this->_agent;
		}
		return array_key_exists($id, $this->_agent) ? $this->_agent[$id] : $this->_agent;
	}

	/**
	* Agent List
	*
	* @return array Set of offices with total listing each
	*/
	public function get_brokers($params, $strip_tag = false){
		$aColumns = array('officeid', 'officename', 'officecity', 'officeprov', 'phone', 'cnt', 'status_id');
		$data['data'] = array();

		$sql = "SELECT officeid,officename,officecity,officeprov,officephone,COUNT(prop_id) AS cnt,status_id
		FROM crea_agents
		JOIN crea_property_agents ON crea_property_agents.agent_id = crea_agents.id";

		if (isset($params['search']) && trim($params['search']['value']) != ''){
			$sql .= " WHERE (`officeid` LIKE " . \DB::escape('%'. trim($params['search']['value']) . '%');
			$sql .= ") OR (`officename` LIKE " . \DB::escape('%'. trim($params['search']['value']) . '%');
			$sql .= ") OR (`officecity` LIKE " . \DB::escape('%'. trim($params['search']['value']) . '%');
			$sql .= ") OR (`officeprov` LIKE " . \DB::escape('%'. trim($params['search']['value']) . '%');
			$sql .= ")";
		}
		$sql .= ' GROUP BY officeid';

		$data['recordsFiltered'] = \DB::query($sql, \DB::SELECT)->execute()->count();

		if (isset($params['order']) && !empty($params['order'])) {
			$order = array();
			foreach($params['order'] as $o) {
				$order[] = $aColumns[$o['column']] . ' ' . $o['dir'];
			}
			$sql .= ' ORDER BY ' . implode(',', $order);
		} else {
			$sql .= ' ORDER BY officename asc';
		}
		if (isset($params['start']) && $params['length'] != '-1') {
			$sql .= ' LIMIT ' . (int) $params['start'] . ',' . (int) $params['length'];
		}
		$result = \DB::query($sql, \DB::SELECT)->execute();
		foreach ($result as $row) {
			$row['cnt'] = \Html::anchor('#', $row['cnt'], array('class' => 'no-link agent-all-listings', 'office-id' => $row['officeid']));
			$row['status_id'] = ($row['status_id'] == 1) ? 'Active' : 'Inactive';
			if ($strip_tag) {
				$row['officeid'] = $row['officeid'];
			} else {
			$row['officeid'] = \HTML::anchor('#', $row['officeid'], array('title' => $row['officeid'], 'class' => 'no-link office-filter', 'data-id' => $row['officeid']));
			}
			$data['data'][] = array_values($row);
		}

		// Total rows
		$data['recordsTotal'] = \DB::query("SELECT COUNT(id) FROM {$this->_table_name} GROUP BY officeid", \DB::SELECT)->execute()->count();
		return $data;
	}

	public function get_summary() {
		$data = array();
		$data['listings'] = \DB::query("SELECT COUNT(id) AS cnt FROM crea_property", \DB::SELECT)->execute()->get('cnt');
		$result = \DB::query("SELECT agent_id FROM crea_property_agents GROUP BY agent_id", \DB::SELECT)->execute();
		$data['agents'] = $result->count();
		$result = \DB::query("SELECT agent_id FROM crea_agents JOIN crea_property_agents ON crea_agents.id = agent_id GROUP BY officeid", \DB::SELECT)->execute();
		$data['brokers'] = $result->count();
		return $data;
	}

	public function email_blast() {
		$sql = 'select crea_agents.id,crea_agents.name,crea_property.id as listing
		from crea_agents
		join crea_property_agents on agent_id = crea_agents.id
		join crea_property on crea_property.id = prop_id
		group by crea_agents.id';

	}

	public function crea_listings($params) {
		$query = \DB::select('crea_property.listingid', 'crea_property.streetaddress', 'crea_property.city_txt', 'crea_property.province', 'crea_agents.crea_id', 'crea_agents.name', array('crea_property.id', 'pid'))
		->from($this->_table_name)
		->where('crea_agents.officeid', '=', $params['office_id'])
		->join('crea_property_agents', 'INNER')->on('crea_agents.id', '=', 'crea_property_agents.agent_id')
		->join('crea_property', 'INNER')->on('crea_property.id', '=', 'crea_property_agents.prop_id')->execute();
		return $query->as_array();
	}
}