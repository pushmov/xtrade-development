<?php
namespace Model\Crea;

class Import extends \Model {
	public static function do_import($id = null, $reset = false, $external_id = null) {
		if ($id === null) {
			$sql = "SELECT prop_id,agent_id,crea_id,nrb_id
			FROM crea_updated
			JOIN crea_property_agents ON prop_id = crea_updated.id
			JOIN crea_agents ON crea_agents.id = agent_id
			JOIN users ON country = 1 AND nrb_id = crea_id";
		} elseif ($reset === true) {
			$sql = "SELECT prop_id,nrb_id,users.id
			FROM users
			JOIN crea_agents ON crea_agents.crea_id = nrb_id
			JOIN crea_property_agents ON agent_id = crea_agents.id 
			WHERE crea_property_agents.prop_id = '{$external_id}' 
			AND users.country = 1 AND users.id = " . (int)$id;
		} else {
			$sql = "SELECT prop_id,nrb_id,users.id
			FROM users
			JOIN crea_agents ON crea_agents.crea_id = nrb_id
			JOIN crea_property_agents ON agent_id = crea_agents.id
			WHERE country = 1 AND users.id = " . (int)$id;
		}
		$result = \DB::query($sql,\DB::SELECT)->execute();
		foreach($result as $row) {
			$xtrade = \Model\Client\Has::forge()->load_by_external_id($row['prop_id']);
			$prop = \Model\Crea\Property::forge($row['prop_id']);
			$bld = \Model\Crea\Building::forge($row['prop_id']);
			$land = \Model\Crea\Land::forge($row['prop_id']);
			$user = \Model\Realtor::forge()->load_by_id($row['nrb_id'], 'nrb');

			if (!$xtrade->loaded()) {
				$xtrade->listing_id = \Sysconfig::create_string('listing_id');
				$xtrade->date_created = date_create()->format('Y-m-d H:i:s');
				$xtrade->external_id = $row['prop_id'];
				$xtrade->realtor_id = $user['realtor_id'];
				$xtrade->imported = 1;
				$info = new \Model\Client\Information;
				$info->listing_id = $xtrade->listing_id;
				$info->realtor_id = $user['realtor_id'];
				$info->save();
				$info = new \Model\Client\Want;
				$info->listing_id = $xtrade->listing_id;
				$info->realtor_id = $user['realtor_id'];
				$info->save();
			}
			$xtrade->type_of_listing = 0;
			$xtrade->country = $prop->country;
			$xtrade->address = $prop->streetaddress;
			$xtrade->city = $prop->city;
			$xtrade->prov_state = $prop->prov_state;
			$xtrade->z_p_code = $prop->postalcode;
			$xtrade->listing_price = $prop->price;
			$xtrade->bedrooms = $bld->bedroomstotal;
			$xtrade->bathrooms = $bld->bathroomtotal;
			$xtrade->lastupdate = $prop->lastupdated;
			$xtrade->comments = $prop->publicremarks;
			//$xtrade->attachment = $bld->constructionstyleattachment;
			// Only update if the current value is blank
			if (($reset === true) || (($reset == false) && ($xtrade->prop_style == ''))) {
				$options = array("Apartment" => "a","1 Storey, Apartment" => "a","2 Storey, Apartment" => "a","Loft" => "a","Stacked" => "a","1 Storey, Stacked" => "a","Manufactured Home" => "b","1 Storey, Mobile Home" => "b","2 Storey, Semi Detached" => "c","3 Storey, Semi Detached" => "c","3 Storey, Duplex" => "c","2 1/2 Storey, Duplex" => "c","2 Storey, Stacked" => "c","2 Storey, Duplex" => "c","2 Storey, Link" => "c","2 1/2 Storey, Semi Detached" => "c","1-1/2 Storey, Semi Detached" => "c","3 Storey, Stacked" => "c","2 1/2 Storey" => "d","1 3/4 Storey" => "d","1-1/2 Storey" => "d","2 Storey" => "d","1 1/2 Storey" => "d","3 Storey" => "d","2 Storey, Detached " => "d","1-1/2 Storey, Detached" => "d","3 Storey, Detached" => "d","2 1/2 Storey, Detached" => "d","2 Storey, Cottage/Recreational" => "d","1-1/2 Storey, Loft" => "d","2 Storey, Loft" => "d","2 1/2 Storey, Loft" => "d","2 Storey" => "d","1-1/2 Storey, Cottage/Recreational" => "d","Multiplex" => "e","Triplex" => "e","Fourplex" => "e","2 1/2 Storey, Triplex" => "e","Duplex" => "f","1 Storey, Semi Detached" => "f","1 Storey, Link" => "f","Raised-Elevtd Ranch, Link" => "f","Bungalow" => "j","Ranch" => "j","Bi-Level" => "j","1 Storey, Detached" => "j","1 Storey, Cottage/Recreational" => "j","Raised-Elevtd Ranch, Detached" => "j","Detached" => "j","1 Storey, Loft" => "j","Single Storey" => "j","Raised-Elevtd Ranch, Semi Detached" => "j","4 Level Back Split, Detached" => "g","4 Level Back Split, Semi Detached" => "g","4 Level Back Split, Link" => "g","2 Storey Split" => "h","4-Level Split" => "h","3-Level Split" => "h","5-Level Split" => "h","4 Level Side Split, Detached" => "h","3 Level Side Split, Detached" => "h","5 Level Side Split, Detached" => "h","5 Level Back Split, Detached" => "h","3 Level Back Split, Detached" => "h","3 Storey, Townhouse Row Unit" => "i","1-1/2 Storey, Townhouse Row Unit" => "i","Townhouse" => "i","2 Storey, Townhouse Row Unit" => "i","3 Level Back Split, Townhouse Row Unit" => "i","1 Storey, Townhouse Row Unit" => "i","2 1/2 Storey, Townhouse Row Unit" => "i","Raised-Elevtd Ranch, Townhouse Row Unit" => "i","Townhouse Row Unit" => "i","4 Level Back Split, Townhouse Row Unit" => "i", "Single Family - House" => "f", "Split level entry" => "h", "2 Level"  => "j", "Ranch" => "j", "Log house/cabin" => "j", "Cottage" => "j", "Ground level entry" => "d", "Contemporary" => "d", "Multi-level" => "c", "Mini" => "b", "Chalet" => "d", "Bi-level, Chalet" => "d", "Raised bungalow" => "f", "Bungalow" => "f", "Raised ranch" => "d", "4 Level" => "d", "3 Level" => "d", "Cape Cod" => "", "Basement entry" => "d", "Bi-level, Contemporary" => "d", "Bi-level, Cape Cod" => "d", "Chalet, Cottage" => "d", "Cathedral entry" => "d", "Penthouse" => "a", "Camp" => "j", "A-Frame" => "j", "Cape Cod, Bi-level" => "d", "4 Level, Chalet" => "d", "Bi-level" => "j");
				// Check these two fields to try to get a prop_style
				if (isset($options[$bld->architecturalstyle])) {
					$xtrade->prop_style = $options[$bld->architecturalstyle];
				} elseif (isset($options[$bld->constructionstyleattachment])) {
					$xtrade->prop_style = $options[$bld->constructionstyleattachment];
				}
			}
			if (($reset === true) || (($reset == false) && ($xtrade->prop_type == ''))) {
				$options = \Model\Listing::forge()->prop_options_nonstyle_import();
				$xtrade->prop_type = (isset($options[$prop->propertytype])) ? $options[$prop->propertytype] :  'a';
			}
			$title_options = array("Freehold" => "d","Condominium/Strata" => "c","Leasehold" => "a","Strata" => "a","Freehold Condo" => "d","Condominium" => "c","Leasehold Condo/Strata" => "c","Shares in Co-operative" => "b","Undivided Co-ownership" => "c","Timeshare/Fractional" => "c","Life Lease" => "a");
			if (isset($title_options[$prop->ownershiptype])) {
				$xtrade->p_title = $title_options[$prop->ownershiptype];
			}
			$xtrade->listing_status = (empty($prop->city)) ? \Model\Listing::LS_NOCITY : \Model\Listing::LS_ACTIVE;
			if (($reset === true) || (($reset == false) && ($xtrade->sq_footage == 0))) {
				// Convert m2 to sqft if required
				if (strpos($bld->sizeinterior, 'm2') === false) {
					$sqft = str_replace(' sqft', '', $bld->sizeinterior);
				} else {
					$sqft = str_replace(' m2', '', $bld->sizeinterior);
					$sqft = $sqft * 10.7639;
				}
				$xtrade->sq_footage = $sqft;
			}
			$xtrade->save();
			$prop->xtrade_imported = 1;
			$prop->save();
			$user->last_import = date_create()->format('Y-m-d H:i:s');
			$user->save();
		}
	}
}