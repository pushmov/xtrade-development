<?php
namespace Model;

class emaillog extends Automodeler {
	protected $_table_name = 'email_log';
	protected $_data = array('id' => NULL, 'date' => '', 'email_from_name' => '', 'email_from' => '', 'email_to_name' => '',
			'email_to' => '', 'cc' => '', 'bcc' => '', 'subject' => '', 'homebase_id' => '',);
  
  public static function track_email($param){
    
    \DB::insert('email_log')
      ->set($param)
      ->execute();
    
  }
}