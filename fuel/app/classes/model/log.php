<?php
namespace Model;

class Log extends \Automodeler {
	public function delete_record(){
		$response['status'] = 'error';
		$result = \DB::delete($this->_table_name)->where('id', '=', $this->id)->execute();
		if($result){
			$response['status'] = 'OK';
		}
		return $response;
	}
}