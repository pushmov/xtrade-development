<?php
namespace Model;

class Matches extends \Automodeler{
	const MATCH_UNSEEN = 0;
	const MATCH_SEEN = 1;

	protected $_table_name = 'matches';
	protected $_data = array('id' => NULL, 'realtor_id' => '', 'listing_id' => '', 'matched_listings' => NULL, 'ignore_listings' => NULL,
		'save_listings' => NULL, 'buyers_match' => NULL, 'seen' => '', 'match_potential' => NULL );

	protected $_rules = array();

	public function set_matches_as_seen($listing_id){
		\DB::update($this->_table_name)->set(array('seen' => self::MATCH_SEEN))->where('listing_id', '=', $listing_id)->execute();
	}

	public function save($v = null) {
		parent::save($v);
	}

	public function check_for_ignore($inc, $listing_id, $desire_list){
		if($inc){
			$check_for_ignore = $this->load(\DB::select_array()->where('listing_id', '=', $listing_id)->where('ignore_listings', '=', $desire_list), NULL)->as_array();
		} else {
			$check_for_ignore = \DB::select_array()->from('matches')
			->where('listing_id', '=', $listing_id)
			->where_open()
			->where('ignore_listings', '=', $desire_list)
			->or_where('save_listings', '=', $desire_list)
			->where_close()->execute();
		}

		return $check_for_ignore;
	}

	public function check_for_desires_in_matches($inc_maybe, $lID, $search_ID){
		if($inc_maybe){
			$result = \DB::select_array()->from('matches')
			->where('listing_id', '=', $lID)
			->where('ignore_listings', '=', $search_ID)->execute();
		} else {
			$result = \DB::select_array(array('id'))->from('matches')
			->where('listing_id', '=', $lID)
			->or_where('listing_id', '=', $lID)
			->where('save_listings', '=', $search_ID)->execute();
		}

		return $result;
	}

	public function check_if_match_exists($listing_id, $round){
		$match_exists = \DB::select_array()->from('matches')
		->where('listing_id', '=', $listing_id)
		->where_open()
		->where('matched_listings', '=', $round)
		->or_where('save_listings', '=', $round)
		->or_where('buyers_match', '=', $round)
		->where_close()->execute();

		return $match_exists;
	}

	public function unseen_matches($listing_id){
		$get_unseen_matches = \DB::select_array()->from('matches')
		->join('client_has')->on('client_has.listing_id', '=', 'matches.listing_id')
		->where('client_has.listing_status', '=', \Model\Listing::LS_ACTIVE)
		->where('matches.listing_id', '=', $listing_id)
		->where('matches.seen', '=', self::MATCH_UNSEEN)
		->where(\DB::expr('matches.ignore_listings IS NULL'))
		->order_by('matches.seen', 'desc')->limit(\Sysconfig::get('max_matches'))->execute();
		return $get_unseen_matches;
	}

	public function seen_matches($listing_id){
		$get_seen_matches = \DB::select_array()->from('matches')
		->join('client_has')->on('client_has.listing_id', '=', 'matches.listing_id')
		->where('client_has.listing_status', '=', \Model\Listing::LS_ACTIVE)
		->where('matches.listing_id', '=', $listing_id)
		->where('matches.seen', '=', self::MATCH_SEEN)
		->where(\DB::expr('matches.ignore_listings IS NULL'))->limit(\Sysconfig::get('max_matches'))->execute();

		return $get_seen_matches;
	}

	public function count_matches($listing_id){
		if (\Model\Listing::forge()->is_ready_for_xtrade($listing_id) === false) {
			return 0;
		}
		$get_unseen_matches = $this->seen_matches($listing_id);
		$get_seen_matches = $this->unseen_matches($listing_id);
		return sizeof($get_unseen_matches) + sizeof($get_seen_matches);
	}

	public function realtor_matches($realtor_id){
		//$sql = "SELECT COUNT(id) AS cnt FROM "
		$matches = \DB::select(\DB::expr('COUNT(*) AS cnt'))->from($this->_table_name)->where('realtor_id', '=', $realtor_id)
		->where(\DB::expr('ignore_listings IS NULL'))->execute();
		return $matches->get('cnt');
	}

	/**
	* this function counting matches item based on max matches in sysconfig
	*/
	public function count_seen_unseen_matches($listing_id){
		$seen = $unseen = 0;
		if (\Model\Listing::forge()->is_ready_for_xtrade($listing_id) === false) {
			$return['seen'] = $seen;
			$return['unseen'] = $unseen;
			return $return;
		}
		$allmatches = \DB::select_array()->from('matches')
		->join('client_has')->on('client_has.listing_id', '=', 'matches.listing_id')
		->where('client_has.listing_status', '=', \Model\Listing::LS_ACTIVE)
		->where('matches.listing_id', '=', $listing_id)
		->where(\DB::expr('matches.ignore_listings IS NULL'))
		->order_by('matches.seen', 'desc')->execute();

		foreach($allmatches as $row){
			if($row['seen'] == self::MATCH_SEEN) {
				$seen++;
			} else {
				$unseen++;
			}
		}
		$return['seen'] = $seen;
		$return['unseen'] = $unseen;
		return $return;
	}

	public function insert_matches($search_id, $listing_id, $cron, $match_pot, $realtor_id = 0){
		$check_for_ignore = \DB::select_array()->from('matches')
		->where('listing_id', '=', $listing_id)
		->where_open()
		->where('ignore_listings', '=', $search_id)
		->or_where('save_listings', '=', $search_id)
		->where_close()->execute();

		if($check_for_ignore['id'] != ''){
			$check_if_match_exists = \DB::select_array()->from('matches')
			->where('listing_id', '=', $listing_id)
			->where_open()
			->where('matched_listings', '=', $search_id)
			->or_where('save_listings', '=', $search_id)
			->or_where('buyers_match', '=', $search_id)
			->where_close()->execute();

			if($check_if_match_exists['id'] == ''){
				$this->load();
				$this->set_fields(array(
					'realtor_id' => $realtor_id,
					'listing_id' => $listing_id,
					'matched_listings' => $search_id,
					'seen' => $cron,
					'match_potential' => $match_pot
				));
				$this->save();

			}elseif($check_if_match_exists['seen'] == 1){
				$this->load(\DB::select_array()->where('listing_id', '=', $listing_id)->where('matched_listings', '=', $search_id));
				$this->set_fields(array(
					'match_potential' => $match_pot
				));
				$this->save();

			}else{

				$this->load(\DB::select_array()->where('listing_id', '=', $listing_id)->where('matched_listings', '=', $search_id));
				$this->set_fields(array(
					'seen' => $cron,
					'match_potential' => $match_pot
				));
				$this->save();

			}
		}
		return true;
	}

	public function ignore_match($post){
		$auth_realtor = \Session::get('auth_realtor');
		$realtor_id = $auth_realtor->realtor_id;
		$response = array('status' => 'error');
		$check_for_match = \DB::select_array()->from('matches')
		->where('listing_id', '=', $post['listing'])
		->where_open()
		->where('matched_listings', '=', $post['this_listing'])
		->where('save_listings', '=', $post['this_listing'])
		->where_close()->execute();
		if(!empty($check_for_match)){
			\DB::delete('matches')->where('listing_id', '=', $post['listing'])
			->where_open()
			->where('matched_listings', '=', $post['this_listing'])
			->or_where('save_listings', '=', $post['this_listing'])
			->or_where('ignore_listings', '=', $post['this_listing'])
			->where_close()
			->execute();
		}
		$fields = array(
			'realtor_id' => $realtor_id,
			'listing_id' => $post['listing'],
			'ignore_listings' => $post['this_listing'],
			'seen' => self::MATCH_SEEN
		);
		$insert = \DB::insert('matches')->set($fields)->execute();
		if($insert){
			$response['status'] = 'ok';
		}
		return $response;
	}

	public function save_later($post){
		$response = array('status' => 'ok');
		$check_for_match = \DB::select_array()->from('matches')
		->where('listing_id', '=', $post['listing'])
		->where('matched_listings', '=', $post['this_listing'])->execute();
		if(!empty($check_for_match)){
			\DB::delete('matches')->where('listing_id', '=', $post['listing'])
			->where_open()
			->where('matched_listings', '=', $post['this_listing'])
			->or_where('save_listings', '=', $post['this_listing'])
			->or_where('ignore_listings', '=', $post['this_listing'])
			->where_close()
			->execute();
		}
		$auth_realtor = \Session::get('auth_realtor');
		$realtor_id = $auth_realtor->realtor_id;
		$fields = array(
			'realtor_id' => $realtor_id,
			'listing_id' => $post['listing'],
			'save_listings' => $post['this_listing'],
			'seen' => \Model\Matches::MATCH_SEEN
		);
		$insert = \DB::insert('matches')->set($fields)->execute();
		if($insert){
			$response['status'] = 'ok';
		}
		return $response;
	}

	public function send_matches($post){
		\Lang::load('common');
		$response = array();
		$listing = \Model\Client\Has::forge()->load(\DB::select_array()->where('listing_id', '=', $post['listing_id']));
		if(!$listing->loaded()){
			$response['status'] = 'ERROR';
			$response['message'] = __('not_found');
			return $response;
		}

		$validation = \Validation::forge();
		$validation->add_field('name', __('client_name'), 'required');
		$validation->add_field('email', __('client_email'), 'required|valid_email');
		if($validation->run($post['data']) === false){
			$response['status'] = 'ERROR';
			$response['message'] = $validation->show_errors();
			return $response;
		}

		$match_class = \Match::forge();
		$matches = $match_class->find_all_way_matches($listing->listing_id);
		$this_listing_box = $match_class->get_current_listing_box($listing->listing_id, $listing->type_of_listing, true);

		$found_matches = 0;
		$buyers_match = $x_1_match_high = $x_1_match_med = $x_1_match_low = $y_1_match_high
		= $y_1_match_low = $y_1_match_med = $z_1_match_high
		= $z_1_match_med = $z_1_match_low = array();

		if($listing->type_of_listing == \Model\Listing::TYPE_BUY_ONLY){
			if(!empty($matches['desirable_all'])){
				foreach($matches['desirable_all'] as $value){
					$buyers_match[] = $match_class->setup_box_information_email($value, 'buyer_match', NULL);
					$found_matches++;
				}
			}
		} else {

			if(!empty($GLOBALS["Want_Each_Other_IDs_exact_high"])){
				foreach($GLOBALS["Want_Each_Other_IDs_exact_high"] as $value){
					$x_1_match_high[] = $match_class->setup_box_information_email($value, 'B', NULL);
					$found_matches++;
				}
			}

			if(!empty($GLOBALS["Want_Each_Other_IDs_exact_med"]) && $found_matches <= 10){
				foreach($GLOBALS["Want_Each_Other_IDs_exact_med"] as $value){
					$x_1_match_med[] = $match_class->setup_box_information_email($value, 'B', NULL);
					$found_matches++;
				}
			}

			if(!empty($GLOBALS["Want_Each_Other_IDs_exact_low"]) && $found_matches <= 10){
				foreach($GLOBALS["Want_Each_Other_IDs_exact_low"] as $value){
					$x_1_match_low[] = $match_class->setup_box_information_email($value, 'B', NULL);
					$found_matches++;
				}
			}

			//high

			if(!empty($GLOBALS["Want_Each_Other_IDs_high_high"]) && $found_matches <= 10){
				foreach($GLOBALS["Want_Each_Other_IDs_high_high"] as $value){
					$y_1_match_high[] = $match_class->setup_box_information_email($value, 'B', NULL);
					$found_matches++;
				}
			}

			if(!empty($GLOBALS["Want_Each_Other_IDs_high_med"]) && $found_matches <= 10){
				foreach($GLOBALS["Want_Each_Other_IDs_high_med"] as $value){
					$y_1_match_med[] = $match_class->setup_box_information_email($value, 'B', NULL);
					$found_matches++;
				}
			}

			if(!empty($GLOBALS["Want_Each_Other_IDs_high_low"]) && $found_matches <= 10){
				foreach($GLOBALS["Want_Each_Other_IDs_high_low"] as $value){
					$y_1_match_low[] = $match_class->setup_box_information_email($value, 'B', NULL);
					$found_matches++;
				}
			}

			//medium

			if(!empty($GLOBALS["Want_Each_Other_IDs_med_high"]) && $found_matches <= 10){
				foreach($GLOBALS["Want_Each_Other_IDs_med_high"] as $value){
					$z_1_match_high[] = $match_class->setup_box_information_email($value, 'B', NULL);
					$found_matches++;
				}
			}

			if(!empty($GLOBALS["Want_Each_Other_IDs_med_med"]) && $found_matches <= 10){
				foreach($GLOBALS["Want_Each_Other_IDs_med_med"] as $value){
					$z_1_match_med[] = $match_class->setup_box_information_email($value, 'B', NULL);
					$found_matches++;
				}
			}


			if(!empty($GLOBALS["Want_Each_Other_IDs_med_low"]) && $found_matches <= 10){
				foreach($GLOBALS["Want_Each_Other_IDs_med_low"] as $value){
					$z_1_match_low[] = $match_class->setup_box_information_email($value, 'B', NULL);
					$found_matches++;
				}
			}
		}

		$buyers_matches_found = '';
		if(sizeof($buyers_match) > 0){
			$view = \View::forge('email/matches_buyer');
			$view->this_listing_box = $this_listing_box;
			$view->buyers_match = $buyers_match;
			$buyers_matches_found = $view;
		}
		//exact

		$direct_matches_found = '';
		if(sizeof($x_1_match_high) > 0){
			$view = \View::forge('email/matches_2way');
			$view->this_listing_box = $this_listing_box;
			$view->data = $x_1_match_high;
			$direct_matches_found = $view;
		}

		if(sizeof($x_1_match_med) > 0){
			$view = \View::forge('email/matches_2way');
			$view->this_listing_box = $this_listing_box;
			$view->data = $x_1_match_med;
			$direct_matches_found = $view;
		}

		if(sizeof($x_1_match_low) > 0){
			$view = \View::forge('email/matches_2way');
			$view->this_listing_box = $this_listing_box;
			$view->data = $x_1_match_low;
			$direct_matches_found = $view;
		}

		//high
		if(sizeof($y_1_match_high) > 0){
			$view = \View::forge('email/matches_2way');
			$view->this_listing_box = $this_listing_box;
			$view->data = $y_1_match_high;
			$direct_matches_found = $view;
		}

		if(sizeof($y_1_match_med) > 0){
			$view = \View::forge('email/matches_2way');
			$view->this_listing_box = $this_listing_box;
			$view->data = $y_1_match_med;
			$direct_matches_found = $view;
		}

		if(sizeof($y_1_match_low) > 0){
			$view = \View::forge('email/matches_2way');
			$view->this_listing_box = $this_listing_box;
			$view->data = $y_1_match_low;
			$direct_matches_found = $view;
		}

		//medium
		if(sizeof($z_1_match_high) > 0){
			$view = \View::forge('email/matches_2way');
			$view->this_listing_box = $this_listing_box;
			$view->data = $z_1_match_high;
			$direct_matches_found = $view;
		}

		if(sizeof($z_1_match_med) > 0){
			$view = \View::forge('email/matches_2way');
			$view->this_listing_box = $this_listing_box;
			$view->data = $z_1_match_med;
			$direct_matches_found = $view;
		}

		if(sizeof($z_1_match_low) > 0){
			$view = \View::forge('email/matches_2way');
			$view->this_listing_box = $this_listing_box;
			$view->data = $z_1_match_low;
			$direct_matches_found = $view;
		}

		$three_way_matches_found = '';
		if($matches['x_2_match']){
			$view = \View::forge('email/matches_3way_template');
			$view->this_listing_box = $this_listing_box;
			$view->x_2_match = $matches['x_2_match'];
			$three_way_matches_found .= $view;
		}

		$four_way_matches_found = '';
		if($matches['x_3_match']){
			$view = \View::forge('email/matches_4way_template');
			$view->this_listing_box = $this_listing_box;
			$view->x_3_match = $matches['x_3_match'];
			$four_way_matches_found = $view;
		}

		$five_way_matches_found = '';
		if($matches['x_4_match']){
			$view = \View::forge('email/matches_5way_template');
			$view->this_listing_box = $this_listing_box;
			$view->x_4_match = $matches['x_4_match'];
			$five_way_matches_found = $view;
		}

		$match_array = array();
		if($matches['found_matches'] > 0){
			$content = '<h2>*** Matches For Listing: ' . $post['listing_id'] . ' ***</h2>';
			$content .= $buyers_matches_found;
			$content .= $direct_matches_found;
			$content .= $three_way_matches_found;
			$content .= $four_way_matches_found;
			$content .= $five_way_matches_found;
			$match_array[$listing->realtor_id][$listing->listing_id] = $content;
		}

		foreach($match_array as $key => $value){
			$realtor = \Model\Realtor::forge()->load(\DB::select_array()->where('realtor_id', '=', $key))->as_array();
			$match_info = '';
			foreach($value as $value_2){
				if($value_2){
					$match_info .= $value_2;
				}
			}

			if($match_info != ''){
				$emaildata = array(
					'rname' => $realtor['first_name'].' '.$realtor['last_name'],
					'phone_number' => \Sysconfig::format_phone_number($realtor['phone']),
					'remail' => $realtor['email'],
					'cname' => $realtor['company_name'],
					'match_info' => $match_info,
					'mailto' => $post['data']['email'],
					'mailtoname' => $post['data']['name']
				);
				$response = \Emails::forge()->send_matches($emaildata);
				if($response['status'] == 'ERROR'){
					break;
				}
			}
		}
		return $response;
	}

	public function load_all_listings($post, $tutorial=false){
		$data['result'] = array();
		if (isset($post['listing_id'])) {
			$listing = \Model\Client\Has::forge()->load(\DB::select_array()->where('listing_id', '=', $post['listing_id']))->as_array();
			$match = \Match::forge();
			$match->listing_id = $post['listing_id'];
			$match->number_of_exact_threshold = 10;
			$match->realtor_id = $listing['realtor_id'];
			$match->run();
		}
		//action ajax listing - START
		$query_builder = \DB::select_array(array('client_has.*',
			'client_information.first_name',
			'client_information.last_name',
			'client_information.email',
			'crea_property.listingid',
			array('client_information.completed_section', 'info_completed_section'),
			array('client_wants.completed_section', 'wants_completed_section'))
		);
		$query_builder->from('client_has');
		$query_builder->join('crea_property', 'LEFT')->on('client_has.external_id', '=', 'crea_property.listingid');
		$query_builder->join('client_information', 'INNER')->on('client_information.listing_id', '=', 'client_has.listing_id');
		$query_builder->join('client_wants', 'INNER')->on('client_wants.listing_id', '=', 'client_has.listing_id');
		if($tutorial === false){
			$realtor_id = \Authlite::instance('auth_realtor')->get_user()->realtor_id;
		} else {
			$realtor_id = 'tutorial';
		}
		$query_builder->where('client_has.realtor_id', '=', $realtor_id);
		$order = $this->_sort_order($post['sort_order']);
		$query_builder->order_by($order['col'], $order['type']);
		if($order['col'] == 'client_has.featured_listing'){
			$query_builder->order_by('client_has.lastupdated', 'desc');
		}
		$sort = \Model\Listing::forge()->sort_view($post['sort_view']);
		if(isset($sort['col']) && isset($sort['operator']) && isset($sort['val'])){
			$query_builder->where($sort['col'], $sort['operator'], $sort['val']);
			if($sort['col'] == 'client_has.matches' || $sort['col'] == 'client_has.featured_listing'){
				$query_builder->where('client_has.listing_status', 'REGEXP', '0');
			}
		}
		$query_builder->where('client_has.listing_status', '<>', \Model\Listing::LS_USER_DELETED);
		if($post['term'] != ''){
			/** perform search with LIKE and INNER JOIN query */
			$query_builder->where('client_has.deleted', '<>', 1);
			$query_builder->where(\DB::expr('MATCH(client_has.neighbourhood) AGAINST(\'%'.$post['term'].'%\')'));
			$query_builder->or_where('client_has.listing_price', 'like', '%'.$post['term'].'%');
			$query_builder->or_where('client_information.first_name', 'like', '%'.$post['term'].'%');
			$query_builder->or_where('client_information.last_name', 'like', '%'.$post['term'].'%');
			$query_builder->or_where('client_information.listing_id', 'like', '%'.$post['term'].'%');
			$query_builder->or_where('client_has.address', 'like', '%'.$post['term'].'%');
			$query_builder->or_where('client_has.external_id', 'like', '%'.$post['term'].'%');
			$result = $query_builder->execute()->as_array();
		} else {
			$result = $query_builder->execute()->as_array();
		}

		$data['query_builder'] = $query_builder;
		$data['result'] = $result;
		return $data;
	}

	protected function _sort_order($sort_order){
		$return = array();
		switch($sort_order){
			case 'lp_hl':
				$return['col'] = 'client_has.listing_price';
				$return['type'] = 'desc';
				break;
			case 'lp_lh':
				$return['col'] = 'client_has.listing_price';
				$return['type'] = 'asc';
				break;
			case 'dc_hl':
				$return['col'] = 'client_has.date_listed';
				$return['type'] = 'desc';
				break;
			case 'dc_lh':
				$return['col'] = 'client_has.date_listed';
				$return['type'] = 'asc';
				break;
			case 'pstatus':
				$return['col'] = 'client_has.listing_status';
				$return['type'] = 'desc';
				break;
			case 'xstatus':
				$return['col'] = 'client_has.list_as_sold';
				$return['type'] = 'desc';
				break;
			case 'complete':
				$return['col'] = 'client_has.listing_status';
				$return['type'] = 'asc';
				break;
			case 'incomplete':
				$return['col'] = 'client_has.listing_status';
				$return['type'] = 'desc';
				break;
			case 'last_updated':
				$return['col'] = 'client_has.lastupdated';
				$return['type'] = 'desc';
				break;
			case 'featured':
				$return['col'] = 'client_has.featured_listing';
				$return['type'] = 'desc';
				break;
		}
		return $return;
	}

	public function two_way_batch($listing_id){
		$a = $this->load(\DB::select()->where('listing_id', '=', $listing_id)
			->where_open()
			->where('match_potential', '=', 'Exact')
			->or_where('match_potential', '=', 'Medium')
			->or_where('match_potential', '=', 'High')
			->where_close()
			, null)->as_array();
		$batch = array();
		if(count($a) > 0){
			foreach($a as $row){
				$batch['a'] = $row['matched_listings'];
				\DB::delete('matches')->where('listing_id', '=', $listing_id);

				//batch-a
				\DB::delete('matches')->where('listing_id', '=', $batch['a'])
				->where('matched_listings', '=', $listing_id)
				->execute();
			}
		}
	}

	public function three_way_batch($listing_id){
		$a = $this->load(\DB::select()->where('listing_id', '=', $listing_id)->where('match_potential', '=', '3-Way'), null)->as_array();
		$batch = array();
		if(count($a) > 0){
			foreach($a as $row){
				list($batch['a'], $batch['b']) = explode(',', $row['matched_listings']);
				\DB::delete('matches')->where('listing_id', '=', $listing_id);

				//batch-a
				\DB::delete('matches')->where('listing_id', '=', $batch['a'])
				->where(\DB::expr('FIND_IN_SET(\''.$listing_id.'\', matched_listings)'))
				->where(\DB::expr('FIND_IN_SET(\''.$batch['b'].'\', matched_listings)'))
				->execute();

				//batch-b
				\DB::delete('matches')->where('listing_id', '=', $batch['b'])
				->where(\DB::expr('FIND_IN_SET(\''.$listing_id.'\', matched_listings)'))
				->where(\DB::expr('FIND_IN_SET(\''.$batch['a'].'\', matched_listings)'))
				->execute();
			}
		}
	}

	public function four_way_batch($listing_id){
		$a = $this->load(\DB::select()->where('listing_id', '=', $listing_id)->where('match_potential', '=', '4-Way'), null)->as_array();
		$batch = array();
		if(count($a) > 0){
			foreach($a as $row){
				list($batch['a'], $batch['b'], $batch['c']) = explode(',', $row['matched_listings']);
				\DB::delete('matches')->where('listing_id', '=', $listing_id);

				//batch-a
				\DB::delete('matches')->where('listing_id', '=', $batch['a'])
				->where(\DB::expr('FIND_IN_SET(\''.$listing_id.'\', matched_listings)'))
				->where(\DB::expr('FIND_IN_SET(\''.$batch['b'].'\', matched_listings)'))
				->where(\DB::expr('FIND_IN_SET(\''.$batch['c'].'\', matched_listings)'))
				->execute();

				//batch-b
				\DB::delete('matches')->where('listing_id', '=', $batch['b'])
				->where(\DB::expr('FIND_IN_SET(\''.$listing_id.'\', matched_listings)'))
				->where(\DB::expr('FIND_IN_SET(\''.$batch['a'].'\', matched_listings)'))
				->where(\DB::expr('FIND_IN_SET(\''.$batch['c'].'\', matched_listings)'))
				->execute();

				//batch-c
				\DB::delete('matches')->where('listing_id', '=', $batch['c'])
				->where(\DB::expr('FIND_IN_SET(\''.$listing_id.'\', matched_listings)'))
				->where(\DB::expr('FIND_IN_SET(\''.$batch['a'].'\', matched_listings)'))
				->where(\DB::expr('FIND_IN_SET(\''.$batch['b'].'\', matched_listings)'))
				->execute();
			}
		}
	}

	public function five_way_batch($listing_id){
		$a = $this->load(\DB::select()->where('listing_id', '=', $listing_id)->where('match_potential', '=', '5-Way'), null)->as_array();
		$batch = array();
		if(count($a) > 0){
			foreach($a as $row){
				list($batch['a'], $batch['b'], $batch['c'], $batch['d']) = explode(',', $row['matched_listings']);
				\DB::delete('matches')->where('listing_id', '=', $listing_id);

				//batch-a
				\DB::delete('matches')->where('listing_id', '=', $batch['a'])
				->where(\DB::expr('FIND_IN_SET(\''.$listing_id.'\', matched_listings)'))
				->where(\DB::expr('FIND_IN_SET(\''.$batch['b'].'\', matched_listings)'))
				->where(\DB::expr('FIND_IN_SET(\''.$batch['c'].'\', matched_listings)'))
				->where(\DB::expr('FIND_IN_SET(\''.$batch['d'].'\', matched_listings)'))
				->execute();

				//batch-b
				\DB::delete('matches')->where('listing_id', '=', $batch['b'])
				->where(\DB::expr('FIND_IN_SET(\''.$listing_id.'\', matched_listings)'))
				->where(\DB::expr('FIND_IN_SET(\''.$batch['a'].'\', matched_listings)'))
				->where(\DB::expr('FIND_IN_SET(\''.$batch['c'].'\', matched_listings)'))
				->where(\DB::expr('FIND_IN_SET(\''.$batch['d'].'\', matched_listings)'))
				->execute();

				//batch-c
				\DB::delete('matches')->where('listing_id', '=', $batch['c'])
				->where(\DB::expr('FIND_IN_SET(\''.$listing_id.'\', matched_listings)'))
				->where(\DB::expr('FIND_IN_SET(\''.$batch['a'].'\', matched_listings)'))
				->where(\DB::expr('FIND_IN_SET(\''.$batch['b'].'\', matched_listings)'))
				->where(\DB::expr('FIND_IN_SET(\''.$batch['d'].'\', matched_listings)'))
				->execute();

				//batch-d
				\DB::delete('matches')->where('listing_id', '=', $batch['d'])
				->where(\DB::expr('FIND_IN_SET(\''.$listing_id.'\', matched_listings)'))
				->where(\DB::expr('FIND_IN_SET(\''.$batch['a'].'\', matched_listings)'))
				->where(\DB::expr('FIND_IN_SET(\''.$batch['b'].'\', matched_listings)'))
				->where(\DB::expr('FIND_IN_SET(\''.$batch['c'].'\', matched_listings)'))
				->execute();

			}
		}
	}

	public function get_ignore_listings($listing_id){
		$data = $this->load(\DB::select()->where(\DB::expr('ignore_listings IS NOT NULL'))
			->where('listing_id', '=', $listing_id), null)->as_array();
		$return['data'] = $data;
		$return['count'] = sizeof($data);
		return $return;
	}

	public function remove_by_id($listing_id) {
		\DB::delete('matches')->where('listing_id', '=', $listing_id)
		->or_where(\DB::expr('FIND_IN_SET(\''.$listing_id.'\', matched_listings)'))
		->or_where(\DB::expr('FIND_IN_SET(\''.$listing_id.'\', ignore_listings)'))
		->or_where(\DB::expr('FIND_IN_SET(\''.$listing_id.'\', save_listings)'))
		->or_where('buyers_match', '=', $listing_id)
		->execute();
	}

}