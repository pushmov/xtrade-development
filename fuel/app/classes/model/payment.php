<?php
namespace Model;

class Payment extends \Model\Paypalrest {
	protected $_cc_type = array( '' => 'Select', 'visa' => 'Visa', 'mastercard' => 'Mastercard' );

	protected $_validation;

	public static function forge() {
		return new static();
	}

	public function get_cc_type(){
		return $this->_cc_type;
	}

	public function agreement($post, $valname='') {
		$this->_set_validation($valname);
		if($this->_validation->run($post) === FALSE){
			return array('status' => 'ERROR', 'message' => $this->_validation->show_errors());
		}
		$agreement = new Paypalrest\Agreement;
		return $agreement->create($post);
	}

	public function billing_update($data) {
		$result['status'] = 'FAIL';
		$result['result'] = 'Unknown error occurred';
		$agreement = new Paypalrest\Agreement();
		try {
			$result = $agreement->cancel(array(
				'id' => $data['paypal_sub_id'],
				'note' => $data['note']
			));
		} catch(\Exception $e) {
			// TODO: log the error
			\Sysconfig::logerror(realpath(dirname(__FILE__)), $e);
			return array('status' => 'FAIL', 'result' => $e->getMessage());
		}
		return $result;
	}

	/**
	* Direct payment for upgrade to featured listing
	*
	* @param array $post - credit card data posted
	* @return array
	*/
	public function upgrade_listing($post){
		$this->_set_validation();
		if($this->_validation->run($post) === FALSE){
			return array('status' => 'ERROR', 'message' => $this->_validation->show_errors());
		}
		list($city, $prov_state, $country) = \CityState::get_area_id($post['cpc_address'], TRUE);
		list($city_name, $prov_state_name, $country_name) = \CityState::get_area_name($city, $prov_state, $country, TRUE);

		// Create and save the order
		$order = new \Model\Order;
		$order->user_id = $post['realtor_id'];
		$order->user_type_id = User::UT_REALTOR;
		$order->type_id = $order::OT_FEATURED;
		$order->total = $post['fee'] + $post['tax'];
		$order->sub_total = $post['fee'];
		$order->taxes = $post['tax'];
		$order->tax_code = $post['tax_code'];
		$order->order_date = date_create()->format(DATETIME_FORMAT_DB);
		// Get the actual listing id
		$listing = \Model\Client\Has::forge()->load_by_listing_id($post['listing_id']);
		$order->listing_id = $listing->id;
		$order->status_id = $order::OS_PENDING;
		$order->save();

		// Add the order id
		$post['order_id'] = $order->get_number();
		$post['city'] = $city_name;
		$post['prov_state'] = \CityState::get_state_code($prov_state_name, $country_name);
		$post['country'] = \Sysconfig::country_code($country);
		$post['item_description'] = 'Featured Listing ' . $post['listing_id'];
		$pay = new \Model\Paypalrest\Payment;
		$result = $pay->send($post);

		if (isset($result->state) && ($result->state == 'approved')) {
			$order->status_id = $order::OS_COMPLETE;
			$order->trans_id = $result->id;
			$order->save();
			$listing = new \Model\Client\Has($order->listing_id);
			$listing->featured_listing = 1;
			$listing->featured_start = date_create()->format(DATETIME_FORMAT_DB);
			$listing->featured_end = date_create('+' . \Sysconfig::get('listing_featured_len') . ' days')->format(DATETIME_FORMAT_DB);
			$listing->save();
			\Emails::forge()->upgrade_listing_recurring(array_merge($post, $listing->as_array()));
			return array('status' => 'OK', 'id' => $order->id);
		}
		// TODO: log the error
		return array('status' => 'ERROR', 'message' => 'Payment processing error.<br>Check your infomation and try again.', 'result' => $result);
	}

	function money_format($amount){
		$locale = localeconv();
		return $locale['currency_symbol'].''. number_format($amount, 2, $locale['decimal_point'], $locale['thousands_sep']);
	}

	protected function _set_validation($name='') {
		$this->_validation = \Validation::forge($name);
		$this->_validation->add_field('first_name', 'First Name', 'required');
		$this->_validation->add_field('last_name', 'Last Name', 'required');
		$this->_validation->add_field('address', 'Address', 'required');
		$this->_validation->add_field('cpc_address', 'City/State/Country', 'required');
		$this->_validation->add_field('z_p_code', 'Zip/Postal code', 'required');
		$this->_validation->add_field('cc_number', 'Credit Card', 'required');
		$this->_validation->add_field('exp_month', 'Exp Month', 'required');
		$this->_validation->add_field('exp_year', 'Exp Year', 'required');
	}
}