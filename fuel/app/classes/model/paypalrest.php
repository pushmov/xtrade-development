<?php
namespace Model;

class Paypalrest {
	const PP_ACTIVE = 1;
	const PP_SUSPEND = 2;
	const PP_SUSPENDED = 3;
	const PP_CANCEL = 4;
	const PP_CANCELLED = 5;
	const PP_RESTART = 6;
	const PP_UNSUSPEND = 7;

	const CUR_CA = 'CAD';
	const CUR_US = 'USD';
	protected $_api_context;
	protected $_currency = array('CA' => 'CAD', 'US' => 'USD');

	public function __construct() {
		// Added group to the load so that any time this model was loaded the config would reload correctly
		$config = \Config::load('paypal', 'paypal');
		$token = new \PayPal\Auth\OAuthTokenCredential($config['clientid'], $config['clientsecret']);
		$this->_api_context = new \PayPal\Rest\ApiContext($token);
	}

	public function get_api() {
		return $this->_api_context;
	}

	public function create_plan($info) {
		// Create a new instance of Plan object
		$plan = new \PayPal\Api\Plan();

		// Basic information that is required for the plan
		$plan->setName($info['name'])
		->setDescription($info['desc'])
		->setType('INFINITE');

		// Payment definitions for this billing plan.
		$regular = new \PayPal\Api\PaymentDefinition();
		$regular->setName($info['name'])
		->setType('REGULAR')
		->setFrequency('MONTH')
		->setFrequencyInterval('1')
		->setCycles('0')
		->setAmount(new \PayPal\Api\Currency(array('value' => $info['amount'], 'currency' => $info['currency'])));

		$paymentDefinition[] = $regular;

		// Different tax for each province
		if ($info['tax'] != 0) {
			$chargeModel = new \PayPal\Api\ChargeModel();
			$chargeModel->setType('TAX')
			->setAmount(new \PayPal\Api\Currency(array('value' => $info['tax'], 'currency' => $info['currency'])));
			$regular->setChargeModels(array($chargeModel));
		}
		if ($info['trial_len'] != 0) {
			// Trial info
			$trial = new \PayPal\Api\PaymentDefinition();
			$trial->setName('Trial Period')
			->setType('TRIAL')
			->setFrequency('MONTH')
			->setFrequencyInterval($info['trial_len'])
			->setCycles($info['trial_duration'])
			->setAmount(new \PayPal\Api\Currency(array('value' => $info['trial_amt'], 'currency' => $info['currency'])));
			$paymentDefinition[] = $trial;
		}

		// Merchant preferences
		$merchantPreferences = new \PayPal\Api\MerchantPreferences();
		$merchantPreferences->setReturnUrl(\Uri::base(false))
		->setCancelUrl(\Uri::base(false))
		->setAutoBillAmount('YES')
		->setInitialFailAmountAction('CONTINUE')
		->setMaxFailAttempts('0');
		$plan->setPaymentDefinitions($paymentDefinition);
		$plan->setMerchantPreferences($merchantPreferences);

		// Create the plan
		try {
			$new_plan = $plan->create($this->_api_context);
		} catch (Exception $ex) {
			throw new Exception($ex->getMessage());
			exit(1);
		}
		// Activate the plan
		$patch = new \PayPal\Api\Patch();
		$value = new \PayPal\Common\PayPalModel('{
			"state":"ACTIVE"
		}');

		$patch->setOp('replace')
		->setPath('/')
		->setValue($value);
		$patchRequest = new \PayPal\Api\PatchRequest();
		$patchRequest->addPatch($patch);

		$new_plan->update($patchRequest, $this->_api_context);
    	return \PayPal\Api\Plan::get($new_plan->getId(), $this->_api_context);
	}
}