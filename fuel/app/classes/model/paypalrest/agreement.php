<?php
namespace Model\Paypalrest;

class Agreement extends \Model\Paypalrest {
	const STATE_ACTIVE = 1;
	const STATE_PENDING = 2;
	const STATE_EXPIRED = 3;
	const STATE_SUSPEND = 4;
	const STATE_REACTIVE = 5;
	const STATE_CANCEL = 6;

	protected $_status = array(self::STATE_ACTIVE => 'Active', 'Pending', 'Expired', 'Suspend', 'Reactive', 'Cancel');

	public function get_status($id=null){
		if($id === null){
			return $this->_status;
		}
		return $this->_status[$id];
	}

	public function create_from_vault($data){
		$agreement = new \PayPal\Api\Agreement();
		$agreement->setName($data['agreement_name']);
		$agreement->setDescription($data['agreement_description']);
		$agreement->setStartDate(date_create('+1 day')->format('Y-m-d\TH:i:s\Z'));

		$plan = new \PayPal\Api\Plan();
		$plan->setId($data['plan_id']);
		$agreement->setPlan($plan);

		$payer = new \PayPal\Api\Payer();
		$payer->setPaymentMethod('credit_card');
		$payer->setPayerInfo(new \PayPal\Api\PayerInfo(array(
			'email' => $data['email'],
			'first_name' => $data['first_name'],
			'last_name' => $data['last_name'],
		)));

		//use stored card
		$creditCard = \PayPal\Api\CreditCard::get($data['card_id'], $this->_api_context);

		$fundingInstrument = new \PayPal\Api\FundingInstrument();
		$fundingInstrument->setCreditCard($creditCard);
		$payer->setFundingInstruments(array($fundingInstrument));
		$agreement->setPayer($payer);

		try {
			$return = $agreement->create($this->_api_context);
			// Activate the agreement
		} catch (\PayPal\Exception\PayPalConnectionException $e){
			// TODO: need to provide better paypal error message
			return array('error' => $e->getMessage());
		} catch (\PayPal\Exception\PayPalMissingCredentialException $e){
			return array('error' => $e->getMessage());
		}
		return $return;
	}

	public function create($data){
		$agreement = new \PayPal\Api\Agreement();
		$agreement->setName($data['agreement_name']);
		$agreement->setDescription($data['agreement_description']);
		if (isset($data['start_date'])) {
			$agreement->setStartDate($data['start_date']);
		} else {
			$agreement->setStartDate(date_create('+1 day')->format('Y-m-d\TH:i:s\Z'));
		}

		$plan = new \PayPal\Api\Plan();
		$plan->setId($data['plan_id']);
		$agreement->setPlan($plan);

		$payer = new \PayPal\Api\Payer();
		$payer->setPaymentMethod('credit_card');
		$payer->setPayerInfo(new \PayPal\Api\PayerInfo(array(
			'email' => $data['email'],
			'first_name' => $data['first_name'],
			'last_name' => $data['last_name'],
		)));

		$creditCard = new \PayPal\Api\CreditCard();
		$creditCard->setType(strtolower($data['cc_type']));
		$creditCard->setNumber($data['cc_number']);
		$creditCard->setExpireMonth($data['exp_month']);
		$creditCard->setExpireYear($data['exp_year']);
		$creditCard->setCvv2($data['cc_cvv']);
		$creditCard->setFirstName($data['first_name']);
		$creditCard->setLastName($data['last_name']);
		$creditCard->setBillingAddress(new \PayPal\Api\Address(array(
			'line1' => $data['address'],
			'city' => $data['city'],
			'country_code' => \Sysconfig::country_code($data['country']),
			'postal_code' => $data['z_p_code'],
			'state' => \CityState::get_state_code($data['state'], $data['country'])
		)));

		$fundingInstrument = new \PayPal\Api\FundingInstrument();
		$fundingInstrument->setCreditCard($creditCard);
		$payer->setFundingInstruments(array($fundingInstrument));
		$agreement->setPayer($payer);
		$pp = new \Model\Paypalrest();
		try {
			// Credit card payments are automatically executed and activated
			$result = $agreement->create($pp->get_api());
			return array('status' => 'OK', 'id' => $result->id);
		} catch (\PayPal\Exception\PayPalConnectionException $e){
			// TODO: need to provide better paypal error message
			\Sysconfig::logerror(realpath(dirname(__FILE__)), $e);
			return array('status' => 'FAIL', 'result' => 'Credit Card Error', 'e' => $e->getData());
		} catch (\PayPal\Exception\PayPalMissingCredentialException $e){
			\Sysconfig::logerror(realpath(dirname(__FILE__)), $e);
			return array('status' => 'FAIL', 'result' => 'Gateway Error', 'e2' => $e->getMessage());
		}
		return array('status' => 'FAIL', 'result' => 'Gateway Error');
	}

	public function detail($agreement_id){
		$data = array('state' => $this->get_status(self::STATE_PENDING), 'amt' => '', 'tax_amt' => '', 'next_date' => '' );
		try {
			$agreement = \PayPal\Api\Agreement::get($agreement_id, $this->_api_context);
			$data['agreement_id'] = $agreement->id;
			$data['state'] = $agreement->state;
			$data['amt'] = $agreement->plan->payment_definitions[0]->amount->value;
			foreach($agreement->plan->payment_definitions[0]->charge_models as $v) {
				if ($v->type == 'TAX') {
					$data['tax_amt'] = $v->amount->value;
				}
			}
			$data['next_date'] = $agreement->agreement_details->next_billing_date;
			$ccinfo = $agreement->payer->funding_instruments[0]->credit_card;
			$data['paypal_first_name'] = $ccinfo->first_name;
			$data['paypal_last_name'] = $ccinfo->last_name;
			$data['paypal_email'] = $agreement->payer->payer_info->email;
		} catch (\PayPal\Exception\PayPalConnectionException $e){
			$data['error'] = $e->getMessage();
		} catch (\PayPal\Exception\PayPalMissingCredentialException $e){
			$data['error'] = $e->getMessage();
		}
		return $data;
	}

	public function get_billing($agreement_id){
		$data = array('cc_type' => '', 'cc_number' => '', 'exp_month' => '', 'exp_year' => '');
		try {
			$agreement = \PayPal\Api\Agreement::get($agreement_id, $this->_api_context);
			$payer = $agreement->getPayer();
			$funding_instrument = $payer->getFundingInstruments();
			$card = $funding_instrument[0]->getCreditCard()->toArray();
			$data['address'] = $card['billing_address']['line1'];
			$data['z_p_code'] = $card['billing_address']['postal_code'];
			$data['cc_number'] = str_pad($card['number'], 16, '*', STR_PAD_LEFT);
			$data['exp_month'] = $card['expire_month'];
			$data['exp_year'] = $card['expire_year'];
			$data['cpc_address'] = $card['billing_address']['city'].', '.\CityState::$state_ca[$card['billing_address']['state']].', '.\CityState::$country[$card['billing_address']['country_code']];
			$data['cc_type'] = $card['type'];
		} catch (\PayPal\Exception\PayPalConnectionException $e){
			$data['error'] = $e->getMessage();
		} catch (\PayPal\Exception\PayPalMissingCredentialException $e){
			$data['error'] = $e->getMessage();
		}
		return $data;
	}

	public function cancel($param){
		$state_descriptor = new \PayPal\Api\AgreementStateDescriptor();
		$state_descriptor->setNote($param['note']);
		$agreement = \PayPal\Api\Agreement::get($param['id'], $this->_api_context);
		try {
			$agreement->cancel($state_descriptor, $this->_api_context);
			$data['status'] = 'success';
		} catch (\PayPal\Exception\PayPalConnectionException $e){
			$data['status'] = 'error';
			$data['error'] = $e->getMessage();
		} catch (\PayPal\Exception\PayPalMissingCredentialException $e){
			$data['status'] = 'error';
			$data['error'] = $e->getMessage();
		}
		return $data;
	}

}
