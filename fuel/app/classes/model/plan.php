<?php
namespace Model;

/**
* PayPal subscription plan information.
*
*/
class Plan extends \Automodeler {
	// Plan types
	const PT_REALTOR = 0;
	const PT_VENDOR = 1;
	const PT_VFEATURE = 2;
	// Status
	const S_NEW = 0;		// New not available yet
	const S_ACTIVE = 1;		// Active and can be used, no users can be deleted
	const S_SET = 2;		// Active and has users, therefore can't be changed
	const S_PAUSE = 3;		// Paused and not available, has users, therefore can't be changed
	const S_ARCHIVED = 9;	// Archived and had users

	const TYPE_FIXED = 'FIXED';
	const TYPE_INFINITE = 'INFINITE';

	protected $_table_name = 'plans';
	protected $_data = array('id' => null, 'country_id' => 1, 'state_id' => 0, 'type_id' => 0, 'name' => '', 'details' => '',
		'info' => '', 'promo_code' => '', 'promo_rate' => 0, 'promo_tax'  => 0, 'promo_len' => 0,
		'rate' => 0, 'rate_tax' => 0, 'plan_id' => '',	'created_at' => '', 'status_id' => 0);
	protected $_type = array(self::PT_REALTOR => 'Realtor', self::PT_VENDOR => 'Vendor', self::PT_VFEATURE => 'Featured Ad');
	protected $_status = array(self::S_NEW => 'New', self::S_ACTIVE => 'Active', self::S_SET => 'Set',
		self::S_PAUSE => 'Pause', self::S_ARCHIVED => 'Archived');

	protected $_valrules = array(
		'name' => array('Name', 'required'),
		'info' => array('Info', 'required'),
		'rate' => array('rate', 'required')
	);

	public static function _validation_unique_plan($value){
		$val = \Validation::active();
		$db = \DB::select()->from('plans');
		$db->where('country_id', '=', $val->input('country_id'));
		$db->where('state_id', '=', $val->input('state_id'));
		$db->where('state_id', '!=', 0);
		$db->where('type_id', '=', $val->input('type_id'));
		if (trim($val->input('promo_code')) != '') {
			$db->where('promo_code', '=', trim($val->input('promo_code')));
		}
		if (trim($val->input('id')) != '') {
			$db->where('id', '!=', $val->input('id'));
		}
		$data = $db->execute();
		return count($data) == 0;
	}

	public function get_status_array($id=null){
		if($id === null) {
			return $this->_status;
		}
		return array($id => $this->_status[$id]);
	}

	public function admin_list($params) {
		// State codes
		$state[1] = \CityState::get_state_array(1);
		$state[2] = \CityState::get_state_array(2);
		$data['data'] = array();
		$data['draw'] = (isset($params['draw'])) ? (int)$params['draw'] : '1';
		$aColumns = array('type_id', 'info', 'country_id', 'state_id', 'name', 'promo_code', 'status_id');

		$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM {$this->_table_name}";
		$data['recordsTotal'] = \DB::query($sql,  \DB::SELECT)->execute()->count();
		if (isset($params['search']['value']) && $params['search']['value'] != ''){
			$sql .= " WHERE(";
			$sql .= " name LIKE '%{$params['search']['value']}%'";
			$sql .= " OR promo_code LIKE '%{$params['search']['value']}%'";
			$sql .= ")";
		}
		if (isset($params['order']) && (count($params['order']) > 0)) {
			$order = array();
			foreach($params['order'] as $o) {
				$order[] = $aColumns[$o['column']] . ' ' . $o['dir'];
			}
			$sql .= " ORDER BY " . implode(',', $order);
		} else {
			$sql .= " ORDER BY status_id";
		}
		if (isset($params['start']) && $params['length'] != '-1') {
			$sql .= " LIMIT " . (int)$params['start'] . ", " . (int)$params['length'];
		}
		$result = \DB::query($sql, \DB::SELECT)->execute();
		$aColumns[] = 'action';
		foreach ($result as $row) {
			$row['state_id'] = ($row['state_id'] == 0) ? 'All' : $state[$row['country_id']][$row['state_id']];
			$row['country_id'] = ($row['country_id'] == 1) ? 'CA' : 'US';
			$row['type_id'] = $this->_type[$row['type_id']];
			$row['status_id'] = $this->_status[$row['status_id']];
			$row['action'] = \Html::anchor('admins/plans/edit/' . $row['id'], 'edit', array('class' => 'button small', 'type' => 'button'));
			$data['data'][] = array_values(\Arr::filter_keys($row, $aColumns));
		}
		$data['recordsFiltered'] = \DB::query('SELECT FOUND_ROWS() AS cnt', \DB::SELECT)->execute()->get('cnt');
		return $data;
	}

	public function doupdate($data) {
		$validation = \Validation::forge();
		$validation->add_callable('\Model\Plan');
		foreach($this->_valrules as $name => $v) {
			$validation->add_field($name, $v[0], $v[1]);
		}
		$validation->add_field('state_id', 'Prov/State', 'unique_plan');
		if ($validation->run($data) === false) {
			return array('errors' => $validation->error_message());
		}
		$promo = \Model\Plan::forge($data['id']);
		$promo->set_fields($data);
		try {
			$promo->save();
			$response = $promo->as_array();
			\Session::set_flash('message', 'Plan data successfully saved');
			$response['status'] = 'ok';
			$response['success'] = true;
		} catch(\AutomodelerException $e){
			$response = array('errors' => $e->errors);
		}
		return $response;
	}

	public function get_type_txt($id = null) {
		\Lang::load('orders');
		if ($id === null) {
			$data = array();
			foreach($this->_type as $v) {
				$data[$v] = $v;
			}
			return $data;
		} else {
			$txt = $this->_type[$id];
			return __($txt);
		}
		return '';
	}

	/**
	* Load the last order for a user
	*
	* @param int $user_id - the user's id
	* @param int $type_id - the user type (realtor, broker, vendor)
	* @return Orders
	*/
	public function load_last($user_id, $type_id) {
		$query = \DB::select_array()->where('user_id', '=', $user_id)->where('user_type_id', '=', $type_id)->order_by('order_date','desc');
		$this->load($query);
		return $this;
	}
	/**
	* Get the order history for a user
	*
	* @param int $user_id - the user's id
	* @param int $type_id - the user type (realtor, broker, vendor)
	* @return array
	*/
	public function get_history($user_id, $type_id) {
		$data = array();
		$result = \DB::select('type_id','total','order_date','sub_total')->from($this->_table_name)
		->where('homebase_id', '=', $user_id)
		->where('user_type_id', '=', $type_id)
		->order_by('order_date','desc')
		->execute();
		foreach($result as $row) {
			$row['type'] = __($row['type_id']);
			$row['total'] = number_format($row['total'], 2);
			$row['order_date'] = date_create($row['order_date'])->format(DATE_FORMAT);
			$data[] = $row;
		}
		return $data;
	}

	public function doinsert($order_fields){
		$this->set_fields(array_map('trim', $order_fields));
		return $this->save();
	}

	public function get_type($id=null){
		if($id === null) {
			return $this->_type;
		}
		if(!array_key_exists($id, $this->_type)){
			return '';
		}
		return $this->_type[$id];
	}

	public function change_plan($plan_id, $change_plan_to=null){
		$agreement = new \Model\Paypalrest\Agreement();
		$trans = $this->load(\DB::select()->where('paypal_plan_id', '=', $plan_id), null)->as_array();
		$ut = \Model\User::forge()->get_types();
		$plan = \Config::load('paypal', 'plan', true);

		foreach($trans as $t){
			$model = '\\Model\\' . ucwords($ut[$t['user_type_id']]);
			$user = $model::forge($t['user_id']);
			$data['card_id'] = $user->paypal_sub_id;
			$data['plan_id'] = ($change_plan_to === null) ? $plan['plan'][\Sysconfig::country_code($user->country)] : $change_plan_to;
			$data['agreement_name'] = 'xTH Monthly Membership';
			$data['agreement_description'] = 'Monthly Membership Payment';
			$data['email'] = $user->email;
			$data['first_name'] = $user->first_name;
			$data['last_name'] = $user->last_name;
			$agreement_id = $agreement->create($data);

			$this->load(\DB::select()->where('id', '=', $t['id']));
			$this->paypal_plan_id = $data['plan_id'];
			$this->paypal_agreement_id = $agreement_id->id;
			$this->save();
		}
		return;
	}

	// Create & activate plan
	public function create($data){
		$data = array_map('trim', $data);
		$currency = ($data['country_id'] == \CityState::CA) ? 'CAD' : 'USD';
		$response = array();
		$data['status_id'] = self::S_ACTIVE;

		$validation = \Validation::forge();
		$validation->add_callable('\Model\Plan');
		$validation->add_field('status_id', 'Plan', 'unique_plan');
		if ($validation->run($data) === false) {
			$response['error'] = $validation->error_message();
			return $response;
		}

		$plan = new \PayPal\Api\Plan();
		$plan->setName($data['name'])
		->setDescription($data['info'])
		->setType(self::TYPE_FIXED);
		$amount = new \PayPal\Api\Currency(array(
			'value' => $data['rate'],
			'currency' => $currency
		));
		$paymentDefinitions = array();

		// Regular Rate
		$paymentDefinitionRegular = new \PayPal\Api\PaymentDefinition();
		$paymentDefinitionRegular->setName($data['name']);
		$paymentDefinitionRegular->setType('REGULAR');
		$paymentDefinitionRegular->setAmount($amount);
		$paymentDefinitionRegular->setFrequency('MONTH');
		$paymentDefinitionRegular->setFrequencyInterval(1);
		$paymentDefinitionRegular->setCycles(12 - $data['promo_len']);

		if( (int) $data['rate_tax'] > 0) {
			$chargeModels = new \PayPal\Api\ChargeModel();
			$chargeModels->setType('TAX');
			$chargeModels->setAmount(new \PayPal\Api\Currency(array(
				'value' => $data['rate_tax'],
				'currency' => $currency
			)));
			$paymentDefinitionRegular->setChargeModels(array($chargeModels));
		}

		$paymentDefinitions[] = $paymentDefinitionRegular;
		$plan->setPaymentDefinitions($paymentDefinitions);

		$merchantPreferences = new \PayPal\Api\MerchantPreferences();
		$merchantPreferences->setAutoBillAmount('YES')
		->setInitialFailAmountAction('CONTINUE')
		->setMaxFailAttempts(0)
		->setReturnUrl(\Config::get('base_url').'admins/plans/returnurl')
		->setCancelUrl(\Config::get('base_url').'admins/plans/cancelurl');
		$plan->setMerchantPreferences($merchantPreferences);

		// Promo Rate
		if (($data['promo_code'] != '') && ($data['promo_len'] != 0)) {
			//use new amount for x month length
			$paymentDefinitionPromo = new \PayPal\Api\PaymentDefinition();
			$paymentDefinitionPromo->setName('Promo of ' . $data['name']);
			$paymentDefinitionPromo->setType('TRIAL');
			$paymentDefinitionPromo->setAmount(new \PayPal\Api\Currency(array(
				'value' => $data['promo_rate'],
				'currency' => $currency
			)));
			$paymentDefinitionPromo->setFrequency('MONTH');
			$paymentDefinitionPromo->setFrequencyInterval(1);
			$paymentDefinitionPromo->setCycles($data['promo_len']);
			if ( (int) $data['promo_tax'] > 0) {
				$chargeModels = new \PayPal\Api\ChargeModel();
				$chargeModels->setType('TAX');
				$chargeModels->setAmount(new \PayPal\Api\Currency(array(
					'value' => $data['promo_tax'],
					'currency' => $currency
				)));
				$paymentDefinitionPromo->setChargeModels(array($chargeModels));
			}
			$paymentDefinitions[] = $paymentDefinitionPromo;
		}

		try {
			$pp = new \Model\Paypalrest();
			$output = $plan->create($pp->get_api());
			$model = self::forge($data['id']);
			$model->plan_id = $output->getId();
			$model->save();
			$patch = new \PayPal\Api\Patch();
			$value = new \PayPal\Common\PayPalModel('{"state":"ACTIVE"}');
			$patch->setOp('replace')
			->setPath('/')
			->setValue($value);
			$patchRequest = new \PayPal\Api\PatchRequest();
			$patchRequest->addPatch($patch);
			$plan->update($patchRequest, $pp->get_api());
			$model->status_id = self::S_ACTIVE;
			$model->created_at = date_create()->format(DATETIME_FORMAT_DB);
			$model->save();
			$response['success'] = 'ok';
		} catch (\PayPal\Exception\PayPalConnectionException $e){
			$output = $response['error'] = $e->getMessage();
			\Log::error(__FILE__.' : '.__LINE__.' <> '.print_r($e, true));
		} catch (\PayPal\Exception\PayPalMissingCredentialException $e){
			$output = $response['error'] = $e->getMessage();
			\Log::error(__FILE__.' : '.__LINE__.' <> '.print_r($e, true));
		}

		return $output;
	}

	//set plan to INACTIVE
	public function pause($data){
		$response = array();
		$model = self::forge($data['id']);
		if ($model->loaded()) {
			$pp = new \Model\Paypalrest();
			try {
				$plan = \PayPal\Api\Plan::get($data['plan_id'], $pp->get_api());
				$patch = new \PayPal\Api\Patch();
				$value = new \PayPal\Common\PayPalModel('{"state":"INACTIVE"}');
				$patch->setOp('replace')
				->setPath('/')
				->setValue($value);
				$patchRequest = new \PayPal\Api\PatchRequest();
				$patchRequest->addPatch($patch);
				$plan->update($patchRequest, $pp->get_api());
				$model->status_id = self::S_PAUSE;
				$model->save();
				$response['success'] = 'ok';
			} catch (\PayPal\Exception\PayPalConnectionException $e){
				$response['error'] = $e->getMessage();
			} catch (\PayPal\Exception\PayPalMissingCredentialException $e){
				$response['error'] = $e->getMessage();
			}
		}
		return $response;
	}


	//set plan to DELETED
	public function deleteplan($data){
		$response = array();
		$model = self::forge($data['id']);
		if ($model->loaded()) {
			if($data['plan_id'] != '') {
				$pp = new \Model\Paypalrest();
				try {
					$plan = \PayPal\Api\Plan::get($data['plan_id'], $pp->get_api());
					$patch = new \PayPal\Api\Patch();
					$value = new \PayPal\Common\PayPalModel('{"state":"DELETED"}');
					$patch->setOp('replace')
					->setPath('/')
					->setValue($value);
					$patchRequest = new \PayPal\Api\PatchRequest();
					$patchRequest->addPatch($patch);
					$plan->update($patchRequest, $pp->get_api());
				} catch (\PayPal\Exception\PayPalConnectionException $e){
					$response['error'] = $e->getMessage();
				} catch (\PayPal\Exception\PayPalMissingCredentialException $e){
					$response['error'] = $e->getMessage();
				}
			}
			$realtors = \Model\Realtor::forge()->load(\DB::select()
				->where('promo_code', '=', $data['promo_code'])
				->where('promo_code', '<>', ''), null);
			if(count($realtors) > 0) {
				$model->status_id = self::S_ARCHIVED;
				$model->save();
				$response['success'] = '/admins/plans/edit/'.$data['id'];
			} else {
				\DB::delete('plans')->where('id', '=', $data['id'])->execute();
				$response['success'] = '/admins/plans';
			}
		}
		return $response;
	}

	public function set($data){
		$response = array();
		$model = self::forge($data['id']);
		if ($model->loaded()) {
			$model->status_id = self::S_SET;
			$model->save();
			$response['success'] = true;
		}
		return $response;
	}

	public function close($data){
		$response = array();
		$model = self::forge($data['id']);
		if ($model->loaded()) {
			$model->status_id = self::S_ACTIVE;
			$model->save();
			$response['success'] = true;
		}
		return $response;
	}

	/**
	* Load using country, state and promo code
	*
	* @param int $country
	* @param int $state
	* @param string $promo
	*/
	public function load_by_csp($country, $state, $promo = '', $type_id = \Model\Plan::PT_REALTOR) {
		$promo = strtoupper(trim($promo));
		if ($state != 0) {
			// If not all then try this first
			$this->load(\DB::select_array()->where('status_id', 'IN', array(1,2))
			->where('country_id',$country)->where('state_id',$state)
			->where('promo_code',$promo)->where('type_id', '=', $type_id));

		}
		if (!$this->loaded()) {
			// If not loaded from state the load default
			$this->load(\DB::select_array()->where('status_id', 'IN', array(1,2))
			->where('country_id',$country)->where('state_id',0)->where('promo_code',$promo)->where('type_id', '=', $type_id));
		}
		return $this;
	}

	public function promo_list($country = null) {
		$data = array('' => 'Select One');
		$query = \DB::select_array()->from($this->_table_name)->where('status_id', 'IN', array(1,2))
			->where('country_id',$country)->where('type_id',self::PT_REALTOR)->where('promo_code', '<>', '');
		$result = $query->execute();
		foreach($result as $row) {
			$data[$row['id']] = $row['promo_code'] . ' : ' . $row['info'];
		}
		return $data;
	}

	public function is_exists($country_id, $state_id, $type_id){
		$this->load(\DB::select_array()->where('country_id', '=', $country_id)
			->where('state_id', '=', $state_id)
			->where('type_id', '=', $type_id));
		return $this->loaded() ? true : false;
	}

}