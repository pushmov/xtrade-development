<?php
namespace Model\Realtor;

class Payment extends \Model\Payment {
	
	/**
	* The inital order and payment
	*
	* @param array $post
	*/
	public function realtor($post){
		$post['agreement_name'] = 'Realtor Monthly';
		$post['agreement_description'] = 'Realtor monthy payment agreement';
		return $this->agreement($post, 'realtor');
	}
	
	public function billing_update($data){
		$realtor = \Model\Realtor::forge($data['id']);
		$data['paypal_sub_id'] = $realtor->paypal_sub_id;
		$data['note'] = 'Realtor monthly membership cancel - Update billing info';
		list($city,$state,$country) = \CityState::get_area_id($data['cpc_address'], true);
		list($data['city'],$data['state'],$data['country']) = \CityState::get_area_name($city, $state, $country, true);
		// Load plan info
		$plan = \Model\Plan::forge()->load_by_csp($country, $state, '', \Model\Plan::PT_REALTOR);
		$data['plan_id'] = $plan->plan_id;
		$result = parent::billing_update($data);
		$data['start_date'] = date_create()->format('Y-m-d\TH:i:s\Z');
		if($result['status'] == 'success') {
			$realtor->first_name = $data['first_name'];
			$realtor->last_name = $data['last_name'];
			$realtor->address = $data['address'];
			$realtor->z_p_code = $data['z_p_code'];
			$realtor->city = $city;
			$realtor->prov_state = $state;
			$realtor->country = $country;
			$realtor->paypal_sub_id = '';
			$realtor->plan_id = '';
			$realtor->save(false);
			$result = $this->realtor($data);
			if($result['status'] == 'OK') {
				$realtor->paypal_sub_id = $result['id'];
				$realtor->plan_id = $plan->plan_id;
				$realtor->save(false);
				//override authlite session
				$auth_realtor = \Authlite::instance('auth_realtor');
				\Model\User::forge()->unset_user_type();
				$auth_realtor->logout();
				$auth_realtor->force_login($realtor->email);
				$result = array('status' => 'OK', 'Conf_Num' => $realtor->id);
			}
		}
		return $result;
	}

}