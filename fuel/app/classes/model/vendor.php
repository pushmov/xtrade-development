<?php
namespace Model;

class Vendor extends User {
	const ID_PREFIX = 'vxTH';

	//    const VENDOR_STATUS_AD_COMPLETED = 0;
	//    const VENDOR_STATUS_AD_DELETED = 1;
	//    const VENDOR_STATUS_HIDDEN = 2;
	//    const VENDOR_STATUS_AD_VERIFIED = 3;
	//    const VENDOR_STATUS_AD_UNVERIFIED = 9;

	const VENDOR_STATUS_FEATURED = 1;
	const VENDOR_STATUS_NON_FEATURED = 0;

	const IMG_PATH = 'assets/images/vendors/';

	protected $_class = 'vendor';
	protected $_table_name = 'directory_vendors';
	protected $_data = array('id' => null,'vendor_id' => '','first_name' => '','last_name' => '','company_name' => '',
		'address' => '','location_address' => '','z_p_code' => '','phone' => '','cell' => '','toll_free' => '',
		'email' => '','public_email' => '','password' => '', 'temp_pass' => '',	'pass_reset' => '','web_site' => '',
		'comments' => '','locale' => '','type' => '','featured' => 0,'promo_code' => '','account_creation' => '',
		'remember' => 0,'browser' => '','active_till' => '','email_type' => 0,'activate' => '','promotional_end' => '',
		'logged_in' => 0,'session_id' => '','ip_address' => '','last_login' => '','paypal_status' => 0,
		'paypal_sub_id' => '','plan_id' => '','paypal_fet_id' => '','plan2_id' => '','account_status' => self::AS_NEW);

	protected $_valrules =	array(
		'type' => array('type_of_service', 'required'),
		'locale' => array('advert_addr', 'required'),
		'first_name' => array('first', 'required'),
		'last_name' => array('last', 'required'),
		'company_name' => array('company', 'required'),
//		'address' => array('address', 'required'),
//		'location_address' => array('location_address', 'required'),
//		'z_p_code' => array('postal', 'required'),
		'phone' => array('phone', 'required'),
		'web_site' => array('site', 'valid_domain'),
		'cemail' => array('this_email', 'required|match_field[email]|valid_email'),
		'public_email' => array('public_facing_email', 'required|valid_email'),
		'comments' => array('service_desc', 'required')
	);

	public function all_services(){
		$all = array();
		$services = \Model\Vendor\Dlist::forge()->load(\DB::select_array(), NULL)->as_array();
		foreach($services as $row){
			$all[$row['id']] = $row['name'];
		}
		return $all;
	}

	public function load_by_id($id) {
		$this->load(\DB::select_array()->where('vendor_id', '=', $id));
		return $this;
	}

	public function get_details(){
		$this->load(\DB::select_array()->where('vendor_id', '=', $this->id));
		return $this;
	}

	public function get_payment_info(){
		$agree = new \Model\Paypalrest\Agreement();
		$billing = $agree->get_billing($this->paypal_sub_id);
		if (isset($billing['next_date'])) {
			$extra['next_payment'] = '$' . number_format(($billing['amt']+$billing['tax_amt']), 2) . ' '.__('on').' ' . date_create($billing['next_date'])->format(DATE_FORMAT);
		} else {
			$extra['next_payment'] = __('pending');
		}
		return $extra;
	}

	public function save($validation = NULL) {
		// Do some fomatting before saving
		$this->phone = preg_replace('/\D+/', '', $this->phone);
		$this->cell = preg_replace('/\D+/', '', $this->cell);
		$this->toll_free = preg_replace('/\D+/', '', $this->toll_free);
		$this->email = strtolower($this->email);
		$this->web_site = $this->format_url($this->web_site);
		return parent::save($validation);
	}

	/**
	* Returns data used for the profile page
	*
	*/
	public function profile() {
		$data = parent::profile();
		$data['toll_free'] = $this->toll_free;
		$data['member_id'] = $this->vendor_id;
		$extra['info'] = __('profile_reminder');
		return array_merge($data, $extra);
	}

	public function doinsert($post){
		$this->set_fields(array_map('trim', $post));
		$this->password = \Authlite::instance('auth_')->hash($post['password']);
		$this->account_creation = date_create()->format(DATETIME_FORMAT_DB);
		return parent::save();
	}

	public function register($post){
		\Lang::load('common');
		\Lang::load('signup');
		//validation
		$validation = \Validation::forge();
		$validation->add_callable(new \Model\Vendor);
		$validation->add_callable(new \Rules);

		foreach($this->_valrules as $field => $rules){
			$validation->add_field($field, __($rules[0]), $rules[1]);
		}
		$validation->add_field('email', __('email'), 'required|valid_email');
		$validation->field('email')->add_rule('unique_email');
		$validation->add_field('password', __('password'), 'required');
		$validation->add_field('cpassword', __('re_enter_password'), 'match_field[password]');
		$validation->add_field('tos', __('terms'), 'required');
		if ($validation->run($post['data'])) {
			$data = array_merge($post['data'], $post);
			$data['user_type'] = $post['user_type'];
			$data['cpc_address'] = $post['data']['location_address'];
			//todo : fix promo code vendor && broker id
			$data['promo_code'] = $post['promo_code'];
			$data['broker_id'] = '';

			return $data;
		} else {
			throw new \AutomodelerException('error', array(), $validation->error_message());
		}
	}

	public function load_result($post, $pagination=NULL, $per_page=0, $offset=0){
		$result = array();
		$area_id = \CityState::get_area_id($post['location'], true);
		if($area_id[0] == '0' || $area_id[1] == '0' || $area_id[2] == '0'){
			return $result;
		}
		$db = \DB::select_array()->where('locale', 'like', '%'.join(',', $area_id).'%')
		->where('account_status', '=', \Model\User::AS_VALID);

		if(isset($post['type']) && count($post['type']) > 0){
			$db->where_open();
			$i = 0;
			foreach($post['type'] as $type){
				if($type == 'all'){
					//where open empty fix
					$db->where(\DB::expr('1 = 1'));
					continue;
				}
				if($i == 0){
					$db->where(\DB::expr("FIND_IN_SET('".$type."', REPLACE(type, ';', ','))"));
				} else {
					$db->or_where(\DB::expr("FIND_IN_SET('".$type."', REPLACE(type, ';', ','))"));
				}
				$i++;
			}
			$db->where_close();
		}
		if($offset > 0){
			$db->offset($pagination->offset);
		}
		if($per_page > 0){
			$db->limit($pagination->per_page);
		}
		$result = \Model\Vendor::forge()->load($db->order_by('featured', 'desc'), null)->as_array();
		return $result;
	}

	/**
	* Convert set of area to advertise string to area code number
	* @param String $area_name Area name
	* @return String Area ID
	*/
	public function advertise_to_areaid($area_name){
		$tmp = array();
		$areas = explode(';', trim($area_name, ';'));
		foreach($areas as $area){
			$tmp[] = \CityState::get_area_id($area);
		}
		if(sizeof($tmp) <= 0){
			return \CityState::get_area_id($area_name);
		}
		$area_id = join(';', $tmp);
		return $area_id;
	}

	/**
	* Convert set of area code nmber to area name
	* @param String $area_id Area ID
	* @return String Area Name
	*/
	public function areaid_to_advertise($area_id){
		$tmp = array();
		$areas = explode(';', trim($area_id, ';'));
		foreach($areas as $area){
			list($city, $prov_state, $country) = explode(',', $area);
			$tmp[] = \CityState::get_area_name($city, $prov_state, $country);
		}
		$area_name = join(';', $tmp);
		return $area_name;
	}

	/**
	* Convert services readable name to service id
	* @param Array $services Services
	* @return String Service IDs
	*/
	public function services_to_typeid($services){
		$tmp = array();
		$names = array_map('trim', explode(';', $services));
		foreach($names as $name){
			$tmp[] = \DB::select('id')->from('directory_list')->where('name', '=', $name)->execute()->get('id');
		}
		return join(';', $tmp);
	}

	/**
	* Convert service ids to service readable name
	* @param String $service_id Service ids
	* @return String Service readable name
	*/
	public function typeid_to_services($service_id){
		$tmp = array();
		$ids = explode(';', trim($service_id, ';'));
		foreach($ids as $id){
			$tmp[] = \DB::select('name')->from('directory_list')->where('id', '=', $id)->execute()->get('name');
		}
		return join('; ', $tmp);
	}

	public function doupdate($post) {
		$validation = \Validation::forge();
		$validation->add_callable(new \Model\Realtor);
		$validation->add_callable('rules');
		// Common rules
		foreach($this->_valrules as $field => $rules) {
			$validation->add_field($field, __($rules[0]), $rules[1]);
		}
		$validation->add_field('email',__('email'), 'required|valid_email');
		if (!$validation->run($post['data'])) {
			throw new \AutomodelerException('error', array(), $validation->error_message());
		}
		list($extra['city'], $extra['prov_state'], $extra['country']) = \CityState::get_area_id($post['data']['location_address']);
		$post['data']['type'] = join(';', $post['data']['type']);
		$post['data']['locale'] = \Model\Vendor::forge()->advertise_to_areaid($post['data']['locale']);
		$this->set_fields($post['data']);
		$this->account_status = \Model\User::AS_VALID;
		$agreement = new \Model\Paypalrest\Agreement();
		$detail = $agreement->detail($this->paypal_sub_id);
		if ($detail['next_date'] != '' && $detail['state'] == $agreement->get_status(\Model\Paypalrest\Agreement::STATE_ACTIVE)) {
			$this->active_till = date_create($detail['next_date'])->format(DATETIME_FORMAT_DB);
		}
		$this->account_status = self::AS_VALID;
		\Authlite::instance('auth_vendor')->force_login($this->email);
		return $this->save();
	}

	/**
	* Delete vendor
	*
	* @param array $id Vendor_id
	* @param String $type User type [vendor, realtor, broker]
	*/
	public function delete_vendor($id){
		$response['status'] = 'error';
		$delete = \DB::delete($this->_table_name)->where('vendor_id', '=', $id)->execute();
		if($delete){
			$response['status'] = 'ok';
			$response['id'] = $id;
		}
		return $response;
	}


	/**
	* Load all vendors available from directory vendor table
	*
	* @return array Vendors record
	*/
	public function load_vendors($params){
		$data['data'] = $drow = array();
		$data['draw'] = (isset($params['draw'])) ? (int)$params['draw'] : '1';
		$aColumns = array('vendor_id', 'company_name', 'name', 'status');
		$statuses = $this->get_status();
		$sqlStatus = '';
		foreach ($statuses as $key => $status) {
			$sqlStatus .= ' WHEN account_status = \''.$key.'\' THEN \''.$status.'\'';
		}
		$subsql = "(SELECT id, vendor_id, company_name, CONCAT(first_name, ' ', last_name) as name, location_address,
		(CASE ".$sqlStatus." END) AS status
		FROM directory_vendors) AS tmp ";
		$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM " . $subsql;

		if (isset($params['search']['value']) && $params['search']['value'] != ''){
			$sql .= ' WHERE (';
			$sql .= 'vendor_id LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ' OR name LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ' OR company_name LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ' OR location_address LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ' OR status LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ')';
		}

		if (isset($params['order']) && ((int)$params['order'] > 0)) {
			$sql .= ' ORDER BY '.$aColumns[$params['order'][0]['column']].' '.$params['order'][0]['dir'];
		} else {
			$sql .= ' ORDER BY id asc';
		}

		if (isset($params['start']) && $params['length'] != '-1') {
			$sql .= " LIMIT " . (int)$params['start'] . ", " . (int)$params['length'];
		}

		$result = \DB::query($sql, \DB::SELECT)->execute();

		foreach ($result as $row) {
			$drow['vendor_id'] = \Html::anchor('admins/profile/vendor/'.$row['id'], $row['vendor_id']);
			$drow['company_name'] = $row['company_name'];
			$drow['name'] = $row['name'];
			list($drow['city'], $drow['state'], $country) = explode(',', $row['location_address']);
			$drow['status'] = $row['status'];
			$data['data'][] = array_values($drow);
		}

		$total = \DB::select()->from($this->_table_name)->execute()->count();
		$data['recordsTotal'] = $total;
		$data['recordsFiltered'] = $total;
		return $data;
	}

	/**
	* Get converted code to Readable ads and return as vendors rowset
	*
	* @param String $vendor_id Vendor ID
	* @return array Rowset of vendors
	*/
	public function get_vendor_ads($vendor_id=''){
		$db = \DB::select_array();
		if($vendor_id != ''){
			$db->where('vendor_id', '=', $vendor_id);
		}
		$result = $this->load($db, NULL)->as_array();
		return $result;
	}

	/**
	* get vendor logo
	*
	* @param string $vendor_id Vendor ID
	* @return string logo path
	*/
	public function get_vendor_logo($vendor_id){
		$vendor = $this->load(\DB::select()->where('vendor_id', '=', $vendor_id));
		$file = glob(\Model\Vendor::IMG_PATH.str_pad($vendor->id, 8, '0', STR_PAD_RIGHT).'.*');
		if(!empty($file) && is_readable(DOCROOT.current($file))){
			$logo = 'vendors/'.basename(current($file));
		} else {
			$logo = 'no-image.png';
		}
		return $logo;
	}

	public function get_locale($vendor_id){
		$data = $tmp = array();
		$this->load(\DB::select()->where('vendor_id', '=', $vendor_id));
		if($this->loaded()){
			$data = explode(';', $this->locale);
			foreach($data as $l){
				list($city, $province, $country) = explode(',', $l);
				$name = \CityState::get_area_name($city, $province, $country);
				$tmp[] = array(
					'name' => $name,
					'id' => $name
				);
			}
		}
		return $tmp;
	}

	public function resend_activation($vendor_id){
		$data = $this->load_by_id($vendor_id)->as_array();
		$data['pass'] = $data['foremail_numbers'] = $data['featured_status'] = $data['receipt_id'] = '';
		$data['type'] = explode(';', $data['type']);
		$data['locale'] = $this->areaid_to_advertise($data['locale']);
		return \Emails::forge()->welcome($data, $this->_class);
	}

	/**
	* Complete the signup and get set payment
	*
	*/
	public function signup($data, $payment) {
		$data['type'] = join(';',$data['type']);
		$data['locale'] = $this->advertise_to_areaid($data['locale']);
		list($city,$state,$country) = \CityState::get_area_id($payment['cpc_address'], true);
		list($payment['city'],$payment['state'],$payment['country']) = \CityState::get_area_name($city, $state, $country, true);
		$data['promotional_end'] = date_create()->format('Y-m-d');
		// Load plan info
		$plan = \Model\Plan::forge()->load_by_csp($country, $state, $payment['promo_code'], \Model\Plan::PT_VENDOR);
		$data['plan_id'] = $plan->plan_id;
		if ($plan->loaded() && ($plan->type_id == \Model\Plan::PT_VENDOR)) {
			$payment['plan_id'] = $plan->plan_id;
			$payment['email'] = $data['email'];
			$payment['start_date'] = date_create('+1 day')->format('Y-m-d\TH:i:s\Z');
			/* set promotional end if plan is : ACTIVE or SET and have promo_code txt */
			$plan_data = $plan->as_array();
			if($plan_data['promo_code'] != '' && $plan_data['promo_len'] > 0 && 
				($plan_data['status_id'] == \Model\Plan::S_ACTIVE || $plan_data['status_id'] == \Model\Plan::S_SET)) {
				$data['promotional_end'] = date_create('+'.$plan_data['promo_len'].' month')->format('Y-m-d');
			}
			try {
				$result = \Model\Vendor\Payment::forge()->vendor($payment);
				if ($result['status'] == 'OK') {
					$vendorid = \Sysconfig::create_string('vendor');
					$this->set_fields($data);
					$this->vendor_id = $vendorid;
					$this->account_creation = date_create()->format('Y-m-d H:i:s');
					$this->ip_address = \Input::ip();
					$this->password = \Authlite::instance('auth_vendor')->hash($data['password']);
					$this->paypal_sub_id = $result['id'];
					$this->activate = \Sysconfig::create_string('activate');
					$this->active_till = date_create('+30 days')->format('Y-m-d H:i:s');
					$this->last_login = date_create()->format('Y-m-d H:i:s');
					$this->paypal_fet_id = $vendorid; //paypal_fet_id have index key, set default to vendorid
					//$this->id = 0;
					$this->save();
					// Send welcome email
					$this->locale = $this->areaid_to_advertise($this->locale);
					$this->type = $this->typeid_to_services($this->type);

					// Create and save the order
					$order = new \Model\Order;
					$order->user_id = $this->id;
					$order->user_type_id = \Model\User::UT_VENDOR;
					$order->type_id = \Model\Order::OT_MONTHLY;
					$order->total = $plan->rate + $plan->rate_tax;
					$order->sub_total = $plan->rate;
					$order->taxes = $plan->rate_tax;
					$order->tax_code = \Model\Tax::forge()->load_by_country_state($country, $state)->code;
					$order->agreement_id = $this->paypal_sub_id;
					$order->order_date = date_create()->format('Y-m-d H:i:s');
					$order->status_id = \Model\Order::OS_COMPLETE;
					$order->save();
					$result = array('status' => 'OK', 'Conf_Num' => $this->id);
				} else {
					if (isset($result['message'])) {
						return array('status' => 'FAIL', 'result' => $result['message']);
					} else {
						return array('status' => 'FAIL', 'result' => $result['result']);
					}
				}
			} catch (\Exception $e) {
				\Sysconfig::logerror(realpath(dirname(__FILE__)), $e);
				return array('status' => 'FAIL', 'result' => $e->getMessage());
			}
		} else {
			\Sysconfig::logerror(__FILE__ . ' : ' , __LINE__, $payment);
			\Sysconfig::logerror(__FILE__ . ' : ' , __LINE__, $data);
			return array('status' => 'FAIL', 'result' => 'There was an error create this order');
		}

		//featured
		if ($data['featured'] == \Model\Vendor::VENDOR_STATUS_FEATURED) {
			$fplan = \Model\Plan::forge()->load_by_csp($country, $state, '', \Model\Plan::PT_VFEATURE);
			if($fplan->loaded() && ($fplan->type_id == \Model\Plan::PT_VFEATURE)) {
				$payment['plan_id'] = $fplan->plan_id;
				$payment['start_date'] = date_create('+1 day')->format('Y-m-d\TH:i:s\Z');
				try {
					$fresult = \Model\Vendor\Payment::forge()->featured_ad($payment);
					if($fresult['status'] == 'OK') {
						$this->load(\DB::select()->where('id', '=', $this->id));
						$this->featured = \Model\Vendor::VENDOR_STATUS_FEATURED;
						$this->paypal_fet_id = $fresult['id'];
						$this->plan2_id = $fplan->plan_id;
						$this->save();

						$order = new \Model\Order;
						$order->user_id = $this->id;
						$order->user_type_id = \Model\User::UT_VENDOR;
						$order->type_id = \Model\Order::OT_FEATURED;
						$order->total = $fplan->rate + $fplan->rate_tax;
						$order->sub_total = $fplan->rate;
						$order->taxes = $fplan->rate_tax;
						$order->tax_code = \Model\Tax::forge()->load_by_country_state($country, $state)->code;
						$order->agreement_id = $this->paypal_fet_id;
						$order->order_date = date_create()->format('Y-m-d H:i:s');
						$order->status_id = \Model\Order::OS_COMPLETE;
						$order->save();
					}

				} catch(\Exception $e){
					// TODO: log the error
					\Sysconfig::logerror(realpath(dirname(__FILE__)), $e);
					return array('status' => 'FAIL', 'result' => $e->getMessage());
				}
			}
		}
		\Emails::forge()->welcome($this->as_array(), 'vendor');
		return $result;
	}

}