<?php
namespace Model\Vendor;

class Payment extends \Model\Payment {

	public function vendor($post){
		$post['agreement_name'] = 'Vendor Monthly';
		$post['agreement_description'] = 'Vendor monthly payment agreement';
		return $this->agreement($post, 'vendor');
	}

	public function featured_ad($post) {
		$post['agreement_name'] = 'Vendor featured ad monthly';
		$post['agreement_description'] = 'Vendor featured ad monthly payment agreement';
		return $this->agreement($post, 'featured');
	}

	public function upgrade($post){
		\Lang::load('common');
		$response = array();
		list($city,$prov_state,$country) = \CityState::get_area_id($post['cpc_address'], true);
		list($post['city'],$post['state'],$post['country']) = \CityState::get_area_name($city, $prov_state, $country, true);
		$post['country'] = trim($country);
		//$post['start_date'] = date_create(null, new \DateTimeZone('UTC'))->format('Y-m-d\TH:i:s\Z');
		$post['start_date'] = date_create("+1 hour")->format('Y-m-d\TH:i:s\Z');
		$post['city'] = $city;
		$fplan = \Model\Plan::forge()->load_by_csp($country, $prov_state, '', \Model\Plan::PT_VFEATURE);
		$post['plan_id'] = $fplan->plan_id;
		try {
			$fresult = $this->featured_ad($post);
			if($fresult['status'] == 'OK') {
				$vendor = \Model\Vendor::forge($post['id']);
				$vendor->featured = \Model\Vendor::VENDOR_STATUS_FEATURED;
				$vendor->paypal_fet_id = $fresult['id'];
				$vendor->plan2_id = $fplan->plan_id;
				$vendor->save();

				$order = new \Model\Order;
				$order->user_id = $vendor->id;
				$order->user_type_id = \Model\User::UT_VENDOR;
				$order->type_id = \Model\Order::OT_FEATURED;
				$order->total = $fplan->rate + $fplan->rate_tax;
				$order->sub_total = $fplan->rate;
				$order->taxes = $fplan->rate_tax;
				$order->tax_code = \Model\Tax::forge()->load_by_country_state($country, $prov_state)->code;
				$order->agreement_id = $vendor->paypal_fet_id;
				$order->order_date = date_create()->format('Y-m-d H:i:s');
				$order->status_id = \Model\Order::OS_COMPLETE;
				$order->save();
				$response = array('status' => 'OK', 'Conf_Num' => $vendor->id);
			} else {
				if ($fresult['status'] == 'ERROR') {
					$response = array('status' => 'ERROR', 'message' => $fresult['message']);
				} else {
					$response = array('status' => 'ERROR', 'message' => $fresult['result']);
				}
			}
		} catch(\Exception $e){
			// TODO: log the error
			\Sysconfig::logerror(realpath(dirname(__FILE__)), $e);
			return array('status' => 'FAIL', 'result' => $e->getMessage());
		}
		return $response;
	}

	public function downgrade($vendor){
		\Lang::load('common');
		$result = new \Model\Paypalrest\Agreement();
		$paypal_result = $result->cancel(array('id' => $vendor->paypal_fet_id, 'note' => 'Downgrade featured advertisement'));
		if($paypal_result['status'] == 'success') {
			$model = \Model\Vendor::forge($vendor->id);
			$model->set_fields(array(
				'featured' => \Model\Vendor::VENDOR_STATUS_NON_FEATURED,
				'paypal_fet_id' => '',
				'plan2_id' => ''
			));
			$model->save();
			$response = array('status' => 'OK', 'Conf_Num' => $vendor->id);
		} else {
			$message = '<ul>'.$paypal_result['error'].'</ul>';
			$response = array('status' => 'ERROR', 'message' => $message);
		}
		return $response;
	}

	public function billing_update($data) {
		$vendor = \Model\Vendor::forge($data['id']);
		$data['paypal_sub_id'] = $vendor->paypal_sub_id;
		$data['note'] = 'Vendor monthly membership cancel - Update billing info';
		list($city,$state,$country) = \CityState::get_area_id($data['cpc_address'], true);
		list($data['city'],$data['state'],$data['country']) = \CityState::get_area_name($city, $state, $country, true);
		$plan = \Model\Plan::forge()->load_by_csp($country, $state, '', \Model\Plan::PT_VENDOR);
		$data['plan_id'] = $plan->plan_id;
		$result = parent::billing_update($data);
		$data['start_date'] = date_create()->format('Y-m-d\TH:i:s\Z');
		if($result['status'] == 'success') {
			$vendor->paypal_sub_id = '';
			$vendor->plan_id = '';
			$vendor->save(false);
			$result = $this->vendor($data);
			if($result['status'] == 'OK') {
				$vendor->paypal_sub_id = $result['id'];
				$vendor->plan_id = $plan->plan_id;
				$vendor->save(false);
				//override authlite session
				$auth_vendor = \Authlite::instance('auth_vendor');
				\Model\User::forge()->unset_user_type();
				$auth_vendor->logout();
				$auth_vendor->force_login($vendor->email);
				$result = array('status' => 'OK', 'Conf_Num' => $vendor->id);
			}
		}
		return $result;
	}

}