<?php
/**
* The staging database settings. These get merged with the global settings.
*/

return array(
	'default' => array(
		'type'           => 'mysqli',
		'connection'     => array(
			'hostname'       => '127.0.0.1',
			'port'           => '3306',
			'database'       => 'xtrade_dev',
			'username'       => 'root',
			'password'       => 'root1',
			'persistent'     => false,
			'compress'       => false,
		),
		'identifier'     => '`',
		'table_prefix'   => '',
		'charset'        => 'utf8',
		'enable_cache'   => true,
		'profiling'      => false,
		'readonly'       => false,
	),
);
