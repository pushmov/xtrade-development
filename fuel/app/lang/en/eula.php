<?php
return array(
	'title' => 'Terms and Conditions of Use<br>TradeHomes by iREX Inc.',
	'text' => '
	<h2>TradeHomes by iREX Inc.</h2>
	<h3>(TradeHomes or &quot; Company, we, our or us&quot;)</h3>
	<p><strong>By you choosing to use this Website, you agree to the following terms and conditions:</strong></p>
	<p>The content of the pages of this Website is subject to change without notice.</p>
	<p>	The information contained in this Website is presented in an <strong>as is</strong> capacity and without any representations or warranties express or implied, on our part. The information and materials are based on contributions from users and as such we do not provide any warranty or assurance as to its accuracy, performance, or suitability for any particular purpose. By using our Website you acknowledge that such information and materials may contain inaccuracies or errors and we expressly exclude liability for any such inaccuracies or errors to the extent permitted by law.</p>
	<p>Your use of any information or materials on this Website is entirely at your own risk. It shall be your responsibility to ensure that any products, services or information available through this Website meet your specific requirements.</p>
	<p>	TradeHomes offers third party vendors the opportunity to promote their products and services on this website and other materials that may be available to users of the website. TradeHomes does not take responsibility in any respect for those products or services including the quality or adequacy of such products or services for their intended purpose and is not in any way responsible for any consequences of the use of those products or services whatsoever.</p>
	<p>This Website contains material which belongs, or is licenced, to us including, the design, layout and graphics. Reproduction in whole or in part is prohibited.</p>
	<p>Any unauthorized use of this Website may give rise to a claim for damages and/or be an offence.
	This Website contains links to other websites. These links are provided for your convenience only. We take no responsibility for those websites and their content.</p>
	<p><strong>Your use of this Website and any dispute arising out of such use of the Website is subject to the rules, qualifications and conditions set out in this Website. You agree to waive any rights and to abide by such rules, qualifications and conditions or not use the Website.</strong></p>
	<p>Nothing in this Website is intended to constitute advice, legal or otherwise, and you are encouraged to consult a professional to provide you with appropriate advice.</p>
	<p>	We strive to ensure that information that you give us is secure. To limit unauthorized access or disclosure of your information we have in place physical and electronic procedures to secure the information that we collect. We will use your information to improve our service, for our recordkeeping, to send promotional emails about special offers, new products or to contact you for market research purposes.</p>
	<p>We will not sell or distribute your personal information to third parties unless you authorize us to do so, or unless we are required to do so by law or Court Order.</p>
	<p>DEFINITIONS:</p>
	<dl>
	<dd>"Company" means: <strong>TradeHomes</strong> by iREX Inc.</dd>
	<dd>"Contribution(s)" means: input from a user including postings on our Website.</dd>
	<dd>"Listing Content" means: text, data, content and images about property listed for sale received from users for posting on the Website.</dd>
	<dd>"Website" means this Website <br> by iRex Inc.</dd>
	</dl>
	<p><strong>If you use this Website in any capacity, you are agreeing to comply with and be bound by the terms herein.</strong></p>
	<p>	The term TradeHomes by iREX Inc. or "us" or "we" refers to the owners of this Website which is subject to copyright protection and is a registered entity that has patents pending.</p>
	<p>YOUR REPRESENTATIONS</p>
	<p>You represent that you are a licensed REALTOR&reg;, duly authorized as such under the laws of the location where you carry on your business.</p>
	<p>You represent and warrant that: all information and Contributions you submit are accurate; you will keep your password confidential and will be responsible for all use of your password and account; you are not a minor in the jurisdiction in which you reside; and your use of the Company\'s services or this Website does not violate any law.</p>
	<p>You will provide accurate and complete information and maintain and promptly update the registration data to keep it accurate and complete. If you provide any information that is inaccurate or incomplete, we have the right delete such information and terminate your account.</p>
	<p>You are responsible for the content of, and any harm resulting from any Contribution made by you. When you create or make available a Contribution, you thereby represent and warrant that:</p>
	<p>Your Contribution does not and will not infringe the rights of other parties, including but not limited to the copyright, patent, or trademark of any third party;</p>
	<p>You will immediately remove all listings and content for which you no longer have the authority to represent.</p>
	<p>You agree that neither we or this website are responsible for any success or failure of listings or transactions that you endeavor to conclude as a result of your use of this website. TradeHomes does not provide the services of a REALTOR&reg; or have any involvement with your customers or clients which you represent in your capacity as a licensed REALTOR&reg;.</p>
	<p>You are the owner of the Contribution or have the necessary permissions to use and to authorize Company and other website users to use your Contribution;</p>
	<p>Your Contribution does not contain any viruses, or other harmful or destructive content;</p>
	<p>	Your Contribution is not obscene, harassing or otherwise objectionable (as reasonably determined by Company), libelous or slanderous, does not advocate the violent overthrow of any government, does not incite physical harm against any person, does not violate any applicable law, regulation, or rule, and does not violate the privacy or publicity rights of any third party;</p>
	<p>Your Contribution does not contain material that solicits personal information from any minor and does not violate any federal, provincial or state law concerning child pornography or otherwise intended to protect the health or wellbeing of minors;</p>
	<p>If any third party has rights to intellectual property you create, you have either received their permission to make available the Contribution, or secured a waiver as to all rights to your Contribution;</p>
	<p>Your Contribution does not attempt to install or promote spyware, malware or other computer code, whether on Company’s or others’ computers or equipment, designed to enable you or others to gather information about or monitor the online or other activities of another party;</p>
	<p>You grant Company the right to display your Contribution to other users who visit the Website and to use the information and data from your Contribution and permit other websites displaying information, to include links to enable visitors to such websites to search and view your Contribution. You further grant to the Company an irrevocable worldwide license to use, copy, perform, publicly display, and distribute such Contribution for any purpose, on or in connection with the Website or the promotion of such Contribution, and to grant and authorize sublicenses.</p>
	<p>You agree that, if you are not the owner of the property forming part of your Contribution, you will not permit the posting of a property on the Website under a name other than the agent that has been legally engaged by the owner to market the property under the terms of an agreement with the owner.</p>
	<p>Your business or personal relations with users viewing your Contribution on the Website is solely between you and such users. Company shall not be responsible or liable for any loss or damage of any sort incurred as the result of any dealings between you and a user viewing your Contribution on the Website or contracting with you.</p>
	<p>You retain full ownership of all of your Contributions and any intellectual property rights or other proprietary rights associated with your Contributions.</p>
	<p><strong>Company has the right, in our discretion, to delete any Contributions that are determined by us to be inappropriate or otherwise in violation of this Agreement.</strong></p>
	<p>THE COMPANY’S SERVICES PROVIDED THROUGH THIS WEBSITE DO NOT CONSTITUTE PROFESSIONAL ADVICE.AND USERS SHOULD CONSULT THEIR OWN PROFESSIONAL ADVISORS.</p>
	<p>	WE RESERVE THE RIGHT, IN OUR SOLE DISCRETION, WITHOUT NOTICE, TO DENY ACCESS AND USE OF THIS WEBSITE TO ANY PERSON,.FOR ANY REASON, INCLUDING FOR BREACH OF ANY REPRESENTATION, WARRANTY OR COVENANT CONTAINED IN THIS AGREEMENT, AND WE MAY TERMINATE ANY USER’S PARTICIPATION IN THIS WEBSITE AND ANY OF THE COMPANY’S SERVICES, DELETE ANY PROFILE AND ANY CONTENT OR INFORMATION THAT HAS BEEN POSTED. THESE RIGHTS MAY BE EXECISED BY US WITHOUT ANY OBLIGATION OR LIABILITY ON OUR PART TO ANY USER.</p>
	<p>	Company cannot control the Contributions that may be posted on the Website. Company does not endorse any Contributions or other content available on the Website or make any representations as to its accuracy. Company is not responsible for unlawful or otherwise objectionable content that may be on the Website or in connection with any Contributions by Users</p>
	<p>	Any use of this Website is at your own risk, and you should consult with appropriate professional advisors prior to making an investment decision. We do not have any responsibility for any consequential loss or cost relating to any action or inaction that may be taken based on this Website.</p>
	<p>	We reserve the right to change any and all content, software and other items used or contained in the Website and any of the Company’s services offered through the Website at any time without notice.</p>
	<p><strong>LIMITATIONS OF LIABILITY</strong></p>
	<p>	IN NO EVENT SHALL COMPANY OR ITS DIRECTORS, EMPLOYEES, OR AGENTS BE LIABLE TO YOU OR ANY THIRD PARTY FOR ANY INDIRECT, CONSEQUENTIAL, EXEMPLARY, INCIDENTAL, SPECIAL OR PUNITIVE DAMAGES, INCLUDING LOST PROFIT DAMAGES ARISING FROM YOUR USE OF THE WEBSITE OR COMPANY SERVICES, EVEN IF COMPANY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.</p>
	<p><strong>INDEMNITY</strong></p>
	<p><strong>You will defend, indemnify and hold Company, its subsidiaries, and affiliates, and their respective officers, agents, partners and employees, harmless from and against, any loss, damage, liability, claim, or demand, including reasonable legal fees and expenses, made by any third party due to or arising out of your Contributions, use of the Company’s services and/or arising from a breach of this Agreement and any breach of your representations and warranties.</strong></p>
	<p>Notwithstanding the foregoing, Company reserves the option,( at its expense related to the additional costs of assuming the exclusive defence and control including the costs of its lawyers, from the date of given you notice of the exercise of such option), to assume the defence and control of any matter for which you are required to indemnify Company, and you agree to cooperate, at your expense, with Company’s defence of such claims. Company will use reasonable efforts to notify you of any such claim, action, or proceeding which is subject to this indemnification upon becoming aware of it.</p>
	',
	'advertiser_body' => '
	<h2>Advertisers Terms & Conditions<br>
	TradeHomes by iREX Inc.</h2>
	<h3>TradeHomes by iREX Inc. (&quot;TradeHomes&quot;)</h3>
	<p><strong>By you choosing to use this Website, you agree to the following terms and conditions:</strong></p>
	<p><strong>No warranties or representations</strong></p>
	    <p>
      This website is provided "<strong>as is</strong>" without any representations or warranties,
      express or implied. <strong>TradeHomes</strong> makes no representations or warranties in
      relation to this website or the information and materials provided on this website. By using
      this website, you implicitly agree to the following.
    </p>
    <p>
      The information contained in this website has been "user provided", and while
      <strong>TradeHomes</strong> endeavors to keep the information up to date and correct,
      <strong>TradeHomes</strong> makes no representations or warranties of any kind, express or
      implied, about the completeness, accuracy, reliability, suitability or availability with
      respect to the website or the information, products, services, or related graphics contained
      on the website for any purpose. Any reliance you place on such information is therefore
      strictly at your own risk.
    </p>
    <p>
      Without prejudice to the generality of the foregoing paragraph, <strong>TradeHomes</strong>
      does not warrant that:
    </p>
    <ul>
      <li>this website will be constantly available, or available at all; or</li>
      <li>the information on this website is complete, true, accurate or non-misleading.</li>
      <li>Every effort is made to keep the website up and running smoothly. However,<strong>TradeHomes</strong> takes no responsibility for, and will not be liable for, the website being temporarily unavailable due to technical issues beyond our control.</li>
    </ul>
    <p>
      Nothing on this website constitutes, or is meant to constitute, advice of any kind. If you
      require advice in relation to any [legal, financial or medical] matter you should consult an
      appropriate professional.
    </p>
    <p>
      <strong>Limitations of liability</strong>
    </p>
    <p>
      <strong>TradeHomes</strong> will not be liable to you (whether under the law of contact, the
      law of torts or otherwise) in relation to the contents of, or use of, or otherwise in
      connection with, this website:
    </p>
    <p>
      In no event will we be liable for any loss or damage including without limitation, indirect
      or consequential loss or damage, or any loss or damage whatsoever arising from loss of data
      or profits arising out of, or in connection with, the use of this website.
    </p>
    <ul>
      <li>to the extent that the website is provided at a nominal user-fee, for any direct loss;</li>
      <li>for any indirect, special or consequential loss; or</li>
      <li>for any business losses, loss of revenue, income, profits or anticipated savings, loss of
      contracts or business relationships, loss of reputation or goodwill, or loss or corruption of
      information or data.</li>
      <li>Through this website if you are able to link to other websites which are not under the
      control of <strong>TradeHomes</strong>. We have no control over the nature, content and
      availability of those sites. The inclusion of any links does not necessarily imply a
      recommendation or endorse the views expressed within them.</li>
    </ul>
    <p>
      These limitations of liability apply even if <strong>TradeHomes</strong> has been expressly
      advised of the potential loss.
    </p>
    <p>
      <strong>Exceptions</strong>
    </p>
    <p>
      Nothing in this website disclaimer will exclude or limit any warranty implied by law that it
      would be unlawful to exclude or limit; and nothing in this website disclaimer will exclude or
      limit <strong>TradeHomes</strong>’s liability in respect of any:
    </p>
    <ul>
      <li>death or personal injury caused by <strong>TradeHomes</strong>’s negligence;
      </li>
      <li>fraud or fraudulent misrepresentation on the part of <strong>TradeHomes</strong>; or
      </li>
      <li>matter which it would be illegal or unlawful for <strong>TradeHomes</strong> to exclude
      or limit, or to attempt or purport to exclude or limit, its liability.
      </li>
    </ul>
    <p>
      <strong>Reasonableness</strong>
    </p>
    <p>
      By using this website, you agree that the exclusions and limitations of liability set out in
      this website disclaimer are reasonable.
    </p>
    <p>
      If you do not think they are reasonable, you must not use this website.
    </p>
    <p>
      <strong>Other parties</strong>
    </p>
    <p>
      You accept that, as a limited liability entity, <strong>TradeHomes</strong> has an interest
      in limiting the personal liability of its officers and employees. You agree that you will not
      bring any claim personally against <strong>TradeHomes</strong> officers or employees in
      respect of any losses you suffer in connection with the website.
    </p>
    <p>
      Without prejudice to the foregoing paragraph, you agree that the limitations of warranties
      and liability set out in this website disclaimer will protect <strong>TradeHomes</strong>’s
      officers, employees, agents, subsidiaries, successors, assigns and sub-contractors as well as
      <strong>TradeHomes</strong>.
    </p>
    <p>
      <strong>No warranties</strong>
    </p>
    <p>
      This website is provided “as is” without any representations or warranties, express or
      implied. <strong>TradeHomes by iRex</strong> makes no representations or warranties in
      relation to this website or the information and materials provided on this website.
    </p>
    <p>
      The information contained in this website has been “user provided”, and while
      <strong>TradeHomes by iRex</strong> endeavors to keep the information up to date and correct,
      <strong>TradeHomes by iRex</strong> makes no representations or warranties of any kind,
      express or implied, about the completeness, accuracy, reliability, suitability or
      availability with respect to the website or the information, products, services, or related
      graphics contained on the website for any purpose. Any reliance you place on such information
      is therefore strictly at your own risk.
    </p>
    <p>
      Without prejudice to the generality of the foregoing paragraph, <strong>TradeHomes by
      iRex</strong> does not warrant that:
    </p>
    <ul>
      <li>this website will be constantly available, or available at all; or
      </li>
      <li>the information on this website is complete, true, accurate or non-misleading.
      </li>
      <li>Every effort is made to keep the website up and running smoothly. However,
      <strong>TradeHomes</strong> takes no responsibility for, and will not be liable for, the
      website being temporarily unavailable due to technical issues beyond our control.
      </li>
    </ul>
    <p>
      Nothing on this website constitutes, or is meant to constitute, advice of any kind. If you
      require advice in relation to any [legal, financial or medical] matter you should consult an
      appropriate professional.
    </p>
    <p>
      <strong>Limitations of liability</strong>
    </p>
    <p>
      <strong>TradeHomes</strong> will not be liable to you (whether under the law of contact, the
      law of torts or otherwise) in relation to the contents of, or use of, or otherwise in
      connection with, this website:
    </p>
    <p>
      In no event will we be liable for any loss or damage including without limitation, indirect
      or consequential loss or damage, or any loss or damage whatsoever arising from loss of data
      or profits arising out of, or in connection with, the use of this website.
    </p>
    <ul>
      <li>to the extent that the website is provided at a nominal user-fee, for any direct loss;
      </li>
      <li>for any indirect, special or consequential loss; or
      </li>
      <li>for any business losses, loss of revenue, income, profits or anticipated savings, loss of
      contracts or business relationships, loss of reputation or goodwill, or loss or corruption of
      information or data.
        <p>
      </li>
      <li>Through this website if you are able to link to other websites which are not under the
      control of <strong>TradeHomes</strong>. We have no control over the nature, content and
      availability of those sites. The inclusion of any links does not necessarily imply a
      recommendation or endorse the views expressed within them.
      </li>
    </ul>
    <p>
      These limitations of liability apply even if <strong>TradeHomes</strong> has been expressly
      advised of the potential loss.
    </p>
    <p>
      <strong>Exceptions</strong>
    </p>
    <p>
      Nothing in this website disclaimer will exclude or limit any warranty implied by law that it
      would be unlawful to exclude or limit; and nothing in this website disclaimer will exclude or
      limit <strong>TradeHomes</strong>’s liability in respect of any:
    </p>
    <ul>
      <li>death or personal injury caused by <strong>TradeHomes</strong>’s negligence;
      </li>
      <li>fraud or fraudulent misrepresentation on the part of <strong>TradeHomes</strong>; or
      </li>
      <li>matter which it would be illegal or unlawful for <strong>TradeHomes</strong> to exclude
      or limit, or to attempt or purport to exclude or limit, its liability.
      </li>
    </ul>
    <p>
      Reasonableness
    </p>
    <p>
      By using this website, you agree that the exclusions and limitations of liability set out in
      this website disclaimer are reasonable.
    </p>
    <p>
      If you do not think they are reasonable, you must not use this website.
    </p>
    <p>
      Other parties
    </p>
    <p></p>
      You accept that, as a limited liability entity, <strong>TradeHomes</strong> has an interest
      in limiting the personal liability of its officers and employees. You agree that you will not
      bring any claim personally against <strong>TradeHomes</strong> officers or employees in
      respect of any losses you suffer in connection with the website.
    </p>
    <p>
      Without prejudice to the foregoing paragraph, you agree that the limitations of warranties
      and liability set out in this website disclaimer will protect <strong>TradeHomes</strong>’s
      officers, employees, agents, subsidiaries, successors, assigns and sub-contractors as well as
      <strong>TradeHomes</strong>.
    </p>

	',
);
