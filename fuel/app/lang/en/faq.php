<?php
return array(
	'title' => 'xTradeHomes Frequently Asked Questions',
	'header' => 'Frequently Asked Questions',
	'q1' => 'What is an xTrade&trade;?',
	'a1' => 'A computerized identification process exclusively created by xTradeHomes.com to assist a REALTOR&reg; and their seller in locating another seller to trade properties with, often involving multiple sellers, each of whom obtains exactly the property they desire and at the same time - selling their property simultaneously to another seller.',
	'q2' => 'What is xTradeHomes?',
	'a2' => 'It is a powerful computer program that makes it possible for real estate agents to bring up to five sellers of residential properties together to trade homes with, whether it\'s down the street, across town or around Canada or the US. xTradeHomes works because most sellers are also in the market for a new home.',
	'q3' => 'What is the scope of xTradeHomes?',
	'a3' => 'Unlike Property Listing Services, which is regional, xTradeHomes will include most residential listings in Canada and the US. Once the site is sufficiently propagated, every real estate professional will have access to just about every listing in Canada and the US. ',
	'q3a' => 'What type of Real Estate qualifies for publication on xTradeHomes.com?',
	'a3a' => 'They would include all residential properties such as: all single family, duplex, condominium, town/row housing, apartment suites, residential strata units, and acreages (with a dwelling). ',
	'q4' => 'How do I get started?',
	'a4' => 'Once you (or your company) joins the xTradeHomes network, you just logon and set up your real estate professional account. Then you enter the information about your property on the xTradeHomes computer forms: What My Client Has and What my Client Wants. The xTradeHomes program does the rest. The more information you provide, the better the system works.',
	'q5' => 'How long does it take to find an xTRADE&trade;?',
	'a5' => 'It depends on a number of variables, the most important of which is the information you provide about the house your client is selling and the description of the new home your client wants to buy. Some transactions will be quick - some  may take a little longer. New properties are added all the time and as xTRADE&trade; members complete their Clients Wants the system finds a match and notifies you automatically.',
	'q6' => 'How do I know when xTradeHomes.com finds a matching xTrade?',
	'a6' => 'xTradeHomes automatically emails you all the information about the matching property and who the other real estate listing agent is. The other agent receives the same email. If the potential xTrade&trade; involves more than two properties, all agents will receive the notification.',
	'q7' => 'How does xTradeHomes facilitate out-of-town xTRADES&trade;?',
	'a7' => 'TradeHomes will match your client’s property with near-perfect properties anywhere north of Mexico as easily as if they were on the same block in the same town. It’s seamless from city to city or country to country.',
	'q8' => 'How is xTradeHomes different from a typical MLS&reg; board?',
	'a8' => 'Think of xTradeHomes as a giant MLS&reg; covering all of North America. Instead of dealing with hundreds of MLS&reg; boards, which can be impossible, you deal only with xTradeHomes.',
	'q9' => 'How much does xTradeHomes cost?',
	'a9' => '<div align="center">' . \Html::anchor('signup', 'Click here for our special discounts.') . '</div>',
	'q10' => 'How long does it take to enter property details into the system?',
	'a10' => 'Usually, 10 minutes or less. It\'s very easy. We know how busy you are so, to make things easier and with your approval, your listings can be uploaded directly to xTradeHomes from CREA.',
	'q11' => 'Can Buyers or Buyers\' Agents benefit using xTradeHomes.com?',
	'a11' => 'Yes! A Property Search on the main site will show all listings in a wide range of neighbourhoods and cities across the country. Register your buyer and xTradeHomes will notify you of any matches. All buyers and buyers’ agents will have access to all listings on the site whether they are marketed for xTrading or just listed for sale.',
	'q12' => 'How do I get a deposit from my sellers for their new homes when they do not yet have the cash from the sale of their current homes?',
	'a12' => 'There are numerous ways to handle this including a simple personal loan or an “Assignment of Funds” in which the property owner fills out a document to state how much equity will be assigned in good faith from the property being offered to the property being xTRADED&trade;. Your local MLS&reg; Board or a real estate lawyer will have the form.',
	'q13' => 'Can For Sale by Owner sellers access xTradeHomes?',
	'a13' => 'They can perform property searches just like anyone else but, they cannot accomplish an xTRADE&trade;. xTradeHomes is intended for real estate professionals. When a FSBO (For Sale By Owner) or any other potential seller understands the scope of xTrading&trade; they will, in all likelihood, call a real estate professional to assist them with their property needs. This makes xTradeHomes both an excellent lead generator and listing tool. This is a win win for the seller and real estate professional.
	<br>There is no charge for leads provided by xTradeHomes.',
	'q14' => 'How is xTradeHomes different from other real estate sites?',
	'a14' => 'TradeHomes was created by real estate professionals for real estate professionals. We simply show you how to xTRADE&trade; your residential real estate listings quickly, efficiently and inexpensively around North America. No other real estate site can do this.',
	'q15' => 'Are there any tax benefits in the United States?',
	'a15' => 'For straight xTRADES&trade; that occur entirely in the US, there may be significant tax benefits under section 1031 in the Tax code. Consult a tax attorney or an accountant.',
	'q16' => 'What do I do when a property does not have a traditional  street address?',
	'a16' => 'Just describe the property in the Comments Section and reference the closest cross streets or highways.',
	'q17' => 'Required "SUBJECT TO" for all xTraded properties',
	'a17' => "
This offer is subject to __ seller of ___  address ____ purchasing address on or before  X date.<br>
For example, Smith owns 10 Fox Drive and Jones owns 25 Cherry Lane. Each is looking to purchase the other’s property by December 1st/2016 with a future closing date of January 15, 2017. The required &quot;subject to&quot; on each offer would read:<br>
For Smith offer on Jones:<br>
Subject to Jones purchasing 10 Fox Drive on or before  December 1st, 2016.<br>
For Jones offer on Smith:<br>
Subject to Smith purchasing 25 Cherry Lane on or before December 1st, 2016.<br>
This &quot;subject to&quot; should be on all xTraded properties and the &quot;subject to&quot; removal dates must match. This will ensure that neither party ends up owning two properties. Any other typical &quot;subject to&quot; conditions may also be used.",
	'q18' => 'Can xTradeHomes handle one-way transactions?',
	'a18' => 'Yes! All you do is enter the details about the home your client has for sale … or about the property your client wants to buy. One-way sales are, of course, traditional straight sales with no xTRADES&trade; involved.',
	'q19' => 'How do Real Estate Agents get paid?',
	'a19' => 'TradeHomes is not responsible for arranging commissions. Commission fees can vary across the USA and Canada. For your protection, we recommend that you arrange for the distribution of fees in a formal agreement. Each end of an xTRADE&trade; should be treated as a &quot;normal&quot; real estate transaction with commissions paid in the normal manner as laid out in your contract with your client.',
	'q20' => 'What is the difference between a &quot;Registered Buyer&quot; and a &quot;casual property search&quot;?',
	'a20' => "When doing a &quot;casual property search&quot;, all the property information pertaining to the property search is lost when the session is finished. When a &quot;Buyer's Agent&quot; registers a buyer on the site, the program continually searches for the property(s) even after the session is finished. Only the Agent can remove or edit the client information. A complete history and record of the client's needs will be securely stored for the Agent.",
	'q21' => 'Can the general public access xTradeHomes?',
	'a21' => 'They can see parts of it. xTradeHomes is intended for real estate professionals but the public can search properties. When they understand the scope of the site, they will, in all likelihood call a Real Estate Professional regarding their own properties. This makes xTradeHomes both an excellent lead-generator and listing tool. There are no charges to the REALTOR&reg; for leads provided by xTradeHomes.',
	'q22' => 'What if I have more questions?',
	'a22' => '<div align="center">' . \Html::anchor('contact', 'Click here to Contact Us') . '</div>',
);
