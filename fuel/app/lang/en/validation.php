<?php

return array(
	'required'        => ':label is required.',
	'min_length'      => ':label requires at least :param:1 characters.',
	'max_length'      => ':label may not contain more than :param:1 characters.',
	'exact_length'    => ':label must contain exactly :param:1 characters.',
	'match_value'     => ':label must contain the value :param:1.',
	'match_pattern'   => ':label must match the pattern :param:1.',
	'match_field'     => ':label must match :param:1.',
	'valid_email'     => ':label must contain a valid email address.',
	'valid_emails'    => ':label must contain a list of valid email addresses.',
	'valid_url'       => ':label must contain a valid URL.',
	'valid_domain'    => ':label must contain a valid URL.',
	'valid_ip'        => ':label must contain a valid IP address.',
	'numeric_min'     => 'The minimum numeric value of :label must be :param:1',
	'numeric_max'     => 'The maximum numeric value of :label must be :param:1',
	'numeric_between' => ':label must contain a numeric value between :param:1 and :param:2',
	'valid_string'    => 'The valid string rule :rule(:param:1) failed for field :label',
	'required_with'   => ':label must contain a value if :param:1 contains a value.',
	'valid_date'      => ':label must contain a valid formatted date.',
	'valid_phone'	  => ':label must be a valid phone number',
	'unique_email'	  => ':label already exists',
	'unique_code'	  => ':label already used',
	'unique_plan'	  => ':label with existing data for country, state, type and (optional) promo_code already exist'
);
