TIMESTAMP=2016%2d03%2d25T09%3a11%3a45Z&CORRELATIONID=8ca5769c94b41&ACK=Failure&VERSION=112%2e0&BUILD=18316154&L_ERRORCODE0=10413&L_SHORTMESSAGE0=Transaction%20refused%20because%20of%20an%20invalid%20argument%2e%20See%20additional%20error%20messages%20for%20details%2e&L_LONGMESSAGE0=The%20totals%20of%20the%20cart%20item%20amounts%20do%20not%20match%20order%20amounts%2e&L_SEVERITYCODE0=Error&AMT=1%2e50&CURRENCYCODE=CADArray
(
    [TIMESTAMP] => 2016-03-25T09:11:45Z
    [CORRELATIONID] => 8ca5769c94b41
    [ACK] => Failure
    [VERSION] => 112.0
    [BUILD] => 18316154
    [L_ERRORCODE0] => 10413
    [L_SHORTMESSAGE0] => Transaction refused because of an invalid argument. See additional error messages for details.
    [L_LONGMESSAGE0] => The totals of the cart item amounts do not match order amounts.
    [L_SEVERITYCODE0] => Error
    [AMT] => 1.50
    [CURRENCYCODE] => CAD
)
