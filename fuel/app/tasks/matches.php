<?php
namespace Fuel\Tasks;

class Matches {
	public function match_array(){
		$listings = \Model\Client\Has::forge()->load(\DB::select_array()->where('listing_status', '=', \Model\Listing::LS_ACTIVE)
			->where('list_as_sold', '=', \Model\Listing::AS_NOT_SOLD)
			->where('completed_section', '=', 1), null)
		->as_array();
		$from_cron = true;
		$match_obj = \Match::forge();
		$content = array();
		$match_array = array();
		foreach($listings as $row){
			$matches = $match_obj->find_all_way_matches($row['listing_id'], $from_cron);
			$this_listing_box = $match_obj->get_current_listing_box($row['listing_id'], $row['type_of_listing'], true, $from_cron);

			$found_matches = 0;
			$buyers_match = $x_1_match_high = $x_1_match_med = $x_1_match_low = $y_1_match_high
			= $y_1_match_low = $y_1_match_med = $z_1_match_high
			= $z_1_match_med = $z_1_match_low = array();

			$listing_id = $row['listing_id'];
			$realtor_id = $row['realtor_id'];
			$match_array[$realtor_id][$listing_id] = array();

			/** buyer match */
			if($row['type_of_listing'] == \Model\Listing::TYPE_BUY_ONLY){
				if(!empty($matches['desirable_all'])){
					foreach($matches['desirable_all'] as $value){
						$buyers_match[] = $match_obj->setup_box_information_email($value, 'buyer_match', null, false, $from_cron);
						$found_matches++;
					}
				}
			} else {

				if(!empty($GLOBALS["Want_Each_Other_IDs_exact_high"])){
					foreach($GLOBALS["Want_Each_Other_IDs_exact_high"] as $value){
						$x_1_match_high[] = $match_obj->setup_box_information_email($value, 'B', null, false, $from_cron);
						$found_matches++;
					}
				}

				if(!empty($GLOBALS["Want_Each_Other_IDs_exact_med"]) && $found_matches <= 10){
					foreach($GLOBALS["Want_Each_Other_IDs_exact_med"] as $value){
						$x_1_match_med[] = $match_obj->setup_box_information_email($value, 'B', null, false, $from_cron);
						$found_matches++;
					}
				}

				if(!empty($GLOBALS["Want_Each_Other_IDs_exact_low"]) && $found_matches <= 10){
					foreach($GLOBALS["Want_Each_Other_IDs_exact_low"] as $value){
						$x_1_match_low[] = $match_obj->setup_box_information_email($value, 'B', null, false, $from_cron);
						$found_matches++;
					}
				}

				//high

				if(!empty($GLOBALS["Want_Each_Other_IDs_high_high"]) && $found_matches <= 10){
					foreach($GLOBALS["Want_Each_Other_IDs_high_high"] as $value){
						$y_1_match_high[] = $match_obj->setup_box_information_email($value, 'B', null, false, $from_cron);
						$found_matches++;
					}
				}

				if(!empty($GLOBALS["Want_Each_Other_IDs_high_med"]) && $found_matches <= 10){
					foreach($GLOBALS["Want_Each_Other_IDs_high_med"] as $value){
						$y_1_match_med[] = $match_obj->setup_box_information_email($value, 'B', null, false, $from_cron);
						$found_matches++;
					}
				}

				if(!empty($GLOBALS["Want_Each_Other_IDs_high_low"]) && $found_matches <= 10){
					foreach($GLOBALS["Want_Each_Other_IDs_high_low"] as $value){
						$y_1_match_low[] = $match_obj->setup_box_information_email($value, 'B', null, false, $from_cron);
						$found_matches++;
					}
				}

				//medium

				if(!empty($GLOBALS["Want_Each_Other_IDs_med_high"]) && $found_matches <= 10){
					foreach($GLOBALS["Want_Each_Other_IDs_med_high"] as $value){
						$z_1_match_high[] = $match_obj->setup_box_information_email($value, 'B', null, false, $from_cron);
						$found_matches++;
					}
				}

				if(!empty($GLOBALS["Want_Each_Other_IDs_med_med"]) && $found_matches <= 10){
					foreach($GLOBALS["Want_Each_Other_IDs_med_med"] as $value){
						$z_1_match_med[] = $match_obj->setup_box_information_email($value, 'B', null, false, $from_cron);
						$found_matches++;
					}
				}


				if(!empty($GLOBALS["Want_Each_Other_IDs_med_low"]) && $found_matches <= 10){
					foreach($GLOBALS["Want_Each_Other_IDs_med_low"] as $value){
						$z_1_match_low[] = $match_obj->setup_box_information_email($value, 'B', null, false, $from_cron);
						$found_matches++;
					}
				}
			}

			$buyers_matches_found = '';
			if(sizeof($buyers_match) > 0){
				$view = \View::forge('email/matches_buyer');
				$view->this_listing_box = $this_listing_box;
				$view->buyers_match = $buyers_match;
				$buyers_matches_found = $view;
			}
			//exact

			$direct_matches_found = '';
			if(sizeof($x_1_match_high) > 0){
				$view = \View::forge('email/matches_2way');
				$view->this_listing_box = $this_listing_box;
				$view->data = $x_1_match_high;
				$direct_matches_found = $view;
			}

			if(sizeof($x_1_match_med) > 0){
				$view = \View::forge('email/matches_2way');
				$view->this_listing_box = $this_listing_box;
				$view->data = $x_1_match_med;
				$direct_matches_found = $view;
			}

			if(sizeof($x_1_match_low) > 0){
				$view = \View::forge('email/matches_2way');
				$view->this_listing_box = $this_listing_box;
				$view->data = $x_1_match_low;
				$direct_matches_found = $view;
			}

			//high
			if(sizeof($y_1_match_high) > 0){
				$view = \View::forge('email/matches_2way');
				$view->this_listing_box = $this_listing_box;
				$view->data = $y_1_match_high;
				$direct_matches_found = $view;
			}

			if(sizeof($y_1_match_med) > 0){
				$view = \View::forge('email/matches_2way');
				$view->this_listing_box = $this_listing_box;
				$view->data = $y_1_match_med;
				$direct_matches_found = $view;
			}

			if(sizeof($y_1_match_low) > 0){
				$view = \View::forge('email/matches_2way');
				$view->this_listing_box = $this_listing_box;
				$view->data = $y_1_match_low;
				$direct_matches_found = $view;
			}

			//medium
			if(sizeof($z_1_match_high) > 0){
				$view = \View::forge('email/matches_2way');
				$view->this_listing_box = $this_listing_box;
				$view->data = $z_1_match_high;
				$direct_matches_found = $view;
			}

			if(sizeof($z_1_match_med) > 0){
				$view = \View::forge('email/matches_2way');
				$view->this_listing_box = $this_listing_box;
				$view->data = $z_1_match_med;
				$direct_matches_found = $view;
			}

			if(sizeof($z_1_match_low) > 0){
				$view = \View::forge('email/matches_2way');
				$view->this_listing_box = $this_listing_box;
				$view->data = $z_1_match_low;
				$direct_matches_found = $view;
			}

			$three_way_matches_found = '';
			if($matches['x_2_match']){
				$view = \View::forge('email/matches_3way_template');
				$view->this_listing_box = $this_listing_box;
				$view->x_2_match = $matches['x_2_match'];
				$three_way_matches_found .= $view;
			}

			$four_way_matches_found = '';
			if($matches['x_3_match']){
				$view = \View::forge('email/matches_4way_template');
				$view->this_listing_box = $this_listing_box;
				$view->x_3_match = $matches['x_3_match'];
				$four_way_matches_found = $view;
			}

			$five_way_matches_found = '';
			if($matches['x_4_match']){
				$view = \View::forge('email/matches_5way_template');
				$view->this_listing_box = $this_listing_box;
				$view->x_4_match = $matches['x_4_match'];
				$five_way_matches_found = $view;
			}


			if($matches['found_matches'] > 0){
				$content = '<h2>*** Matches For Listing: ' . $row['listing_id'] . ' ***</h2>';
				$content .= $buyers_matches_found;
				$content .= $direct_matches_found;
				$content .= $three_way_matches_found;
				$content .= $four_way_matches_found;
				$content .= $five_way_matches_found;
				$match_array[$realtor_id][$listing_id] = $content;
			}
		}
		return $match_array;
	}

	public function run(){
		set_time_limit(0);
		$match_array = $this->match_array();
		foreach($match_array as $key => $value){
			$match_info = '';
			$realtor = \Model\Realtor::forge()->load(\DB::select_array()->where('realtor_id', '=', $key));
			$email_type = $realtor->email_type;
			foreach($value as $value_2){
				if($value_2){
					$match_info .= $value_2;
				}
			}
			if($match_info != ''){
				$emaildata = array(
					'rname' => $realtor->first_name.' '.$realtor->last_name,
					'phone_number' => \Sysconfig::format_phone_number($realtor->phone),
					'remail' => $realtor->email,
					'cname' => $realtor->company_name,
					'match_info' => $match_info,
					'mailto' => $realtor->email,
					'mailtoname' => $realtor->first_name.' '.$realtor->last_name
				);

				if (($realtor->alerts == \Model\User::ALERT_DAILY) && ($realtor->last_email_date < date_create('-24 hours')->format(DATETIME_FORMAT_DB))){
					$response = \Emails::forge()->email_matches_cron($emaildata);
					if($response['status'] == 'OK') {
						\DB::update('users')->set(array('last_email_date' => date_create()->format(DATETIME_FORMAT_DB)))->where('realtor_id', '=', $realtor->realtor_id)->execute();
					}
				} elseif (($realtor->alerts == \Model\User::ALERT_WEEKLY) && ($realtor->last_email_date < date_create('-7 days')->format(DATETIME_FORMAT_DB))){
					$response = \Emails::forge()->email_matches_cron($emaildata);
					if($response['status'] == 'OK') {
						\DB::update('users')->set(array('last_email_date' => date_create()->format(DATETIME_FORMAT_DB)))->where('realtor_id', '=', $realtor->realtor_id)->execute();
					}
				}
			}
		}

		echo 'Matching cron task completed !';
		exit();
	}
}