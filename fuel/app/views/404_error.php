<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Page Not Found</title>
	<?=\Asset::css('app.css');?>
</head>
<body>
	<header>
		<div class="row">
			<?=\Html::anchor('/', \Asset::img('hd_logo.png', array('alt' => 'Xtrade Homes', 'class' => 'left')));?>
		</div>
	</header>
	<div class="container">
		<div class="row">
			<div class="small-12 column">
				<h1><small>We can't find that!</small></h1>
				<hr>
				<p><?=\Html::anchor('/', 'Click here');?> for our home page.</p>
			</div>
		</div>
		<footer>
		</footer>
	</div>
</body>
</html>
