<div class="content column">
	<div class="row about-top">
		<div class="small-12 medium-11 large-10 small-centered column">
			<?=\Asset::img('large_shuffle.png', array('alt' => 'xTrade'));?>
			<h1>xTrades - The Evolution of Real Estate</h1>
			<h4>How it all started</h4>
			<p>We thought we’d spare you the classic rags to riches story of our real estate sales careers—how for years we sacrificed to make a living in this business--going without vacations and free weekends as we climbed our way up the steep ladder of success!</p>
			<p>Instead we will tell you that as experienced real estate professionals who toughed it out in the hard times, and made it through to the good times, we learned a few lessons along the way about how to be successful in the real estate business.  We are an independent company with decades of experience in the Canadian real estate market.</p>
			<p>The biggest lesson we learned was this: Before you can succeed, you have to make sure that your client succeeds ...you know as well as we do that it's anything but simple. Yet there are some methods that can really give you a leg up and make things a whole lot easier to get a deal done, and make it a good deal.</p>
			<p>Over the years we developed a system which would become xTradeHomes to help clients, and agents, to succeed. We believe xTradeHomes will increase your success too.</p>
		</div>
	</div>
	<div class="row about-second">
		<div class="small-12 large-6 small-centered column">
			<div class="small-12 medium-6 column">
				Ever see a great new product and think:
				<div>
					&quot;Why didn't I<br>
					think of that?&quot;
				</div>
				Well, we <em><strong>did</strong></em>&nbsp; think of it. And now we’re sharing it with <em><strong>you</strong></em>.
			</div>
			<div class="small-12 medium-6 column">
				<?=\Asset::img('derr.png', array('class' => 'about-guy'))?>
			</div>
		</div>
	</div>
	<div class="row about-third">
		<div class="small-12 medium-9 column text">
			<p><strong>The Challenge</strong></p>
			<p>One of most frequent questions asked by family and friends when we announce we're selling…<br>
				<em>&quot;Where are you going to live after you sell your house?&quot;</em></p>
			<p>Frequent response: <em>&quot;Well, that depends....&quot;</em><br>
				Most homeowners sell a home to buy another one. That should be easy enough, but for most people, it will be one of the most stressful, complicated, uncertain, time consuming, and costly efforts of their lives. Why? It's the Time Gap. It’s not knowing where they'll move to, how much they'll have to spend on a new home and more importantly - how much can they "really" expect to get for their house.</p>
			<p><strong>Time Gap</strong></p>
			<p>The time gap exists between the sale of one property and the purchase of another property. It is typically a daunting, disruptive and drawn out ordeal, especially if they are located far apart and involve multiple teams of professionals such as agents, loan managers, brokers, inspectors, and lawyers.</p>
			<p><strong>Closing the gap</strong></p>
			<p>The XTradeHomes&trade; solution runs an advanced database that connects sellers with sellers immediately. It was designed to streamline and close the Time Gap based on the simple premise that the sellers have now turned into buyers for each other’s property. In other words, they each want what the other has, and they know what it will cost and how much they will get. No surprises - you can't beat that!</p>
		</div>
		<div class="show-for-medium medium-3 column right-image">
			<?=\Asset::img('about_third.jpg');?>
		</div>
	</div>
	<div class="row about-fifth">
		<h2 class="text-center">
			<?=\Asset::img('hiw_q.png', array('alt' => 'Questions?'));?>
			<br>What is xTradeHomes.com?
		</h2>
		<p>It is a powerful tool for an astute REALTOR&reg; and their Client. The whole system was designed to make real estate transactions faster, easier and less costly for all.</p>
		<p>xTradeHomes.com is proprietary patent pending—a sophisticated, powerful data management system—usable anywhere in North America, designed to connect sellers with sellers  immediately.</p>
		<p>xTradeHomes finds multiple xTrades&trade; to match various sellers with the right properties, and ease them into converting to Buyers as well - quickly and efficiently.</p>
		<p>xTrading&trade; is offered to REALTORS&reg; as a Membership Service through our website (no software is needed). The system does the heavy lifting. It matches the REALTORS&reg;’ client's listed property with what the Client wishes to purchase.</p>
		<em class="smaller">The trademarks REALTOR&reg;, REALTORS&reg;, and the REALTOR&reg; logo are controlled by The Canadian Real Estate Association (CREA) and identify real estate professionals who are members of CREA.  The trademark DDF&reg; is owned by CREA and identifies CREA's Data Distribution Facility (DDF&reg;).  xTradeHomes receives accurate, reliable, and current listing information from REALTORS&reg; through CREA's DDF&reg;</em>
	</div>
	<div class="row about-sixth">
		<div class="small-12 medium-10 large-6 small-centered column">
			<p>So if you think you’d like to retire early or if you just want to make a lot more money, join the evolution of real estate by clicking here:</p>
			<?=\Html::anchor('signup/realtor', 'Get Started Now', array('class' => 'button large secondary'));?>
		</div>
	</div>
</div>