<table width="100%" class="responsive">
	<thead>
		<tr>
			<th>Listing ID</th>
			<th>Street Address</th>
			<th>City</th>
			<th>Province</th>
			<th>CREA ID</th>
			<th>Agent Name</th>
		</tr>
	</thead>
	<tbody>
		<?php if(empty($data)) : ?>
		<tr>
			<td colspan="6">No record found</td>
		</tr>
		<?php else: ?>
		<?php foreach($data as $row) :?>
		<tr>
			<td><?=\Html::anchor('/admins/listing/detail/2_'.$row['pid'], $row['listingid'], array('target' => '_blank'));?></td>
			<td><?=$row['streetaddress']?></td>
			<td><?=$row['city_txt']?></td>
			<td><?=$row['province']?></td>
			<td><?=$row['crea_id']?></td>
			<td><?=$row['name']?></td>
		</tr>
		<?php endforeach; ?>
		<?php endif; ?>
	</tbody>
</table>