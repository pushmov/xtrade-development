<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title><?=$meta_title;?></title>
		<link href="/favicon.ico" rel="shortcut icon" type="image/x-icon" />
		<?=\Asset::css('auto-complete.css');?>
		<?=\Asset::css('app.css');?>
		<?=\Asset::css('style.css');?>
		<?=\Asset::css('admin.css');?>
		<?php echo \Asset::css($styles, array(), null, false); ?>
		<?=\Asset::js('//code.jquery.com/jquery-latest.min.js');?>
		<?=\Asset::js('modernizr.js');?>
		<?=\Asset::js('foundation.min.js');?>
        <?=\Asset::js('jquery.mask.js');?>
		<?=\Asset::js('admin.js');?>
		<?php echo \Asset::js($scripts, array(), null, false); ?>
		<script>
			var baseUrl = '<?=Uri::base(false);?>';
			<?php if (isset($js_vars)) {foreach ($js_vars as $k => $v) echo "var $k = '$v';", PHP_EOL;} ?>
		</script>
	</head>
	<body>
		<div id="header">
			<div class="row"></div>
		</div>
		<div class="row content" id="content">
			<div class="column"><?=$header;?></div>
			<?=$content;?>
		</div>
		<div id="footer">
			<div class="row">
				<div class="small-12 medium-2 column">
					<?=\Html::anchor('/', \Asset::img('ft_logo.png', array('width' => 152, 'height' => 52, 'alt' => 'Xtrade Homes')));?>
				</div>
				<div class='small-12 medium-8 column b-nav'>
					<?=\Html::anchor('about', 'About Us');?>
					<?=\Html::anchor('contact', 'Contact Us');?>
					<?=\Html::anchor('privacy', 'Privacy Policy');?>
					<?=\Html::anchor('terms', 'Terms & Conditions');?>
					<?=\Html::anchor('sitemap', 'Sitemap');?>
				</div>
				<div class="small-12 medium-2 column">
					<span title="The trademarks REALTOR&reg;, REALTORS&reg; and the REALTOR&reg; logo are controlled by The Canadian Real Estate Association (CREA) and identify real estate professionals who are members of CREA" style="font-weight:bold;" class='ft_img'>REALTOR&reg;</span> <?=\Asset::img('ddf.jpg', array('width' => 45, 'height' => 25, 'alt' => 'CREA', 'class' => 'ft_img', 'title' => 'The trademark DDF&reg; is owned by The Canadian Real Estate Association (CREA) and identifies CREA’s Data Distribution Facility (DDF&reg;)'));?>
				</div>
			</div>
		</div>
		<div id="login_dialog" class="reveal tiny" data-reveal data-close-on-click="false">
		</div>
		<script>
			$(document).foundation();
		</script>
	</body>
</html>