<div class="content">
	<div class="column admin-page">
		<div class="small-12 medium-6 column">
			<h4>xTradeHome Listings</h4>
		</div>
	</div>
	<div class="padded">
		<div id="extra_filter">
			<label for="view_select">View:</label>
			<?=\Form::select('filter', '', $filters, array('id' => 'view_filter'));?>
		</div>
		<table id="listings" class="display">
			<thead>
				<tr>
					<th>Listing ID</th>
					<th>Type</th>
					<th>Address</th>
					<th>City</th>
					<th>State</th>
					<th>Price</th>
					<th>Member</th>
					<th>Matches</th>
					<th>Status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>
<div id="dialog" class="reveal tiny" data-reveal data-close-on-click="false"></div>
