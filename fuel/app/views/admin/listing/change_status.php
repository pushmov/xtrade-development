<li class="accordion-item is-active admin-listing-status" id="change_listing_status">
	<a href="#panel_5" role="tab" class="accordion-title" id="panel5d-heading" aria-controls="panel5d">Change Listing Status</a>
	<div id="panel_5" class="accordion-content" role="tabpane5" data-tab-content aria-labelledby="panel5d-heading">
		<div class="row">
			<div class="large-3 columns">
				<label class="req_text" for="has[listing_status]"> Listing Status</label>
			</div>
			<div class="large-9 columns">
				<?=\Form::select('has[listing_status]', $has['listing_status'], \Model\Client\Has::forge()->admin_listing_status(), array('class' => 'no_multi has_listing_status'))?>
			</div>
		</div>
	</div>
</li>