<div class="content column">
    <?=\Form::open(array('id' => 'main_form', 'name' => 'main_form', 'method' => 'post', 'action' => '/listing/update.json'));?>
        <div class="submit_overlay"></div>
        <div style="position: fixed; bottom: 70px; right: 50px; z-index: 100;">
            <?=\Form::button('submit_content', 'Save Now', array('class' => 'wants_buttons submit_content', 'id' => 'submit_content', 'type' => 'button', 'style' => 'display:none;'))?>
        </div>
        <div style="position: fixed; bottom: 70px; right: 50px; z-index: 100; display:none;" id="_message">
            <?=\Form::button('scroll_to_next', 'Almost Finished! Go To Next Area', array('type' => 'button', 'class' => 'wants_buttons submit_content', 'id' => 'scroll_to_next' , 'style' => 'height: 45px;'));?>
        </div>
        <div style="position: fixed; bottom: 25px; right: 50px; z-index: 100;">
            <?=\Form::button('return_home', 'Return to Home', array('type' => 'button', 'class' => 'wants_buttons return_home', 'style' => 'display:block', 'id' => 'return_home'));?>
        </div>
        <div class="submitting multi_option" style="position: fixed; bottom: 70px; right: 50px; z-index: 100; display:none;">
            <?=\Asset::img('preload.gif', array('width' => 36, 'height' => 35, 'style' => 'vertical-align: middle;'));?> Please Wait, Submitting Content
        </div>    
        
        <?= $wants_info; ?>
    <?=\Form::close(); ?> 
    
    <div id="edit_listing_dialog" class="reveal tiny" data-reveal data-close-on-click="false"></div>
    
</div>  