<li class="accordion-item is-active client-info" id="client_info">
	<a href="#panel_1" role="tab" class="accordion-title" id="panel1d-heading" aria-controls="panel1d">Client Info
		<?php if($has['external_id'] != ''):?>
			<span>ML#<?=$has['external_id'];?></span>
		<?php endif; ?>
	</a>
	<div id="panel_1" class="accordion-content" role="tabpanel" data-tab-content aria-labelledby="panel1d-heading">
		<div class="row column text-right attention">
			<?php if ( $info['completed_section'] == \Model\Client::STATUS_INCOMPLETE): ?>
			Please Complete All Required Fields <?=\Asset::img('attention.png', array('width' => 35, 'height' => 35, 'alt' => __('attention')));?>
			<?php else : ?>
			Information Up To Date <?=\Asset::img('up-to-date.png');?>
			<?php endif; ?>
		</div>
		<div class="row">
			<div class="padded-sides"><p>This information is for your purposes only and therefore not mandatory. It will only be used for notifications sent to your email address. None of this information will be visible to the public.</p></div>
			<input name="info[id]" id="id" type="hidden" value="<?=$info['id'];?>">
			<input name="info[listing_id]" id="id" type="hidden" value="<?=$edit['listing_id'];?>">
			<div class="large-4 columns">
				<label for="fname_c" class="">
					<?=__('first_name'); ?>
					<input name="info[first_name]" type="text" class="input_box_required test" id="first_name" value="<?=$info['first_name'];?>" data-field="first_name"/>
				</label>
			</div>
			<div class="large-4 columns">
				<label for="lname_c" class="">
					<?=__('last_name'); ?>
					<input name="info[last_name]" type="text" class="input_box_required" id="last_name" value="<?=$info['last_name'];?>" data-field="last_name"/>
				</label>
			</div>
			<div class="large-4 columns">
				<label for="lname_c" class="">
					<?=__('email_address'); ?>
					<input name="info[email]" type="text" class="input_boxs" id="email" value="<?=$info['email'];?>" data-field="email" />
				</label>
			</div>
			<div class="large-4 columns">
				<label for="lname_c" class="">
					<?=__('phone_number'); ?>
					<input name="info[phone]" type="tel" class="input_box_required" id="phone" value="<?=$info['phone'];?>" data-field="phone"/>
				</label>
			</div>
			<div class="large-4 columns">
				<label for="lname_c" class="">
					<?=__('cell_number'); ?>
					<input name="info[cell]" type="tel" class="input_boxs" id="cell" value="<?=$info['cell'];?>" data-field="cell"/>
				</label>
			</div>
			<div class="large-4 columns"></div>
		</div>
	</div>
</li>