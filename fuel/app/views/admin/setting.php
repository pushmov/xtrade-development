<div class="content">
<div class="small-12 column">
<h3>Settings</h3>
<?=\Form::open(array('action' => 'admins/setting/save.json', 'method' => 'post', 'name' => 'setting_form'));?>
<div class="row">
	<div class="small-12 columns">
		<?php foreach($settings as $k => $v): ?>
			<div class="row">
				<div class="small-4 columns">
					<label for="right-label" class="right inline"><?=ucwords(str_replace('_', ' ', $k));?>:</label>
				</div>
				<div class="small-8 columns">
					<input type="text" name="data[<?=$k;?>]" placeholder="<?=ucwords(str_replace('_', ' ', $k));?>" value="<?=$v;?>">
				</div>
			</div>
		<?php endforeach; ?>
		<div class="row">
			<div class="small-12 columns">
				<?=\Form::button('button', 'Save', array('type' => 'button', 'class' => 'button save_setting'))?>
			</div>
		</div>
		<div class="er"></div>

	</div>
</div>
<?=\Form::close();?>