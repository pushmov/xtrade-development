<table width="100%" class="responsive">
	<thead>
		<tr>
			<th>xTrade ID</th>
			<th>Address</th>
			<th>Match</th>
		</tr>
	</thead>
	<tbody>
		<?php if(!empty($data)) : ?>
		<?php foreach($data as $row):?>
		<tr>
			<td><?=\Html::anchor('admins/listing/match/' .$row['listing_id'].'?realtor_id='.$row['realtor_id'], $row['listing_id']);?></td>
			<td><?=$row['location']?></td>
			<td><?=$row['total_matches']?> Total matches, <?=$row['saved_matches']?> Saved / <?=$row['ignore_matches']?> Ignored</td>
		</tr>
		<?php endforeach; ?>
		<?php else :?>
		<tr>
			<td colspan="3" align="center">No Matches Yet For This REALTOR&reg;.</td>
		</tr>
		<?php endif; ?>
	</tbody>
</table>