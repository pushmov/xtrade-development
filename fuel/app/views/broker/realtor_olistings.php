<h3><?=$data['name']?> Listings</h3>
<?php if(empty($data['listing'])):?>
	No Imported Listings Found For This REALTOR&reg;.
	<?php else: ?>

        <?php foreach($data['listing'] as $row) : ?>
        <dl class="row">
            <dt>These Listings Are Within The xTradeHomes Matching System.</dt>
            <dd>
            	<?php if ($row['type_of_listing'] == \Model\Listing::TYPE_BUY_ONLY): ?>
                <?=$row['address'];?>
                <?php else: ?>
                <?=\Html::anchor('listing/detail/2_'.$row['external_id'], $row['address']);?>
                <?php endif; ?>
				<?php if($row['listing_status'] != \Model\Listing::LS_ACTIVE) :?>
				<h5 class="warning"><strong><?=__('inactive');?> (<?= \Model\Listing::forge()->get_status($row['listing_status']); ?>)</strong></h5>
				<?php endif; ?>
                <br>
                <hr>
            </dd>
        </dl>
        <?php endforeach; ?>
	<?php endif;?>
