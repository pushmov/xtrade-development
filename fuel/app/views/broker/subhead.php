<ul class="unstyled" id="sub_menu">
	<li><?=\Html::anchor('brokers', 'Dashboard');?></li>
	<li><?=\Html::anchor('#', 'Add REALTOR&reg;', array('id' => 'add_realtor', 'class' => 'new_realtor'));?></li>
	<li><?=\Html::anchor('brokers/profile', 'My Profile', array('id' => 'profile'));?></li>
	<li class="float-right"><?=\Html::anchor('vendor/location/'.$location, 'Local Services Directory', array('id' => 'tutorial'));?></li>
</ul>
<hr>
<script>
$(document).ready(function() {
	$('#add_realtor').on('click', function() {
		$.get(baseUrl + 'brokers/dashboard/add_realtor_dialog.html', function(html) {
			$('#page_dialog').html(html);
			$('#page_dialog').foundation('open');
		});
	});

	$('#page_dialog').on('click', '#btn_send_invite', function(){
		var form = $('#invite_form');
		var that = $(this);
		var currTxt = that.text();
		that.html('<i class="fa fa-spinner fa-spin"></i>Processing...');
		$('.input-error').remove();
		$.post(form.attr('action'), form.serialize(), function(json){
			if (json.status === 'OK') {
				$('#invite_form').after('<div class="callout success small">' + json.message + '</div>');
				that.html(currTxt);
				$('#btn_send_invite').remove();
				$('#btn_close').html('Close');
			} else {
				$.each(json.errors, function(k,v) {
					$('#' + k).after('<div class="input-error">' + v + '</div>');
				});
				that.html(currTxt);
			}
		});
	});
});
</script>

