<div class="content column">
	<div class="small-12 column">
		<h3>Contact xTradeHomes</h3>
		<?=\Form::open(array('id' => 'contact', 'name' => 'contact', 'method' => 'post', 'action' => ''));?>
			<div class="row">
				<div class="small-12 medium-7 medium-push-5 column contact-address">
					<strong>TradeHomes by iRex Inc.</strong><br>
					#370 - 425 Carrall Street<br>
					Vancouver, BC<br>V6B 6E3<br>
					Email:  <a href='&#109;&#97;&#105;&#108;&#116;&#111;&#58;&#105;&#110;&#102;&#111;&#64;&#120;&#116;&#114;&#97;&#100;&#101;&#104;&#111;&#109;&#101;&#115;&#46;&#99;&#111;&#109;'>&#105;&#110;&#102;&#111;&#64;&#120;&#116;&#114;&#97;&#100;&#101;&#104;&#111;&#109;&#101;&#115;&#46;&#99;&#111;&#109;</a><br>
					Phone: 844-798-7233
				</div>
				<div class="small-12 medium-5 medium-pull-7 column">
					<label for="email_to"><?=__('who'); ?></label>
					<select name="email_to" id="email_to">
						<option value='contact' selected='selected'><?=__('gi'); ?></option>
						<option value='advertising'><?=__('ai'); ?></option>
						<option value='webmaster'><?=__('wri'); ?></option>
						<option value='media'><?=__('mr'); ?></option>
						<option value='accounts'><?=__('ac'); ?></option>
					</select>
					<label for="name"><?=__('name'); ?><span class="astrict">*</span></label>
					<input name="name" type="text" id="name">
					<label for="phone"><?=__('phone'); ?> </label>
					<input name="phone" type="tel" id="phone" placeholder="555-555-5555">
					<label for="email"><?=__('email'); ?><span class="astrict">*</span></label>
					<input name="email" type="email" id="email">
				</div>
			</div>
			<label for="comment"><?=__('comment'); ?><span class="astrict">*</span></label>
			<textarea name="comment" id="comment" rows="8"></textarea>
			<button type="button" name="submit" id="submit" class="button"><?=__('submit'); ?></button>
		<?=\Form::close();?>
	</div>
</div>
<script>
	$(document).ready(function() {
		$("#phone").mask("999-999-9999");
	});
	$('#submit').on('click', function() {
		$('.input-error').remove();
		$.post(baseUrl + 'contact/send.json', $('#contact').serialize(), function(json) {
			if (json.status == 'OK') {
				$('#submit').after(json.message);
				$('#submit').hide();
			} else {
				$.each(json.errors, function(k,v) {
					$('#' + k).after('<div class="input-error">' + v + '</div>');
				});
			}
			$(document).foundation('alert', 'reflow');
		});
	});
</script>