<div class="modal-header">Add To/Change Broker
	<a class="close-reveal-modal"><?=\Asset::img('close_image.png', array('class' => 'modal-close', 'alt' => 'Close', 'data-close' =>''));?></a>
</div>
<div class="modal-body text-center">
	<p>Add A REALTOR&reg; To Broker Account (<?=$broker_id?>)</p>
    <?=\Form::open(array('name' => 'add_realtor', 'id' => 'add_realtor', 'method' => 'post', 'action' => '/admins/broker/add.json'));?>

        <div class="row">
            <div class="small-12 columns">
                <div class="row">
                    <div class="small-4 columns">
                        <label for="right-label" class="right inline">First Name <span class="astrict">*</span></label>
                    </div>
                    <div class="small-8 columns">
                        <input type="text" name="data[first_name]" id="first_name" placeholder="First name">
                    </div>
              </div>
            </div>
        </div>

        <div class="row">
            <div class="small-12 columns">
                <div class="row">
                    <div class="small-4 columns">
                        <label for="right-label" class="right inline">Last Name <span class="astrict">*</span></label>
                    </div>
                    <div class="small-8 columns">
                        <input type="text" name="data[last_name]" id="last_name" placeholder="Last name">
                    </div>
              </div>
            </div>
        </div>

        <div class="row">
            <div class="small-12 columns">
                <div class="row">
                    <div class="small-4 columns">
                        <label for="right-label" class="right inline">CREA ID <span class="astrict">*</span></label>
                    </div>
                    <div class="small-8 columns">
                        <input type="text" name="data[crea_id]" id="crea_id" placeholder="CREA ID">
                    </div>
              </div>
            </div>
        </div>

        <div class="row">
            <div class="small-12 columns">
                <div class="row">
                    <div class="small-4 columns">
                        <label for="right-label" class="right inline">Email Address <span class="astrict">*</span></label>
                    </div>
                    <div class="small-8 columns">
                        <input type="text" name="data[email]" id="email_address" placeholder="Email Address">
                    </div>
              </div>
            </div>
        </div>

        <div class="row">
            <div class="small-12 columns">
                <div class="row">
                    <?=\Form::button('process', 'Submit',array('type' => 'button', 'class' => 'button add_realtor_submit'))?>
              </div>
            </div>
        </div>
    <input type="hidden" name="data[broker_id]" value="<?=$broker_id?>">

	<?=\Form::close();?>
    <span class="er"></span>
</div>