<div class="modal-header">Reset Password
	<a class="close-reveal-modal"><?=\Asset::img('close_image.png', array('class' => 'modal-close', 'alt' => 'Close', 'data-close' =>''));?></a>
</div>
<div class="modal-body text-center">
    <?=\Form::open(array('action' => '/admins/broker/reset.json', 'method' => 'post', 'name' => 'broker_reset_password'));?>
	<p>This will reset this users password, continue?</p>
    <input type="hidden" name="broker_id" value="<?=$broker_id?>">
    <?=\Form::button('reset', 'Yes', array('type' => 'button', 'id' => 'reset', 'class' => 'button broker_submit_reset'));?>
    <?=\Form::button('close', 'No', array('type' => 'button', 'id' => 'reset', 'class' => 'button close_button'));?>
    <?=\Form::close();?>
    <span class="er"></span>
</div>