<div class="modal-header"><span>Listing Complete!</span>
    <a class="close-reveal-modal"><?=\Asset::img('close_image.png', array('class' => 'modal-close', 'alt' => 'Close', 'data-close' =>''));?></a>
</div>
<div class="modal-body text-center">
    <p>Listing details updated</p>
    <button id="Home" class="button my_listing dashboard">Go Back To Dashboard</button>
    <button id="No" class="button keep_edit">Keep Editing</button>
</div>