<div class="modal-header">Remove Log Record
	<a class="close-reveal-modal"><?=\Asset::img('close_image.png', array('class' => 'modal-close', 'alt' => 'Close', 'data-close' =>''));?></a>
</div>
<div class="modal-body text-center">
	<p>This will permanently delete this record, it is irreversable. Are You Sure?</p>
    <?=\Form::open(array('name' => 'remove_log', 'id' => 'remove_log', 'method' => 'post', 'action' => ''));?>
		<input type="hidden" name="data[id]" value="<?=$id?>">
        <?=\Form::button('submit', 'Yes', array('class' => 'button submit-form', 'type' => 'button'));?>
        <?=\Form::button('close', 'No', array('type' => 'button', 'id' => 'reset', 'class' => 'button close_button'));?>
	<?=\Form::close();?>
</div>