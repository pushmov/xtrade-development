<div class="modal-header">Confirm Activate Plan
	<a class="close-reveal-modal"><?=\Asset::img('close_image.png', array('class' => 'modal-close', 'alt' => 'Close', 'data-close' =>''));?></a>
</div>
<div class="modal-body text-center">
	<p>This will activate the plan. Once activated, the plan data cannot be changed/updated. Are you sure?</p>
	<button type="button" class="button" id="btn_active_submit">Activate</button>
	<button type="button" class="button close_button">Cancel</button>
    <span class="er"></span>
</div>