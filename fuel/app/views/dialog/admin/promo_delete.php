<div class="modal-header">Remove Promo Record
	<a class="close-reveal-modal"><?=\Asset::img('close_image.png', array('class' => 'modal-close', 'alt' => 'Close', 'data-close' =>''));?></a>
</div>
<div class="modal-body text-center">
	<p>This will permanently delete this promo, it is irreversable. Are You Sure?</p>
    <?=\Form::open(array('name' => 'remove_promo', 'id' => 'remove_promo', 'method' => 'post', 'action' => '/admins/promo/delete.json'));?>
		<input type="hidden" name="data[code]" value="<?=$code?>">
        <?=\Form::button('submit', 'Yes', array('class' => 'button submit-form', 'type' => 'button'));?>
        <?=\Form::button('close', 'No', array('type' => 'button', 'id' => 'reset', 'class' => 'button close_button'));?>
	<?=\Form::close();?>
</div>