<div class="modal-header">Resend Welcome Email
	<a class="close-reveal-modal"><?=\Asset::img('close_image.png', array('class' => 'modal-close', 'alt' => 'Close', 'data-close' =>''));?></a>
</div>
<div class="modal-body text-center">
	<?=\Form::open(array('action' => 'admins/profile/resend_welcome_email.json', 'method' => 'post'));?>
		<p>You are about to resend welcome email to : <?="{$data['first_name']} {$data['last_name']}";?> (<?=$data['email']?>)</p>
		<p>Do you want to continue?</p>
		<input type="hidden" name="data[utype]" value="<?=$data['utype']?>">
		<input type="hidden" name="data[id]" value="<?=$data['id']?>">
		<button type="button" class="button" id="submit_resend">Send</button>
		<button type="button" class="button" class="other_buttons" id="btn_close" name="btn_close" data-close>Close</button>
	<?=\Form::close();?>
	<p class="er"></p>
</div>