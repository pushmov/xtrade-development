<div class="modal-header">Success
	<a class="close-reveal-modal"><?=\Asset::img('close_image.png', array('class' => 'modal-close', 'alt' => 'Close', 'data-close' =>''));?></a>
</div>
<div class="modal-body text-center">
	<p>Welcome email sent successfully</p>
	<button type="button" class="button" class="other_buttons" id="btn_close" name="btn_close" data-close>Close</button>
</div>