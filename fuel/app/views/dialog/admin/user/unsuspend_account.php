<div class="modal-header">Unsuspend Users Account
	<a class="close-reveal-modal"><?=\Asset::img('close_image.png', array('class' => 'modal-close', 'alt' => 'Close', 'data-close' =>''));?></a>
</div>
<div class="modal-body text-center">
	<p>This will unsuspend the users account. Making it accessible to them again. Are you sure?</p>
    <?=\Form::open(array('name' => 'change_broker', 'id' => 'change_broker', 'method' => 'post', 'action' => '/admins/user/unsuspend.json'));?>
        <input name="data[realtor_id]" type="hidden" value="<?=$realtor_id;?>">
        <?=\Form::button('submit', 'Submit', array('class' => 'button submit_suspend', 'type' => 'button'));?>
        <?=\Form::button('close', 'No', array('type' => 'button', 'id' => 'reset', 'class' => 'button close_button'));?>
	<?=\Form::close();?>
    <span class="er"></span>
</div>