<div class="modal-header">Ad Preview
	<a class="close-reveal-modal"><?=\Asset::img('close_image.png', array('class' => 'modal-close', 'alt' => 'Close', 'data-close' =>''));?></a>
</div>
<div class="modal-body text-center">
	<?php foreach($data as $row) : ?>
    <div class="row small-12 ad-preview">
        <?php if($row['featured'] == \Model\Vendor::VENDOR_STATUS_FEATURED) : ?>
        <div class="directory-results-featured"></div>
        <?php endif; ?>
        <div class="small-12 medium-4 column vendor-logo">

        </div>
        <div class="small-12 medium-4 column text-left vendor-ad">
            <p>Service: <strong><ul><li><?=str_replace(';', '</li><li>', \Model\Vendor::forge()->typeid_to_services($row['type']))?></li></ul></strong></p>
            <h3><?=($row['web_site'] == '') ? $row['company_name'] : \Html::anchor($row['web_site'], $row['company_name'], array('target' => '_blank'))?></h3>
            <p class="address"><?=$row['address']?></p>
            <p><?=$row['location_address']?></p>
            <em>Telephone:</em> <strong><?=\Sysconfig::format_phone_number($row['phone']);?></strong><br>
            <em>Email:</em> <strong><?=\Html::mail_to($row['public_email'], $row['public_email']);?></strong><br>
            <em>Website:</em> <strong><?=$row['web_site'] == '' ? '-' : \Html::anchor($row['web_site'], $row['web_site']);?></strong><br>
        </div>
        <div class="small-12 medium-4 column"><?=$row['comments']?></div>
    </div>
    <hr>
    <?php endforeach; ?>
</div>
