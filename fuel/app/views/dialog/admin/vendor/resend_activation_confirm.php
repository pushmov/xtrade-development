<div class="modal-header">Resend Activation Confirm
	<a class="close-reveal-modal"><?=\Asset::img('close_image.png', array('class' => 'modal-close', 'alt' => 'Close', 'data-close' =>''));?></a>
</div>
<div class="modal-body text-center">
	<p>This will resend member activation link through their email. Are you sure?</p>
    <?=\Form::open(array('name' => 'resend_activation', 'id' => 'resend_activation', 'method' => 'post', 'action' => '/admins/vendor/resend_activation.json'));?>
        <input name="data[vendor_id]" type="hidden" value="<?=$vendor_id;?>">
		<input name="data[utype]" type="hidden" id="utype" value="vendor">
        <?=\Form::button('submit', 'Submit', array('class' => 'button submit-resend-activation', 'type' => 'button'));?>
        <?=\Form::button('close', 'No', array('type' => 'button', 'id' => 'reset', 'class' => 'button close_button'));?>
	<?=\Form::close();?>
    <span class="er"></span>
</div>