<div class="modal-header"><span>Resend Activation Success!</span>
    <a class="close-reveal-modal"><?=\Asset::img('close_image.png', array('class' => 'modal-close', 'alt' => 'Close', 'data-close' =>''));?></a>
</div>
<div class="modal-body text-center">
    <p>Activation link successfully sent</p>
    <button class="button close_button" type="button">Close</button>
</div>