<div class="modal-header">Reset Password
	<a class="close-reveal-modal"><?=\Asset::img('close_image.png', array('class' => 'modal-close', 'alt' => 'Close', 'data-close' =>''));?></a>
</div>
<div class="modal-body text-center">
    <?=\Form::open(array('action' => '/admins/vendor/reset.json', 'method' => 'post', 'name' => 'vendor_reset_password'));?>
	<p>This will reset this users password, continue?</p>
    <input type="hidden" name="vendor_id" value="<?=$vendor_id?>">
    <?=\Form::button('reset', 'Yes', array('type' => 'button', 'id' => 'reset', 'class' => 'button vendor_submit_reset'));?>
    <?=\Form::button('close', 'No', array('type' => 'button', 'id' => 'reset', 'class' => 'button close_button'));?>
    <?=\Form::close();?>
    <span class="er"></span>
</div>