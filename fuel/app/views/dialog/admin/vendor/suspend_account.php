<div class="modal-header">Suspend Users Account
	<a class="close-reveal-modal"><?=\Asset::img('close_image.png', array('class' => 'modal-close', 'alt' => 'Close', 'data-close' =>''));?></a>
</div>
<div class="modal-body text-center">
	<p>This will suspend the users account. Making it inaccessible to them. Are you sure?</p>
    <?=\Form::open(array('name' => 'suspend_vendor', 'id' => 'suspend_vendor', 'method' => 'post', 'action' => '/admins/vendor/suspend.json'));?>
        <input name="data[vendor_id]" type="hidden" value="<?=$vendor_id;?>">
        <?=\Form::button('submit', 'Submit', array('class' => 'button submit_suspend_vendor', 'type' => 'button'));?>
        <?=\Form::button('close', 'No', array('type' => 'button', 'id' => 'reset', 'class' => 'button close_button'));?>
	<?=\Form::close();?>
    <span class="er"></span>
</div>