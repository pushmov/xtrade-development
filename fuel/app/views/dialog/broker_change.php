<div class="modal-header">Change Broker
	<a class="close-reveal-modal"><?=\Asset::img('close_image.png', array('class' => 'modal-close', 'alt' => 'Close', 'data-close' =>''));?></a>
</div>
<div class="modal-body text-center">
	<p>Change or Remove Your Current Broker</p>
	<button type="button" class="button" id="btn_change_broker">Request Change</button>
	<button type="button" class="button" id="btn_remove_broker">Remove Broker</button>
</div>
