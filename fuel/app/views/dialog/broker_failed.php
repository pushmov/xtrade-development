<div class="modal-header">Save Error
	<a class="close-reveal-modal"><?=\Asset::img('close_image.png', array('class' => 'modal-close', 'alt' => 'Close', 'data-close' =>''));?></a>
</div>
<div class="modal-body text-center">
	<p>There was an error saving your information, try again <br>or please call 1-844-7XTRADE (1-844-798-7233)</p>
	<button type="button" class="button" id="btn_close" name="btn_close">Close</button>
</div>
