<div class="modal-header">Broker Request
	<a class="close-reveal-modal"><?=\Asset::img('close_image.png', array('class' => 'modal-close', 'alt' => 'Close', 'data-close' =>''));?></a>
</div>
<div class="modal-body">
	<p>Invite Broker To xTradeHomes</p>
	<?=\Form::open(array('action' => '', 'method' => 'post', 'name' => 'change_form', 'id' => 'invite_form'));?>
		<div class="row">
			<div class="medium-12 large-12 columns">
				<div class="row">
					<div class="small-12 medium-12 large-4 columns">
						<label for="invite_name">Broker Name <span class="astrict">*</span></label>
					</div>
					<div class="small-12 medium-12 large-8 columns">
						<input name="invite_name" type="text" id="invite_name" value="" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="medium-12 large-12 columns">
				<div class="row">
					<div class="small-12 medium-12 large-4 columns">
						<label for="invite_email">Email <span class="astrict">*</span></label>
					</div>
					<div class="small-12 medium-12 large-8 columns">
						<input name="invite_email" type="text" id="invite_email" value="" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="medium-12 large-12 columns">
				<div class="row">
					<div class="small-12 medium-12 large-12 columns text-center">
						<button type="button" class="button" id="btn_send_invite">Send</button> <button type="button" class="button" id="btn_close">Cancel</button>
					</div>
				</div>
			</div>
		</div>
	<?=\Form::close();?>
</div>