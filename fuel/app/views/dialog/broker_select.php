<div class="modal-header"><?=$title;?>
	<a class="close-reveal-modal"><?=\Asset::img('close_image.png', array('class' => 'modal-close', 'alt' => 'Close', 'data-close' =>''));?></a>
</div>
<div class="modal-body text-center">
	<?=$broker;?>
	<p>The Broker must approve this request before you will be added to their account.</p>
	<label for="broker">Broker Name:</label>
	<?=$cboBrokers?>
	<button type="button" class="button" id="btn_confirm_broker">Confirm</button> <button type="button" class="button" id="btn_close">Cancel</button>
</div>
