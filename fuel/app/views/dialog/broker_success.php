<div class="modal-header">Check Your Email
	<a class="close-reveal-modal"><?=\Asset::img('close_image.png', array('class' => 'modal-close', 'alt' => 'Close', 'data-close' =>''));?></a>
</div>
<div class="modal-body text-center">
	<p><strong>Sign Up process almost complete.</strong></p>
	<p>Please check your email and click on the email activation link provided to complete the Sign Up process.</p>
	<p><strong>Thank you for signing up.</strong></p>
	<button type="button" class="button" id="btn_close" name="btn_close">Close</button>
</div>
