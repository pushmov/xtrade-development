<div class="modal-header">Cancel Request
	<a class="close-reveal-modal"><?=\Asset::img('close_image.png', array('class' => 'modal-close', 'alt' => 'Close', 'data-close' =>''));?></a>
</div>
<div class="modal-body text-center">
	<p>Are you sure you want to cancel this request? You will have to request to be added to their account again.</p>
	<button type="button" class="button" id="btn_cancel_request_yes">Yes</button>
	<button type="button" class="button" id="btn_close">No</button>
</div>
