<div class="modal-header">Billing Details
	<a class="close-reveal-modal"><?=\Asset::img('close_image.png', array('class' => 'modal-close', 'alt' => 'Close', 'data-close' =>''));?></a>
</div>
<div class="modal-body">
	<div id="login_error"></div>
	<?=\Asset::img('hd_logo.png');?>
	<?=\Form::open(array('id' => 'cc_form', 'name' => 'cc_form', 'method' => 'post', 'action' => ''));?>
	First Name: <input name="data[first_name]" class="input_field req" id="first_name" value="<?=$data['first_name'];?>" size="24" type="text" />
	Last Name: <input name="data[last_name]" class="input_field req" id="last_name" value="<?=$data['last_name'];?>" size="24" type="text" /><br>
	Billing Address: <input name="data[address]" type="text" class="input_field_ext req" id="address" value="<?=$data['address'];?>" size="24" /><br>
	City/Province/State/Country: <input name="data[cpc_address]" type="text" class="input_field_ext req" id="cpc_address" value="<?=$data['cpc_address'];?>" placeholder="Start Typing City" size="24" /><br>
	Postal/Zip Code: <input name="data[z_p_code]" class="input_field req" id="z_p_code" value="<?=$data['z_p_code'];?>" size="24" type="text" /><br>
	Credit Card Type: <select class='no_multi req' name='cc_type' id='cc_type' style="width: 150px;" required>
		<option value=''>Select Card Type</option>
		<option value='MasterCard'>MasterCard</option>
		<option value='Visa'>Visa</option>
	</select><br>
	Credit Card Number: <input name="cc_number" class="input_field_ext req" id="cc_number" value="" size="16" type="text" /><br>
	Expiry date:  <select name="exp_month" id="exp_month" class="req" required>
		<option value="">Month</option>
		<option value="01">01</option>
		<option value="02">02</option>
		<option value="03">03</option>
		<option value="04">04</option>
		<option value="05">05</option>
		<option value="06">06</option>
		<option value="07">07</option>
		<option value="08">08</option>
		<option value="09">09</option>
		<option value="10">10</option>
		<option value="11">11</option>
		<option value="12">12</option>
	</select>
	<?=\Form::select('exp_year', '', $cboYears, array('id' => 'exp_year', 'class' => 'req', 'required' => 'required'));?>
	CVV/CSC: <input name="cc_cvv" class="input_field req" id="cc_cvv" value="" size="6" type="text" />
	<input name="rid" id="rid" type="hidden" value="">
	<input name="cc_email" id="cc_email" type="hidden" value="">
	<input name="cc_pcode" id="cc_pcode" type="hidden" value="">
	<input name="area" id="area" type="hidden" value="Realtor">
	<input name="cc_featured" id="cc_featured" type="hidden" value="">
	<input name="data[broker_id]" id="broker_id" type="hidden" value="<?=$data['broker_id'];?>">
	<button type="button" class="button" id="btn_change_billing">Update</button>
	<?=\Form::close();?>
</div>
<script>
	$(document).ready(function() {
		new autoComplete({
			selector: '#cpc_address',
			minChars: 3,
			source: function(term, response){
				$.get(baseUrl + 'autocomplete/cpc.json', { q: term }, function(data){ response(data); });
			}
		});
	});
</script>