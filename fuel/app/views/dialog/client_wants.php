<div class="modal-header">Client Wants
	<a class="close-reveal-modal"><?=\Asset::img('close_image.png', array('class' => 'modal-close', 'alt' => 'Close', 'data-close' =>''));?></a>
</div>
<div class="modal-body text-left">
	<?=\Asset::img('hd_logo.png', array('alt' => 'Xtrade Homes'));?>
  <div>
	<h4>What they want in a new home:</h4>
	<ul class="unstyled option-list wide">
		<li><span>Location</span>: <strong><?=  \CityState::get_area_name($wants->city, $wants->prov_state, $wants->country); ?></strong></li>
		<li><span>Title Type</span>: <strong><?= \Model\Client\Want::forge()->p_title($wants->p_title);?></strong></li>
		<li><span>Home Style</span>: <strong><?= \Model\Client\Want::forge()->prop_style($wants->prop_style);?></strong></li>
		<li><span>Amount of bedrooms</span>: <strong><?= \Model\Client\Want::forge()->bedrooms($wants->bedrooms);?></strong></li>
		<li><span>Amount of bathrooms</span>: <strong><?= \Model\Client\Want::forge()->bathrooms($wants->bathrooms);?></strong></li>
		<li><span>Basement Type</span>: <strong><?= \Model\Client\Want::forge()->finished_basement($wants->finished_basement);?></strong></li>
		<li><span>Square Footage</span>: <strong> <?= $between; ?></strong></li>
		<li><span>Type of Cooling</span>: <strong><?= \Model\Client\Want::forge()->cooling($wants->cooling);?></strong></li>
		<li><span>Type of Heating</span>: <strong><?= \Model\Client\Want::forge()->heating($wants->heating);?></strong></li>
		<li><span>Type of Water System</span>: <strong><?= \Model\Client\Want::forge()->water($wants->water);?></strong></li>
		<li><span>Type of Sewer System</span>: <strong><?= \Model\Client\Want::forge()->sewer($wants->sewer);?></strong></li>
		<li><span>Amount of Acreage</span>: <strong><?= \Model\Client\Want::forge()->total_acreage($wants->total_acreage);?></strong></li>
		<li><span>Size of Lot</span>: <strong><?= \Model\Client\Want::forge()->total_lot_size($wants->total_lot_size);?></strong></li>
		<li><span>Type of Parking</span>: <strong><?= \Model\Client\Want::forge()->garage_type($wants->garage_type);?></strong></li>
	</ul>
  </div>
</div>
