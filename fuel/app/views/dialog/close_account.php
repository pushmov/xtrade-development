<div class="modal-header">Close Account
	<a class="close-reveal-modal"><?=\Asset::img('close_image.png', array('class' => 'modal-close', 'alt' => 'Close', 'data-close' =>''));?></a>
</div>
<div class="modal-body text-center">
	<p>This will close your account and stop all future payments on this account.<br>Are You Sure?</p>
	<button type="button" class="button" id="btn_close_account">Close My Account</button>
	<button type="button" class="button" id="btn_close">No! Go Back!</button><br>
</div>
