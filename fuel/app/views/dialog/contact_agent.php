<div class="modal-header">Contact Agent
	<a class="close-reveal-modal"><?=\Asset::img('close_image.png', array('class' => 'modal-close', 'alt' => 'Close', 'data-close' =>''));?></a>
</div>
<div class="modal-body">
	<?=\Form::open(array('id' => 'contact_agent_form', 'name' => 'contact_agent_form', 'method' => 'post', 'action' => 'contact/agent.json'));?>
		<div class="row">
			<div class="medium-12 large-12 columns">
				<div class="row">
					<div class="small-12 medium-12 large-5 columns">
						<label>Name<span class="astrict">*</span></label>
					</div>
					<div class="small-12 medium-12 large-7 columns">
						<input type="text" class="input_field req" name="contact[name]" id="name" value="<?=$data['name'];?>"/>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="medium-12 large-12 columns">
				<div class="row">
					<div class="small-12 medium-12 large-5 columns">
						<label>Phone Number</label>
					</div>
					<div class="small-12 medium-12 large-7 columns">
						<input type="tel" name="contact[phone]" id="phone" value="<?=$data['phone']?>"/>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="medium-12 large-12 columns">
				<div class="row">
					<div class="small-12 medium-12 large-5 columns">
						<label>Email Address<span class="astrict">*</span></label>
					</div>
					<div class="small-12 medium-12 large-7 columns">
						<input type="text" class="input_field req" name="contact[email]" id="email" value="<?=$data['email']?>"/>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="medium-12 large-12 columns">
				<div class="row">
					<div class="small-12 medium-12 large-5 columns">
						<label>Message<span class="astrict">*</span></label>
					</div>
					<div class="small-12 medium-12 large-7 columns">
						<textarea name="contact[message]" rows="5" class="input_field req" id="message"><?=$data['message']?></textarea>
					</div>
				</div>
			</div>
		</div>
		<?php if($data['logged_in'] === false) :?>
		<div class="row">
			<label class="medium-6 column">Are you working with a REALTOR&reg;?<span class="astrict">*</span></label>
			<div class="medium-6 column">
				<label><input type="radio" class="req" value="Yes" name="contact[working_realtor]"/> Yes
					<input type="radio" class="req" value="No" name="contact[working_realtor]"/> No</label>
			</div>
		</div>
		<div class="row">
			<label class="medium-6 column">Are you interested in:?<span class="astrict">*</span></label>
			<div class="medium-6 column">
				<label><input type="radio" class="req" value="Trading" name="contact[trade]"/> Trading your home<br>
					<input type="radio" class="req" value="Selling" name="contact[trade]"/> Just Selling
					<input type="radio" class="req" value="Buying" name="contact[trade]"/> Buying a home</label>
			</div>
		</div>
		<?php endif; ?>
		<input type="hidden" name="contact[feed]" value="<?=$data['feed']?>">
		<input type="hidden" name="contact[listing_id]" value="<?=$data['listing_id']?>">
		<input type="hidden" name="contact[direction]" value="<?=$data['direction']?>">
		<input type="hidden" name="contact[ut]" value="<?=$data['ut']?>">
		<input type="hidden" name="contact[user_xid]" value="<?=$data['user_xid']?>">
		<div class="text-center" id="submit_button">
			<button type="submit" class="button" id="submit" name="submit">Submit</button>
			<button type="button" class="button" id="btn_close" name="btn_close" data-close>Cancel</button>
		</div>
	<?=\Form::close();?>
</div>