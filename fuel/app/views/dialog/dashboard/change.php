<div class="modal-header">Change Type Of Listing
	<a class="close-reveal-modal"><?=\Asset::img('close_image.png', array('class' => 'modal-close', 'alt' => 'Close', 'data-close' =>''));?></a>
</div>
<div class="modal-body text-center">
    <p>Change Listing Type to:</p>
    <?=\Form::open(array('action' => 'realtors/listing/change.json', 'method' => 'post', 'name' => 'change'));?>
    <button type="button" class="button change-type" data-id="<?=$listing_id?>" name="xtrade">xTrade</button>
    <button type="button" class="button change-type" data-id="<?=$listing_id?>" name="sell" title="This property will not be included in xTrade matches.">Just Sell</button>
    <?php if($import != \Model\Listing::IMPORTED) : ?>
    <button type="button" class="button change-type" data-id="<?=$listing_id?>" name="buy">Just Buy</button>
    <?php endif; ?>
    <?=\Form::close();?>
</div>