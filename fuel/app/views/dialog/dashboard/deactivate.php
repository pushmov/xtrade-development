<div class="modal-header">Deactivate Listing
	<a class="close-reveal-modal"><?=\Asset::img('close_image.png', array('class' => 'modal-close', 'alt' => 'Close', 'data-close' =>''));?></a>
</div>
<div class="modal-body text-center">
	<p>This will remove your listing from the xTrade matching system.</p>
    <?=\Form::open(array('action' => 'realtors/listing/deactivate.json', 'name' => 'deactivate', 'method' => 'post'));?>
	<p class="double-check">Are You Sure?</p>
	<button type="button" class="button btn-deactivate" data-id="<?=$listing_id?>">Yes</button>
	<button type="button" class="button" id="btn_close" name="btn_close">No</button>
    <?=\Form::close();?>
</div>