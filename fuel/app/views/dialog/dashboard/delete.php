<div class="modal-header">Delete Listing
	<a class="close-reveal-modal"><?=\Asset::img('close_image.png', array('class' => 'modal-close', 'alt' => 'Close', 'data-close' =>''));?></a>
</div>
<div class="modal-body text-center">
    <?=\Form::open(array('action' => 'realtors/listing/delete.json'));?>
	<p>This will permanently delete this listing, it is irreversable.</p>
	<p class="double-check">Are You Sure?</p>
	<button type="button" class="button btn-delete" data-id="<?=$listing_id?>">Yes</button>
	<button type="button" class="button" id="btn_close" name="btn_close">No</button>
    <?=\Form::close();?>
</div>
