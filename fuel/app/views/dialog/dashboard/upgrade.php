<script src="/assets/js/er_check.js" type="text/javascript"></script>
<script src="/assets/js/listing_upgrade.js" type="text/javascript"></script>
<div class="modal-header">Upgrade Listing
	<a class="close-reveal-modal"><?=\Asset::img('close_image.png', array('class' => 'modal-close', 'alt' => 'Close', 'data-close' =>''));?></a>
</div>

<div class="modal-body">
  <?=\Asset::img('hd_logo.png', array('alt' => 'Xtrade Homes'));?>
  <div>
    Listing Upgrade Fee: <span style="font-weight:600;"> <?php echo money_format('%n', $payment_info['fee']); ?></span><br>
    <?php if($payment_info['promo_terms'] != ''){ ?>
      <span id="show_promo"><?php echo money_format('%n', $payment_info['promo_terms']); ?></span><br>
    <?php } ?>
    <?php if($payment_info['reg_gst']){ ?><span id="gst_span">GST: <span style="font-weight:600;" id="tax_rate"> <?php echo $payment_info['reg_gst']; ?></span></span><br><?php } ?>
    <?php if($payment_info['reg_pst']){ ?><span id="pst_span">PST: <span style="font-weight:600;" id="tax_rate"> <?php echo $payment_info['reg_pst']; ?></span></span><br><?php } ?>
    <?php if($payment_info['reg_hst']){ ?><span id="hst_span">HST: <span style="font-weight:600;" id="tax_rate"> <?php echo $payment_info['reg_hst']; ?></span></span><br><?php } ?>
    <?php if($payment_info['reg_qst']){ ?><span id="hst_span">QST: <span style="font-weight:600;" id="tax_rate"> <?php echo $payment_info['reg_qst']; ?></span></span><br><?php } ?>
    <div class="sub_line"></div><br>
    Total Fee: <span style="font-weight:600;" id="total_due"><?php echo money_format('%n', $payment_info['total_due']); ?></span><br>
  </div>
  <br>
  <div>
    <?=\Form::open(array('id' => 'cc_form', 'name' => 'cc_form', 'method' => 'post', 'action' => 'payment/check'));?>
      First Name: <input name="payment[first_name]" class="input_field req" id="cc_fname" value="<?=$data['first_name'];?>" size="24" type="text" required /> <br>
      Last Name: <input name="payment[last_name]" class="input_field req" id="cc_lname" value="<?=$data['last_name'];?>" size="24" type="text" required /><br>
      Billing Address: <input name="payment[address]" type="text" class="input_field_ext req" id="cc_address" value="<?=$data['address'];?>" size="24" required /><br>
      City/Province/State/Country: <input name="payment[cpc_address]" type="text" class="input_field_ext req" id="cc_cpc_address" value="<?=$data['cpc_address'];?>" placeholder="Start Typing City" size="24" required /><br>
      Postal/Zip Code: <input name="payment[z_p_code]" class="input_field req" id="cc_z_pcode" value="<?=$data['z_p_code'];?>" size="24" type="text" required /><br>
      Credit Card Type:
      <select class="no_multi req" name="payment[cc_type]" id="cc_type" style="width: 150px;" required>
        <option value="">Select Card Type</option>
        <option value="MasterCard">MasterCard</option>
        <option value="Visa">Visa</option>
      </select><br>

      Credit Card Number: <input name="payment[cc_number]" class="input_field_ext req" id="cc_number" value="" size="16" type="text" required /><br>
      Expiry date:
      <select name="payment[exp_month]" id="exp_month" class="req" required>
        <option value="">Month</option>
        <option value="01">01</option>
        <option value="02">02</option>
        <option value="03">03</option>
        <option value="04">04</option>
        <option value="05">05</option>
        <option value="06">06</option>
        <option value="07">07</option>
        <option value="08">08</option>
        <option value="09">09</option>
        <option value="10">10</option>
        <option value="11">11</option>
        <option value="12">12</option>
      </select>
      <?=\Form::select('payment[exp_year]', '', $cboYears, array('id' => 'exp_year', 'class' => 'req', 'required' => 'required'));?>

        CVV/CSC:
        <input name="payment[cc_cvv]" class="input_field req" id="cc_cvv" value="" size="6" type="text" required />
        <input name="payment[realtor_id]" id="real_id" type="hidden" value="<?=$data['realtor_id'];?>">
        <input name="payment[listing_id]" id="lid" type="hidden" value="<?=$data['listing_id'];?>">
        <input name="payment[email]" id="cc_email" type="hidden" value="<?=$data['email'];?>">
        <input name="area" id="area" type="hidden" value="Listing_Upgrade">
        <br>
        <div>
          <div class="sub_line"></div>
          <br>
          <div style="text-align:center;">
            <input type="submit" class="submit_button" name="process" id="process" value="Submit Payment" />
            <div id="cc_sending" name="sending" style="display:none; height: 55px; margin-top: 10px;">
              <?=\Asset::img('preload.gif');?> Sending Information...
            </div>
            <div id="cc_success" name="success" style="display:none; height: 70px; margin-top: 10px;">Thank You! Your Listing Has Been Upgraded!<br>
              <button id="close_button">Close</button>
            </div>
            <div id="cc_error" name="error" style="display:none; height: 55px; margin-top: 10px;"></div>
          </div>
          <div class="fine_print" id="promo_terms">
            This is a one time payment that features your listing on the front page of our site. The featured status will last for 90 days from payment confirmation or until the listing goes offmarket.
          </div>
        </div>
    <?=\Form::close();?>
    <div>
      <?=\Asset::img('paypal.png');?>
    </div>
  </div>
</div>
<script>
	$(document).ready(function() {
		new autoComplete({
			selector: '#cpc_address',
			minChars: 3,
			source: function(term, response){
				$.get(baseUrl + 'autocomplete/cpc.json', { q: term }, function(data){ response(data); });
			}
		});
	});
</script>