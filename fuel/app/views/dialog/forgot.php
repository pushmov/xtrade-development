<div class="modal-header"><?=__('forgot');?>
	<a class="close-reveal-modal"><?=\Asset::img('close_image.png', array('class' => 'modal-close', 'alt' => 'Close', 'data-close' =>''));?></a>
</div>
<div class="modal-body text-center">
	<div id="login_error"></div>
	<?=\Form::open(array('action' => '', 'method' => 'post', 'name' => 'login_form', 'id' => 'login_form'));?>
		<input name="login_type" id="login_type" type="hidden" value="<?=$type;?>"><br>
		<input name="login_email" type="text" class="" id="login_email" value="<?=$saved_email; ?>" placeholder="Email Address"  /><br>
		<button type="button" class="button" id="login_sforgot" name="login_sforgot"><?=__('submit_email');?></button><br>
	<?=\Form::close();?>
	<a href="#" id="back_login" name="back_login"><?=__('back_login');?></a> <br>
	<?=\Html::anchor('signup/' . $type, __('signup'));?>
</div>
