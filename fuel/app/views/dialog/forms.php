<div class="modal-header">Forms Download
	<a class="close-reveal-modal"><?=\Asset::img('close_image.png', array('class' => 'modal-close', 'alt' => 'Close', 'data-close' =>''));?></a>
</div>
<div class="modal-body text-center">
	<ul class="unstyled">
		<li><?=\Html::anchor('assets/pdf/xtrade_form.pdf', 'xTrade Form', array('target' => '_blank'));?></li>
		<li><?=\Html::anchor('assets/pdf/seller_form.pdf', 'Seller Form', array('target' => '_blank'));?><li>
		<li><?=\Html::anchor('assets/pdf/buyer_form.pdf', 'Buyer Form', array('target' => '_blank'))?><li>
	</ul>
	<div class="padded-vertical"><button type="button" class="button modal-close" id="btn_close">Go Back</button></div>
</div>