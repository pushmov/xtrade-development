<div class="modal-header"><span>Your Wants Updated!</span>
    <a class="close-reveal-modal"><?=\Asset::img('close_image.png', array('class' => 'modal-close', 'alt' => 'Close', 'data-close' =>''));?></a>
</div>
<div class="modal-body text-center">
    <p>Your listing wants are complete! Thank you!</p>
    <button id="Home" class="button" onclick="window.location='/'">Dismiss</button>
</div>