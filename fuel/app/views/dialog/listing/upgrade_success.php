<div class="modal-header"><span>Upgrade Success!</span>
    <a class="close-reveal-modal"><?=\Asset::img('close_image.png', array('class' => 'modal-close', 'alt' => 'Close', 'data-close' =>''));?></a>
</div>
<div class="modal-body text-center">
    <p><?=$txt?></p>
    <button type="button" class="button modal-close">Continue</button>
</div>