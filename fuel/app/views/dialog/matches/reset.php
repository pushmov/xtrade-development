<div class="modal-header">Reset Matches
	<a class="close-reveal-modal"><?=\Asset::img('close_image.png', array('class' => 'modal-close', 'alt' => 'Close', 'data-close' =>''));?></a>
</div>
<div class="modal-body text-center">
    <?=\Form::open(array('action' => 'realtors/match/reset.json', 'name' => 'reset', 'method' => 'post'));?>
	<p>This will reset all saved and matched listings.</p>
    <p>Are You Sure?</p>
    <button type="button" class="button reset_submit" id="<?php echo $listing_id; ?>">Yes</button>
	<button type="button" class="button dismiss">No</button>
    <?=\Form::close();?>
</div>