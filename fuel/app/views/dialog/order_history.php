<div class="modal-header"><?=__('order_history');?>
	<a class="close-reveal-modal"><?=\Asset::img('close_image.png', array('class' => 'modal-close', 'alt' => 'Close', 'data-close' =>''));?></a>
</div>
<div class="modal-body">
	<p class=" text-center"><?=__('this_includes_all_upgraded');?></p>
	<table class="bill-history">
		<thead>
			<tr>
				<th style="width: 40%"><?=__('purchase_type');?></th>
				<th style="width: 30%"><?=__('amount_paid');?></th>
				<th style="width: 22%"><?=__('date_paid');?></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($data as $row):?>
			<tr>
				<td style="width: 40%"><?= \Model\Order::forge()->get_type_txt($row['type_id']);?></td>
				<td style="width: 20%">$<?=$row['sub_total'];?></td>
				<td style="width: 30%"><?=$row['order_date'];?></td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
</div>