<div class="modal-header">Payment Details
	<a class="close-reveal-modal"><?=\Asset::img('close_image.png', array('class' => 'modal-close', 'alt' => 'Close', 'data-close' =>''));?></a>
</div>
<div class="modal-body">
	<div class="small-12 medium-7 column">
		<?=\Asset::img('hd_logo.png', array('alt' => 'Xtrade Homes'));?>
		<?=\Form::open(array('id' => 'cc_form', 'name' => 'cc_form', 'method' => 'post', 'action' => ''));?>
			<div class="row">
				<div class="small-12 medium-6 column">
					<label for="first_name">First Name: </label><input name="data[first_name]" id="first_name" value="<?=$data['first_name'];?>" type="text">
				</div>
				<div class="small-12 medium-6 column">
					<label for="last_name">Last Name: </label><input name="data[last_name]" id="last_name" value="<?=$data['last_name'];?>" type="text">
				</div>
			</div>
			<label for="address">Billing Address: </label><input name="data[address]" type="text" id="address" value="<?=$data['address'];?>">
			<label for="pay_cpc_address">City/Province/State/Country: </label><input name="data[cpc_address]" type="text" id="pay_cpc_address" value="<?=$data['cpc_address'];?>" placeholder="Start Typing City">
			<div class="row">
				<div class="small-12 medium-6 end column">
					<label for="z_p_code">Postal/Zip Code: </label><input name="data[z_p_code]" id="z_p_code" value="<?=$data['z_p_code'];?>" type="text">
				</div>
			</div>
			<div class="row">
				<div class="small-12 medium-6 column">
					<label for="cc_type">Credit Card Type: </label>
					<?=\Form::select('data[cc_type]', $data['cc_type'], $data['cbo_cc_type'], array('id' => 'cc_type'));?>
				</div>
				<div class="small-12 medium-6 column">
					<label for="cc_number">Credit Card Number: </label>
					<input name="data[cc_number]" id="cc_number" value="<?=$data['cc_number'];?>" type="text" >
				</div>
			</div>
			<div class="row">
				<div class="small-12 medium-6 column">
					<label for="exp_month">Expiry date: </label>
					<?=\Form::select('data[exp_month]', $data['exp_month'], $data['cbo_month'], array('id' => 'exp_month', 'class' => 'small-6 column'));?>

					<?=\Form::select('data[exp_year]', $data['exp_year'], $cboYears, array('id' => 'exp_year', 'class' => 'small-6 column'));?>
				</div>
				<div class="small-12 medium-6 end column">
					<label for="cc_cvv">CVV/CSC: </label><input name="data[cc_cvv]" id="cc_cvv" type="text" value="<?=$data['cc_cvv'];?>">
				</div>
			</div>
			<input name="data[realtor_id]" id="rid" type="hidden" value="<?=(isset($data['realtor_id'])) ? $data['realtor_id'] : '';?>">
			<input name="data[promo_code]" id="promo_code" type="hidden" value="<?=(isset($data['promo_code'])) ? $data['promo_code'] : '';?>">
			<input name="data[user_type]" id="user_type" type="hidden" value="<?=$data['user_type'];?>">
			<input name="data[broker_id]" id="broker_id" type="hidden" value="<?=$data['broker_id'];?>">
			<input name="data[fee]" id="fee" type="hidden" value="<?=$data['fee'];?>">
			<input name="data[tax]" id="tax" type="hidden" value="">

			<?php if($data['ut'] == \Model\User::UT_VENDOR):?>
				<input name="data[featured_ad]" id="featured_fee" type="hidden" value="<?=$data['featured_ad'];?>">
			<?php else : ?>
				<input name="data[featured_ad]" id="featured_fee" type="hidden" value="0">
			<?php endif; ?>
			<input name="data[featured_tax]" id="featured_tax" type="hidden" value="">

			<input name="data[tax_code]" id="tax_code" type="hidden" value="<?=$data['tax_code'];?>">
			<input name="data[listing_id]" id="listing_id" type="hidden" value="<?=(isset($data['listing_id'])) ? $data['listing_id'] : '';?>">
			<div class="fine-print padded">
				Pricing subject to change.
			</div>
		<?=\Form::close();?>
	</div>
	<div class="small-12 medium-5 column text-right">
		<button id="previous_page" class="button" data-close>Previous Page</button><br>
		<?php if($data['ut'] == \Model\User::UT_REALTOR) : ?>
			<div class="text-left"><strong>Summary Terms</strong>
				<ul>
					<li>One year Subscription</li>
					<li>$99 monthly (Plus applicable taxes)</li>
					<li>Automatically renews</li>
				</ul>
			</div>
		<?php endif; ?>
		<?=\Html::anchor('terms', 'Click Here For Details', array('class' => 'button expanded', 'target' => '_blank'));?>
		<?php if($data['promo_txt'] != '') :?>
			<div id="promo_div">
				<?=$data['promo_txt']?>: <strong class="fee">$<?=number_format(0, 2);?></strong>
			</div>
		<?php endif; ?>
		<?php if($data['fee'] != 0) :?>
			<div id="fee_div">
				<?=$data['summary']?>: <strong id='cc_monthly_fee' class="fee">$<?=number_format($data['fee'], 2);?></strong>
			</div>
		<?php endif; ?>

		<?php if($data['ut'] == \Model\User::UT_VENDOR && $data['featured_ad'] != 0) : ?>
			<?=$data['featured_ad_summary']?>: <strong id='cc_monthly_fee' class="fee-featured-ad">$<?=number_format($data['featured_ad'], 2);?></strong><br>
		<?php endif; ?>
		<?php if ($data['tax_code'] != '') echo $data['tax_code'] . ': ' . number_format($data['tax'], 2) . '<br>'; ?>
		<hr id="summary">
		<?=$data['summary_total']?>: <strong id="cc_monthly_total_fee">$<?=number_format($total, 2);?></strong><br>(Plus applicable taxes)<br>
		<?=\Asset::img('paypal.png', array('alt' => 'Paypal', 'class' => 'padded'));?>
		<button type="button" name="process" id="process_payment" class="button expanded">Click Here to Accept Terms and Submit Payment</button>
		<div id="post_error"></div>
	</div>
</div>
<script>
	$(document).ready(function() {
		new autoComplete({
			selector: '#pay_cpc_address',
			minChars: 3,
			source: function(term, response){
				$.get(baseUrl + 'autocomplete/cpc.json', { q: term }, function(data){ response(data); });
			}
		});
	});
</script>