<div class="modal-header">Payment Error
	<a class="close-reveal-modal"><?=\Asset::img('close_image.png', array('class' => 'modal-close', 'alt' => 'Close', 'data-close' =>''));?></a>
</div>
<div class="modal-body text-center">
	<p>There was an error setting up your payment. Please try again.</p>
	<button type="button" class="button" id="btn_close" name="btn_close" data-close>Close</button>
</div>
