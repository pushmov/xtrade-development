<div class="modal-header">Thank You!
	<a class="close-reveal-modal"><?=\Asset::img('close_image.png', array('class' => 'modal-close', 'alt' => 'Close', 'data-close' =>''));?></a>
</div>
<div class="modal-body text-center">
	<p>Thank You! A confirmation email has been sent to you to activate your account.</p>
	<button type="button" class="button" id="btn_close" name="btn_close">Close</button>
</div>
