<div class="modal-header">Thank You!
	<a class="close-reveal-modal"><?=\Asset::img('close_image.png', array('class' => 'modal-close', 'alt' => 'Close', 'data-close' =>''));?></a>
</div>
<div class="modal-body text-center">
	<p>Thank You! Your account has been setup.</p>
	<button type="button" class="button" id="btn_login" name="btn_login">Login Now</button>
	<button type="button" class="button" id="btn_close" name="btn_close">Close</button>
</div>
<script>
$(document).ready(function() {
	$('#btn_login').on('click', function() {
		window.location = baseUrl + 'realtors';
	});
});
</script>