<div class="modal-header">xTradeHomes Tutorial</div>
<div class="modal-body text-center">
	<p>Almost finished! Now that we've completed the required fields, we'll do our best to fill in other information our client has provided us with.</p>
	<button class="button client-wants-fill-additional" type="button">Next</button>
	<button class="button cancel" type="button">Cancel Tutorial</button>
</div>
