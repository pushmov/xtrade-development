<div class="modal-header">xTradeHomes Tutorial</div>
<div class="modal-body text-center">
	<p>The listing is now complete! If you go to My Listings, you'll see that the xTrade Status is &quot;Ready for xTrading&quot;. Be sure to subscribe to Match Notifications so you'll be alerted immediately of new matches for your listings. Happy xTrading!</p>
	<button class="button done" type="button">Done</button>
</div>
