<div class="modal-header">Unsubscribed
	<a class="close-reveal-modal"><?=\Asset::img('close_image.png', array('class' => 'modal-close', 'alt' => 'Close', 'data-close' =>''));?></a>
</div>
<div class="modal-body text-center">
	<p>You will no longer receive emails.</p>
	<p>If you wish to change your email options, please login and go to your profile page.</p>
	<button type="button" class="button btn-close" id="btn_close" name="btn_close">Close</button>
</div>
