<div class="modal-header">Downgrade Account
	<a class="close-reveal-modal"><?=\Asset::img('close_image.png', array('class' => 'modal-close', 'alt' => 'Close', 'data-close' =>''));?></a>
</div>
<div class="modal-body text-center">
	<p>This will downgrade your account.<br>Are You Sure?</p>
	<button type="button" class="button downgrade_vendor">Downgrade My Account</button>
	<button type="button" class="button" id="btn_close" data-close>No! Go Back!</button><br>
</div>
