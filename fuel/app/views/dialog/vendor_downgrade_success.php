<div class="modal-header"><?=__('success')?>
	<a class="close-reveal-modal"><?=\Asset::img('close_image.png', array('class' => 'modal-close', 'alt' => 'Close', 'data-close' =>''));?></a>
</div>
<div class="modal-body text-center">
	<p><?=$txt?></p>
	<button type="button" class="button" id="btn_close" data-close>Continue</button>
</div>