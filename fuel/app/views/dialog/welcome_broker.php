<div class="modal-header">Welcome to xTradeHomes
	<a class="close-reveal-modal"><?=\Asset::img('close_image.png', array('class' => 'modal-close', 'alt' => 'Close', 'data-close' =>''));?></a>
</div>
<div class="modal-body text-center">
	<p>Welcome to xTradeHomes!<br>
	Please review all the information on this page then click Add REALTOR&reg; to invite your agents to sign up for an account too.</p>
	<button type="button" class="button" id="btn_close" name="btn_close">Review Account</button>
</div>
