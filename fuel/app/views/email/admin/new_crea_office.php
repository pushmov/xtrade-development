Below is a list of new offices from the crea feed : <?=date_create()->format('Y-m-d H:i:s');?>
<table>
	<tr>
		<th>Office</th>
		<th>Address</th>
		<th>City</th>
		<th>Phone</th>
		<th>CREA ID</th>
		<th>Listings</th>
	</tr>
	<?php foreach($data as $row): ?>
		<tr>
			<td><?=$row['officename'];?></td>
			<td><?=$row['officeaddress'];?></td>
			<td><?=$row['officecity'];?></td>
			<td><?=$row['officephone'];?></td>
			<td><?=$row['officeid'];?></td>
			<td><?=$row['cnt'];?></td>
		</tr>
		<?php endforeach; ?>
</table>