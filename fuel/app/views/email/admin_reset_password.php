<?=\View::forge('email/header');?>
You must use the link provided within this email to login again.
<br />
<?=\Html::anchor($link, 'Click here to login and create a new password.');?>
<br />
<?=$temp_pass_txt?>
<br />
If you cannot click on the link please copy and paste this link into your browsers address bar:
<br />
<?=\Uri::base(false).$link?>
<br /><br />
<br />If you have received this email and did not reset your password, please inform xTradeHomes by iRex at <?=\Html::mail_to('accounts@xtradehomes.com', 'this email');?>
<?=\View::forge('email/footer');?>