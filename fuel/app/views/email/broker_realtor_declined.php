<?=\View::forge('email/header');?>
Hello <?=$first_name . ' ' . $last_name;?>!
<br>
Your broker, <?=$company_name;?>, has declined your request to be added to their account;<br>
<?=\Html::anchor($url, 'Click here');?> to sign in.
<br>
<br>
If you cannot click on the link, copy and paste this url: <?=\Html::anchor($url, $url);?>
<br>
<br>
Thank You
<?=\View::forge('email/footer');?>