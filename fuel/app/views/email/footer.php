<br>
<div align="center">
	<?php if(isset($unsubscribe_link)): ?>
		<span style="color: grey;">
			xTradeHomes values your privacy.
			If you wish to stop receiving these emails, click <?=\Html::anchor($unsubscribe_link, 'here');?> to unsubscribe.
		</span>
		<br>
		<br>
	<?php endif; ?>
	TradeHomes by iRex, Inc - <span style="color: navy;">#370 - 425 Carrall Street,	&nbsp;Vancouver, BC V6B 6E3</span>
	<br>
	<?=isset($gst_num) ? $gst_num : '';?>
	<br>
	<img src="/assets/images/email/twitter.png" />
    <img src="/assets/images/email/google.png" />
    <img src="/assets/images/email/facebook.png" />
</div>
</body>
</html>
