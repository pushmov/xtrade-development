<br>
Hello <?=$first_name?> <?=$last_name?>,<br>
<br>
We noticed that you have active listing(s) not ready for xTrading and your Clients Wants are not completed. In order for a property to be included in the xTradeHomes matching process this information is necessary. Click on the link below to update now.<br>
<br>
<?=$email_info?>
<br>
You have the option to send a private invitation to your Client asking them help you fill in their information or if you wish to exclude a property from the matching process, change the Type to Sell Only on your Members Dashboard.<br>
<br>
Thank you. 
<br>