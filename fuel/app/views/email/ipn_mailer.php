<html>
    <head></head>
    <body>
        
      <?php echo $head; ?><br />
      
      <?php if(isset($homebase_id)) : ?>
      Realtor/Vendor/Listing: <?php echo $homebase_id; ?>
      <br />
      <?php endif; ?>
      
      <?php if(isset($pay_stat)) :?>
      Code / Pay Status: <?php echo $pay_stat; ?>
      <br />
      <?php endif; ?>
      
      <?php if(isset($ipn_type)) : ?>
      Type: <?php echo $ipn_type; ?>
      <br />
      <?php endif; ?>
      
      <?php if(isset($payer_id)) : ?>
      Updated Listing With/Paypal ID: <?php echo $payer_id; ?>
      <br />
      <?php endif; ?>
      
      <?php if(isset($trans_id)) : ?>
      Transaction Code/ID: <?php echo $trans_id; ?>
      <br />
      <?php endif; ?>
      
      <?php if(isset($trans_type)) : ?>
      Transaction Type: <?php echo $trans_type; ?>
      <br />
      <?php endif; ?>
      
      <?php if(isset($body)) : ?>
        <?=$body?>
      <?php endif; ?>
    </body>
</html>