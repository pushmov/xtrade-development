<br>
The following listing(s) on xTradeHomes are about to expire:
<br>
<?=$email_info?>
<br>
This is a friendly reminder that your listing above is about to expire.
<br>
If you will be extending your listing; please login and update the Expiry Date or it will be suppressed from searches.
<br>
Thank you. 
<br>