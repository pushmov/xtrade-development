<table width="756" border="0" cellspacing="5px">
	<tr>
		<td>
			<span>xTrade Match:</span> &nbsp;&nbsp;&nbsp; 
			<span style="color: rgba(36,157,219,1);font-weight: 600;">4-way trade</span>
			<br>
			<br>
			<?php foreach($x_3_match as $match => $next_match_1): ?>
				<table width="756" border="0" cellspacing="5px" style="padding-top: 10px;">
					<tr>
						<td align="center" colspan="7">
							<img src="<?=\Config::get('uri').'assets/images/email/4_way_arrow_r.png'?>" alt="" />
						</td>
					</tr>
					<tr>
						<td align="center"><?=$this_listing_box?></td>
						<td align="center"><img src="<?=\Config::get('uri').'assets/images/email/shuffle.png'?>" alt="" /></td>
						<?php foreach($next_match_1 as $match_1 => $next_match_2): ?>
						<td align="center"><?=  \Match::forge()->setup_box_information_email($match_1, 'B', null, false, true)?></td>
							<td align="center"><img src="<?=\Config::get('uri').'assets/images/email/shuffle.png'?>" alt="" /></td>
							<?php foreach($next_match_2 as $next_match_3 => $next_match_4): ?>
								<td align="center"><?=  \Match::forge()->setup_box_information_email($next_match_3, 'C', 1, false, true); ?></td>
								<td align="center"><img src="<?=\Config::get('uri').'assets/images/email/shuffle.png'?>" alt="" /></td>
								<td align="center"><?=  \Match::forge()->setup_box_information_email($next_match_4, 'D', 1, false, true); ?></td>
							<?php endforeach; ?>
						<?php endforeach; ?>
					</tr>
				</table>
				<br>	
			<?php endforeach; ?>
		</td>
	</tr>
</table>
				