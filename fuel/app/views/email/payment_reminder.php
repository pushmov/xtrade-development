<?=\View::forge('email/header');?>
<p>Thank you for being a valued part of our growing community of agents who are using xTradeHomes to find real estate matches for their listing and buyer clients.</p>
<p>We just wanted to remind you that we'll be requesting a withdrawal soon for the upcoming billing on your xTradeHomes monthly membership fee.</p>
<p>Your payment of <?=$pay_total;?> is scheduled for <?=$pay_date;?> and will be charged to your PayPal account.</p>
<p>The payment will show on your bank statement as a PayPal purchase. Your billing history is available in your xTradeHomes Profile page under the accounts menu.</p>
<p>Here are the details of your upcoming payment:<br>
xTradeHomes Monthly Membership Fee: $<?=$pay_rate?> per Month + Plus Applicable Taxes: $<?=$pay_tax?> = Total Billing: $<?=$pay_total?> CAD<br>
Billed to: <?php echo "{$paypal_first_name} {$paypal_last_name}";?>, xTradeHomes Membership ID: <?=$realtor_id?>, Email: <?=$paypal_email;?></p>
<p>Sincerely,</p>
<p>xTradeHomes Customer Service</p>
<p>If you run into any problems using our service we'd be glad to help. Please email <?=\Html::anchor('helpdesk@xtradehomes.com', 'helpdesk@xtradehomes.com');?> to open a support ticket and a member of our customer support staff will respond as soon as possible.</p>
<?=\View::forge('email/footer');?>