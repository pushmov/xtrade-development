<?=\View::forge('email/header');?>
<p>Thank you for advertising with xTradeHomes, <?=$first_name.' '.$last_name?> at <?=$company_name?>!</p>
<p>Account Number: <?=$vendor_id?><br>
We will review your application and once complete, your ad will appear on the site usually within 48 hours.
</p>
<p>The information you provided to us is below.<br>
Contact: <?=$first_name.' '.$last_name?><br>
Company Name: <?=$company_name?><br>
Address: <?=join(' ', array($address, $location_address, $z_p_code))?><br>
Email: <?=$email?><br>
<?php if($web_site != '') : ?>
Website: <?=\Html::anchor($web_site, $web_site);?><br>
<?php endif;?>

<?php if($featured == \Model\Vendor::VENDOR_STATUS_FEATURED) :?>
Featured : Yes<br>
<?php endif;?>
</p>
<p>Areas you wish to advertise in:</p>
<ul>
	<li><?=str_replace(';', '</li><li>', $locale)?></li>
</ul>
<p>Categories you are advertising in:</p>
<ul>
	<li><?=str_replace(';', '</li><li>', $type)?></li>
</ul>
<p>Listing Description:</p>
<?=$comments?>
<p><a href="<?=\Uri::create('/',array(),array('showlogin' => 'vendor'));?>">Click Here</a> to login to your xTradeHomes account.</p>
<p>Have any questions? We’re here to help! Email us at <a href='mailto:info@xtradehomes.com'>info@xtradehomes.com</a>.</p>
<p>Sincerely,</p>
<p>xTradeHomes Customer Service</p>
<?=\View::forge('email/footer');?>