<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>xTradeHomes</title>
		<?=\Asset::css('auto-complete.css');?>
		<?=\Asset::css('app.css');?>
		<?=\Asset::css('lightslider.min.css');?>
		<?=\Asset::css('//vjs.zencdn.net/5.4.6/video-js.min.css');?>
		<?=\Asset::css('slicknav.min.css');?>
		<?=\Asset::css('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');?>
		<?=\Asset::css('style.css');?>
		<?=\Asset::css('tooltips.css');?>
		<?=\Asset::js('//code.jquery.com/jquery-latest.min.js');?>
		<?=\Asset::js('foundation.min.js');?>
		<?=\Asset::js('lightslider.min.js');?>
		<?=\Asset::js('jquery.slicknav.min.js');?>
		<?=\Asset::js('auto-complete.min.js');?>
		<?=\Asset::js('common.js');?>

		<script>
			var baseUrl = '<?=Uri::base(false);?>';
			$(document).ready(function() {
				$("#light_slider").lightSlider({
					item: 1,
					auto: true,
					controls: false,
					loop: true,
					pause: 3000,
					speed: 800,
					gallery: false
				});
				$('#see_how').on('click', function(e) {
					e.preventDefault();
					$('#video_title').html('');
					$('.close-video').on('click', function(e) {
						e.preventDefault();
						//video.pause();
						$('#video_div').html('<iframe width="560" height="315" src="https://www.youtube.com/embed/ap9-lEMKlaM?autoplay=0&autohide=2&showinfo=0&rel=0" frameborder="0" allowfullscreen></iframe>');
						$('#video_dialog').foundation('close');
					});
					$('#video_dialog').foundation('open');
					//					video.currentTime(0);
					//					video.play();
					//					video.on('ended', function() {
					//						$('#video_title').html('<button class="button close-video" data-close> Close Video </button>');
					//					});
				});
				<?php if (isset($show_video)) echo '$("#see_how").click()'; ?>
				//var video = videojs("see_video", {fluid: true, controls: true, autoplay: false}, function(){});
			});
			<?=$tracking_code;?>
			<?php if (isset($js_vars)) {foreach ($js_vars as $k => $v) echo "var $k = $v;", PHP_EOL;} ?>
		</script>
		<?php
		if(isset($scripts)) :
			echo \Asset::js($scripts, array(), null, false);
			endif;
		if (isset($showlogin)): echo $showlogin; endif;
		?>
	</head>
	<body>
		<div id="ajax_loading"></div>
		<div class="home-page">
			<div id="header">
				<div class="row">
					<div class="small-12 medium-12 large-5 column padded-top">
						<?=\Html::anchor('/', \Asset::img('hd_logo.png', array('alt' => 'Xtrade Homes', 'class' => 'float-left')));?>
						<h1 class="evo-title-index">The Evolution Of Real Estate</h1>
					</div>
					<div class="small-12 medium-12 large-7 column t-nav">
						<ul class="unstyled" id="site_menu">
							<li><?=\Html::anchor('about', 'About Us');?></li>
							<li><?=\html::anchor('hiw', 'How xTrading Works');?></li>
							<li><?=\Html::anchor('faq', 'FAQ');?></li>
							<?php if(isset($user_info)): ?>
							<li><?=$user_info?></li>
							<li><button class="button logout_button" name="<?=$logout?>">Logout</button></li>
							<?php else: ?>
							<li><button class="button login_button">Member Login/Join</button></li>
							<?php endif; ?>
						</ul>
					</div>
				</div>
				<span id="small_menu"></span>
			</div>
			<div class="ct-home">
				<div class="row home-content">
					<div class="home-main-content small-12 medium-6 large-6 column">
						<?=\Form::open(array('action' => '', 'method' => 'post', 'name' => 'home_search', 'id' => 'home_search'));?>
							<div class="input-group">
								<input name="search_bar" id="search_bar" type="text" size="30" class="input-group-field home-search" placeholder="Search by city">
								<div class="input-group-button">
									<button class="button">Search</button>
								</div>
							</div>
						<?=\Form::close();?>
						<h1>Help your client find their real estate match. Quick and efficient!</h1>
						<span>Real Estate Agents can use xTradeHomes to bring together multiple sellers to trade homes down the street, across town or around the country.<br />
						</span><br>
						<h2><?=\Html::anchor('hiw', 'Learn How', array('class' => 'button large'));?> &nbsp; <button type="button" class="button large" id="see_how">See How</button></h2>
					</div>
					<div class="small-12 medium-6 large-6 column">
						<div class="join-box">
							<h2>Real Estate Agents:</h2>
							<h3>Join xTradeHomes<br />
								and Increase Sales! </h3>
							<?=\Html::anchor('signup/realtor', '<div class="button secondary" onClick="">Signup Now</div>');?>
						</div>
					</div>
				</div>
				<div class="row home-bottom">
					<div class="home-main-content small-12 medium-6 large-6 column">
						<h2>Learn How xTradeHomes Offers More!</h2>
						<ul>
							<li>Excellent Listing and Selling Tool <div class="tooltip"><?=\Asset::img('qmark.png')?>
								<div class="tooltip-txt"><h4>Perfect Marketing Tool for Clients who Just Want to Sell First</h4>
									<p>In a <strong>heated market</strong>, xTrading&trade; is what you and your client have been looking for! Now, through you, they’ll have the security of listing and selling knowing that they'll find a new home.<br>
										In a <strong>buyers market</strong>, xTrading&trade; helps your client finally sell their house and find a new one all at the same time.<br>
										<strong>NOTE:</strong> It is crucial to make sure that your client's <strong>"Wants"</strong> form has been filled out. Without this, no matches will be found.</p>
								</div></div>
							</li>
							<li>Perfect Marketing Tool for Sellers <div class="tooltip"><?=\Asset::img('qmark.png')?>
								<div class="tooltip-txt"><h4>xTrading&trade; is an Excellent Listing and Selling Tool</h4>
									<p>Your listings are now exposed to all <strong>Buyers</strong> and <strong>Buyers Agents</strong> in the US and Canada.</p>
								</div></div>
							</li>
							<li>Super Resource for Buyers Agents <div class="tooltip"><?=\Asset::img('qmark.png')?>
								<div class="tooltip-txt"><h4>Super Resource for Buyers Agents</h4>
									<p><strong>Agents</strong>, you can securely register your clients and perform very detailed searches for them. Based on this information, xTradeHomes will then find the appropriate matches. When they are found, you are notified and can peruse the matches to confirm that they are suitable. You have the ability to select, delete or save any of the searches for future use.<br>
										<strong>Note:</strong> Client records and information are for your use only and are never shared. Our job is to help you build a detailed database for your current and future use.</p>
								</div></div></li>
							<li>Great Tool for Buyers Searching <div class="tooltip"><?=\Asset::img('qmark.png')?>
								<div class="tooltip-txt"><h4>Great Tool for Buyers Searching for Property</h4>
									<p>Buyers will be able to perform property searches anywhere in Canada or the US. When a property of interest is selected, instructions are given on how to connect with the listing Realtor&reg;<br>
										At xTradeHomes, only the listing agent gets the lead. We feel strongly about this. You did the work, and you deserve the leads.
										There are never any charges to anyone for this service.</p>
								</div></div></li>
						</ul>
					</div>
					<div class="small-12 medium-6 large-6 column">
						<div class="featured-homepage">
							<ul id="light_slider">
								<li>
									<?=\Html::anchor('signup/realtor', \Asset::img('scroll/sc_1.jpg'));?>
								</li>
								<li>
									<?=\Html::anchor('signup/realtor', \Asset::img('scroll/sc_2.jpg'));?>
								</li>
								<li>
									<?=\Html::anchor('signup/realtor', \Asset::img('scroll/sc_3.jpg'));?>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div id="footer">
				<div class="row">
					<div class="small-12 medium-2 column">
						<?=\Html::anchor('/', \Asset::img('ft_logo.png', array('alt' => 'Xtrade Homes')));?>
					</div>
					<div class='small-12 medium-8 column b-nav'>
						<?=\Html::anchor('about', 'About Us');?>
						<?php if(!isset($user_info)): ?>
							<a href="#" class="no-link" id="broker_login">Brokers</a>
							<a href="#" class="no-link" id="vendor_login">Advertisers</a>
						<?php endif; ?>
						<?=\Html::anchor('contact', 'Contact Us');?>
						<?=\Html::anchor('privacy', 'Privacy Policy');?>
						<?=\Html::anchor('terms', 'Terms & Conditions');?><br>
						<em class="smaller">The trademarks REALTOR&reg;, REALTORS&reg;, and the REALTOR&reg; logo are controlled by The Canadian Real Estate Association (CREA) and identify real estate professionals who are members of CREA.  The trademark DDF&reg; is owned by CREA and identifies CREA's Data Distribution Facility (DDF&reg;).  xTradeHomes receives accurate, reliable, and current listing information from REALTORS&reg; through CREA's DDF&reg;</em>
					</div>
					<div class="small-12 medium-2 column padded-top">
						<span title="The trademarks REALTOR&reg;, REALTORS&reg; and the REALTOR&reg; logo are controlled by The	Canadian Real Estate Association (CREA) and identify real estate professionals who are members of CREA" >
							REALTOR&reg;</span> <?=\Asset::img('ddf.jpg', array('title' => 'The trademark DDF&reg; is owned by The Canadian Real Estate Association (CREA) and identifies CREA’s Data Distribution Facility (DDF&reg;)')); ?>
					</div>
				</div>
			</div>
		</div>
		<div id="login_dialog" class="reveal" data-reveal data-close-on-click="false"></div>
		<div id="video_dialog" class="reveal see-video" data-reveal data-close-on-click="false">
			<div class="text-right video-title"><span id="video_title"></span><?=\Asset::img('close_image2.png', array('class' => 'close-video', 'alt' => 'Close', 'data-close' =>''));?></div>
			<?php /*
			<div class="text-center" id="video_div"><video id="see_video" class="video-js">
			<source src="/assets/images/xTradeHomes_v003.mp4" type="video/mp4">
			<source src="/assets/images/xTradeHomes_v003.webm" type="video/webm">
			<source src="/assets/images/xTradeHomes_v003.ogg" type="video/ogg; codecs=theora, vorbis">
			Your browser does not support the HTML5 video tag. Try updating your browser or using a different one.</video>
			</div>
			*/?>
			<div class="text-center" id="video_div"><iframe width="560" height="315" src="https://www.youtube.com/embed/ap9-lEMKlaM?autoplay=0&autohide=2&showinfo=0&rel=0" frameborder="0" allowfullscreen></iframe></div>
		</div>
		<script>$(document).foundation();</script>
		<?php /*<script src="//vjs.zencdn.net/5.4.6/video.min.js"></script>*/?>
	</body>
</html>