<?php if($feed == \Model\Listing::FEED_IMPORTED) :?>
<div class="row">
	<?php foreach($agent as $row) : ?>
	<div class="small-12 medium-6 column">
		<div><em><?= \Model\Crea\Agent::forge()->get_agent_type($row['pos'])?></em></div>
		<h5><?=$row['name'];?></h5>
		<h6 class="clear"><?=$row['broker'];?></h6>
		<?php if($row['phone'] != '' && $row['phone'] != '0') : ?>
		<div class="small-12">Office: <?=$row['phone'];?></div>
		<?php endif;?>
		<?php if($row['mobile'] != '' && $row['mobile'] != '0') : ?>
		<div class="small-12">Cell: <?= $row['mobile'];?></div>
		<?php endif; ?>

		<div class="small-12">
			<?=\Form::button('button', 'Contact Agent', array('type' => 'button', 'class' => 'button contact_agent', 'data-id' => $param))?>
			<?php if($has_wants && $is_logged_in && $row['pos'] == \Model\Crea\Agent::AGENT_PRIMARY): ?>
			<?=\Form::button('button', 'See What This Client Wants', array('type' => 'button', 'class' => 'button client_wants'))?>
			<?php endif; ?>
		</div>
	</div>
	<?php endforeach;?>
</div>
<?php elseif($feed == \Model\Listing::FEED_NON_IMPORTED) :?>
	<em class="clear">Listing Agent</em>
	<div class="row">
		<div class="small-12 column">
			<h5><?=$agent['name'];?></h5>
		</div>
		<?php if($agent['broker'] != '') : ?>
			<h6 class="column"><?=$agent['broker'];?></h6>
		<?php endif; ?>
		<div class="small-12 column">Office: <?=\Num::format_phone($agent['phone']);?></div>
		<?php if($agent['mobile'] != '' && $agent['mobile'] != '0'):?>
			<div class="small-12 column">Cell: &nbsp; &nbsp; <?=\Num::format_phone($agent['mobile']);?></div>
		<?php endif;?>
		<div class="small-12 column">
			<?=\Form::button('button', 'Contact xTrade Agent', array('type' => 'button', 'class' => 'button contact_agent', 'data-id' => $param))?>
			<?php if($has_wants && $is_logged_in): ?>
				<?=\Form::button('button', 'See What This Client Wants', array('type' => 'button', 'class' => 'button client_wants'))?>
			<?php endif; ?>
		</div>
	</div>
<?php endif;?>