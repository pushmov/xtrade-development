<script src="//maps.googleapis.com/maps/api/js?v=3&key=<?=$data['map_key'];?>" type="text/javascript"></script>
<script type="text/javascript" src="/assets/js/buttons.js"></script>
<div class="content column">
	<div class="row detail-top">
		<div class="small-12 medium-6 large-6 columns text-left">
			<h3>xTradeHomes Listing Details</h3>
		</div>
		<div class="small-12 medium-6 large-6 columns text-right">
			<div class="padded-top">
				<?=\Asset::img('dir_image.png', array('width' => 14, 'height' => 14, 'alt' => 'directory services'));?>
				<?=\Html::anchor('vendor/location/'.$data['location'], 'Local Services Directory', array('style' => 'margin-left: 0px;'));?>
			</div>
		</div>
	</div>

	<div class="details-content padded-top padded-bottom">
		<?php if($data['listing_status'] == \Model\Listing::LS_SOLD_EX): ?>
			<div class="listing_sold"></div>
		<?php elseif($data['listing_status'] == \Model\Listing::LS_PENDING_EX): ?>
			<div class="listing_pending"></div>
		<?php endif; ?>

		<div class="fine-print">
			<?=$data['listing_num'];?>
		</div>
		<div class="row">
			<div class="small-12 large-6 column">
				<ul id="light_slider" class="listing">
					<?=$data['photos']['images_list'];?>
				</ul>
			</div>
			<div class="small-12 large-6 column listing-details">
				<div>
					<div class="price-nav details-price clearfix">
						<div class="nav-items"><?=$data['nav'];?></div>
						<h2>$<?=number_format($data['listing_price'], 0);?></h2>
					</div>
					<p class="clear address"><?=preg_replace("/,/", ", ", $data['address']); ?></p>
					<em>Listing Information</em>
					<div class="row">
						<div class="small-12 medium-6 column">
							<?= $data['left_options']; ?>
						</div>
						<div class="small-12 medium-6 column">
							<?= $data['right_options']; ?>
						</div>
					</div>
					<div class="listing-details-center">
						<div class="row">
							<div class="listing-details-column column small-12 medium-6 view-map">
								<?=\Asset::img('map_image.png', array('width' => 25, 'height' => 25, 'alt' => 'View Map'));?>
								View Map
							</div>

							<div class="listing-details-column column small-12 medium-6">
								<div class="addthis_custom_sharing">
									<span class="st_email_large" displayText="Email"></span>
									<span class="st_facebook_large" displayText="Facebook"></span>
									<span class="st_twitter_large" displayText="Tweet"></span>
									<span class="st_linkedin_large" displayText="LinkedIn"></span>
									<span class="st_pinterest_large" displayText="Pinterest"></span>
									<span class="st_googleplus_large" st_url="<?=\Uri::current();?>" st_title="<?=preg_replace("/,/", ", ", $data['address']); ?>" st_summary="<?=preg_replace("/,/", ", ", $data['address']); ?>"></span>
								</div>
							</div>
						</div>
					</div>

					<!-- listing agent block -->
					<?=\View::forge('listing/agent', $data)?>

				</div>
			</div>
		</div>
		<?php if(!empty($data['extra_options'])) :?>
			<div class="row small-up-1 medium-up-2 large-up-3 extra-options">
				<?=$data['extra_options'];?>
			</div>
		<?php endif;?>

		<?php if(!empty($data['features'])) :?>
			<div class="row">
				<div class="column">
					<?php foreach($data['features'] as $title => $list): ?>
						<div class="small-12 medium-4 large-3 features_list">
							<div class="padded-top">
								<em><?=__($title);?></em>
								<?=$list?>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		<?php endif; ?>

		<div class="padded-bottom padded-top">
			<?php if($data['comments'] != '') : ?>
				<h6>Additional Comments</h6>
				<p><?=preg_replace("/\\|\\|/", "<br><br>", $data['comments']);?></p>
			<?php endif; ?>
		</div>
	</div>
	<div class="details-added text-right padded-bottom">Date Added: <?=date_create($data['date_created'])->format(DATE_FORMAT);?></div>
	<?php if (isset($data['analyticsview'])): ?>
		<?php echo $data['analyticsview'] . PHP_EOL . $data['analyticsclick']; ?>
		<p><button class="button" onclick="redirect();return false;">REALTOR&reg;’s website</button></p>
		<em>This listing content provided by <a href="https://realtor.ca" target="_blank">REALTOR.ca</a> has been licensed by REALTOR&reg; members of <a href="http://crea.ca" target="_blank">The Canadian Real Estate Association</a></em>
	<?php endif; ?>
</div>
<div id="contact_agent_dialog" data-listing="<?=$data['listing_id']?>" data-type="<?=$data['type_of_listing']?>" class="reveal medium" data-reveal></div>
<div id="client_want_dialog" data-id="<?=$data['param']?>" class="reveal medium" data-reveal></div>
<div id="map_dialog" data-lat="<?=$data['lat']?>" data-lon="<?=$data['lng']?>" class="reveal medium" data-reveal></div>
<script>
	$(document).ready(function() {
		$("#light_slider").lightSlider({
			gallery:true,
			item:1,
			loop:true,
			thumbItem:8,
			slideMargin:0,
			enableDrag: false
		});
		stButtons.locateElements();
	});
</script>