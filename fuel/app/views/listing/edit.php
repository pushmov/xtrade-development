<div class="content column">
	<?=\Form::open(array('id' => 'main_form', 'name' => 'main_form', 'method' => 'post', 'action' => ''));?>
	<input name="listing_id" type="hidden" value="<?=$edit['listing_id']; ?>">
	<input name="sub_type" id="sub_type" type="hidden" value="<?=$edit['sub_type']; ?>">
	<div class="submit_overlay"></div>
	<div class="btn-submit-content">
		<?=\Form::button('submit_content', 'Save Now', array('class' => 'wants_buttons submit_content', 'id' => 'submit_content', 'type' => 'button'))?>
	</div>
	<div class="btn-submit-content btn-next-area" id="_message">
		<?=\Form::button('scroll_to_next', 'Almost Finished! Go To Next Area', array('type' => 'button', 'class' => 'wants_buttons submit_content', 'id' => 'scroll_to_next'));?>
	</div>
	<div class="btn-submit-content btn-return-home">
		<?=\Form::button('return_home', 'Return to Home', array('type' => 'button', 'class' => 'wants_buttons return_home', 'id' => 'return_home'));?>
	</div>
	<div class="submitting multi_option btn-submit-content">
		<?=\Asset::img('preload.gif', array('width' => 36, 'height' => 35, 'style' => 'vertical-align: middle;'));?> Please Wait, Submitting Content
	</div>
	<ul class="accordion" data-accordion role="tablist" data-multi-expand="true">
		<?=$client_info; ?>
		<?=$has_info; ?>
		<?=$image_info; ?>
		<?=$wants_info; ?>
	</ul>
	<?=\Form::close();?>
	<div id="edit_listing_dialog" class="reveal tiny" data-reveal data-close-on-click="false"></div>
	<div id="page_dialog" class="reveal tiny" data-reveal></div>
	<div id="tutorial_dialog" class="reveal medium" data-reveal data-close-on-click="false">
		<div class="modal-header">xTradeHomes Tutorial
		<a class="close-reveal-modal"><?=\Asset::img('close_image.png', array('class' => 'modal-close', 'alt' => 'Close', 'data-close' => ''));?></a>
		</div>
		<div class="modal-body text-center">
			<p><i class="fa fa-spinner fa-spin"></i>Processing...</p>
		</div>
	</div>
</div>