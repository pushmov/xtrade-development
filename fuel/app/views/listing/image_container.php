<input name="image_order" id="image_order" type="hidden" value="<?=$image_order;?>">
<?php if($imported == \Model\Listing::NON_IMPORTED): ?>
	<h5>Drag &amp; Drop Photos To Re-order</h5>
	<?php else :?>
	<h5>Listing Photos Where Imported And Can Not Be Changed</h5>
	<?php endif;?>
<div class="row small-up-2 medium-up-6 large-up-8 collapse grid-sortable">
	<?php foreach($images as $image) :?>
		<div class="column">
			<?=\Asset::img($image['url'], array('class' => 'thumbnail', 'data-id' => $image['id']))?>
		</div>
	<?php endforeach; ?>
</div>