<div class="clearfix">
	<div class="content">
		<div class="row column search-result-header">
			<div class="small-12 medium-6 column">
				<strong>xTradeHomes Listing Results For	<span id="listing_results_for"><?=$location?></span></strong>
			</div>
			<div class="small-12 medium-6 column text-right">
				<?=\Asset::img('dir_image.png', array('alt' => 'directory services'));?>
				<?=\Html::anchor('vendor/location/'.$location, 'Local Services Directory');?>
			</div>
		</div>
		<div class="row column search-result-filter">
			<div class="clearfix padded-vertical">
				<div class="small-12 medium-4 column">
					<div class="filter-label"><?=\Html::anchor('#', 'Refine Search', array('id' => 'refine_search', 'class' => 'no-link'))?></div>
				</div>
				<div class="small-12 medium-8 column">
					<div class="row" id="sorting_option">
						<?=\Form::open(array('action' => 'listing/search/'.$location.'.html', 'method' => 'get', 'id' => 'refine_form'));?>
						<div class="small-12 medium-6 column">
							<div class="row">
								<div class="small-12 medium-3 column">
									<div class="filter-label">Sort:</div>
								</div>
								<div class="small-12 medium-9 column"><?=\Form::select('sort', $search['sort'], \Model\Listing::forge()->filter_sort_fields(), array('id' => 'sort'));?></div>
							</div>
						</div>
						<div class="small-12 medium-6 column">
							<div class="row">
								<div class="small-12 medium-5 column">
									<div class="filter-label">Num of Listings:</div>
								</div>
								<div class="small-12 medium-7 column"><?=\Form::select('per', $search['limit'], \Model\Listing::forge()->filter_per_page(), array('id' => 'per'));?></div>
							</div>
						</div>
						<?=\Form::close();?>
					</div>
				</div>
			</div>
		</div>
		<div id="results_div" class="padded-vertical result-div">
			<div class="text-center">
				<p><i class="fa fa-spinner fa-spin"></i>Processing...</p>
			</div>
		</div>
	</div>
</div>
<?php if(!empty($featured)): ?>
	<div class="clearfix content padded" id="featured_list">
		<div class="column">
			<h3>Featured Homes</h3>
			<div class="listing">
				<ul id="light_slider" class="listing featured-home">
					<?php foreach($featured as $row):?>
						<li><?=\Html::anchor('listing/detail/'.$row['target'], \Asset::img($row['picture'], array(
								'class' => 'thumbnail tooltip',
								'data-id' => $row['id']
							)));?>
						</li>
						<?php endforeach; ?>
				</ul>
			</div>
		</div>
	</div>
	<?php endif; ?>
<div id="refine_dialog" class="reveal medium refine-dialog" data-reveal></div>