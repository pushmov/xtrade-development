<div class="content column">
	<div style="position: fixed; bottom: 25px; right: 25px; z-index: 100;">
		<?=$button['return_home'];?>
	</div>
	<div class="row matches-client-house">
		<div class="padded-sides">
			<div class="small-12 medium-9 large-6 columns">
				<div class="row">
					<div class="small-12 medium-5 large-5 columns">
						<?=\Asset::img($client_house_img, array('width' => 170, 'height' => 129, 'alt' => 'Listing'));?>
					</div>
					<div class="small-12 medium-7 large-7 columns">
						<ul class="unstyled">
							<li><?=$header['address']?></li>
							<li><?=$header['client']?></li>
							<li><?=$header['listing_num']?></li>
							<li><?=$header['public_status']?></li>
							<li><?=$header['xtrade_status']?></li>
						</ul>
					</div>
				</div>
			</div>

			<div class="small-12 medium-3 large-6 columns action-group">
				<div><?=$button['optimize'];?></div>
				<div><?=\Form::button('hiw', __('hiw'), array('type' => 'button', 'class' => 'button hiw'))?></div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="padded-sides">
			<div class="text-right padded-bottom padded-top">
				<h3><?= ($total_matches > 0) ? $total_matches.$found_matches : 'No Matches Found' ?></h3>
				<?php if($total_matches > 10): ?>
					<?=\Html::anchor('realtors/listing/edit/'.$client['info']['listing_id'].'&pos=wants', __('refine'))?>
				<?php endif; ?>
			</div>
			<?php if(!empty($match_exists) && !$inc_maybe):?>
			<button type="button" class="button include match_button reset_button"><?=__('inc_saved_listing')?></button>
			<?php endif; ?>

			<?php if($ignore_listing_exists['count'] > 0 || $total_matches > 0): ?>
			<?=$button['reset'];?>
			<?php endif; ?>

			<?php if($total_matches > 0 && !empty($desirable_all)): ?>
			<?=$button['send_matches']?>
			<?php else :?>
				<p>No matches found! Try to <?=\Html::anchor('realtors/listing/edit/'.$client['listing']['listing_id'].'?pos=wants', __('expand_wants'))?> there may be more matches available <br> or check back later, listings are added daily! </p>
			<?php endif; ?>

			<?php if($total_matches > 0) : ?>
			<button type="button" class="button" id="export_match" data-id="<?=$client['info']['listing_id']?>">Export <?=\Inflector::pluralize('Match', $total_matches);?></button>
			<?php endif; ?>
		</div>
	</div>

	<div class="row matches-matches">
		<?php if(!empty($buyers_match)): ?>
			<div class="matches-title"><span><?=__('direct_match')?>:</span> <span class="matches-blue-text"><?=__('buyers_match')?></span></div>
			<?php foreach($buyers_match as $match): ?>
				<div class="match-box one-way-match-box matches-margin clearfix">
					<div class="sided-arrow-area" data-target="small">
						<canvas></canvas>
						<i class="fa fa-random fa-lg"></i>
					</div>
					<div class="row padded-sides matches-wrapper">
						<i class="fa fa-random fa-lg"></i>
						<canvas class="canvas-arrow"></canvas>
						<div class="list clearfix">
							<!-- Single House Box Start -->
							<?=$this_listing_box?>
							<!-- Single House Box End -->
							<div class="matches-column matches-container matches-hiw">
								<?= \Html::anchor('#', '<i class="fa fa-random fa-lg"></i>', array('title' => '', 'class' => 'no-link')); ?>
							</div>
							<div class="matches-column matches-container sided-random">
								<div class="matches-outer-box">
									<a href="#" class="no-link">
										<i class="fa fa-random fa-lg"></i>
									</a>
								</div>
							</div>
							<!-- Single House Box Start -->
							<?=$match?>
							<!-- Single House Box End -->
						</div>

					</div>
				</div>
				<?php $match_found = true; endforeach; ?>
			<?php endif; ?>


		<!-- x-section -->
		<?php if(!empty($x_1_match_high)) : ?>
			<div class="matches-title"><span><?=__('xtrade_match')?>:</span> <span data-value="x-match-high" class="matches-blue-text"><?=__('two_way_trade')?></span></div>
			<?php foreach($x_1_match_high as $match): ?>
				<div class="match-box two-way-match-box matches-margin clearfix">
					<div class="sided-arrow-area" data-target="small">
						<canvas></canvas>
						<i class="fa fa-random fa-lg"></i>
					</div>
					<div class="row padded-sides matches-wrapper">
						<i class="fa fa-random fa-lg"></i>
						<canvas class="canvas-arrow"></canvas>
						<div class="list clearfix">
							<!-- Single House Box Start -->
							<?= $this_listing_box; ?>
							<!-- Single House Box End -->
							<div class="matches-column matches-container matches-hiw">
								<?= \Html::anchor('hiw', '<i class="fa fa-random fa-lg"></i>', array('target' => '_blank', 'title' => __('hiw_title'), 'class' => 'tooltip')); ?>
							</div>
							<div class="matches-column matches-container sided-random">
								<div class="matches-outer-box">
									<a href="#" target="_blank" class="tooltip" title="<?=__('hiw_title')?>">
										<i class="fa fa-random fa-lg"></i>
									</a>
								</div>
							</div>
							<!-- Single House Box Start -->
							<?=$match?>
							<!-- Single House Box End -->
						</div>

					</div>
				</div>
				<?php $match_found = true; endforeach; ?>
			<?php endif; ?>

		<?php if(!empty($x_1_match_med)) : ?>
			<div class="matches-title"><span><?=__('xtrade_match')?>:</span> <span data-value="x-match-medium" class="matches-blue-text"><?=__('two_way_trade')?></span></div>
			<?php foreach($x_1_match_med as $match): ?>
				<div class="match-box two-way-match-box matches-margin clearfix">
					<div class="sided-arrow-area" data-target="small">
						<canvas></canvas>
						<i class="fa fa-random fa-lg"></i>
					</div>
					<div class="row padded-sides matches-wrapper">
						<i class="fa fa-random fa-lg"></i>
						<canvas class="canvas-arrow"></canvas>
						<div class="list clearfix">
							<!-- Single House Box Start -->
							<?= $this_listing_box; ?>
							<!-- Single House Box End -->
							<div class="matches-column matches-container matches-hiw">
								<?= \Html::anchor('hiw', '<i class="fa fa-random fa-lg"></i>', array('target' => '_blank', 'title' => __('hiw_title'), 'class' => 'tooltip')); ?>
							</div>
							<div class="matches-column matches-container sided-random">
								<div class="matches-outer-box">
									<a href="#" target="_blank" class="tooltip" title="<?=__('hiw_title')?>">
										<i class="fa fa-random fa-lg"></i>
									</a>
								</div>
							</div>

							<!-- Single House Box Start -->
							<?=$match?>
							<!-- Single House Box End -->
						</div>
					</div>
				</div>
				<?php $match_found = true; endforeach; ?>
			<?php endif; ?>

		<?php if(!empty($x_1_match_low)) : ?>
			<div class="matches-title"><span><?=__('xtrade_match')?>:</span> <span data-value="x-match-low" class="matches-blue-text"><?=__('two_way_trade')?></span></div>
			<?php foreach($x_1_match_low as $match): ?>
				<div class="match-box two-way-match-box matches-margin clearfix">
					<div class="sided-arrow-area" data-target="small">
						<canvas></canvas>
						<i class="fa fa-random fa-lg"></i>
					</div>
					<div class="row padded-sides matches-wrapper">
						<i class="fa fa-random fa-lg"></i>
						<canvas class="canvas-arrow"></canvas>
						<div class="list clearfix">
							<!-- Single House Box Start -->
							<?= $this_listing_box; ?>
							<!-- Single House Box End -->
							<div class="matches-column matches-container matches-hiw">
								<?= \Html::anchor('hiw', '<i class="fa fa-random fa-lg"></i>', array('target' => '_blank', 'title' => __('hiw_title'), 'class' => 'tooltip')); ?>
							</div>
							<div class="matches-column matches-container sided-random">
								<div class="matches-outer-box">
									<a href="#" target="_blank" class="tooltip" title="<?=__('hiw_title')?>">
										<i class="fa fa-random fa-lg"></i>
									</a>
								</div>
							</div>
							<!-- Single House Box Start -->
							<?=$match?>
							<!-- Single House Box End -->
						</div>
					</div>
				</div>
				<?php $match_found = true; endforeach; ?>
			<?php endif; ?>
		<!-- end x-section -->


		<!-- y-section -->
		<?php if(!empty($y_1_match_high)) : ?>
			<div class="matches-title"><span><?=__('xtrade_match')?>:</span> <span data-value="y-match-high" class="matches-blue-text"><?=__('two_way_trade')?></span></div>
			<?php foreach($y_1_match_high as $match): ?>
				<div class="match-box two-way-match-box matches-margin clearfix">
					<div class="sided-arrow-area" data-target="small">
						<canvas></canvas>
						<i class="fa fa-random fa-lg"></i>
					</div>
					<div class="row padded-sides matches-wrapper">
						<i class="fa fa-random fa-lg"></i>
						<canvas class="canvas-arrow"></canvas>
						<div class="list clearfix">
							<!-- Single House Box Start -->
							<?= $this_listing_box; ?>
							<!-- Single House Box End -->
							<div class="matches-column matches-container matches-hiw">
								<?= \Html::anchor('hiw', '<i class="fa fa-random fa-lg"></i>', array('target' => '_blank', 'title' => __('hiw_title'), 'class' => 'tooltip')); ?>
							</div>
							<div class="matches-column matches-container sided-random">
								<div class="matches-outer-box">
									<a href="#" target="_blank" class="tooltip" title="<?=__('hiw_title')?>">
										<i class="fa fa-random fa-lg"></i>
									</a>
								</div>
							</div>
							<!-- Single House Box Start -->
							<?=$match?>
							<!-- Single House Box End -->
						</div>

					</div>
				</div>
				<?php $match_found = true; endforeach; ?>
			<?php endif; ?>

		<?php if(!empty($y_1_match_med)) : ?>
			<div class="matches-title"><span><?=__('xtrade_match')?>:</span> <span data-value="y-match-medium" class="matches-blue-text"><?=__('two_way_trade')?></span></div>
			<?php foreach($y_1_match_med as $match): ?>
				<div class="match-box two-way-match-box matches-margin clearfix">
					<div class="sided-arrow-area" data-target="small">
						<canvas></canvas>
						<i class="fa fa-random fa-lg"></i>
					</div>
					<div class="row padded-sides matches-wrapper">
						<i class="fa fa-random fa-lg"></i>
						<canvas class="canvas-arrow"></canvas>
						<div class="list clearfix">
							<!-- Single House Box Start -->
							<?= $this_listing_box; ?>
							<!-- Single House Box End -->
							<div class="matches-column matches-container matches-hiw">
								<?= \Html::anchor('hiw', '<i class="fa fa-random fa-lg"></i>', array('target' => '_blank', 'title' => __('hiw_title'), 'class' => 'tooltip')); ?>
							</div>
							<div class="matches-column matches-container sided-random">
								<div class="matches-outer-box">
									<a href="#" target="_blank" class="tooltip" title="<?=__('hiw_title')?>">
										<i class="fa fa-random fa-lg"></i>
									</a>
								</div>
							</div>
							<!-- Single House Box Start -->
							<?=$match?>
							<!-- Single House Box End -->
						</div>

					</div>
				</div>
				<?php $match_found = true; endforeach; ?>
			<?php endif; ?>

		<?php if(!empty($y_1_match_low)) : ?>
			<div class="matches-title"><span><?=__('xtrade_match')?>:</span> <span data-value="y-match-low" class="matches-blue-text"><?=__('two_way_trade')?></span></div>
			<?php foreach($y_1_match_low as $match): ?>
				<div class="match-box two-way-match-box matches-margin clearfix">
					<div class="sided-arrow-area" data-target="small">
						<canvas></canvas>
						<i class="fa fa-random fa-lg"></i>
					</div>
					<div class="row padded-sides matches-wrapper">
						<i class="fa fa-random fa-lg"></i>
						<canvas class="canvas-arrow"></canvas>
						<div class="list clearfix">
							<!-- Single House Box Start -->
							<?= $this_listing_box; ?>
							<!-- Single House Box End -->
							<div class="matches-column matches-container matches-hiw">
								<?= \Html::anchor('hiw', '<i class="fa fa-random fa-lg"></i>', array('target' => '_blank', 'title' => __('hiw_title'), 'class' => 'tooltip')); ?>
							</div>
							<div class="matches-column matches-container sided-random">
								<div class="matches-outer-box">
									<a href="#" target="_blank" class="tooltip" title="<?=__('hiw_title')?>">
										<i class="fa fa-random fa-lg"></i>
									</a>
								</div>
							</div>
							<!-- Single House Box Start -->
							<?=$match?>
							<!-- Single House Box End -->
						</div>

					</div>
				</div>
				<?php $match_found = true; endforeach; ?>
			<?php endif; ?>
		<!-- y-section -->


		<!-- z-section -->
		<?php if(!empty($z_1_match_high)) : ?>
			<div class="matches-title"><span><?=__('xtrade_match')?>:</span> <span data-value="z-match-high" class="matches-blue-text"><?=__('two_way_trade')?></span></div>
			<?php foreach($x_1_match_high as $match): ?>
				<div class="match-box two-way-match-box matches-margin clearfix">
					<div class="sided-arrow-area" data-target="small">
						<canvas></canvas>
						<i class="fa fa-random fa-lg"></i>
					</div>
					<div class="row padded-sides matches-wrapper">
						<i class="fa fa-random fa-lg"></i>
						<canvas class="canvas-arrow"></canvas>
						<div class="list clearfix">
							<!-- Single House Box Start -->
							<?= $this_listing_box; ?>
							<!-- Single House Box End -->
							<div class="matches-column matches-container matches-hiw">
								<?= \Html::anchor('hiw', '<i class="fa fa-random fa-lg"></i>', array('target' => '_blank', 'title' => __('hiw_title'), 'class' => 'tooltip')); ?>
							</div>
							<div class="matches-column matches-container sided-random">
								<div class="matches-outer-box">
									<a href="#" target="_blank" class="tooltip" title="<?=__('hiw_title')?>">
										<i class="fa fa-random fa-lg"></i>
									</a>
								</div>
							</div>
							<!-- Single House Box Start -->
							<?=$match?>
							<!-- Single House Box End -->
						</div>

					</div>
				</div>
				<?php $match_found = true; endforeach; ?>
			<?php endif; ?>

		<?php if(!empty($z_1_match_med)) : ?>
			<div class="matches-title"><span><?=__('xtrade_match')?>:</span> <span data-value="z-match-medium" class="matches-blue-text"><?=__('two_way_trade')?></span></div>
			<?php foreach($x_1_match_med as $match): ?>
				<div class="match-box two-way-match-box matches-margin clearfix">
					<div class="sided-arrow-area" data-target="small">
						<canvas></canvas>
						<i class="fa fa-random fa-lg"></i>
					</div>
					<div class="row padded-sides matches-wrapper">
						<i class="fa fa-random fa-lg"></i>
						<canvas class="canvas-arrow"></canvas>
						<div class="list clearfix">
							<!-- Single House Box Start -->
							<?= $this_listing_box; ?>
							<!-- Single House Box End -->
							<div class="matches-column matches-container matches-hiw">
								<?= \Html::anchor('hiw', '<i class="fa fa-random fa-lg"></i>', array('target' => '_blank', 'title' => __('hiw_title'), 'class' => 'tooltip')); ?>
							</div>
							<div class="matches-column matches-container sided-random">
								<div class="matches-outer-box">
									<a href="#" target="_blank" class="tooltip" title="<?=__('hiw_title')?>">
										<i class="fa fa-random fa-lg"></i>
									</a>
								</div>
							</div>
							<!-- Single House Box Start -->
							<?=$match?>
							<!-- Single House Box End -->
						</div>

					</div>
				</div>
				<?php $match_found = true; endforeach; ?>
			<?php endif; ?>

		<?php if(!empty($z_1_match_low)) : ?>
			<div class="matches-title"><span><?=__('xtrade_match')?>:</span> <span data-value="z-match-low" class="matches-blue-text"><?=__('two_way_trade')?></span></div>
			<?php foreach($x_1_match_low as $match): ?>
				<div class="match-box two-way-match-box matches-margin clearfix">
					<div class="sided-arrow-area" data-target="small">
						<canvas></canvas>
						<i class="fa fa-random fa-lg"></i>
					</div>
					<div class="row padded-sides matches-wrapper">
						<i class="fa fa-random fa-lg"></i>
						<canvas class="canvas-arrow"></canvas>
						<div class="list clearfix">
							<!-- Single House Box Start -->
							<?= $this_listing_box; ?>
							<!-- Single House Box End -->
							<div class="matches-column matches-container matches-hiw">
								<?= \Html::anchor('hiw', '<i class="fa fa-random fa-lg"></i>', array('target' => '_blank', 'title' => __('hiw_title'), 'class' => 'tooltip')); ?>
							</div>
							<div class="matches-column matches-container sided-random">
								<div class="matches-outer-box">
									<a href="#" target="_blank" class="tooltip" title="<?=__('hiw_title')?>">
										<i class="fa fa-random fa-lg"></i>
									</a>
								</div>
							</div>
							<!-- Single House Box Start -->
							<?=$match?>
							<!-- Single House Box End -->
						</div>

					</div>
				</div>
				<?php $match_found = true; endforeach; ?>
			<?php endif; ?>
		<!-- z-section -->

		<?php if($total_matches > 0){ ?>

			<br>
			<?php } ?>

		<?php if(!empty($x_2_match)):?>
			<div class="matches-title">
				<span><?=__('xtrade_match')?>:</span> <span class="matches-blue-text"><?=__('three_way_trade')?></span>
			</div>
			<?php foreach($x_2_match as $match => $next_match):?>
				<div class="match-box three-way-match-box matches-margin clearfix">
					<div class="sided-arrow-area" data-target="medium">
						<canvas></canvas>
						<i class="fa fa-random fa-lg"></i>
					</div>
					<div class="row padded-sides matches-wrapper">
						<i class="fa fa-random fa-lg"></i>
						<canvas class="canvas-arrow"></canvas>
						<div class="list clearfix">
								<?= $this_listing_box; ?>
								<div class="matches-column matches-container matches-hiw">
									<?= \Html::anchor('hiw', '<i class="fa fa-random fa-lg"></i>', array('target' => '_blank', 'title' => __('hiw_title'), 'class' => 'tooltip')); ?>
								</div>
								<div class="matches-column matches-container sided-random">
									<div class="matches-outer-box">
										<a href="#" target="_blank" class="tooltip" title="<?=__('hiw_title')?>">
											<i class="fa fa-random fa-lg"></i>
										</a>
									</div>
								</div>
								<?php foreach($next_match as $match_1 => $match_2):?>
									<?= \Match::forge()->setup_box_information($match_1, "B", NULL, FALSE, FALSE);?>
									<div class="matches-column matches-container matches-hiw">
										<?= \Html::anchor('hiw', '<i class="fa fa-random fa-lg"></i>', array('target' => '_blank', 'title' => __('hiw_title'), 'class' => 'tooltip')); ?>
									</div>
									<div class="matches-column matches-container sided-random">
										<div class="matches-outer-box">
											<a href="#" target="_blank" class="tooltip" title="<?=__('hiw_title')?>">
												<i class="fa fa-random fa-lg"></i>
											</a>
										</div>
									</div>
									<?= \Match::forge()->setup_box_information($match_2, "C", 1, FALSE, TRUE); ?>
								</div>
								<?php endforeach; ?>

						</div>
				</div>
				<?php endforeach; ?>
			<?php endif; ?>


		<?php if(!empty($x_3_match)):?>
			<div class="matches-title">
				<span><?=__('xtrade_match')?>:</span> <span class="matches-blue-text"><?=__('four_way_trade')?></span>
			</div>
			<?php foreach($x_3_match as $match => $next_match_1) :?>
				<div class="match-box four-way-match-box matches-margin clearfix">
					<div class="sided-arrow-area" data-target="medium">
						<canvas></canvas>
						<i class="fa fa-random fa-lg"></i>
					</div>
					<div class="row padded-sides matches-wrapper">
						<i class="fa fa-random fa-lg"></i>
						<canvas class="canvas-arrow"></canvas>
						<div class="list clearfix">
								<?= $this_listing_box; ?>
								<div class="matches-column matches-container matches-hiw">
									<?= \Html::anchor('hiw', '<i class="fa fa-random fa-lg"></i>', array('target' => '_blank', 'title' => __('hiw_title'), 'class' => 'tooltip')); ?>
								</div>
								<div class="matches-column matches-container sided-random">
									<div class="matches-outer-box">
										<a href="#" target="_blank" class="tooltip" title="<?=__('hiw_title')?>">
											<i class="fa fa-random fa-lg"></i>
										</a>
									</div>
								</div>
								<?php foreach($next_match_1 as $next_match_2 => $next_match_3):?>
									<?= \Match::forge()->setup_box_information($next_match_2, "B", NULL, FALSE, FALSE);?>
									<div class="matches-column matches-container matches-hiw">
										<?= \Html::anchor('hiw', '<i class="fa fa-random fa-lg"></i>', array('target' => '_blank', 'title' => __('hiw_title'), 'class' => 'tooltip')); ?>
									</div>
									<div class="matches-column matches-container sided-random">
										<div class="matches-outer-box">
											<a href="#" target="_blank" class="tooltip" title="<?=__('hiw_title')?>">
												<i class="fa fa-random fa-lg"></i>
											</a>
										</div>
									</div>
									<?php foreach($next_match_3 as $next_match_4 => $next_match_5): ?>
										<?= \Match::forge()->setup_box_information($next_match_4, "C", NULL, TRUE, TRUE); ?>
										<div class="matches-column matches-container matches-hiw">
											<?= \Html::anchor('hiw', '<i class="fa fa-random fa-lg"></i>', array('target' => '_blank', 'title' => __('hiw_title'), 'class' => 'tooltip')); ?>
										</div>
										<div class="matches-column matches-container sided-random">
											<div class="matches-outer-box">
												<a href="#" target="_blank" class="tooltip" title="<?=__('hiw_title')?>">
													<i class="fa fa-random fa-lg"></i>
												</a>
											</div>
										</div>
										<?= \Match::forge()->setup_box_information($next_match_5, "D", 1, FALSE, TRUE); ?>
										<?php endforeach; ?>
								</div>
								<?php endforeach; ?>

					</div>
				</div>
				<?php endforeach; ?>
			<?php endif; ?>


		<?php if(!empty($x_4_match)) :?>
			<div class="matches-title">
				<span><?=__('xtrade_match')?>:</span> <span class="matches-blue-text"><?=__('five_way_trade')?></span>
			</div>

			<?php foreach($x_4_match as $match => $next_match_1):?>
				<div class="match-box five-way-match-box matches-margin clearfix">
					<div class="sided-arrow-area" data-target="medium">
						<canvas></canvas>
						<i class="fa fa-random fa-lg"></i>
					</div>
					<div class="row padded-sides matches-wrapper">
						<i class="fa fa-random fa-lg"></i>
						<canvas class="canvas-arrow"></canvas>

						<div class="list clearfix">
								<?= $this_listing_box; ?>
								<div class="matches-column matches-container matches-hiw">
									<?= \Html::anchor('hiw', '<i class="fa fa-random fa-lg"></i>', array('target' => '_blank', 'title' => __('hiw_title'), 'class' => 'tooltip')); ?>
								</div>
								<div class="matches-column matches-container sided-random">
									<div class="matches-outer-box">
										<a href="#" target="_blank" class="tooltip" title="<?=__('hiw_title')?>">
											<i class="fa fa-random fa-lg"></i>
										</a>
									</div>
								</div>
								<?php foreach($next_match_1 as $next_match_2 => $next_match_3): ?>
									<?= \Match::forge()->setup_box_information($next_match_2, "B", NULL, FALSE, FALSE);?>
									<div class="matches-column matches-container matches-hiw">
										<?= \Html::anchor('hiw', '<i class="fa fa-random fa-lg"></i>', array('target' => '_blank', 'title' => __('hiw_title'), 'class' => 'tooltip')); ?>
									</div>
									<div class="matches-column matches-container sided-random">
										<div class="matches-outer-box">
											<a href="#" target="_blank" class="tooltip" title="<?=__('hiw_title')?>">
												<i class="fa fa-random fa-lg"></i>
											</a>
										</div>
									</div>
									<?php foreach($next_match_3 as $next_match_4 => $next_match_5): ?>
										<?= \Match::forge()->setup_box_information($next_match_4, "C", NULL, TRUE, TRUE); ?>
										<div class="matches-column matches-container matches-hiw">
											<?= \Html::anchor('hiw', '<i class="fa fa-random fa-lg"></i>', array('target' => '_blank', 'title' => __('hiw_title'), 'class' => 'tooltip')); ?>
										</div>
										<div class="matches-column matches-container sided-random">
											<div class="matches-outer-box">
												<a href="#" target="_blank" class="tooltip" title="<?=__('hiw_title')?>">
													<i class="fa fa-random fa-lg"></i>
												</a>
											</div>
										</div>
										<?php foreach($next_match_5 as $next_match_6 => $next_match_7): ?>
											<?= \Match::forge()->setup_box_information($next_match_6, "D", NULL, TRUE, TRUE); ?>
											<div class="matches-column matches-container matches-hiw">
												<?= \Html::anchor('hiw', '<i class="fa fa-random fa-lg"></i>', array('target' => '_blank', 'title' => __('hiw_title'), 'class' => 'tooltip')); ?>
											</div>
											<div class="matches-column matches-container sided-random">
												<div class="matches-outer-box">
													<a href="#" target="_blank" class="tooltip" title="<?=__('hiw_title')?>">
														<i class="fa fa-random fa-lg"></i>
													</a>
												</div>
											</div>
											<?= \Match::forge()->setup_box_information($next_match_7, "E", 1, FALSE, TRUE);?>
											<?php endforeach; ?>
										</div>
										<?php endforeach; ?>

						</div>
						<?php endforeach; ?>
				</div>
				<?php endforeach; ?>
			<?php endif; ?>
	</div>
</div>
<div id="page_dialog" class="reveal" data-reveal></div>
<div id="matches_dialog" class="reveal" data-reveal></div>