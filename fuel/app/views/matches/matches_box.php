<?php if($data['in_set'] != 'A' && !$data['buyer'] && $data['not_direct']): ?>
<div class="matches-column matches-container">
	<div class="matches-outer-box">
		<div class="letter"><?=$data['in_set']?></div>
		<div class="matches-box">
			<div class="<?=$data['top']?>"><div><?=$data['home_title']?> <?=$data['add_to']?></div></div>
			<?=\Asset::img($data['image_set'], array('class' => 'img-responsive'))?>
			<div class="text padded-top padded-bottom">
				<h3><?=$data['price']?></h3>
				<p><?=$data['street']?></p>
				<?php if($data['city'] != '' && $data['prov'] != '') :?>
				<p><?=$data['city']?>, <?=$data['prov']?></p>
				<?php else : ?>
				<p><?=$data['city']?> <?=$data['prov']?></p>
				<?php endif; ?>
				<span><?=$data['client_or_view']?><br><?=$data['ID_or_contact']?></span>
			</div>
		</div>
	</div>
</div>
<?php elseif($data['in_set'] != 'A' && !$data['buyer']): ?>

<div class="matches-column matches-container">
	<div class="matches-outer-box">
		<div class="letter"><?=$data['in_set']?></div>
		<div class="matches-box">
			<div class="<?=$data['top']?>"><?=$data['home_title']?> <?=$data['add_to']?></div>
			<?=\Asset::img($data['image_set'], array('class' => 'img-responsive'))?>
			<div class="text padded-top padded-bottom">
				<h3><?=$data['price']?></h3>
				<p><?=$data['street']?></p>
				<?php if($data['city'] != '' && $data['prov'] != '') :?>
				<p><?=$data['city']?>, <?=$data['prov']?></p>
				<?php else : ?>
				<p><?=$data['city']?> <?=$data['prov']?></p>
				<?php endif; ?>
				<span><?=$data['client_or_view']?><br><?=$data['ID_or_contact']?></span>
			</div>
		</div>
		<div class="matches-buttons">
			<button type="button" class="button small ignore match_button" id="<?=$data['listing_id']?>">Ignore Match</button>
			<button type="button" class="button small save match_button" id="<?=$data['listing_id']?>">Save For Later</button>
		</div>
	</div>

</div>
<?php else: ?>
<div class="matches-column matches-container">
	<div class="matches-outer-box">
		<div class="letter"><?=$data['in_set']?></div>
		<div class="matches-box">
			<div class="<?=$data['top']?>"><?=$data['home_title']?> <?=$data['add_to']?></div>
			<?=\Asset::img($data['image_set'], array('class' => 'img-responsive'))?>
			<div class="text padded-top padded-bottom">
				<h3><?=$data['price']?></h3>
				<p><?=$data['street']?></p>
				<?php if($data['city'] != '' && $data['prov'] != '') :?>
				<p><?=$data['city']?>, <?=$data['prov']?></p>
				<?php else : ?>
				<p><?=$data['city']?> <?=$data['prov']?></p>
				<?php endif; ?>
				<span><?=$data['client_or_view']?><br><?=$data['ID_or_contact']?></span>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>