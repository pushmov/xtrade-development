<?php if($data['in_set'] != 'A' && !$data['buyer'] && $data['not_direct']): ?>
<table width="140" border="0" cellspacing="0px" height="320">
	<tbody>
		<tr>
			<td align="center" style="border-bottom: 1px solid black;"><?=$data['in_set']?></td>
		</tr>
		<tr>
			<td height="50px" align="center" valign="middle" style="background-color: #249DDB;color: #fff;border-left: 1px solid black;border-right: 1px solid black;">
				<?=$data['home_title']?> <?=$data['add_to']?>
			</td>
		</tr>
		<tr>
			<td style="color: #000;border-left: 1px solid #000;border-right: 1px solid #000" valign="top">
				<?=\Asset::img($data['image_set'], array('style' => 'width:140px'))?>
			</td>
		</tr>
		<tr>
			<td style="color: #000;border-bottom: 1px solid black;border-left: 1px solid black;border-right: 1px solid black;" valign="top">
				<div align="center" style="width: 140px;max-width: 140px">
					<div class="text" style="color: #000;width: 140px;color: #000;">
						<h3 style="color: #47B55B;"><b><?=$data['price']?></b></h3>
						<p style="color: #000;margin: 0;font-size: 11px;">
							<?=$data['street']?><br>
							<?=$data['city']?><br>
							<?=$data['prov']?><br>
						</p>
						<span style="color: #000;font-size: 11px;"><?=$data['client_or_view']?><br><?=$data['ID_or_contact']?></span>
					</div>
				</div>
			</td>
		</tr>
	</tbody>
</table>
<?php elseif($data['in_set'] != 'A' && !$data['buyer']): ?>
<table width="140" border="0" cellspacing="0px" height="320">
	<tbody>
		<tr>
			<td align="center" style="border-bottom: 1px solid black;"><?=$data['in_set']?></td>
		</tr>
		<tr>
			<td height="50px" align="center" valign="middle" style="background-color: #249DDB;color: #fff;border-left: 1px solid black;border-right: 1px solid black;">
				<?=$data['home_title']?> <?=$data['add_to']?>
			</td>
		</tr>
		<tr>
			<td style="color: #000;border-left: 1px solid #000;border-right: 1px solid #000" valign="top">
				<?=\Asset::img($data['image_set'], array('style' => 'width:140px'))?>
			</td>
		</tr>
		<tr>
			<td style="color: #000;border-bottom: 1px solid black;border-left: 1px solid black;border-right: 1px solid black;" valign="top">
				<div align="center">
					<div class="text" style="width: 140px;color: #000;">
						<h3 style="color: #47B55B;"><b><?=$data['price']?></b></h3>
						<p style="color: #000;margin:0;font-size: 11px;">
							<?=$data['street']?><br>
							<?=$data['city']?><br>
							<?=$data['prov']?><br>
						</p>
						<span style="color: #000;font-size: 11px;"><?=$data['client_or_view']?><br><?=$data['ID_or_contact']?></span>
					</div>
				</div>
			</td>
		</tr>
	</tbody>
</table>
<?php else: ?>
<table width="140" border="0" cellspacing="0px" height="320">
	<tbody>
		<tr>
			<td align="center" style="border-bottom: 1px solid black;"><?=$data['in_set']?></td>
		</tr>
		<tr>
			<td height="50px" align="center" valign="middle" style="background-color: #249DDB;color: #fff;border-left: 1px solid black;border-right: 1px solid black;">
				<?=$data['home_title']?> <?=$data['add_to']?>
			</td>
		</tr>
		<tr>
			<td style="color: #000;border-left: 1px solid #000;border-right: 1px solid #000" valign="top">
				<?=\Asset::img($data['image_set'], array('style' => 'width:140px'))?>
			</td>
		</tr>
		<tr>
			<td style="color: #000;border-bottom: 1px solid black;border-left: 1px solid black;border-right: 1px solid black;" valign="top">
				<div align="center">
					<div class="text" style="color: #000;width: 140px;color: #000;">
						<h3 style="color: #47B55B;"><b><?=$data['price']?></b></h3>
						<p style="color: #000;margin:0;font-size: 11px;">
							<?=$data['street']?><br>
							<?=$data['city']?><br>
							<?=$data['prov']?><br>
						</p>
						<span style="color: #000;font-size: 11px;"><?=$data['client_or_view']?><br><?=$data['ID_or_contact']?></span>
					</div>
				</div>
			</td>
		</tr>
	</tbody>
</table>
<?php endif; ?>