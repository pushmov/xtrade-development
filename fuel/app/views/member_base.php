<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title><?=$meta_title;?></title>
		<link href="/favicon.ico" rel="shortcut icon" type="image/x-icon" />
		<?=\Asset::css('auto-complete.css');?>
		<?=\Asset::css('tooltipster.css');?>
		<?=\Asset::css('slicknav.min.css');?>
		<?=\Asset::css('app.css');?>
		<?=\Asset::css('style.css');?>
		<?=\Asset::css('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');?>
		<?php echo \Asset::css($styles, array(), null, false); ?>
		<?=\Asset::js('//code.jquery.com/jquery-latest.min.js');?>
		<?=\Asset::js('modernizr.js');?>
		<?=\Asset::js('foundation.min.js');?>
        <?=\Asset::js('jquery.mask.js');?>
		<?=\Asset::js('jquery.slicknav.min.js');?>
		<?=\Asset::js('auto-complete.min.js');?>
		<?=\Asset::js('jquery.tooltipster.min.js');?>
		<?=\Asset::js('common.js')?>
		<?php echo \Asset::js($scripts, array(), null, false); ?>
		<script type="text/javascript" src="http://livejs.com/live.js"></script>
		<script>
			var baseUrl = '<?=Uri::base(false);?>';
			<?php if (isset($js_vars)) {foreach ($js_vars as $k => $v) echo "var $k = '$v';", PHP_EOL;} ?>
			<?=$tracking_code;?>
		</script>
	</head>
	<body>
		<div id="header">
			<div class="row"><?=$header;?></div>
		</div>
		<div class="row content" id="content">
			<?=$subhead;?>
			<?=$content;?>
		</div>
		<div id="footer">
			<div class="row">
				<div class="small-12 medium-2 column">
				<a href="/"><?=\Asset::img('ft_logo.png', array('width' => 152, 'height' => 52, 'alt' => 'Xtrade Homes'));?></a>
				</div>
				<div class='small-12 medium-8 column b-nav'>
					<a href="/about-us">About Us</a>
					<a href="/privacy-policy">Privacy Policy</a>
					<a href="/terms-and-conditions-of-use-tradehomes-by-irex-inc">Terms &amp; Conditions</a><br>
					<em class="smaller">The trademarks REALTOR&reg;, REALTORS&reg;, and the REALTOR&reg; logo are controlled by The Canadian Real Estate Association (CREA) and identify real estate professionals who are members of CREA.  The trademark DDF&reg; is owned by CREA and identifies CREA's Data Distribution Facility (DDF&reg;).  xTradeHomes receives accurate, reliable, and current listing information from REALTORS&reg; through CREA's DDF&reg;</em>
				</div>
				<div class="small-12 medium-2 column">
					<span title="The trademarks REALTOR&reg;, REALTORS&reg; and the REALTOR&reg; logo are controlled by The Canadian Real Estate Association (CREA) and identify real estate professionals who are members of CREA" style="font-weight:bold;" class='ft_img'>REALTOR&reg;</span> <?=\Asset::img('ddf.jpg', array('width' => 45, 'height' => 25, 'alt' => 'CREA', 'class' => 'ft_img', 'title' => 'The trademark DDF&reg; is owned by The Canadian Real Estate Association (CREA) and identifies CREA\'s Data Distribution Facility (DDF&reg;)'));?>
				</div>
			</div>
		</div>
		<div id="login_dialog" class="reveal tiny" data-reveal data-close-on-click="false">
		</div>
		<div id="forms_dialog" class="reveal tiny" data-reveal></div>
		<script>
			$(document).foundation();
		</script>
	</body>
</html>