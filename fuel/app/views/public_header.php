<div class="header">
	<div class="row">
		<div class="logo-div">
			<a href="/"><?=\Asset::img('hd_logo.png', array('alt' => 'Xtrade Homes', 'class' => 'left'));?></a>
			<p class="evo-title">The Evolution Of Real Estate</p>
		</div>
		<div class="search-bar-top">
			<input name="search_bar" id="search_bar" type="text" class="top-search-input" placeholder="Search by City" />
		</div>
		<div class="t-nav">
			<ul class="unstyled" id="site_menu">
				<li><a href="/about-us">About Us</a></li>
				<li><a href="/how-xtrading-works">How xTrading Works</a></li>
				<li><a href="/faq">FAQ</a></li>
				<?php if(isset($user_info)): ?>
					<?=$user_info;?>
					<li><button class="button logout_button" name="<?=$logout?>">Logout</button></li>
					<?php else: ?>
					<li><button class="button login_button">Member Login/Join</button></li>
				<?php endif; ?>
			</ul>
		</div>
	</div>
</div>