<table width="640" height="700" border="1" cellspacing="0" cellpadding="10" style="margin:auto 0; height: 700px;">
	<tr>
		<td>
			<div style="text-align: center"><?=\Asset::img('hd_logo.png');?></div>
			<h1 align="center">Advertising Receipt</h1>
			<p>&nbsp;</p>
			<div style="text-align:left; margin-left: 50px;">
				<p><?=$date;?></p>
				<p><?=$name;?></p>
				<br>
				<p><?=$company_name;?><br>
				<?=$address;?></p>
				<br>
			</div>
			<div style="text-align:right; width: 200px;">
				<p>Monthly Advertising Subscription: $<?=$amount;?></p>
				<?=$features;?>
				<br>
				<?=$taxes;?><br>
				_______________<br>
				<br>
				<span style="width:200px;">Total: $<?=$total;?></span>
			</div>
			<div align="center">
				<br>
				<br>
				TradeHomes by iRex, Inc - <span style="color: navy;">#370 - 425 Carrall Street,	Vancouver, BC V6B 6E3</span>
				<br>Phone #: 1-844-798-7233 - Email: <?=\Html::mail_to('info@xtradehomes.com');?>
				<br>
				<br>
				<?=$gst_num;?>
				<br>
				<br>
			</div>
		</td>
	</tr>
</table>