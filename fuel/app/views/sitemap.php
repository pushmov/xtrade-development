<div class="row content">
	<h1 class="text-green text-center">xTradeHomes SiteMap</h1>
	<div class="small-12 column">
		<ul>
			<li><?=\Html::anchor('/', 'xTradeHomes');?>
				<ul>
					<li><?=\Html::anchor('signup', 'xTradeHomes Sign Up', array('title' => 'xTradeHomes Sign Up'));?></li>
					<li><?=\Html::anchor('about', 'xTradeHomes About Us', array('title' => 'xTradeHomes About Us'));?></li>
					<li><?=\Html::anchor('hiw', 'xTradeHomes - How Matching Works', array('title' => 'xTradeHomes - How Matching Works'));?></li>
					<li><?=\Html::anchor('faq', 'xTradeHomes FAQ', array('title' => 'xTradeHomes FAQ'));?></li>
					<li><?=\Html::anchor('vendor/location', 'xTradeHomes Advertiser Results', array('title' => 'xTradeHomes Directory Results'));?></li>
					<li><?=\Html::anchor('signup/vendor', 'xTradeHomes Advertiser Sign Up', array('title' => 'xTradeHomes Advertiser Sign Up'));?></li>
					<li><?=\Html::anchor('privacy', 'xTradeHomes Privacy Policy', array('title' => 'xTradeHomes Privacy Policy'));?></li>
					<li><?=\Html::anchor('terms', 'xTradeHomes Terms &amp; Conditions', array('title' => 'xTradeHomes Terms &amp; Conditions'));?></li>
					<li><?=\Html::anchor('contact', 'xTradeHomes Contact', array('title' => 'xTradeHomes Contact'));?></li>
				</ul>
			</li>
		</ul>
			</div>
</div>