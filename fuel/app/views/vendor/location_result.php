<div class="content columns padded">
    
	<div class="directory-result-header clearfix">
		<div class="small-12 medium-6 column">
			<strong class="sub-main-headers"><?=\Asset::img('dir_image.png', array('width' => 14, 'height' => 14, 'alt' => 'directory services'));?> xTradeHomes Local Services Directory</strong>
		</div>
		<div class="small-12 medium-6 column text-right">
			<a href="javascript:history.go(-1)"><i class="fa fa-arrow-left"></i>Go Back</a>
		</div>
	</div>
    
    <?=\Form::open(array('action' => 'vendor/result.html', 'method' => 'post', 'name' => 'directory_result', 'id' => 'directory_result'));?>
	<div>
        <div class="directory-results-sub">
            <h4>Local Services in</h4>
			<div class="row">
				<div class="small-12 medium-12 large-4 column">
					<?=\Form::input('location', $location, array('id' => 'location', 'placeholder' => 'Burnaby, British Columbia, Canada', 'title' => 'Type Advertiser Location Here'));?>
				</div>
				<div class="small-12 medium-12 large-1 column">for</div>
				<div class="small-12 medium-12 large-4 column">
					<?=\Form::select('type[]', array('all'), \Model\Vendor\Dlist::forge()->select_category(), array('multiple' => 'multiple', 'id' => 'type_service'))?>
				</div>
				<div class="small-12 medium-12 large-3 column">
					<?php if(!\Authlite::instance('auth_vendor')->logged_in()) : ?>
					<?= \Form::button('button', 'If you would like to advertise real estate services click here', array('class' => 'button btn-advertise', 'type' => 'button'))?>
					<?php endif; ?>
				</div>
			</div>
                
        </div>
        <div class="directory_results_sort"></div>
	</div>
	
    <div class="directory-results-wrapper" id="full_results">
		<div class="text-center"><i class="fa fa-spinner fa-spin"></i>Processing...</div>
	</div>
    <?=\Form::close();?>
</div>