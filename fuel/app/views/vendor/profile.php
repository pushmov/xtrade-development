<div>
	<?=\Form::open(array('action' => 'realtors/profile', 'autocomplete' => 'off', 'id' => 'client_form', 'name' => 'client_form', 'method' => 'post'));?>
		<input name="data[id]" id="id" type="hidden" value="<?=$data['id'];?>">
		<input name="data[account_status]" type="hidden" id="account_status" value="<?=$data['account_status'];?>">
		<input name="data[utype]" type="hidden" id="usertype" value="<?=$data['utype']?>">
		<div class="profile-wrapper clearfix">
			<div class="small-12 medium-12 large-6 column profile-info">
				<div class="padded-sides">
					<br />
					<h2>Vendor Profile</h2>
					<p><?=$data['info'];?></p>
					<div class="clearfix">
						<dl>
							<dt>xTradeHomes ID:</dt>
							<dd><?=$data['member_id'];?></dd>
							<dt>Account Created On:</dt>
							<dd><?=$data['account_creation'];?></dd>
							<dt>You Last Logged In:</dt>
							<dd><?=$data['last_login'];?></dd>
							<dt>Last IP Address at Login:</dt>
							<dd><?=$data['ip_address'];?></dd>
							<?php if (isset($data['promo_ends'])): ?>
								<dt>Free Promotional Period Ends:</dt>
								<dd><?=$data['promo_ends'];?></dd>
							<?php endif; ?>
							<dt>Next Payment:</dt>
							<dd></dd>
							<dt>Last Amount Paid:</dt>
							<dd></dd>
							<dt>History:</dt>
							<dd><?=$data['history'];?></dd>
							<dt>Receive Alerts:</dt>
							<dd><?=$data['alerts'];?></dd>
							<dt>Email Status:</dt>
							<dd><?=$data['email_type'];?></dd>
						</dl>
					</div>
					<div class="small-12 medium-12 large-12">
						<button id="advanced_info" type="button" class="button">Additional Options</button>
					</div>
					<div id="advanced_info_options" class="advanced-info-options hide">
						<button id="change_billing" type="button" class="button">Change Billing Details</button>
						<button id="change_pass" type="button" class="button">Update Password</button>
						<button id="close_account" type="button" class="button">Close Account</button>
					</div>
					<div class="row">
						<div class="small-12 column">
							<div class="padded-sides">
								<h5>Please provide the following information for your directory listing:</h5>
								<label for="types_service"><?=__('types_service'); ?><span class="astrict">*</span></label>
								<?= \Form::select('data[type]', $data['type'], \Model\Vendor::forge()->all_services(), array('multiple' => 'multiple', 'id' => 'types_service'));?>
								<label for="locale"><?=__('advert_address'); ?><span class="astrict">*</span></label>
								<input name="data[locale]" type="text" id="locale" value="<?=$data['locale'];?>" placeholder="Start Typing City">
							</div>
						</div>
					</div>
					<?php if(\Authlite::instance('auth_admin')->logged_in()) : ?>
					<div class="row">
						<div class="small-12 column">
							<div class="padded-sides">
								<button type="button" class="button" id="resend_welcome_email" data-id="<?=$data['id']?>" data-utype="<?=$data['utype']?>">Resend Welcome Email</button>
							</div>
						</div>
					</div>
					<?php endif; ?>
				</div>
			</div>
			<div class="small-12 medium-12 large-6 column">
				<div class="padded">
					<div class="row">
						<div class="small-12 medium-6 column">
							<label for="first_name"><?=__('first'); ?><span class="astrict">*</span></label>
							<input name="data[first_name]" type="text" id="first_name" value="<?=$data['first_name'];?>">
						</div>
						<div class="small-12 medium-6 column">
							<label for="last_name"><?=__('last'); ?><span class="astrict">*</span></label>
							<input name="data[last_name]" type="text" id="last_name" value="<?=$data['last_name'];?>">
						</div>
					</div>

					<label for="company_name"><?=__('company_name'); ?><span class="astrict">*</span></label>
					<input name="data[company_name]" type="text" id="company_name" value="<?=$data['company_name'];?>">

					<label for="address"><?=__('address');?><span class="astrict">*</span></label>
					<input name="data[address]" type="text" id="address" value="<?=$data['address'];?>">

					<label for="cpc_address"><?=__('city_prov_state_country');?><span class="astrict">*</span></label>
					<input name="data[location_address]" type="text" id="location_address" value="<?=$data['location_address'];?>" placeholder="Start Typing City">

					<div class="row">
						<div class="small-12 medium-6 column">
							<label for="z_p_code"><?=__('zip_postal_code');?><span class="astrict">*</span></label>
							<input name="data[z_p_code]" type="text" id="z_p_code" value="<?=$data['z_p_code'];?>">
						</div>
						<div class="small-12 medium-6 column">
							<?php if (isset($data['toll_free'])): ?>
								<label for='toll_free'><?=__('toll_free');?></label>
								<input name="data[toll_free]" type="text" id="toll_free" value="<?=$data['toll_free'];?>">
							<?php endif;?>
						</div>
					</div>

					<div class="row">
						<div class="small-12 medium-6 column">
							<label for="phone"><?=__('phone'); ?><span class="astrict">*</span></label>
							<input name="data[phone]" type="tel" id="phone" placeholder="555-555-5555" value="<?=$data['phone'];?>" maxlength="14" title="Enter Numbers Only Please" class="tooltip">
						</div>
						<div class="small-12 medium-6 column">
							<label for="cell"><?=__('cell'); ?></label>
							<input name="data[cell]" type="tel" class="input_field" id="cell" placeholder="555-555-5555" value="<?=$data['cell'];?>" maxlength="14" title="Enter Numbers Only Please">
						</div>
					</div>

					<label for="web_site"><?=__('site'); ?></label>
					<input name="data[web_site]" type="text" placeholder="http://www.yoursite.com" id="web_site" value="<?=$data['web_site']?>" size="24" />
					<div class="row">
						<div class="small-12 medium-6 column">
							<label for="email"><?=__('email'); ?><span class="astrict">*</span></label>
							<input name="data[email]" type="email" placeholder="your@emailaddress.com" id="email" value="<?=$data['email'];?>">
						</div>
						<div class="small-12 medium-6 column">
							<label for="cemail">Confirm <?=__('email'); ?><span class="astrict">*</span></label>
							<input name="data[cemail]" type="email" placeholder="your@emailaddress.com" id="cemail" value="<?=$data['cemail'];?>">
						</div>
					</div>

					<div class="row">
						<div class="small-12 medium-6 end column">
							<label for="public_email"><?=__('public_facing_email'); ?><span class="astrict">*</span></label>
							<input name="data[public_email]" type="email" placeholder="your@emailaddress.com" id="public_email" value="<?=$data['public_email'];?>">
						</div>
					</div>

					<div class="row column">
						<label for="comments">Service Description<span class="astrict">*</span> (800 characters max) (Characters Left: <span id='limit_count'></span>)</label>
						<textarea name="data[comments]" id="comments"><?=$data['comments']?></textarea>
					</div>

					<div class="row">
						<div class="vendor-logo"></div>
					</div>
					<div class="row">
						<div class="small-12 medium-7 small-centered column text-center">
							<small class="padded">Drop Images In The Box Below Or Click Upload.<br>
								Image will be resized to 140px by 140px.</small>
							<div class="padded-top"  id="file_upload">Select File To Upload</div>
							<input type="hidden" id="target-upload" value="<?=$data['target']?>">
							<input type="hidden" id="target-upload-id" value="<?=$data['id']?>">
						</div>
					</div>

				</div>
			</div>
		</div>
		<div align="center" id="submit_div" class="padded-top">
			<button type="button" id="submit" class="button">Submit Changes</button>
		</div>
	<?=\Form::close();?>
</div>
<div id="page_dialog" class="reveal" data-reveal></div>
<div id="billing_dialog" class="reveal large" data-reveal></div>
<div id="dialog_preview" class="reveal large" data-reveal data-close-on-click="false"></div>
<div id="dialog" class="reveal tiny" data-reveal data-close-on-click="false"></div>
<script>
	$(document).ready(function() {
		new autoComplete({
			selector: '#location_address',
			minChars: 3,
			source: function(term, response){
				$.get(baseUrl + 'autocomplete/cpc.json', { q: term }, function(data){ response(data); });
			}
		});
		$('#types_service').SumoSelect();
		$.get('/vendor/locale.json', {vendor_id: vendor_id}, function(json){
			$('#locale').tokenInput(baseUrl + 'autocomplete/cpc.json?from=tokeninput', {
				minChars: 3,
				tokenDelimiter: ';',
				preventDuplicates: true,
				prePopulate: json
			});
		});
		$('#comments').jqte();
		var limitCount = 800;
		var limitNum = 800;
		var obj;
		var cur_val;
		var re = /(<\/*[\s\S]*?>)/g;
		var re2 = /(&nbsp;)/g;
		var re_sp = /(&nbsp;)|(<\/*[\s\S]*?>)/g;

		$("#limit_count").html(limitCount);
		$('.jqte_editor').on("keydown", function(e){
			if(e.which == 8 || e.which == 46){
				return true;
			}else{
				cur_var = $('#comments').val();
				cur_var_stripped = cur_var.replace(re, "");
				cur_var_stripped = cur_var_stripped.replace(re2, " ");
				obj = cur_var_stripped.length;

				if(obj >= limitNum){
					return false;
				}
			}
		});

		$('.jqte_editor').on("keyup", function(){
			cur_var = $('#comments').val();
			cur_var_stripped = cur_var.replace(re, "");
			cur_var_stripped = cur_var_stripped.replace(re2, " ");
			obj = cur_var_stripped.length;

			limitCount = limitNum - obj;
			$("#limit_count").html(limitCount);
		});
	});

</script>