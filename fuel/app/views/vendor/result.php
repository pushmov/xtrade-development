<div class="directory-results">
    <?php if(!empty($result)) : ?>
	<div class="clearfix">
		<?=$pagination;?>
	</div>
    <?php foreach($result as $row) : ?>
    <div class="row">
		<div class="clearfix padded-vertical vendor-result-item">
			<?php if($row['featured'] == \Model\Vendor::VENDOR_STATUS_FEATURED) : ?>
				<div class="directory-results-featured"></div>
			<?php endif; ?>
			<div class="small-12 medium-4 large-4 column text-center">
				<!--<img src="homes/vendors/<?=$row['vendor_id'];?>" >-->
				<?= \Asset::img(\Model\Vendor::forge()->get_vendor_logo($row['vendor_id']), array('width' => 220, 'height' => 220));?>
			</div>
			<div class="small-12 medium-4 large-4 column vendor-ad">
		        <p>Service: <strong><ul><li><?=str_replace(';', '</li><li>', \Model\Vendor::forge()->typeid_to_services($row['type']))?></li></ul></strong></p>
		        <h3><?=($row['web_site'] == '') ? $row['company_name'] : \Html::anchor($row['web_site'], $row['company_name'], array('target' => '_blank'))?></h3>
		        <p class="address"><?=$row['address']?></p>
		        <p><?=$row['location_address']?></p>
		        <em>Telephone:</em> <strong><?=\Sysconfig::format_phone_number($row['phone']);?></strong><br>
		        <em>Email:</em> <strong><?=\Html::mail_to($row['public_email'], $row['public_email']);?></strong><br>
		        <em>Website:</em> <strong><?=$row['web_site'] == '' ? '-' : \Html::anchor($row['web_site'], $row['web_site']);?></strong><br>
			</div>
			<div class="small-12 medium-4 large-4 column"><?=$row['comments']?></div>
		</div>
    </div>
    <hr>
    <?php endforeach; ?>
    <?php else :?>
    <div class="no_results">No results found for local services in <br><?=$location;?><br> <?= \Model\Vendor::forge()->typeid_to_services($type);?><br>Please type in another city.</div>
    <?php endif;?>
</div>