<? if($_SESSION['SESS_ACCOUNT_TYPE'] == "Realtor"){ ?>
<div id="members_menu">
    <?=\Html::anchor('members_home.php', 'Member\'s Home', array('class' => 'buttons', 'style' => 'margin-left: 10px; position: relative;', 'id' => 'member_home'));?> <button class="html_button" id='new_listing' style='font-size: 1em;'>Start New Listing</button> <button class="buttons" id="resource_button">Forms</button> <?=\Html::anchor('profile.php', 'My Profile', array('class' => 'buttons', 'id' => 'profile', 'style' => 'position: relative;'));?> <?=\Html::anchor('members_home.php?tutorial=true', 'Tutorial', array('class' => 'buttons', 'id' => 'profile', 'style' => 'position: relative;'));?>
    <span style="float: right;">
        <?=\Asset::img('dir_image.png', array('width' => 14, 'height' => 14, 'alt' => 'directory services'));?>
        <?=\Html::anchor('vendor/location', 'Local Services Directory', array('class' => 'buttons', 'style' => 'margin-left: 0px;'));?>
    </span>
    <div id="resources" class="resources" style="display: none;">
        <div class="container">
        	<div style="background-color: #39B54A; color: white; width: 103px; height: 20px; margin-bottom: 5px;">Download</div>
                <?=\Html::anchor('assets/pdf/xtrade_form.pdf', '<strong>xTrade Form</strong>', array('target' => '_blank', 'style' => 'margin-left: 0px;'));?><br>
                <?=\Html::anchor('assets/pdf/seller_form.pdf', '<strong>Seller Form</strong>', array('target' => '_blank', 'style' => 'margin-left: 0px;'));?><br>
                <?=\Html::anchor('assets/pdf/buyer_form.pdf', '<strong>Buyer Form</strong>', array('target' => '_blank', 'style' => 'margin-left: 0px;'));?><br>
        </div>
    </div>
</div>
<? }elseif($_SESSION['SESS_ACCOUNT_TYPE'] == "Broker"){ ?>
<div id="members_menu">
    <?=\Html::anchor('members_home.php', 'Broker\'s Home', array('class' => 'buttons', 'style' => 'margin-left: 10px; position: relative;', 'id' => 'member_home'));?> <button class="html_button" id='add_user' style='font-size: 1em;'>Add REALTOR&reg;</button> <button class="buttons" id="resource_button">Forms</button> <?=\Html::anchor('profile.php', 'My Profile', array('style' => 'position: relative;', 'class' => 'buttons', 'id' => 'profile'));?> <? if(count($pending_list) >= 1){ ?><?=\Html::anchor('members_home.php', 'Review REALTOR&reg; Requests', array('class' => 'buttons', 'style' => 'margin-left: 30px; position: relative;', 'id' => 'member_home'));?> <? } ?>
    <span style="float: right;">
        <?=\Asset::img('dir_image.png', array('width' => 14, 'height' => '14', 'alt' => 'directory services'));?>
        <?=\Html::anchor('vendor/location', 'Local Services Directory', array('class' => 'buttons', 'style' => 'margin-left: 0px;'));?>
    </span>
    <div id="resources" class="resources_b" style="display: none;">
        <div class="container">
        	<div style="background-color: #39B54A; color: white; width: 103px; height: 20px; margin-bottom: 5px;">Download</div>
            <?=\Html::anchor('images/pdf/xtrade_form.pdf', '<strong>xTrade Form</strong>', array('target' => '_blank', 'style' => 'margin-left: 0px;'));?><br>
            <?=\Html::anchor('images/pdf/seller_form.pdf', '<strong>Seller Form</strong>', array('target' => '_blank', 'style' => 'margin-left: 0px;'));?><br>
            <?=\Html::anchor('images/pdf/buyer_form.pdf', '<strong>Buyer Form</strong>', array('target' => '_blank', 'style' => 'margin-left: 0px;'));?><br>
        </div>
    </div>
</div>
<? }elseif($_SESSION['SESS_ACCOUNT_TYPE'] == "Vendor"){ ?>
<div id="members_menu">
    <?=\Html::anchor('members_home.php', 'Advertiser\'s Home', array('class' => 'buttons', 'style' => 'margin-left: 10px; position: relative;', 'id' => 'member_home'));?> <?=\Html::anchor('profile.php', 'My Profile', array('style' => 'position: relative;', 'id' => 'profile', 'class' => 'buttons'));?>
    <span style="float: right;">
        <?=\Asset::img('dir_image.png', array('width' => 14, 'height' => 14, 'alt' => 'directory services'));?>
        <?=\Html::anchor('vendor/location', 'Local Services Directory', array('class' => 'buttons', 'style' => 'margin-left: 0px;'));?>
    </span>
</div>
<? }elseif($_SESSION['SESS_ACCOUNT_TYPE'] == "Admin"){ ?>
<div id="members_menu">
    <?=\Html::anchor('members_home.php', 'Admin Home', array('style' => 'margin-left: 10px; position: relative; padding-left: 16px;', 'id' => 'member_home', 'class' => 'buttons'));?> <button class="html_button" id='new_listing' style='font-size: 1em;'>Start New Listing</button> <button class="buttons" id="resource_button">Forms</button>
    <span style="float: right;">
        <?=\Asset::img('dir_image.png', array('width' => 14, 'height' => 14, 'alt' => 'directory services'));?>
        <?=\Html::anchor('vendor/location', 'Local Services Directory', array('class' => 'buttons', 'style' => 'margin-left: 0px;'));?>
    </span>
    <div id="resources" class="resources" style="display: none;">
        <div class="container">
        	<div style="background-color: #39B54A; color: white; width: 103px; height: 20px; margin-bottom: 5px;">Download</div>
            <?=\Html::anchor('images/pdf/xtrade_form.pdf', '<strong>xTrade Form</strong>', array('style' => 'margin-left: 0px;', 'target' => '_blank'));?><br>
            <?=\Html::anchor('images/pdf/seller_form.pdf', '<strong>Seller Form</strong>', array('style' => 'margin-left: 0px;', 'target' => '_blank'));?><br>
            <?=\Html::anchor('images/pdf/buyer_form.pdf', '<strong>Buyer Form</strong>', array('style' => 'margin-left: 0px;', 'target' => '_blank'));?><br>
        </div>
    </div>
    | Admin:
    <select name="Select" id="admin_select">
    	<option value="">Choose Admin Area</option>
      <option value="users">Members</option>
      <option value="brokers">Brokers</option>
      <option value="vendors">Advertisers</option>
      <option value="settings">Settings</option>
      <option value="promos">Promotions</option>
      <option value="p2">CREA Agents and Offices</option>
      <option value="email_log">Email Log</option>
      <option value="login_log">Login Log</option>
    </select>
</div>
<? } ?>