<?php
\Autoloader::add_core_namespace('Automodeler');

\Autoloader::add_classes(array(
	'Automodeler\\Automodeler' => __DIR__.'/classes/Automodeler.php',
	'Automodeler\\AutomodelerException' => __DIR__.'/classes/Automodeler.php',
));
