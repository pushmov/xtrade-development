$(document).ready(function() {
    var loader = '<i class="fa fa-spinner fa-spin"></i>Processing...';
    $('.details').on('click', function() {
        $('#details_div').html(loader);
        $('#details_div').load(baseUrl + 'brokers/dashboard/details.html', {'id': $(this).parent('dd').parent('dl').attr('id')});
	});

	$('#details_div').on('click', '.btn-remove-realtor', function(e) {
		$.get(baseUrl + 'brokers/dashboard/remove_realtor_dialog.html', {id: $(this).attr('id')}, function(html) {
			$('#page_dialog').html(html);
			$('#page_dialog').foundation('open');
		});
	});
	$('.approve-request').click(function(){
		var id = $(this).attr('id');
		$.get(baseUrl + 'brokers/dashboard/approve_realtor.json', {id: id.replace('ar_', '')}, function(json){
			if(json.status === 'OK'){
                window.location = baseUrl + 'brokers';
            }
		});
	});

	$('.decline-request').click(function(){
		var id = $(this).attr('id');
		$.get(baseUrl + 'brokers/dashboard/decline_realtor.json', {id: id.replace('dr_', '')}, function(json){
			if(json.status === 'OK'){
                window.location = baseUrl + 'brokers';
            }
		});
	});

	$('#page_dialog').on('click', '#btn_remove_realtor_yes', function() {
        $('#page_dialog .callout').remove();
		$.get(baseUrl + 'brokers/dashboard/remove_realtor.json', {id: $(this).attr('name')}, function(json) {
			if (json.status === 'ERROR') {
				$('#page_dialog .modal-body').prepend('<div class="callout error small text-center">' + json.message + '</div>');
			} else {
                $('#page_dialog #btn_remove_realtor_yes').remove();
                $('#page_dialog #btn_close').html('Close');
                $('#page_dialog .modal-body').prepend('<div class="callout success small text-center">' + json.message + '</div>');
			}
		});
	});
	$('#page_dialog').on('click', '#btn_broker_member_yes', function() {
		$.get(baseUrl + member + 'profile/broker_change.html', {'id': $(this).attr('id')}, function(html) {
			$('#page_dialog').html(html);
		});
	});

	$('.xlistings').on('click', function() {
        $('#details_div').html(loader);
		$('#details_div').load(baseUrl + 'brokers/dashboard/xlistings.html', {'id': $(this).parent('dd').parent('dl').attr('id')});
	});

	$('.olistings').on('click', function() {
        $('#details_div').html(loader);
		$('#details_div').load(baseUrl + 'brokers/dashboard/olistings.html', {'id': $(this).parent('dd').parent('dl').attr('id')});
	});
	$('.matches').on('click', function() {
        $('#details_div').html(loader);
		$('#details_div').load(baseUrl + 'brokers/dashboard/matches.html', {'id': $(this).parent('dd').parent('dl').attr('id')});
		 $("html, body").animate({ scrollTop: 0 }, "slow");
	});
	$('#page_dialog').on('click', '#btn_close', function() {
		$('#page_dialog').foundation('close');
		location.reload();
	});

    if(action === 'add_realtor'){
        $.get(baseUrl + 'brokers/dashboard/add_realtor_dialog.html', {rid: rid}, function(html) {
			$('#page_dialog').html(html);
			$('#page_dialog').foundation('open');
		});
    }
});