/*
// Every time ajax is called delay for 1 sec
$(document).ajaxSend(function () {
self.setTimeout(function (){
$('#ajax_loading').show();
});
})

// Every time ajax is completed
$(document).ajaxComplete(function () {
self.setTimeout(function (){
$('#ajax_loading').hide();
});
});
*/
var spinner = '<i class="fa fa-spinner fa-spin"></i>Processing...';
$(function(){
	$('#site_menu,#sub_menu').slicknav({
		prependTo: '.t-nav',
		label: '',
		closeOnClick: true
	});
});

function loginDialog(type) {
	$.get(baseUrl + 'login/login.html', {type:type}, function(html) {
		$('#login_dialog').html(html);
		$('#login_dialog').foundation('open');
	});
}

$(document).ready(function() {
	$('#ajax_loading').hide();
	$('.login_button').on('click', function(e) {
		e.preventDefault();
		loginDialog('realtor');
	});
	$('#broker_login').on('click', function() {
		loginDialog('broker');
	});
	$('#vendor_login').on('click', function() {
		loginDialog('vendor');
	});
	$('#login_dialog').on('click', '#forgot_signin', function() {
		$.get(baseUrl + 'login/forgot.html', {type: $('#login_type').val()}, function(html) {
			$('#login_dialog').html(html);
			$('#back_login').click(function() {
				loginDialog($('#login_type').val());
			});
		});
	});
	$('#login_dialog').on('click', '#login_sbutton', function() {
        var that = $(this);
        var currTxt = that.text();
        $(this).html(spinner);
        $(this).prop('disabled', true);
		$('.input-error').remove();
		$.post(baseUrl + 'login/check.json', $("#login_form").serialize(), function(json) {
			if (json.status == 'OK') {
				window.location = json.message;
			} else {
                that.html(currTxt);
                that.prop('disabled', false);
				$("#login_error").html('<span class="input-error">' + json.message + '</span>');
			}
		}).fail( function(xhr, textStatus, errorThrown) {
            that.html(currTxt);
			alert(xhr.responseText);
		});
	});
	$('#login_dialog').on('click', '#login_sforgot', function() {
        var that = $(this);
        var currTxt = that.text();
        $(this).html(spinner);
        $(this).prop('disabled', true);
		$.post(baseUrl + 'login/password.html', $("#login_form").serialize(), function(html) {
            that.html(currTxt);
            that.prop('disabled', false);
			$("#login_error").html('<span class="input-error">' + html + '</span>');
		});
	});
	$('.logout_button').on('click', function() {
		window.location = baseUrl + $(this).attr('name') + '/auth/logout';
	});
	new autoComplete({
		selector: '#search_bar',
		minChars: 3,
		source: function(term, response){
			$.get(baseUrl + 'autocomplete/cpc.json', { q: term }, function(data){ response(data); });
		},
		onSelect: function(event,term,item) {
			window.location = baseUrl + 'listing/search/' + encodeURI(term);
		}
	});
	if($('[data-admin]').length > 0){
		loginDialog('admin');
	}

	$('.no-link').click(function(e){
		e.preventDefault();
	});
    
    $('#download_forms').on('click', function(){
        $.get('/user/forms.html', function(html){
            $('#forms_dialog').html(html);
            $('#forms_dialog').foundation('open');
        });
    });
    
    $('#forms_dialog').on('click', '.modal-close', function(){
        $('#forms_dialog').foundation('close');
    });
});