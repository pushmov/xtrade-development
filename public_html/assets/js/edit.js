var submitting;
var form_name;
var req_field = false;
var animated = false;
var current_name;
var click_area = "client_info";
var scrollto = '';


$(document).ready(function(){
	// Need to bind these first
    customSumoSelect($('.has-siding-type'), $('#siding_type_h'));
	$('#siding_type_h').SumoSelect();
    customSumoSelect($('.floor-type-h-wrapper'), $('#floor_type_h'));
	$('#floor_type_h').SumoSelect();
	$('#roof_type_h').SumoSelect();
    customSumoSelect($('.roof-type-h-wrapper'), $('#roof_type_h'));
    $('#prop_type_h').SumoSelect();
    $('#p_title_h').SumoSelect();
    $('#prop_style_h').SumoSelect();
    $('#finished_basement_h').SumoSelect();
    $('#garage_type_h').SumoSelect();
    $('#view_h').SumoSelect();
    $('#heat_source_h').SumoSelect();
    $('#heating_h').SumoSelect();
    $('#cooling_h').SumoSelect();
    $('#water_h').SumoSelect();
    $('#sewer_h').SumoSelect();
    $('#year_built_h').SumoSelect();

	$('#prop_style_w').SumoSelect();
    customSumoSelect($('.prop-style-w-wrapper'), $('#prop_style_w'));
	$('#siding_type_w').SumoSelect();
    customSumoSelect($('.siding-type-w-wrapper'), $('#siding_type_w'));
	$('#garage_type_w').SumoSelect();
    customSumoSelect($('.garage-type-w-wrapper'), $('#garage_type_w'));
	$('#floor_type_w').SumoSelect();
    customSumoSelect($('.floor-type-w-wrapper'), $('#floor_type_w'));
	$('#roof_type_w').SumoSelect();
    customSumoSelect($('.roof-type-w-wrapper'), $('#roof_type_w'));

    $('#listing_price_low').SumoSelect();
    $('#listing_price_high').SumoSelect();
    $('#sq_footage').SumoSelect();
    $('#prop_type_w').SumoSelect();
    $('#bedrooms_w').SumoSelect();
    $('#bathrooms_w').SumoSelect();
    $('#p_title_w').SumoSelect();
    $('#total_lot_size_w').SumoSelect();
    $('#total_acreage_w').SumoSelect();
    $('#finished_basement_w').SumoSelect();
    $('#view_w').SumoSelect();
    $('#heat_source_w').SumoSelect();
    $('#heating_w').SumoSelect();
    $('#cooling_w').SumoSelect();
    $('#water_w').SumoSelect();
    $('#sewer_w').SumoSelect();

	$('.no-link').click(function(e){
		e.preventDefault();
	});
	$('#submit_content').prop('disabled', false);
	$('input[type="tel"]').mask('999-999-9999');
	var modal_context = $('#edit_listing_dialog');
	$('#wants_request').click(function(e){
		$.get('/listing/wantrequest.html', {listing_id: listing_id}, function(data){
			modal_context.html(data);
			modal_context.foundation('open');
		});
	});

	$(document).on('click', '#submit_request', function(data){
		var form = $(this).closest('form');
        var that = $(this);
        var currTxt = that.text();
        that.html('<i class="fa fa-spinner fa-spin"></i>Processing...');
        that.prop('disabled', true);
		$('.callout').remove();
		$.post(form.attr('action'), form.serialize(), function(json){
            that.html(currTxt);
            that.prop('disabled', false);
			if (json.status === 'OK') {
				form.before('<div class="callout success small text-left">' + json.message + '</div>');
				form.before('<button type="button" class="button" id="close_button">Close</button>');
				form.remove();
			} else {
				form.before('<div class="callout alert small text-left">' + json.message + '</div>');
			}
		});
	});

	$(document).on('click', '#close_button', function(){
		modal_context.foundation('close');
	});

	$(document).on('click', '.keep_edit', function(){
		modal_context.foundation('close');
	});

	$('input[type="text"].req').on('keyup', function(){
		if($(this).val() === '0' || $(this).val() === ''){
			$('label[for="'+$(this).attr('name')+'"]').removeClass('req_text_done').addClass('req_text');
		} else {
			$('label[for="'+$(this).attr('name')+'"]').removeClass('req_text').addClass('req_text_done');
		}
	});

	var has_address = $('input[name="has[c_address]"]');
	if(has_address.val() !== ''){
		neighinline($('#neighbourhood_h'), has_address.val(), listing_id, 1);
	} else {
        has_address.val('');
    }

	var wants_address = $('input[name="wants[address]"]');
	if(wants_address.val() !== ''){
		neighinline($('#neighbourhood_w'), wants_address.val(), listing_id, 2);
	} else {
        wants_address.val('');
    }

	$('#address').on('blur', function(){
		neighinline($('#neighbourhood_h'), $(this).val(), listing_id, 1);
	});

	$('#wants_address').on('blur', function(){
		neighinline($('#neighbourhood_w'), $(this).val(), listing_id, 2);
	});

	$(document).on('change', '#neighbourhood_h', function(){
		if($(this).val() === '' || $(this).val() === '0'){
			$('label[for="'+$(this).attr('name')+'"]').removeClass('req_text_done').addClass('req_text');
		} else{
			$('label[for="'+$(this).attr('name')+'"]').removeClass('req_text').addClass('req_text_done');
		}
	});

	$(document).on('change', '#neighbourhood_w', function(){
		if($(this).val() === '' || $(this).val() === '0'){
			$('label[for="'+$(this).attr('name')+'"]').removeClass('req_text_done').addClass('req_text');
		} else{
			$('label[for="'+$(this).attr('name')+'"]').removeClass('req_text').addClass('req_text_done');
		}
	});

	$('#wants[listing_price_low]').on('change', function(){
		if(+$('#wants[listing_price_low]').val() > + $('#wants[listing_price_high]').val() && +$('#wants[listing_price_high]').val() !== 0){
			$('#wants[listing_price_high]').val($('#wants[listing_price_low]').val());
		}
	});

	$(window).keydown(function(event){
		if(event.keyCode === 13) {
			event.preventDefault();
			return false;
		}
	});

	$('.no_edit').find('input, textarea, button, select').prop('disabled','disabled');
	$('.no_edit').slideToggle('slow');

	//document ready event check label color
	$('#main_form').find('input,textarea,select').filter('.req').each(function(index, element){
		if($(element).attr('name') === 'wants[prop_style][]'){
			if($(element).val() === 9){
				switch_to_red($(element).attr('name'));
			} else {
				switch_to_green($(element).attr('name'));
			}
		} else {
			if($(element).val() === '' || !$(element).val() || $(element).val() === '0'){
				if($(element).attr('name') === 'wants[listing_price_low]'){
					switch_to_green($(element).attr('name'));
				} else {
					switch_to_red($(element).attr('name'));
				}
			}else{
				switch_to_green($(element).attr('name'));
			}
		}
	});

	//user action : change color to green or red
	$('#main_form').find('.req').on('change keyup paste cut', function(){
		if($(this).attr('name') === 'wants[prop_style][]'){
			if($(this).val() === 9){
				switch_to_red($(this).attr('name'));
			} else {
				switch_to_green($(this).attr('name'));
			}
		} else {
			if($(this).val() === '' || !$(this).val() || $(this).val() === '0'){
				if($(this).attr('name') === 'wants[listing_price_low]'){
					switch_to_green($(this).attr('name'));
				} else {
					switch_to_red($(this).attr('name'));
				}
			}else{
				switch_to_green($(this).attr('name'));
			}
		}
	});

	//form has changed action handler
	$(document).on('change keyup paste cut', 'input, textarea, select', function(){
		$(this).closest('.accordion-item').find('.attention')
		.html('')
		.html('Changes made in this area, click Save Now <img width="35" height="35" class="attention_req" alt="Attention" style="vertical-align: middle;" src="' + baseUrl + 'assets/images/attention.png">');
		//save button event [appearing when form has changed]
		$('#submit_content').fadeIn();
	});

	$("#return_home").click(function() {
		if (typeof isAdmin === 'undefined') {
			window.location = '/realtors';
		} else {
			window.location = '/admins';
		}
	});

	$('#submit_content').click(function() {
		//$(this).closest('form').on('submit', function (e){
		//	e.preventDefault();
		//});
		$(this).closest('form').find('.callout').remove();
		form_name = $('#main_form');
		submitting = $(this);
		$(this).prop('disabled', true).html('<i class="fa fa-spinner fa-spin"></i>Updating...');
		$('#return_home').attr('disabled', true);
		// Get the images order
		var imgOrder = [];
		$('.grid-sortable .del-picture').each(function() {
			imgOrder.push($(this).attr('data-id'));
		});
		$('#image_order').val(imgOrder.join());
        $('.input-error').remove();
		$.post( '/listing/update.json', form_name.serialize(), function(data){
			$('.attention').html('Information Up To Date <img alt="" src="' + baseUrl + 'assets/images/up-to-date.png">');
            if(data.errors){
                $.each(data.errors, function(k,v) {
                    if(k === 'listing_price' && data.fields === 'has'){
                        $('[name="' + data.fields + '[' + k + ']"]').parent().after('<div class="input-error input-error-listing-price">' + v + '</div>');
                    } else if(k === 'address' && data.fields === 'wants') {
                        $('[name="' + data.fields + '[' + k + ']"]').after('<div class="input-error">' + v + '</div>');
                    } else {
                        if(k === 'prop_type' || k === 'garage_type' || k === 'finished_basement' || k === 'prop_style' || k === 'p_title') {
                            $('[name="' + data.fields + '[' + k + ']"]').parent().find('.SlectBox').after('<div class="input-error input-error-nomargin">' + v + '</div>');
                        } else {
                            //non sumoselect
                            $('[name="' + data.fields + '[' + k + ']"]').after('<div class="input-error">' + v + '</div>');
                        }
                    }
				});
                scroll_to_next_area(data.area);
//				$('#'+data.area).find('.accordion-content').prepend('<div class="callout alert small">' + data.message + '</div>');
            } else if(data.ok === 'OK'){
                $(document).find('.matches-notification').html(data.matches);
				$.get(data.url, {status: data.status}, function(completed){
					modal_context.html(completed);
					modal_context.foundation('open');
				});
			}
			$('#submit_content').prop('disabled', false).html('Save Now').hide();
            $('#return_home').attr('disabled', false);
		});
	});

	if(scrollto){
		setTimeout(function(){
			scroll_to_next_area(scrollto);
			}, 1000);
	}

	$('.title_button').click(function() {
		if($(this).find('.title_box').hasClass('wants_sub_header')){
			if($(this).parent().find('.attention').is(':visible')){
				$(this).parent().find('.attention_closed').fadeIn('slow');
				$(this).parent().find('.attention').fadeOut('slow');
			}

		}else{
			if($(this).parent().find('.attention_closed').is(':visible')){
				$(this).parent().find('.attention_closed').fadeOut('slow');
				$(this).parent().find('.attention').fadeIn('slow');
			}
		}
		$(this).parent().find('.client_content_sub').slideToggle('slow');

	});

	new autoComplete({
		selector: '#address',
		source: function(term, response){
			$.get(baseUrl + 'autocomplete/cpc.json', { q: term }, function(data) { response(data); });
		},
        onSelect: function(event,term,item) {
			$('label[for="has[c_address]"]').addClass('req-text-done');
		}
	});

	new autoComplete({
		selector: '#wants_address',
		source: function(term, response){
			$.get(baseUrl + 'autocomplete/cpc.json', { q: term }, function(data) { response(data); });
		},
        onSelect: function(event,term,item) {
			$('label[for="has[c_address]"]').addClass('req-text-done');
		}
	});

	$('#file_upload').uploadFile({
		url: baseUrl + 'realtors/listing/doupload.json',
		fileName: 'myfile',
		allowedTypes: 'jpg,jpeg,png',
		returnType: 'json',
		showPreview: false,
		sequential: true,
		sequentialCount: 1,
		formData: {listing_id: $('input[name="listing_id"]').val()},
		afterUploadAll: function() {
			refresh_images();
		}
	});

	$('#image_info_wrapper').on('click', '.del-picture', function(){
		$.post('/realtors/listing/delete_images.json', {id: $(this).attr('data-id')}, function(json){
			if(json.status === 'OK'){
				refresh_images();
			}
		});
	});

	$('.features-check-all a').click(function(){
		var parent = $(this).parent().parent();
		parent.find('input[type="checkbox"]').each(function(){
			if($(this).prop('checked') === false){
				$(this).prop('checked', 'checked');
			}
		});
	});

	$('#edit_listing_dialog').on('click', '.my_listing', function(){
		window.location = '/realtors';
	});

	if(type !== '2'){
		refresh_images();
	}

    $('#client_wants').on('click', '.matches-link', function(){
        if($(this).attr('data-target').length > 0 && $(this).attr('data-target') !== ''){
            window.open($(this).attr('data-target'), '_blank');
        }
    });

    $('input.req, select.req').each(function(){
        if($(this).val() !== ''){
            $('label[for="' + $(this).attr('id') + '"].req-text').addClass('req-text-done');
        }
    });

    $('input.req, select.req').on('change blur keyup', function(){
        if($(this).val() !== '' && $(this).val() !== '0'){
            $('label[for="' + $(this).attr('id') + '"].req-text').addClass('req-text-done');
            $('label[for="' + $(this).attr('id') + '"].req-text').parent().find('.input-error').remove();
        } else {
            $('label[for="' + $(this).attr('id') + '"].req-text').removeClass('req-text-done');
        }
    });

    //client_info asynchronous single field save
    $(document).on('change blur keyup', '#client_info input, #client_info select', function(){
        var btn = $('#submit_content');
        btn.html('<i class="fa fa-spinner fa-spin"></i>Updating...');
        btn.prop('disabled', true);
        var data = {
            listing_id: listing_id,
            area: 'information',
            field: $(this).attr('data-field'),
            value: $(this).val()
        };

        $.post('/listing/update_field.json', data, function(json){
            //done, then checking and updating the completed_section flag
            $.post('/listing/completed_section.json', {listing_id: listing_id, area: 'has'}, function(json){
                btn.prop('disabled', false);
                btn.html('Save Now');
            });
        });
    });

    //client_has asynchronous single field save
    $(document).on('change blur keyup', '#client_has input, #client_has select', function(){
        var btn = $('#submit_content');
        btn.html('<i class="fa fa-spinner fa-spin"></i>Updating...');
        btn.prop('disabled', true);
        if($(this).hasClass('customized-checkbox')){
        	var val = ($(this).is(':checked')) ? '1' : '0';
        	var field = $(this).parent().find('input[type="hidden"]').attr('data-field');
        } else {
        	var val = $(this).val();
        	var field = $(this).attr('data-field');
        }
        var data = {
            listing_id: listing_id,
            area: 'has',
            field: field,
            value: val,
            index: $(this).attr('data-index'),
            fn_name: $(this).attr('data-name')
        };
        $.post('/listing/update_field.json', data, function(json){
            //done, then checking and updating the completed_section flag
            $.post('/listing/completed_section.json', {listing_id: listing_id, area: 'has'}, function(json){
                btn.prop('disabled', false);
                btn.html('Save Now');
            });
        });
    });

    //client_wants asynchronous single field save
    $('#client_wants input, #client_wants select').on('change blur keyup', function(){
        var data = {
            listing_id: listing_id,
            area: 'want',
            field: $(this).attr('data-field'),
            value: $(this).val()
        };
        var btn = $('#submit_content');
        btn.html('<i class="fa fa-spinner fa-spin"></i>Updating...');
        btn.prop('disabled', true);
        $.post('/listing/update_field.json', data, function(json){
            //done, then checking and updating the completed_section flag
            $.post('/listing/completed_section.json', {listing_id: listing_id, area: 'has'}, function(json){
                btn.prop('disabled', false);
                btn.html('Save Now');
            });
        });
    });

    //special case : wants neighbourhood to be saved to different table
    $(document).on('change', '#neighbourhood_w', function(){
        console.log($(this).val());
        var data = {
            listing_id: listing_id,
            field: $(this).attr('data-field'),
            value: $(this).val()
        };
        var btn = $('#submit_content');
        btn.html('<i class="fa fa-spinner fa-spin"></i>Updating...');
        btn.prop('disabled', true);
        $.post('/listing/update_wants_neighbourhood.json', data, function(json){
            //done, then checking and updating the completed_section flag
            $.post('/listing/completed_section.json', {listing_id: listing_id, area: 'has'}, function(json){
                btn.prop('disabled', false);
                btn.html('Save Now');
            });
        });
    });
    //asynchronous submit data
    $('#client_wants').on('change blur', 'select:not(".multi"),input[type="text"]', function(){
    	async_submit();
    });
});
//asynchronous submit data
function async_submit() {
    $('#main_form').find('.input-error').remove();
    var target = $(document).find('.matches-notification');
    target.html('<i class="fa fa-spinner fa-spin"></i>Finding Matches...');
    var form = $('#main_form');
    $.post('/listing/update.json', form.serialize(), function(data){
        target.html(data.matches);
    });
}
function refresh_images(){
	var loader = '<div class="text-center"><i class="fa fa-spinner fa-spin"></i>Processing...</div>';
	$('#image_info_wrapper').html(loader);

	$.get(baseUrl + 'listing/images.html', {listing_id: $('input[name="listing_id"]').val()}, function(html){
		$('#image_info_wrapper').html(html);
		if ($('#dropzone').attr('class')) {
			$('.grid-sortable').dad({
				callback: function(e) {
					var new_order = [];
					$('img.thumbnail').each(function(){
						if($.inArray($(this).attr('data-id'), new_order) < 0){
							new_order.push($(this).attr('data-id'));
						}
					});
                    $('.grid-sortable').closest('.accordion-item').find('.attention')
					.html('')
					.html('<i class="fa fa-spinner fa-spin"></i>Processing...');
					update_order(new_order.toString());
                    $('.grid-sortable').closest('.accordion-item').find('.attention')
					.html('')
					.html('Information Up To Date<img alt="" src="' + baseUrl + 'assets/images/up-to-date.png">');
				}
			}).addDropzone('#dropzone',function(e){
				var i = e.find('img.thumbnail').attr('data-id');
				e.remove();
				$.post('/realtors/listing/delete_images.json', {id: i}, function(json){
					if(json.status === 'OK'){
						refresh_images();
					}
				});
			});
		}
	});
}

function update_order(order){
	$.post('/realtors/listing/image_order.json', {order: order}, function(json){
		if(json.status === 'OK'){

		}
	});
}

function neighinline(target, addr, listing_id, type){
	$.post('/listing/neighinline.html', {type: type, location: addr, lid: listing_id}, function(data){
		if(type === 2){
			$('#neighbourhood_w_wrapper').html(data);
		} else {
			$('#neighbourhood_h_wrapper').html(data);
		}
		var val = target.val();
		if(val !== '' || val !== '0'){
			$(document).find('label[for="'+target.attr('name')+'"]').removeClass('req_text').addClass('req_text_done');
		} else {
			$(document).find('label[for="'+target.attr('name')+'"]').addClass('req_text');
		}
	});
}

function switch_to_red(label_name){
	$('label[for="'+label_name+'"]').addClass("req_text").removeClass("req_text_done");
}

function switch_to_green(label_name){
	$('label[for="'+label_name+'"]').removeClass("req_text").addClass("req_text_done");
    $('label[for="'+label_name+'"]').parent().find('.input-error').remove();
}

function scroll_to_next_area(where){
	$('html, body').delay(250).animate({
		scrollTop: $('#'+where).offset().top
		}, 1000);
}

function customSumoSelect(wrapper, target){
    wrapper.find('.options > li').on('click', function(){
        if($(this).hasClass('selected') && ($(this).attr('data-val') === '' || $(this).attr('data-val') === '9')){
            target[0].sumo.unSelectAll();
            target[0].sumo.selectItem(0);
        } else {
            target[0].sumo.unSelectItem(0);
        }
        // Make sure at lease one value is selected
        if ($(target).val() == null) {
            target[0].sumo.selectItem(0);
		}
		async_submit();
    });
}
