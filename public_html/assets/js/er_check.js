//(function($) {
var error = 0;

function isValidPostal(code){
	var pattern = new RegExp( /(^\d{5}(-\d{4})?$)|^([ABCEGHJKLMNPRSTVXYabceghjklmnprstvxy]{1}\d{1}[A-Za-z]{1}\s?\d{1}[A-Za-z]{1}\d{1}$)/i );
	return pattern.test(code);
};

function isValidURL(code){
	var pattern = new RegExp( /^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!10(?:\.\d{1,3}){3})(?!127(?:\.‌​\d{1,3}){3})(?!169\.254(?:\.\d{1,3}){2})(?!192\.168(?:\.\d{1,3}){2})(?!172\.(?:1[‌​6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1‌​,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00‌​a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u‌​00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/[^\s]*)?$/i );
	return pattern.test(code);
};

function isValidPhone(code){
	var pattern = new RegExp( /(\d)?([\s-])?[(]{0,1}\d{3}[)]{0,1}[\s-]{0,1}\d{3}[\s-]{0,1}\d{4}(\W|$)/ );
	return pattern.test(code);
};

function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return pattern.test(emailAddress);
};

function req_fields(name, length, type, checktype, misc){
	$.ajaxSetup({ async:false })
	//console.log(name+" - "+length+" - "+type+" - "+checktype);
	error = false;
	if(!checktype){
		checktype = 1;
	}
	//console.log("err length: "+length+" Error Name: "+name);
	//console.log($('#'+name+'').attr("class"));
	if(!length && $('#'+name+'').prop("name") != "site" && $('#'+name+'').hasClass("req") || !length && $('#'+name+'').prop("name") == "captcha_code"){
		$('#err_'+name+'').remove();
		$('#err_'+name+'l').remove();
		$('#'+name+'').removeClass("input_error");
		if($('#'+name+'').hasClass("no_multi") || $('#'+name+'').hasClass("multi")){
			$('#'+name+'').next("button").after("<span id='err_"+name+"' name='err_"+name+"' class='error_label'><img src='" + baseUrl + "assets/images/req_image.gif' width='27' height='12'></span>");
			$('#'+name+'').next("button").addClass("input_error");
		}else{
			$('#'+name+'').after("<span id='err_"+name+"' name='err_"+name+"' class='error_label'><img src='" + baseUrl + "assets/images/req_image.gif' width='27' height='12'></span>");
			$('#'+name+'').addClass("input_error");
		}
		error = true;
		//return false;
	}else if($('#'+name+'').attr("type") == "tel"){
		if(!isValidPhone($('#'+name+'').val())){
			$('#err_'+name+'').remove();
			$('#err_'+name+'l').remove();
			$('#'+name+'').removeClass("input_error");
			$('#'+name+'').after("<span id='err_"+name+"' name='err_"+name+"' class='error_label'><img src='" + baseUrl + "assets/images/req_image.gif' width='27' height='12'></span>");
			$('label[for="'+name+'"]').append("<span id='err_"+name+"l' name='err_"+name+"l' class='error_label_small'>Invalid Phone Number</span>");
			$('#'+name+'').addClass("input_error");
			error = true;
		}else{
			$('#err_'+name+'').remove();
			$('#err_'+name+'l').remove();
			$('#'+name+'').removeClass("input_error");
			if(!error){
				error = false;
			}
		}
	}else if($('#'+name+'').attr("type") == "email"){
		if(!isValidEmailAddress($('#'+name+'').val())){
			$('#err_'+name+'').remove();
			$('#err_'+name+'l').remove();
			$('#'+name+'').removeClass("input_error");
			$('#'+name+'').after("<span id='err_"+name+"' name='err_"+name+"' class='error_label'><img src='" + baseUrl + "assets/images/req_image.gif' width='27' height='12'></span>");
			$('label[for="'+name+'"]').append("<span id='err_"+name+"l' name='err_"+name+"l' class='error_label_small'>Invalid Email Format</span>");
			$('#'+name+'').addClass("input_error");
			error = true;
			//return false;
		}else if(isValidEmailAddress($('#'+name+'').val())){
			$('#err_'+name+'').remove();
			$('#err_'+name+'l').remove();
			$('#'+name+'').removeClass("input_error");
			if(!error){
				error = false;
			}
		}
		if(type == 1 && name != "Pemail"){
			$.post(baseUrl + 'checks/email.json', { Email: $('#'+name+'').val(), check: 2, check_type: checktype, ID: $('#c_id').val()})
				.done(function( data ){
					
					if(data.status == 1){
						$('#err_'+name+'').remove();
						$('#err_'+name+'l').remove();
						$('#'+name+'').removeClass("input_error");
						$('#'+name+'').after("<span id='err_"+name+"' name='err_"+name+"' class='error_label'><img src='" + baseUrl + "assets/images/req_image.gif' width='27' height='12'></span>");
						$('label[for="'+name+'"]').append("<span id='err_"+name+"l' name='err_"+name+"l' class='error_label_small'>" + data.msg + "</span>");
						//alert("Email In Use. Choose Another.");
						$('#'+name+'').addClass("input_error");
						if(misc == "broker"){
							$("#b_error").html("");
							$("#b_error").html("Email address already in system");
							$("#b_error").show();
						}
						error = true;
						//return false;
					}else{
						if($('#Email').val() != $('#cemail').val() && $('#Email').val() && $('#cemail').val()){
							$('#err_'+name+'').remove();
							$('#err_'+name+'l').remove();
							$('#'+name+'').removeClass("input_error");
							$('#'+name+'').after("<span id='err_"+name+"' name='err_"+name+"' class='error_label'><img src='" + baseUrl + "assets/images/req_image.gif' width='27' height='12'></span>");
							$('label[for="'+name+'"]').append("<span id='err_"+name+"l' name='err_"+name+"l' class='error_label_small'>" + data.msg + "</span>");
							$('#'+name+'').addClass("input_error");
							if(misc == "broker"){
								$("#b_error").html("");
								$("#b_error").html("Email address already in system");
								$("#b_error").show();
							}
							error = true;
							//return false;
						}else if(!$('#cemail').length && $('#Email').val()){
							$('#err_cemail').remove();
							$('#err_cemaill').remove();
							$('#cemail').removeClass("input_error");
							$('#err_Email').remove();
							$('#err_Emaill').remove();
							$('#Email').removeClass("input_error");
							if(misc == "broker"){
								$("#b_error").html("");
								$("#b_error").hide();
							}
							if(!error){
								error = false;
							}
						}else{
							$('#err_cemail').remove();
							$('#err_cemaill').remove();
							$('#cemail').removeClass("input_error");
							$('#err_Email').remove();
							$('#err_Emaill').remove();
							$('#Email').removeClass("input_error");
							if(!error){
								error = false;
							}
							if(misc == "broker"){
								$("#b_error").html("");
								$("#b_error").hide();
							}
						}
					}
				}
			);
		}
	}else if($('#'+name+'').attr("type") == "password"){
		if(length < 5){
			$('#err_'+name+'').remove();
			$('#err_'+name+'l').remove();
			$('#'+name+'').removeClass("input_error");
			$('#'+name+'').after("<span id='err_"+name+"' name='err_"+name+"' class='error_label'><img src='" + baseUrl + "assets/images/req_image.gif' width='27' height='12'></span>");
			$('label[for="'+name+'"]').append("<span id='err_"+name+"l' name='err_"+name+"l' class='error_label_small'>Passwords Too Short!</span>");
			$('#'+name+'').addClass("input_error");
			error = true;
			//return false;
		}else if($('#password').val() != $('#cpassword').val() && $('#password').val() && $('#cpassword').val()){
			$('#err_'+name+'').remove();
			$('#err_'+name+'l').remove();
			$('#'+name+'').removeClass("input_error");
			$('#'+name+'').after("<span id='err_"+name+"' name='err_"+name+"' class='error_label'><img src='" + baseUrl + "assets/images/req_image.gif' width='27' height='12'></span>");
			$('label[for="'+name+'"]').append("<span id='err_"+name+"l' name='err_"+name+"l' class='error_label_small'>Passwords Must Match!</span>");
			$('#'+name+'').addClass("input_error");
			error = true;
			//return false;
		}else{
			$('#err_cpassword').remove();
			$('#err_cpasswordl').remove();
			$('#cpassword').removeClass("input_error");
			$('#err_password').remove();
			$('#err_passwordl').remove();
			$('#password').removeClass("input_error");
			if(!error){
				error = false;
			}
		}
	}else if($('#'+name+'').prop("name") == "z_pcode" || $('#'+name+'').prop("name") == "z_pcode_h" || $('#'+name+'').prop("name") == "z_pcode_c"){
		if($('#'+name+'').val() == "" && !$('#'+name+'').hasClass("req")){
			$('#err_'+name+'').remove();
			$('#err_'+name+'l').remove();
			$('#'+name+'').removeClass("input_error");
			if(!error){
				error = false;
			}
		}else if(!isValidPostal($('#'+name+'').val())){
			$('#'+name+'').val($('#'+name+'').val().toUpperCase());
			$('#err_'+name+'').remove();
			$('#err_'+name+'l').remove();
			$('#'+name+'').removeClass("input_error");
			$('#'+name+'').after("<span id='err_"+name+"' name='err_"+name+"' class='error_label'><img src='"+baseUrl+"assets/images/req_image.gif' width='27' height='12'></span>");
			$('label[for="'+name+'"]').append("<span id='err_"+name+"l' name='err_"+name+"l' class='error_label_small'>Invalid Input!</span>");
			$('#'+name+'').addClass("input_error");
			error = true;
			//return false;
		}else{
			$('#'+name+'').val($('#'+name+'').val().toUpperCase());
			$('#err_'+name+'').remove();
			$('#err_'+name+'l').remove();
			$('#'+name+'').removeClass("input_error");
			if(!error){
				error = false;
			}
		}
	}else if($('#'+name+'').attr("type") == "checkbox" && $('#'+name+'').hasClass("req")){
		if(!$('#'+name+'').is(':checked')){
			$('#'+name+'').after("<span id='err_"+name+"' name='err_"+name+"' class='error_label' style='left: 0px; margin-top: 37px;'><img src='" + baseUrl + "assets/images/req_image.gif' width='27' height='12'></span>");
			$('#'+name+'').addClass("input_error");
			error = true;
			//return false;
		}else{
			$('#err_'+name+'').remove();
			$('#'+name+'').removeClass("input_error");
			if(!error){
				error = false;
			}
		}
	}else if($('#'+name+'').prop("name") == "site" && $('#'+name+'').val() != "" && $('#'+name+'').val() != "http://"){
		if(!isValidURL($('#'+name+'').val())){
			$('#err_'+name+'').remove();
			$('#err_'+name+'l').remove();
			$('#'+name+'').removeClass("input_error");
			$('#'+name+'').after("<span id='err_"+name+"' name='err_"+name+"' class='error_label'><img src='" + baseUrl + "assets/images/req_image.gif' width='27' height='12'></span>");
			$('label[for="'+name+'"]').append("<span id='err_"+name+"l' name='err_"+name+"l' class='error_label_small'>Invalid Input!</span>");
			$('#'+name+'').addClass("input_error");
			error = true;
			//return false;
		}else{
			$('#err_'+name+'').remove();
			$('#err_'+name+'l').remove();
			$('#'+name+'').removeClass("input_error");
			if(!error){
				error = false;
			}
		}
	}else{
		if($('#'+name+'').hasClass("no_multi") || $('#'+name+'').hasClass("multi")){
			$('#'+name+'').next("button").removeClass("input_error");
		}else{
			$('#'+name+'').removeClass("input_error");
		}
		$('#err_'+name+'').remove();
		if(!error){
			error = false;
		}
	}
	//console.log("Current error: "+error);
	$.ajaxSetup({ async:true })
	if(!error){
		return true;
	}else{
		$('#'+name+'').focus();
		return false;
	}
}