$(document).ready(function() {
    $('.view-map').click(function(){
        $.get('/listing/view_map.html', {lat: $('#map_dialog').attr('data-lat'), lon: $('#map_dialog').attr('data-lon')}, function(html){
            $('#map_dialog').html(html);
            $('#map_dialog').foundation('open');
        });
    });

    $(document).on('click', '.close-reveal-modal', function(){
        $('#map_dialog').foundation('close');
    });

    $('.client_wants').on('click', function(){
        var id = $('#client_want_dialog').attr('data-id');
        $.get('/listing/client_want/' + id + '.html', function(html){
            $('#client_want_dialog').html(html);
            $('#client_want_dialog').foundation('open');
        });
    });
});
