$(document).ready(function(){
    var loader = '<i class="fa fa-spinner fa-spin"></i>Processing...';
	$("#phone").mask("999-999-9999");
	$("#cell").mask("999-999-9999");
	$("#toll_free").mask("1-999-999-9999");

	$("#submit").click(function(e) {
        var currBtn = $(this);
        var currTxt = currBtn.html();
        currBtn.html(loader);
        currBtn.prop('disabled', true);
		$('.alert-box,.input-error,.callout').remove();
		$.post(baseUrl + member + 'profile/update.json', $('#client_form').serialize(), function(json) {
            currBtn.html(currTxt);
            currBtn.prop('disabled', false);
			if (json.status == 'OK') {
				$('#submit').after(json.message);
			} else {
				$.each(json.errors, function(k,v) {
                    if (k === 'type') {
                        $(document).find('.SlectBox').after('<div class="input-error input-error-nomargin">' + v + '</div>');
                    } else if(k === 'comments') {
                        $('label[for="'+k+'"]').after('<div class="input-error input-error-nomargin">' + v + '</div>');
                    } else {
                        $('#' + k).after('<div class="input-error">' + v + '</div>');
                    }
					if (k === 'cpc_address' || k === 'location_address') {
						$('#cpc_address').val('');
					}
				});
                $('html, body').delay(250).animate({
                    scrollTop: $('body').offset().top
                }, 1000);
			}
		});
		return false;
	});
	$('#advanced').on("click", function() {
		$("#advanced_options").toggleClass('display-inline');
	});

    $('#advanced_info').on('click', function(){
        $('#advanced_info_options').toggleClass('display-inline');
    });
    $('#listing_forms').on("click", function(e){
		$.get(baseUrl + 'user/forms.html', function(html) {
			$('#page_dialog').html(html).foundation('open');
		});
	});

    $('#account_activity').on('click', function(){
        var btn = $(this);
        var txt = btn.html();
        btn.html(loader);
        btn.prop('disabled', true);
        $.get(baseUrl + member + 'profile/activity.html', function(html){
            btn.prop('disabled', false);
            btn.html(txt);
            $('#page_dialog').html(html);
            $('#page_dialog').foundation('open');
        });
    });

	$('#change_pass').on("click", function(e){
		$.get(baseUrl + member + 'profile/password.html', {'id': $('#id').val(), 'status': $('#account_status').val()}, function(html) {
			$('#page_dialog').html(html).foundation('open');
		});
	});

	$('#page_dialog').on('click', '#btn_change_pass', function() {
		var data = $('#change_form').serialize();
		$('.callout').remove();
		$.post(baseUrl + member + 'profile/pass_update.json', $('#change_form').serialize(), function(json) {
			if (json.status == 'OK') {
				$('#change_form').html('<div class="callout success small">' + json.message + '</div>');
			} else {
				$('#change_form').before('<div class="callout alert small">' + json.message + '</div>');
			}
		});
	});

	$('#broker_assoc').on('click', '#btn_broker_request', function(e) {
		$.get(baseUrl + member + 'profile/broker_request.html', function(html) {
			$('#page_dialog').html(html).foundation('open');
		});
	});

	$('#page_dialog').on('click', '#btn_broker_member_yes', function() {
		$.get(baseUrl + member + 'profile/broker_change.html', {'id': $(this).attr('id')}, function(html) {
			$('#page_dialog').html(html).foundation('open');
		});
	});

	$('#page_dialog').on('click', '#btn_broker_member_no', function() {
		$.get(baseUrl + member + 'profile/broker_invite.html', function(html) {
			$('#page_dialog').html(html).foundation('open');
		});
	});

	$('#page_dialog').on('click', '#btn_send_invite', function() {
		$('.input-error,.callout').remove();
		$.post(baseUrl + member + 'profile/broker_invite.json', $('#invite_form').serialize(), function(json) {
			if (json.status == 'OK') {
				$('#invite_email').after(json.message);
				$('#btn_send_invite').remove();
				$('#btn_close').html('Close');
				$('#broker_assoc').html(json.assoc);
			} else {
				$.each(json.errors, function(k,v) {
					$('#' + k).after('<div class="input-error">' + v + '</div>');
				});
			}
		});
	});

	$('#page_dialog').on('click', '#btn_confirm_broker', function() {
		$.post(baseUrl + member + 'profile/broker_request.json', {'id': $('#broker').val()}, function(json) {
			$('.error').remove();
			$('#broker').after(json.message);
			if (json.status == 'OK') {
				$('#btn_confirm_broker').remove();
				$('#btn_close').html('Close');
				$('#broker_assoc').html(json.assoc);
			}
		});
	});

	$('#page_dialog').on('click', '#btn_cancel_request_yes', function() {
		$.post(baseUrl + member + 'profile/broker_request_cancel.json', function(json) {
			$('#broker_assoc').html(json.assoc);
			$('#page_dialog').foundation('close');
		});
	});

	$('#broker_assoc').on('click', '#btn_broker_change', function() {
		$.get(baseUrl + member + 'profile/broker_change.html', {'id': $(this).attr('id')}, function(html) {
			$('#page_dialog').html(html).foundation('open');
		});
	});

	$('#broker_assoc').on('click', '#btn_broker_cancel', function() {
		$.get(baseUrl + member + 'profile/broker_cancel.html', function(html) {
			$('#page_dialog').html(html).foundation('open');
		});
	});

	$('#close_account').on('click', function(e) {
		$.get(baseUrl + member + 'profile/close_account.html', function(html) {
			$('#page_dialog').html(html).foundation('open');
		});
	});

	$('#billing_history').on("click", function(e){
		$.get(baseUrl + member + 'profile/billing_history.html', {type: type, id: $('#id').val()}, function(html) {
			$('#page_dialog').html(html).foundation('open');
		});

	});

	$('#change_billing').on("click", function(e){
        var btn = $(this);
        var txt = btn.html();
        btn.html(loader);
        btn.prop('disabled', true);
		$.get(baseUrl + member + 'profile/change_billing.html', function(html) {
            btn.html(txt);
            btn.prop('disabled', false);
			$('#billing_dialog').html(html).foundation('open');
		});
	});

	$('input[name="data[alerts]"]').on("change", function(){
		$.post(baseUrl + member + 'profile/alert_update.json', {alerts: $('input[name="data[alerts]"]:checked').val()});
	});

	if ($('#account_status').val() == 7) {
		$('#change_pass').click();
	} else if ($('#account_status').val() == 8) {
		$.get(baseUrl + member + 'profile/welcome.html', function(html) {
			$('#page_dialog').html(html).foundation('open');
		});
	}

    $('#billing_dialog').on('click', '#btn_update', function(e){
        var form = $(this).closest('form');
        form.find('.input-error').remove();
        form.find('.billing-update-status').html('');
        var btn = $(this);
        var txt = btn.html();
        btn.html(loader);
        btn.prop('disabled', true);
        $.post(baseUrl + member + 'profile/billing_update.json', form.serialize(), function(json){
            btn.prop('disabled', false);
            btn.html(txt);
            if(json.status === 'FAIL'){
                $.each(json.errors, function(k,v) {
                    $('#billing_dialog').find('[name="data[' + k + ']"]').after('<div class="input-error">' + v + '</div>');
				});
                $('#billing_dialog').find('.billing-update-status').html(json.result).css({'color':'#ff0000'});
            } else if(json.status === 'OK'){
                $.get(baseUrl + member + 'profile/billing_update_success.html', function(html){
                    $('#billing_dialog').foundation('close');
                    $('#page_dialog').html(html);
                    $('#page_dialog').foundation('open');
                    $('#page_dialog').on('click', '.continue', function(){
                        $('#page_dialog').foundation('close');
                        window.location.reload();
                    });
                });

            }
        });
    });

    $('#billing_dialog').on('click', '#change_cc', function(e){
        e.preventDefault();
        $('#billing_dialog').find('#cc_number').val('').prop('disabled', false).focus();
    });

	$('#page_dialog').on('click', '#btn_close', function() {
		$('#page_dialog').foundation('close');
	});

    $('.tooltip-crea').tooltipster({
        contentAsHTML: true,
        theme: 'tooltip-white-theme',
        position: 'top-left',
        content: $('#crea_exp').html(),
        interactive: true
    });

    $('.tooltip-nrds').tooltipster({
        contentAsHTML: true,
        theme: 'tooltip-white-theme',
        position: 'top-left',
        content: $('#nrds_exp').html(),
        interactive: true
    });

    if(type === 'broker'){
        $('#change_billing').hide();
    }
});