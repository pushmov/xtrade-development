$(document).ready(function(){
    var form = $('#refine_form');
    var result_context = $('#results_div');
    var loader = '<div class="text-center"><i class="fa fa-spinner fa-spin"></i>Processing...</div>';
    var dialog_context = $('#refine_dialog');
    //initial items onload
    $.get(form.attr('action'), form.serialize(), function(data){
        result_context.html(data);
    });

    $('.result_listing_drop_down').change(function(){
        result_context.html(loader);
        $.get( form.attr('action'), form.serialize(), function(data){
            result_context.html(data);
        });
    });

    $(document).on('click', '#pagination a', function(e){
        e.preventDefault();
        result_context.html(loader);
        $.get($(this).attr('href') , form.serialize(), function(data){
            result_context.html(data);
        });
    });

    $(document).on('click', '#refine_more_submit', function(e){
        e.preventDefault();
        var p = $('#refine_more_form, #refine_form').serialize();
        result_context.html(loader);
        dialog_context.foundation('close');
        $.get($(this).closest('form').attr('action'), p, function(data){
            result_context.html(data);
        });
    });

    $(document).on('click', '#refine_more_reset', function(e){
        e.preventDefault();
        $.get('/listing/reset.json', function(json){
            if(json.status === 'OK'){
                $('#per').val(5);
                $('#sort').val(1);
                window.location.reload();
            }
        });
    });

    $('#sort').change(function(){
        var params = $('#refine_more_form, #refine_form').serialize();
        result_context.html(loader);
        $.get($(this).closest('form').attr('action'), params, function(html){
            result_context.html(html);
        });
    });

    $('#per').change(function(){
        var params = $('#refine_more_form, #refine_form').serialize();
        result_context.html(loader);
        $.get($(this).closest('form').attr('action'), params, function(html){
            result_context.html(html);
        });
    });

    $(document).on('click', '.view-details', function(){
      window.location = '/listing/detail/' + $(this).attr('data-id');
    });

    $('#refine_search').click(function(e){
        e.preventDefault();
        var location = $('#listing_results_for').text();
        var url = $('#results_listing').attr('data-filter');
        var params = merge_options(URLToArray(form.serialize()));

        $.get('/listing/refine.html', {location:location, filter: params, url: url}, function(html) {
            dialog_context.html(html);
        	//$('#dlg_prop_style').SumoSelect();
            dialog_context.foundation('open');
        });
    });

    dialog_context.on('click', '#refine_more_close', function(){
        dialog_context.foundation('close');
    });

    // Featured homes
    $("#light_slider").lightSlider({
		gallery:false,
		item:6,
		loop:true,
		thumbItem:6,
		slideMargin:0,
		enableDrag: false
	});

    $('.tooltip').tooltipster({
        contentAsHTML: true,
        theme: 'tooltip-custom-theme',
        content: 'Loading...',
        interactive: false,
        functionBefore: function(origin, continueTooltip) {
            continueTooltip();
            $.ajax({
                type: 'GET',
                data: {id: $(this).attr('data-id')},
                url: baseUrl + 'listing/featured.html',
                success: function(data) {
                    origin.tooltipster('content', data);
                }
            });
        }
    });
});

function merge_options(obj1,obj2){
    var obj3 = {};
    for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
    for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
    return obj3;
}

function URLToArray(url) {
    var request = {};
    var pairs = url.substring(url.indexOf('?') + 1).split('&');
    for (var i = 0; i < pairs.length; i++) {
        if(!pairs[i])
            continue;
        var pair = pairs[i].split('=');
        request[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1]);
     }
     return request;
}
