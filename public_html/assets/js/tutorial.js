$(document).ready(function(){
    
    var tutorial = $('#tutorial_dialog');
    var submitBtn = $('#submit_content');
    var loader = '<div class="text-center"><i class="fa fa-spinner fa-spin"></i>Processing...</div>';
    var next_letter;
    var dtnext;
    scroll_to_next_area('content');
    submitBtn.attr('title', 'Click here to save data');
	$('.no-link').click(function(e){
		e.preventDefault();
	});
    
    $('input[type="text"].req, select.req').each(function(){
        var label = $(this).attr('id');
        if($(this).val() !== ''){
            $('label[for="'+label+'"]').removeClass('req-text').addClass('req-text-done');
        } else {
            $('label[for="'+label+'"]').removeClass('req-text-done').addClass('req-text');
        }
    });
    
    $('select.req').on('change', function(){
        var label = $(this).attr('id');
        if($(this).val() !== ''){
            $('label[for="'+label+'"]').removeClass('req-text').addClass('req-text-done');
        } else {
            $('label[for="'+label+'"]').removeClass('req-text-done').addClass('req-text');
        }
    });
    
    function neighinline(target, addr, listing_id, type){
        $.post('/listing/neighinline.html', {type: type, location: addr, lid: listing_id}, function(data){
            if(type === 2){
                $('#neighbourhood_w_wrapper').html(data);
            } else {
                $('#neighbourhood_h_wrapper').html(data);
            }
            var val = target.val();
            if(val !== '' || val !== '0'){
                $(document).find('label[for="'+target.attr('name')+'"]').removeClass('req_text').addClass('req_text_done');
            } else {
                $(document).find('label[for="'+target.attr('name')+'"]').addClass('req_text');
            }
        });
    }
    function load_dialog(step){
        $.get('/realtors/tutorial/dialog.html', {step: step, page: page}, function(html){
            tutorial.html(html);
            tutorial.foundation('open');
        }); 
    }
    function typeText(placement, text_value, speed){
		var txt = text_value;
		var txt_content = txt.split("");
		var i = 0;
		function start_type(){
			if(i<txt_content.length){		
				$(placement).val($(placement).val()+txt_content[i]);
				i++;
			} else {
                clearInterval(next_letter);
            }
		};
		var next_letter = setInterval(start_type,speed);
	}
    function mouse_move(name, text_func){
		setTimeout(function(){
			if(text_func){
                text_function(name, text_func);
			}
        }, 1500);
        
	}
	function text_function(name, text_func){
        $(name).removeAttr('readonly');
		$(name).focus();
		typeText(name, text_func, 200);
	}
    
    function scroll_to_next_area(where){
        var elem = (where === 'body') ? $('body') : $('#'+where);
        $('html, body').delay(250).animate({
            scrollTop: elem.offset().top
            }, 1000);
    }
    function animate_save_btn(){
        submitBtn.wrap('<div style="position:relative"></div>');
        submitBtn.animate({
            height: '+=10',
            width: '+=50'
        });
        setTimeout(function(){
            submitBtn.animate({
                height: '-=10',
                width: '-=50'
            });
        }, 1000);
    }
    
    submitBtn.on('click', function(e){
        submitBtn.prop('disabled', true);
        var txt = 'Save Now';
        submitBtn.html(loader);
        setTimeout(function(){
            var next = submitBtn.attr('data-next-area');
            if(next === 'finish'){
                setTimeout(function(){
                    load_dialog('k');
                    submitBtn.hide();
                }, 1500);
            } else {
                scroll_to_next_area(next);
                setTimeout(function(){
                    load_dialog(dtnext);
                    submitBtn.html(txt);
                    submitBtn.hide();
                }, 1000);
            }
            submitBtn.prop('disabled', false);
        }, 1500);
    });
    //dashboard page
    if(page === 'dashboard'){
        $('#all_listings').html(loader);
        $.get('/realtors/tutorial/listing.html', function(html){    
            $('#all_listings').html(html);
            $('#all_listings').find('a').attr('href', '#');
            $('#all_listings').find('.members-listing .actions').first()
                .find('li:first-child > a')
                .attr('data-value', 'continue')
                .attr('title', 'Click here to continue')
                .attr('data-val', listing_id);
            load_dialog('a');
        });
        tutorial.on('click', '.next', function(){
            $(this).html(loader);
            load_dialog('b');
        });
        tutorial.on('click', '.dismiss', function(){
            tutorial.foundation('close');
            var elem = $('#all_listings').find('[data-value="continue"]');
            elem.tooltipster({
                position: 'top-left',
                theme: 'tooltip-white-theme',
                trigger: 'hover',
                multiple: true
            });
            elem.parent().addClass('dotted');
            elem.tooltipster('show');
            elem.on('click', function(e){
                e.preventDefault();
                window.location = '/realtors/tutorial/edit/' + $(this).attr('data-val');
            });
            $('body').hover(function(){
                elem.tooltipster('show');
            }, function(){
                elem.tooltipster('show');
            });
            elem.hover(function(){
                elem.tooltipster('show');
            }, function(){
                elem.tooltipster('show');
            });
        });
    }
        
    if(page === 'edit'){
        refresh_images();
        submitBtn.hide();
        $(document).find('.upload-area').hide();
        $('input[type="tel"]').mask('999-999-9999');
        $('#siding_type_h').SumoSelect();
        customSumoSelect($('.has-siding-type'), $('#siding_type_h'));
        $('#floor_type_h').SumoSelect();
        customSumoSelect($('.floor-type-h-wrapper'), $('#floor_type_h'));
        $('#roof_type_h').SumoSelect();
        customSumoSelect($('.roof-type-h-wrapper'), $('#roof_type_h'));
        $('#prop_type_h').SumoSelect();
        $('#p_title_h').SumoSelect();
        $('#prop_style_h').SumoSelect();
        $('#finished_basement_h').SumoSelect();
        $('#garage_type_h').SumoSelect();
        $('#view_h').SumoSelect();
        $('#heat_source_h').SumoSelect();
        $('#heating_h').SumoSelect();
        $('#cooling_h').SumoSelect();
        $('#water_h').SumoSelect();
        $('#sewer_h').SumoSelect();
        $('#year_built_h').SumoSelect();

        $('#prop_style_w').SumoSelect();
        customSumoSelect($('.prop-style-w-wrapper'), $('#prop_style_w'));
        $('#siding_type_w').SumoSelect();
        customSumoSelect($('.siding-type-w-wrapper'), $('#siding_type_w'));
        $('#garage_type_w').SumoSelect();
        customSumoSelect($('.garage-type-w-wrapper'), $('#garage_type_w'));
        $('#floor_type_w').SumoSelect();
        customSumoSelect($('.floor-type-w-wrapper'), $('#floor_type_w'));
        $('#roof_type_w').SumoSelect();
        customSumoSelect($('.roof-type-w-wrapper'), $('#roof_type_w'));

        $('#listing_price_low').SumoSelect();
        $('#listing_price_high').SumoSelect();
        $('#sq_footage').SumoSelect();
        $('#prop_type_w').SumoSelect();
        $('#bedrooms_w').SumoSelect();
        $('#bathrooms_w').SumoSelect();
        $('#p_title_w').SumoSelect();
        $('#total_lot_size_w').SumoSelect();
        $('#total_acreage_w').SumoSelect();
        $('#finished_basement_w').SumoSelect();
        $('#view_w').SumoSelect();
        $('#heat_source_w').SumoSelect();
        $('#heating_w').SumoSelect();
        $('#cooling_w').SumoSelect();
        $('#water_w').SumoSelect();
        $('#sewer_w').SumoSelect();
        
        var has_address = $('input[name="has[c_address]"]');
        if(has_address.val() !== ''){
            neighinline($('#neighbourhood_h'), has_address.val(), listing_id, 1);
        } else {
            has_address.val('');
        }

        var wants_address = $('input[name="wants[address]"]');
        if(wants_address.val() !== ''){
            neighinline($('#neighbourhood_w'), wants_address.val(), listing_id, 2);
        } else {
            wants_address.val('');
        }
        //edit listing page
        $('#client_info input[type="text"]').addClass('tutorial');
        $('#client_info input[type="text"]').val('');
        $('#client_info input[type="text"]').attr('readonly', 'readonly');
        load_dialog('a');
        tutorial.on('click', '.continue', function(){
            $(this).html(loader);
            load_dialog('b');
        });
        tutorial.on('click', '.continue-client-info-fill', function(){
            tutorial.foundation('close');
            //client info fill
            mouse_move('input[name="info[first_name]"]', 'John');
            setTimeout(function(){
				mouse_move('input[name="info[last_name]"]', 'Smith');
				setTimeout(function(){
					mouse_move('input[name="info[email]"]', 'email@example.com');
                    setTimeout(function(){
						$('input[name="info[email]"]').trigger('change');
					}, 6000);
				}, 1750);
			}, 2500);
        });
        
        $('input[name="info[email]"]').on('change', function(){
            load_dialog('c');
            tutorial.on('click', '.save-now', function(){
                tutorial.foundation('close');
                submitBtn.show();
                dtnext = $(this).attr('data-next');
                submitBtn.attr('data-next-area', 'client_has');
                animate_save_btn();
            });
        });
        
        tutorial.on('click', '.next-client-has', function(){
            $(this).html(loader);
            scroll_to_next_area('client_has');
            setTimeout(function(){
                load_dialog('e');
            }, 1500);
        });
        
        tutorial.on('click', '.client-has-fill-required', function(){
            tutorial.foundation('close');
            var wrapper = $(document).find('#neighbourhood_h_wrapper').find('.optWrapper');
            wrapper.addClass('open');
            setTimeout(function(){
                var elem = wrapper.find('li:nth-child(3)');
                elem.addClass('sel');
                setTimeout(function(){
                    var txt = elem.text();
                    wrapper.removeClass('open');
                    $(document).find('#neighbourhood_h_wrapper').find('.SlectBox').find('span').text(txt);
                    
                    //tutorial select title, basement, garage
                    setTimeout(function(){
                        
                        //title
                        var wrapper = $(document).find('.p_title_h_wrapper');
                        sumo_select_item(wrapper);
                        setTimeout(function(){
                            //basement
                            var wrapper = $(document).find('.finished_basement_h_wrapper');
                            sumo_select_item(wrapper);
                            setTimeout(function(){
                                //garage
                                var wrapper = $(document).find('.garage_type_h_wrapper');
                                sumo_select_item(wrapper);
                                setTimeout(function(){
                                    load_dialog('f');
                                    tutorial.on('click', '.client-has-save-now', function(){
                                        submitBtn.show();
                                        tutorial.foundation('close');
                                        dtnext = $(this).attr('data-next');
                                        submitBtn.attr('data-next-area', 'image_info');
                                        animate_save_btn();
                                    });
                                }, 500);
                            }, 1500 );
                        }, 1500);
                    }, 1500);
                }, 500);
            }, 500);
            
        });
        
        tutorial.on('click', '.image-info-next', function(){
            $(this).html(loader);
            //wants section loader
            scroll_to_next_area('client_wants');
            setTimeout(function(){
                load_dialog('h');
            }, 1500);
        });
        
        
        function sumo_select_item(wrapper){
            wrapper.find('.optWrapper').addClass('open');
            var elem = wrapper.find('li:nth-child(2)');
            elem.addClass('sel');
            var txt = elem.text();
            setTimeout(function(){
                wrapper.find('.optWrapper').removeClass('open');
                wrapper.find('.SlectBox').find('span').text(txt);
                if(wrapper.find('input,select').hasClass('req')){
                    wrapper.find('label').removeClass('req-text').addClass('req-text-done');
                }
            }, 500);
        }
        
        new autoComplete({
            selector: '#wants_address',
            source: function(term, response){
                $.get(baseUrl + 'autocomplete/cpc.json', { q: term }, function(data) { response(data); });
            }
        });
        
        tutorial.on('click', '.client-wants-fill-required', function(){
            tutorial.foundation('close');
            $('#client_wants input[type="text"]').addClass('tutorial');
            $('#client_wants input[type="text"]').val('');
            $('#client_wants input[type="text"]').attr('readonly', 'readonly');
            mouse_move('input[name="wants[address]"]', 'Abbotsfor');
            setTimeout(function(){
                $('input[name="wants[address]"]').trigger('keyup');
                $('.autocomplete-suggestions').append(
                        '<div data-val="Abbotsford, British Columbia, Canada" class="autocomplete-suggestion"><b>Abbotsford</b>, British Columbia, Canada</div>'
                        + '<div data-val="Abbotsford, Wisconsin, United States" class="autocomplete-suggestion"><b>Abbotsford</b>, Wisconsin, United States</div>'
                );
                var target = $('#wants_address');
                $('.autocomplete-suggestions').css({
                    left: target.offset().left, 
                    top: parseFloat(target.offset().top) + parseFloat(target.outerHeight()),
                    width: target.outerWidth()
                }).show();
                $('.autocomplete-suggestions:nth-child(1)').show();
                $('.autocomplete-suggestion:nth-child(1)').addClass('selected');
                setTimeout(function(){
                    $('.autocomplete-suggestion').remove();
                    $('input[name="wants[address]"]').val('');
                    $('input[name="wants[address]"]').val('Abbotsford, British Columbia, Canada');
                    $('.autocomplete-suggestions').hide();
                    var label = $('input[name="wants[address]"]');
                    $('label[for="'+label.attr('id')+'"]').removeClass('req-text').addClass('req-text-done');
                    $('input[name="wants[address]"]').blur();
                    $('#neighbourhood_w').html('<option value="" selected="selected">Loading...</option>');
                    $('#neighbourhood_w').SumoSelect();
                    $.post('/listing/neighinline.html', {type: 2, location: 'Abbotsford, British Columbia, Canada', lid: 'NA5377587207'}, function(data){
                        $('#neighbourhood_w_wrapper').html(data);
                        var val = $('#neighbourhood_w').val();
                        if(val !== '' || val !== '0'){
                            $(document).find('label[for="'+$('#neighbourhood_w').attr('name')+'"]').removeClass('req_text').addClass('req_text_done');
                        } else {
                            $(document).find('label[for="'+$('#neighbourhood_w').attr('name')+'"]').addClass('req_text');
                        }
                    }).done(function(){
                        var wrapper = $(document).find('#neighbourhood_w_wrapper').find('.optWrapper');
                        
                        setTimeout(function(){
                            var elem = wrapper.find('li:nth-child(3)');
                            wrapper.addClass('open');
                            setTimeout(function(){
                                elem.addClass('sel');
                                var txt = elem.text();
                                setTimeout(function(){
                                    wrapper.removeClass('open');
                                    $(document).find('#neighbourhood_w_wrapper').find('.SlectBox').find('span').text(txt);

                                    //tutorial select title, basement, garage
                                    setTimeout(function(){
                                        //title
                                        var wrapper = $(document).find('.listing-price-low-wrapper');
                                        sumo_select_item(wrapper);
                                        setTimeout(function(){
                                            //basement
                                            var wrapper = $(document).find('.listing-price-high-wrapper');
                                            sumo_select_item(wrapper);
                                            setTimeout(function(){
                                                //garage
                                                var wrapper = $(document).find('.sq-footage-w-wrapper');
                                                sumo_select_item(wrapper);

                                                setTimeout(function(){
                                                    sumo_select_item($(document).find('.prop-type-w-wrapper'));
                                                    setTimeout(function(){
                                                        sumo_select_item($(document).find('.bedrooms-w-wrapper'));
                                                        setTimeout(function(){
                                                            sumo_select_item($(document).find('.bathrooms-w-wrapper'));
                                                            setTimeout(function(){
                                                                sumo_select_item($(document).find('.p-title-w-wrapper'));
                                                                setTimeout(function(){
                                                                    sumo_select_item($(document).find('.prop-style-w-wrapper'));
                                                                    setTimeout(function(){
                                                                        load_dialog('i');
                                                                    }, 500);
                                                                }, 1500);
                                                            }, 1500);
                                                        }, 1500);
                                                    }, 1500);
                                                }, 1500);
                                            }, 1500 );
                                        }, 1500);
                                    }, 1500);
                                }, 1000);
                            }, 1000);
                                
                        }, 1000);
                    });
                }, 1500);
            }, 2000);
        });
        
        tutorial.on('click', '.client-wants-fill-additional', function(){
            tutorial.foundation('close');
            setTimeout(function(){
                sumo_select_item($(document).find('.finished-basement-w-wrapper'));
                setTimeout(function(){
                    sumo_select_item($(document).find('.garage-type-w-wrapper'));
                    setTimeout(function(){
                        load_dialog('j');
                    }, 500);
                }, 1500);
            }, 1500);
        });
        
        tutorial.on('click', '.client-wants-finish', function(){
            $('#submit_content').removeAttr('id').addClass('finish-tutorial');
            tutorial.foundation('close');
            submitBtn.attr('data-next-area', 'finish');
            submitBtn.show();
            animate_save_btn();
        });
        
    }
    
    function refresh_images(){
        var loader = '<div class="text-center"><i class="fa fa-spinner fa-spin"></i>Processing...</div>';
        $('#image_info_wrapper').html(loader);

        $.get(baseUrl + 'listing/images.html', {listing_id: $('input[name="listing_id"]').val()}, function(html){
            $('#image_info_wrapper').html(html);
            if ($('#dropzone').attr('class')) {
                $('.grid-sortable').dad({
                    callback: function(e) {
                        var new_order = [];
                        $('img.thumbnail').each(function(){
                            if($.inArray($(this).attr('data-id'), new_order) < 0){
                                new_order.push($(this).attr('data-id'));
                            }
                        });
                        update_order(new_order.toString());
                        $('.grid-sortable').closest('.accordion-item').find('.attention')
                        .html('')
                        .html('Changes made in this area, click Save Now <img width="35" height="35" class="attention_req" alt="Attention" style="vertical-align: middle;" src="' + baseUrl + 'assets/images/attention.png">');
                        //save button event [appearing when form has changed]
                        $('#submit_content').fadeIn();
                    }
                }).addDropzone('#dropzone',function(e){
                    var i = e.find('img.thumbnail').attr('data-id');
                    e.remove();
                    $.post('/realtors/listing/delete_images.json', {id: i}, function(json){
                        if(json.status === 'OK'){
                            refresh_images();
                        }
                    });
                });
            }
        });
    }
    tutorial.on('click', '.cancel, .modal-close, .done', function(){
        tutorial.foundation('close');
        window.location = '/realtors';
    });
    
});

function customSumoSelect(wrapper, target){
    wrapper.find('.options > li').on('click', function(){
        if($(this).hasClass('selected') && ($(this).attr('data-val') === '' || $(this).attr('data-val') === '9')){
            target[0].sumo.unSelectAll();
            target[0].sumo.selectItem(0);
        } else {
            target[0].sumo.unSelectItem(0);
        }
    });
}