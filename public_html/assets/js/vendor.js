$(document).ready(function(){
    var result = $('#full_results');
    var form = $('#directory_result');
    perform_submit(form, result);

    new autoComplete({
		selector: '#location',
		source: function(term, response){
			$.get(baseUrl + 'autocomplete/cpc.json', { q: term }, function(data) { response(data); });
		},
        onSelect: function(event,term,item) {
			perform_submit(form, result);
		}
	});

    $('#type').on('change', function(){
        perform_submit(form, result);
    });

    $(document).on('click', '.pagination a', function(e){
        e.preventDefault();
        perform_submit(form, result, '?page=' + $(this).text());
    });

    form.on('submit', function(e){
        e.preventDefault();
        perform_submit(form, result);
    });

    $('.btn-advertise').click(function(){
       window.location = '/signup/vendor/';
    });

    $('#type_service').SumoSelect();
    $('.SumoSelect .options > li').on('click', function(){
        if($(this).hasClass('selected') && $(this).attr('data-val') === 'all'){
            $('#type_service')[0].sumo.unSelectAll();
            $('#type_service')[0].sumo.selectItem(0);
        } else {
            $('#type_service')[0].sumo.unSelectItem(0);
        }
        perform_submit(form, result, '?page=' + $(this).text());
    });
});

function perform_submit(form, result, options){
    if(!options){
        options = '';
    }
    result.html('<div class="text-center"><i class="fa fa-spinner fa-spin"></i>Processing...</div>');
    $.post(form.attr('action') + options, form.serialize(), function(data){
        result.html(data);
    });
}